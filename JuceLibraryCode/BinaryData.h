/* =========================================================================================

   This is an auto-generated file: Any edits you make may be overwritten!

*/

#ifndef BINARYDATA_H_14674486_INCLUDED
#define BINARYDATA_H_14674486_INCLUDED

namespace BinaryData
{
    extern const char*   ZMP_CTR_Test;
    const int            ZMP_CTR_TestSize = 20664;

    extern const char*   BDL_sct2;
    const int            BDL_sct2Size = 2995708;

    extern const char*   ZMP_CTR;
    const int            ZMP_CTRSize = 20664;

    // Points to the start of a list of resource names.
    extern const char* namedResourceList[];

    // Number of elements in the namedResourceList array.
    const int namedResourceListSize = 3;

    // If you provide the name of one of the binary resource variables above, this function will
    // return the corresponding data and its size (or a null pointer if the name isn't found).
    const char* getNamedResource (const char* resourceNameUTF8, int& dataSizeInBytes) throw();
}

#endif
