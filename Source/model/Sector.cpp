/*
  ==============================================================================

    Sector.cpp
    Created: 1 Jan 2015 11:38:46am
    Author:  Jim Mazur

  ==============================================================================
*/

#include "Sector.h"

namespace sector
{

void SectorContainer::addLowAirayLine(const NamedLine &airwayLine)
{
    addToNamedDiagramsContainer(&lowAirways, airwayLine);
}
    
void SectorContainer::addHighAirwayLine(const NamedLine &airwayLine)
{
    addToNamedDiagramsContainer(&highAirways, airwayLine);
}
    
void SectorContainer::addArtccBoundaryLine(const sector::NamedLine &line)
{
    addToNamedDiagramsContainer(&artccBoundaries, line);
}

void SectorContainer::addArtccHighBoundaryLine(const sector::NamedLine &line)
{
    addToNamedDiagramsContainer(&artccHighBoundaries, line);
}

void SectorContainer::addArtccLowBoundaryLine(const sector::NamedLine &line)
{
    addToNamedDiagramsContainer(&artccLowBoundaries, line);
}
    
void SectorContainer::addGeoLine(const sector::ColouredLine &line)
{
    geos.add(line);
}

void SectorContainer::addLabel(const sector::Label &label)
{
    labels.add(label);
}
    
bool SectorContainer::containsNamedColour(const juce::String &colourName) const
{
    if (colours.contains(colourName))
        return true;
    
    return false;
}

NamedColour SectorContainer::getNamedColourFor(const juce::String &colourName) const
{
    return colours.getNamedColourFor(colourName);
}
    
bool SectorContainer::containsCoordinatesFor(const juce::String &navaid) const
{
    if (fixes.contains(navaid))
        return true;
    
    if (vors.contains(navaid))
        return true;
    
    if (ndbs.contains(navaid))
        return true;
    
    if (airports.contains(navaid))
        return true;
    
    return false;
}


Coordinates SectorContainer::getCoordinatesFor(const String &navaid) const
{
    if (fixes.contains(navaid))
        return fixes.getCoordinateFor(navaid);
    
    if (vors.contains(navaid))
        return vors.getCoordinateFor(navaid);
    
    if (ndbs.contains(navaid))
        return ndbs.getCoordinateFor(navaid);
    
    if (airports.contains(navaid))
        return airports.getCoordinateFor(navaid);
    
    return Coordinates();
}

NamedCoordinate SectorContainer::getNamedCoordinatesFor(const String &navaid) const
{
    if (fixes.contains(navaid))
        return fixes.getNamedCoordinateFor(navaid);
    
    if (vors.contains(navaid))
        return vors.getNamedCoordinateFor(navaid);
    
    if (ndbs.contains(navaid))
        return ndbs.getNamedCoordinateFor(navaid);
    
    if (airports.contains(navaid))
        return airports.getNamedCoordinateFor(navaid);
    
    return NamedCoordinate();
}
    
void SectorContainer::addToAirwayContainer(AirwaysContainer *container, const sector::NamedLine &line)
{
    container->addToExistingDiagram(line.getName(), line);
}
    
template<class Tcontainer, class Tline>
void SectorContainer::addToNamedDiagramsContainer(Tcontainer *container, const Tline& line)
{
    container->addToExistingDiagram(line.getName(), line);
}
    
class SectorContainerUnitTests  : public UnitTest
{
private:
    NamedCoordinate coordinate1a;
    NamedCoordinate coordinate1b;
    NamedCoordinate coordinate2a;
    NamedCoordinate coordinate2b;
    
    AlignedNote note1;
    AlignedNote note2;
    
    NamedLine line1;
    NamedLine line2;

public:
    SectorContainerUnitTests()
    :
    UnitTest("SectorContainer tests"),
    coordinate1a("KCO1", Coordinates(11.11, 22.22)),
    coordinate1b("KCO2", Coordinates(33.33, 44.44)),
    coordinate2a("KC2a", Coordinates(00.11, 00.22)),
    coordinate2b("KC2b", Coordinates(00.33, 00.44)),
    note1("note1"),
    note2("note2"),
    line1("line1", coordinate1a, coordinate1b, note1),
    line2("line2", coordinate2a, coordinate2b, note2)
    {
    }
    
    void runTest()
    {
        beginTest("Constructor Tests");
        {
            SectorContainer sector;
            expect(sector.getLowAirways()->size() == 0);
        }
        
        beginTest("add Tests");
        {
            SectorContainer sector;
            sector.addLowAirayLine(line1);
            expect(sector.getLowAirways()->size() == 1);
            expect(sector.getLowAirways()->atDiagram(0).atLine(0) == line1);
        }
    }
};

static SectorContainerUnitTests sectorContainerUnittests;

} // End of sector namespace