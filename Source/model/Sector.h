/*
  ==============================================================================

    Sector.h
    Created: 1 Jan 2015 11:38:46am
    Author:  Jim Mazur

  ==============================================================================
*/

#ifndef SECTOR_H_INCLUDED
#define SECTOR_H_INCLUDED

#include "SectorContainers.h"

namespace sector
{

class SectorContainer
{
public:
    SectorContainer() {};
    
    // Adds sector notes, these are the notes that exist before the first #define or
    // section header block
    void setNotes (const String &notes)                         { sectorNotes = notes; }
    
    // Get the notes for the overall sector, these will be written first to the
    // sector file
    String getNotes () const                                    { return sectorNotes; }
    
    void addInfo (const InfoBlock& info)                        { this->info = info; }
    
    InfoBlock* getInfo ()                                       { return &info; }
    
    void add (const NamedColour& colour)                        { colours.add(colour); }
    
    NamedColours* getNamedColours ()                            { return &colours; }
    
    void addFix (const NavAid& fix)                             { fixes.add(fix); }
    
    Fixes* getFixes()                                           { return &fixes; }
    
    void addVOR (const RadioNavAid& vor)                        { vors.add(vor); }
    
    VORs* getVORs()                                             { return &vors; }
    
    void addNDB (const RadioNavAid& ndb)                        { ndbs.add(ndb); }
    
    NDBs* getNDBs()                                             { return &ndbs; }
    
    void addAirport (const Airport& airport)                    { airports.add(airport); }
    
    Airports* getAirportss()                                    { return &airports; }
    
    void addRunway (const Runway& runway)                       { runways.add(runway); }
    
    Runways* getRunways()                                       { return &runways; }
    
    void addLowAirayLine (const NamedLine& airwayLine);
    
    AirwaysContainer* getLowAirways ()                          { return &lowAirways; }
    
    void addHighAirwayLine (const NamedLine& airwayLine);
    
    AirwaysContainer* getHighAirways ()                         { return &highAirways; }
    
    void addArtccBoundaryLine (const NamedLine& line);
    
    ArtccContainer* getArtccBoundaries ()                       { return &artccBoundaries; }
    
    void addArtccHighBoundaryLine (const NamedLine& line);
    
    ArtccContainer* getArtccHighBoundaries ()                   { return &artccHighBoundaries; }
    
    void addArtccLowBoundaryLine (const NamedLine& line);
    
    ArtccContainer* getArtccLowBoundaries ()                    { return &artccLowBoundaries; }
    
    SID_STAR_Container* getSIDs()                                { return &sids; }
    
    SID_STAR_Container* getSTARs ()                              { return &stars; }
    
    void addGeoLine (const ColouredLine& line);
    
    GeoLinesContainer* getGeoLines ()                               { return &geos; }
    
    Regions* getRegions ()                                          { return &regions; }
    
    void addLabel (const Label& label);
    
    Labels* getLabels()                                             { return &labels; }
    
    bool containsNamedColour (const String& colourName) const;
    
    NamedColour getNamedColourFor (const String& colourName) const;
    
    bool containsCoordinatesFor (const String& navaid) const;
    
    Coordinates getCoordinatesFor (const String& navaid) const;
    
    NamedCoordinate getNamedCoordinatesFor (const String& navaid) const;

private:
    void addToAirwayContainer(AirwaysContainer* container, const NamedLine& line);
    
    template<class Tcontainer, class Tline>
    void addToNamedDiagramsContainer(Tcontainer *container, const Tline& line);
    
private:
    String sectorNotes;
    InfoBlock info;
    NamedColours colours;
    Fixes fixes;
    VORs vors;
    NDBs ndbs;
    Airports airports;
    Runways runways;
    AirwaysContainer lowAirways;
    AirwaysContainer highAirways;
    ArtccContainer artccBoundaries;
    ArtccContainer artccHighBoundaries;
    ArtccContainer artccLowBoundaries;
    SID_STAR_Container sids;
    SID_STAR_Container stars;
    GeoLinesContainer geos;
    Regions regions;
    Labels labels;
    
};
    
}

#endif  // SECTOR_H_INCLUDED
