/*
  ==============================================================================

    Sector.h
    Created: 1 Jan 2015 11:38:46am
    Author:  Jim Mazur

  ==============================================================================
*/

#ifndef SECTORCONTAINER_H_INCLUDED
#define SECTORCONTAINER_H_INCLUDED

class SectorContainer
{
public:
    SectorContainer() {};
    
    // Adds sector notes, these are the notes that exist before the first #define or
    // section header block
    void setNotes (const String &notes)                     { sectorNotes = notes; }
    
    // Get the notes for the overall sector, these will be written first to the
    // sector file
    String getNotes () const                                { return sectorNotes; }
    
    void add (const InfoBlock& info)                        { this->info = info; }
    
    void add (const NamedColour& colour)                    { colours.add(colour); }
    
    void add (const Fix& fix)                               { fixes.add(fix); }
    
    void add (const VOR& vor)                               { vors.add(vor); }
    
    void add (const NDB& ndb)                               { ndbs.add(ndb); }
    
    void addAirport (const Airport& airport)                       { airports.add(airport); }
    
    void add (const SectorTypes::Runway& runway)            { runways.add(runway); }
    
    void add (const LowAirwayLine& airwayLine)              { lowAirways.add(airwayLine); }
    
    void add (const HighAirwayLine& airwayLine)             { highAirways.add(airwayLine); }
    
    Airports* getAirportss()                                 { return &airports; }
    
    Runways* getRunways()                                   { return &runways; }
    
    VORs* getVORs()                                         { return &vors; }
    
    NDBs* getNDBs()                                         { return &ndbs; }
    
    Fixes* getFixes()                                       { return &fixes; }
    
    bool contains (const NamedColour& colour) const;
    
    bool containsCoordinatesFor (const String& navaid) const;
    
    Coordinates getCoordinatesFor (const String& navaid) const;
    
    NamedCoordinate getNamedCoordinatesFor (const String& navaid) const;
    
    //juce_DeclareSingleton(Sector, false);
    
private:
    String sectorNotes;
    InfoBlock info;
    NamedColours colours;
    Fixes fixes;
    VORs vors;
    NDBs ndbs;
    Airports airports;
    Runways runways;
    LowAirways lowAirways;
    HighAirways highAirways;
};



#endif  // SECTORCONTAINER_H_INCLUDED
