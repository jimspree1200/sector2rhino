/*
  ==============================================================================

    SectorTypes.cpp
    Created: 18 Jan 2015 5:08:51pm
    Author:  Jim Mazur

  ==============================================================================
*/

#include "SectorTypes.h"

namespace sector
{

SectorFileLine::SectorFileLine()
:
lineNumber(-1)
{}
    
SectorFileLine::SectorFileLine(const String& lineFromSectorFile, const int& lineInSectorFile)
:
text(lineFromSectorFile),
lineNumber(lineInSectorFile)
{}
    
bool SectorFileLine::hasValidSectorFileLine() const
{
    if (lineNumber < 0 || text.isEmpty())
        return false;
    
    return true;
}
    

void SectorFileLine::setLineFromSectorFile(const juce::String &text)
{
    this->text = text;
}
    
String SectorFileLine::getLineFromSectorFile() const
{
    return text;
}
    
void SectorFileLine::setLineNumberInSectorFile(const int &lineNumber)
{
    this->lineNumber = lineNumber;
}

int SectorFileLine::getLineNumberInSectorFile() const
{
    return lineNumber;
}
    
Coordinates::Coordinates ()
{}

Coordinates::Coordinates (const GeoCoords& coordinates)
:
GeoCoords(coordinates)
{}

Coordinates::Coordinates (const String& coordinates)
:
GeoCoords(coordinates.toStdString())
{}

Coordinates::Coordinates (GeographicLib::Math::real lat, GeographicLib::Math::real lon)
:
GeoCoords(lat, lon)
{}

bool Coordinates::operator==(const Coordinates &other) const
{
    /** As GeographicLib::GeoCoords will by default construct it's lat / lon 
        members to NaN. We will need to first check if either or both of the 
        Coordinates contain a NaN coordinate.
     */
    bool thisLatIsNan = GeographicLib::Math::isnan(this->Latitude());
    bool thisLonIsNan = GeographicLib::Math::isnan(this->Longitude());
    bool otherLatIsNan = GeographicLib::Math::isnan(other.Latitude());
    bool otherLonIsNan = GeographicLib::Math::isnan(other.Longitude());
    
    // Both Coordinates are empty and contain NaN coordinates
    if ((thisLatIsNan == otherLatIsNan) && (thisLonIsNan == otherLonIsNan))
        return true;
    
    // One of the coordinates contains a NaN coordinate while the other doesnt
    if ((thisLonIsNan != otherLatIsNan) || (thisLonIsNan != otherLonIsNan))
        return false;
    
    if (this->Latitude() != other.Latitude())
        return false;
    
    if (this->Longitude() != other.Longitude())
        return false;
    
    return true;
}

bool Coordinates::operator!=(const Coordinates &other) const
{
    return !operator==(other);
}

bool Coordinates::isValid() const
{
    /** Hacky evaluation, if there is no valid data in the GeoCoords object, the
     DMSRepresentation() function will return "nan nan" (not a number)
     */
    if (*this == Coordinates())
        return false;
    
    return true;
}

//==============================================================================

AlignedNote::AlignedNote()
:
alignment(Alignment::right)
{}

AlignedNote::AlignedNote(const String& text)
:
alignment(Alignment::right),
text(text)
{}

AlignedNote::AlignedNote (const String& text, Alignment alignment)
:
alignment(alignment),
text(text)
{}

AlignedNote::AlignedNote (const AlignedNote& other)
:
alignment(other.alignment),
text(other.text)
{}

AlignedNote::operator String() const
{
    return text;
}

bool AlignedNote::hasNote() const
{
    return !text.isEmpty() ? true : false;
}

String AlignedNote::getText() const
{
    return text;
}

void AlignedNote::setNote(const String &text)
{
    this->text = text;
}

AlignedNote::Alignment AlignedNote::getAlignment() const
{
    return alignment;
}

void AlignedNote::setAlignment(const AlignedNote::Alignment &alignment)
{
    this->alignment = alignment;
}

//==============================================================================

Notes::Notes (const String& text)
{
    notes.add(text);
}

Notes::Notes (const char* text)
{
    String s(text);
    notes.add(s);
}

Notes::Notes (const AlignedNote& note)
{
    notes.add(note);
}
    
void Notes::clear ()
{
    notes.clear();
}

void Notes::addNotes(const juce::String &text)
{
    notes.add(text);
}
    
bool Notes::hasNotes() const
{
    return notes.size();
}

String Notes::getNotes () const
{
    //jassert(notes.size());
    return notes.joinIntoString(" ");
}

int Notes::numOfNotes() const
{
    return notes.size();
}
    
void Notes::addPrecedingNotes(const juce::String &text)
{
    precedingNotes.add(text);
}

bool Notes::hasPrecedingNotes() const
{
    return numOfPrecedingNotes();
}

int Notes::numOfPrecedingNotes() const
{
    return precedingNotes.size();
}

String Notes::getPrecedingNotes() const
{
    return precedingNotes.joinIntoString(newLine);
}

AlignedNote Notes::getNoteAt(const int index)
{
    if (isPositiveAndBelow(index, notes.size()))
        return notes.getReference(index);
    
    return AlignedNote();
}

//==============================================================================

NamedObject::NamedObject ()
{}

NamedObject::NamedObject (const String& name)
:
name(name)
{}

bool NamedObject::operator== (const NamedObject& other) const
{
    return this->name == other.name;
}

bool NamedObject::operator!= (const NamedObject& other) const
{
    return !operator==(other);
}

void NamedObject::setName(const String &name)
{
    this->name = name;
}

String NamedObject::getName() const
{
    return this->name;
}

bool NamedObject::hasName() const
{
    if (name == String::empty)
        return false;
    
    return true;
}

bool NamedObject::isName(const juce::String &name) const
{
    return this->name == name ? true : false;
}

//==============================================================================

NamedColour::NamedColour (const String& name, const Colour& colour, const Notes& notes)
:
Notes(notes),
NamedObject(name),
colour(colour)
{}

NamedColour::NamedColour (const Colour& colour)
:
colour(colour)
{}
    
bool NamedColour::operator== (NamedColour &other) const
{
    if (other.colour != this->colour ||
        other.getName() != this->getName())
        return false;
    
    return true;
}
    
void NamedColour::setNamedColour(const sector::NamedColour &colour)
{
    *this = colour;
}

//==============================================================================

NamedCoordinate::NamedCoordinate (const String& name, const Coordinates& location)
:
NamedObject(name),
coordinate(location)
{}

NamedCoordinate::NamedCoordinate (const Coordinates& location)
:
coordinate(location)
{}

void NamedCoordinate::setCoordinate(const Coordinates &coordinate)
{
    this->coordinate = coordinate;
}

Coordinates NamedCoordinate::getCoordinate () const
{
    return coordinate;
}

//==============================================================================

NamedCoordinateLine::NamedCoordinateLine ()
{}

NamedCoordinateLine::NamedCoordinateLine (const NamedCoordinate& start, const NamedCoordinate& end)
:
start(start),
end(end)
{}

NamedCoordinateLine::NamedCoordinateLine (const Coordinates& start, const Coordinates& end)
:
start(start),
end(end)
{}

bool NamedCoordinateLine::operator==(const NamedCoordinateLine &other) const
{
    if (this->start != other.start)
        return false;
    
    if (this->end != other.end)
        return false;
    
    return true;
}

bool NamedCoordinateLine::operator!=(const NamedCoordinateLine &other) const
{
    return !operator==(other);
}

void NamedCoordinateLine::setLine(const NamedCoordinateLine &line)
{
    start = line.getStart();
    end = line.getEnd();
}

void NamedCoordinateLine::setStart(const NamedCoordinate &start)
{
    this->start = start;
}

NamedCoordinate NamedCoordinateLine::getStart() const
{
    return start;
}

void NamedCoordinateLine::setEnd(const NamedCoordinate &end)
{
    this->end = end;
}

NamedCoordinate NamedCoordinateLine::getEnd() const
{
    return end;
}

bool NamedCoordinateLine::containsNamedCoordinates() const
{
    if (start.hasName())
        return true;
    
    if (end.hasName())
        return true;
    
    return false;
}

//==============================================================================

class CoordinatesTests      : public UnitTest
{
private:
    GeographicLib::Math::real lat;
    GeographicLib::Math::real lon;
    String geolibFormatted;
public:
    
    CoordinatesTests()
    :
    UnitTest ("Coordinates Class tests"),
    lat(12.12345),
    lon(-21.65432),
    geolibFormatted("12d07'24.420\"N 021d39'15.552\"W")
    {}
    
    void runTest()
    {
        beginTest("Constructor test");
        
        {
            // Empty constructor
            Coordinates coords;
            expect(coords.isValid() == false);
            
            // String constructor
            Coordinates coords2(geolibFormatted);
            expect(coords2.isValid());
            expect(coords2.Latitude() == lat);
            expect(coords2.Longitude() == lon);
            
            // GeoCoords constructor
            GeographicLib::GeoCoords geolibCoord(geolibFormatted.toStdString());
            Coordinates coords3(geolibCoord);
            expect(coords3.isValid());
            expect(coords3.Latitude() == lat);
            expect(coords3.Longitude() == lon);
            
            // Lat / Lon constructor
            Coordinates coords4(lat, lon);
            expect(coords4.isValid());
            expect(coords4.Latitude() == lat);
            expect(coords4.Longitude() == lon);
        }
        
        beginTest("Equality operator test");
        {
            Coordinates coords(geolibFormatted);
            
            // Empty coordinates
            expect(Coordinates() == Coordinates());
            
            // same coord
            expect(coords == coords);
            
            // Empty to known
            expect(!(coords == Coordinates()));
            expect(!(Coordinates() == coords));
            
            // Empty to known
            expect(coords != Coordinates());
            expect(Coordinates() != coords);
            
            Coordinates coords1(lat, lon);
            expect(coords == coords1);
        }
    }
};

class SectorTypesTests   : public UnitTest
{
public:
    SectorTypesTests()   : UnitTest ("Sector Types testing") {}
    
    void runTest()
    {
        beginTest("AlignedNote tests");
        {
            // Empty constructor
            AlignedNote note;
            expect(note.hasNote() == false);
            expect(note.getAlignment() == AlignedNote::Alignment::right);
            
            // Set note
            note.setNote("a note");
            expect(note.hasNote() == true);
            expect(note.getText() == "a note");
            
            // Set alignmet
            note.setAlignment(AlignedNote::Alignment::below);
            expect(note.getAlignment() == AlignedNote::Alignment::below);
            
            // Copy constructor test
            AlignedNote note2(note);
            expect(note2.getText() == note.getText());
            
            // String constructor test
            AlignedNote note3("Another note");
            expect(note3.getText() == "Another note");
            expect(note3.getAlignment() == AlignedNote::Alignment::right);
            
            // Full constructor test
            AlignedNote note4("One last note!", AlignedNote::Alignment::above);
            expect(note4.getText() == "One last note!");
            expect(note4.getAlignment() == AlignedNote::Alignment::above);
        }
        
        beginTest("Notes tests");
        {
            // Empty constructor test
            Notes notes;
            expect(notes.hasNotes() == false);
            expect(notes.numOfNotes() == 0);
            
            notes.addNotes((String) "Empty note");
            expect(notes.hasNotes() == true);
            expect(notes.numOfNotes() == 1);
            expect(notes.getNotes() == "Empty note");
            expect(notes.getNoteAt(0) == "Empty note");
            
            // Out of bounds get, should return empty String
            expect(notes.getNoteAt(1) == String::empty);
            
            // String constructor
            Notes notes2("note2");
            expect(notes2.numOfNotes() == 1);
            expect(notes2.hasNotes() == true);
            expect(notes2.getNotes() == "note2");
            
            // Aligned note constructor
            Notes notes3(AlignedNote("note"));
            expect(notes3.numOfNotes() == 1);
            expect(notes3.hasNotes() == true);
            expect(notes3.getNotes() == "note");
        }
        
        beginTest("Named Colour tests");
        {
            NamedColour colour;
            expect(colour.getName().isEmpty());
            expect(colour.getColour() == Colour());
            
            Colour c(Colours::blue);
            expect(NamedColour("name", Colours::blue).getColour() == Colours::blue);
            
            colour.setColour(Colours::blue);
            expect(colour.getColour() == Colours::blue);
            
            colour.setName("a name");
            expect(colour.getName() == "a name");
            
            NamedColour colour2("another name", Colours::red, "some text");
            expect(colour2.getName() == "another name");
            expect(colour2.getColour() == Colours::red);
            expect(colour2.getNotes() == "some text");
        }
        
        beginTest("NamedCoordinate tests");
        {
            // Empty constructor test
            NamedCoordinate navAid;
            expect(!navAid.hasName());
            expect(!navAid.getCoordinate().isValid());
            
            // Full constructor test
            NamedCoordinate navAid2("name2", Coordinates(12.32, 12.34));
            expect(navAid2.getName() == "name2");
            expect(navAid2.getCoordinate().Latitude() == 12.32 && navAid2.getCoordinate().Longitude() == 12.34);
            
            // SetCoordinate() test
            navAid2.setCoordinate(Coordinates(21.23, 21.32));
            expect(navAid2.getCoordinate().Latitude() == 21.23 && navAid2.getCoordinate().Longitude() == 21.32);
            
            // Set name test TODO: move to NamedObject test, once created
            navAid2.setName("name3");
            expect(navAid2.getName() == "name3");
        }
        
        beginTest("RadioNamedCoordinate Class test");
        {
            RadioNamedCoordinate radioNavAid;
            expect(radioNavAid.getFrequency().isEmpty());
            
            RadioNamedCoordinate radioNavAid2("name", "123.321", Coordinates());
            expect(radioNavAid2.getFrequency() == "123.321");
            
            radioNavAid2.setFrequency("321.123");
            expect(radioNavAid2.getFrequency() == "321.123");
        }
        
        beginTest("Airport tests");
        {
            Airport airport;
        }
        
        beginTest("RadioNavAid tests");
        {
            RadioNavAid vor;
            expect(vor.getName() == String::empty);
            expect(vor.getFrequency() == String::empty);
            expect(vor.hasNotes() == false);
        }
        
    }
    
};

static CoordinatesTests coordinatesTests;
static SectorTypesTests sectorTypesTests;

}