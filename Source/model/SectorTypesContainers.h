/*
  ==============================================================================

    SectorContainers.h
    Created: 2 Jan 2015 10:34:17pm
    Author:  Jim Mazur

  ==============================================================================
*/

#ifndef SECTORTYPESCONTAINERS_H_INCLUDED
#define SECTORTYPESCONTAINERS_H_INCLUDED



using std::map;
using std::vector;
using GeographicLib::GeoCoords;


//==============================================================================
/**
 Template definition for a container based upon a std::map that stores named
 Elements.
 
 This will wrap around Fixes, VORs, NDBs and Airports. The use of a map ensures
 that there will be fast access times as the contains() function will be called
 offten when parsing a SCT file (for named coordinates).
 
 Any class that is used with this template must have a public member function:
 String getName() const;
 Which returns the name of the object.
 */

class NamedColours
{
public:
    bool contains (const NamedColour& colour) const;
    
    void add (const NamedColour& colour);
    
    int size () const;
    
    NamedColour at (const int& index);
    
private:
    map<String, NamedColour> colours;
};

template <class T>
class NamedCoordinateContainer
{
public:
    bool contains (const T& element) const
    {
        if (elements.find(element.getName()) == elements.end())
            return false;
        
        return true;
    }
    
    bool contains (const String& name) const
    {
        if (elements.find(name) != elements.end())
            return true;
        
        return false;
    }
    
    const Coordinates getCoordinateFor (const String& name) const
    {
        const_elementsIterator it = elements.find(name);
        
        if (it == elements.end())
            return Coordinates();
        
        return it->second.getCoordinate();
    }
    
    NamedCoordinate getNamedCoordinateFor (const String& name) const
    {
        const_elementsIterator it = elements.find(name);
        
        if (it == elements.end())
            return NamedCoordinate();
        
        return it->second;
    }
    
    void add (const T& element)
    {
        elements[element.getName()] = element;
    }
    
    int size() const
    {
        return (int) elements.size();
    }
    
    T& at (const int& index)
    {
        jassert(index < elements.size());
        
        elementsIterator it = elements.begin();
        std::advance(it, index);
        
        return it->second;
    }
    
private:
    typedef typename map<String, T>::iterator elementsIterator;
    typedef typename map<String, T>::const_iterator const_elementsIterator;
    map<String, T> elements;
};

typedef NamedCoordinateContainer<Fix> Fixes;
typedef NamedCoordinateContainer<VOR> VORs;
typedef NamedCoordinateContainer<NDB> NDBs;
typedef NamedCoordinateContainer<Airport> Airports;

//==============================================================================
/**
 NamedDiagram
 
 Template definition for a diagram that has a name and stores lines of linetype
 T. Use this for storing basic diagrams like SIDs and STARs
 
 */

template <class T>
class NamedDiagram
{
public:
    NamedDiagram ()
    {}
    
    /** Create an empty digram with the name specified */
    NamedDiagram (const String& name)
    :
    name(name)
    {}
    
    /** Create a diagram with the specified name and add a line to it */
    NamedDiagram (const String& name, const T& line)
    :
    name(name)
    {
        addLine(line);
    }

    
    void addLine (const T& line)
    {
        lines.push_back(line);
    }
    
    int size () const
    {
        return lines.size();
    }
    
    T& atLine (const int& index)
    {
        jassert ((index >= 0 && index < lines.size()));
        
        return lines.at(index);
    }
    
    void setName (const String& name)
    {
        this->name = name;
    }

    
    String getName() const
    {
        return name;
    }
    
    bool isName(const String& name)
    {
        return this->name == name ? true : false;
    }
    
private:
    String name;
    vector<T> lines;
};

/** Typedefs for Diagrams */
typedef NamedDiagram<ArtccLine> ArtccDiagram;
typedef NamedDiagram<ArtccHighLine> ArtccHighDiagram;
typedef NamedDiagram<ArtccLowLine> ArtccLowDiagram;

//==============================================================================
/**
 NamedDiagramsContainer
 
 Template definition for a container that stores multiple NamedDiagram containers.
 
 */

template<class TDiagramType, class TLineType>
class NamedDiagramsContainer
{
public:
    NamedDiagramsContainer ()
    {}
    
    NamedDiagramsContainer(const String& diagramName)
    {
        diagrams.push_back(TDiagramType(diagramName));
    }
    
    NamedDiagramsContainer(const String& diagramName, const TLineType& firstLine)
    {
        diagrams.push_back(TDiagramType(diagramName, firstLine));
    }
    
    void addline (const String& diagramName, const TLineType& line)
    {
        // Is there already a named diagram in the vector?
        auto diagram = findName(diagramName);
        
        if (diagram != nullptr)
            diagram->addLine(line);
        
        // if not, add it
        diagrams.push_back(TDiagramType(diagramName, line));
    }
    
    int size() const
    {
        return diagrams.size();
    }
    
    TDiagramType& atDiagram(const int index)
    {
        jassert(diagrams.size() > index);
        
        return diagrams.at(index);
    }
    
    
private:
    TDiagramType* findName (const String& diagramName)
    {
        for (diagramsIterator it = diagrams.begin(); it != diagrams.end(); it++)
        {
            if (it->isName(diagramName))
                return &(*it);
        }
        return nullptr;
    }

private:
    typedef typename vector<TDiagramType>::iterator diagramsIterator;
    vector<TDiagramType> diagrams;
};

typedef NamedDiagramsContainer<ArtccDiagram, ArtccLine> ArtccDiagramContainer;

template <class T>
class Diagram
{
public:
    Diagram () {}
    
    Diagram (const T& line)
    {
        addLine(line);
    }
    
    
    void addLine (const T& line)
    {
        lines.pushback(line);
    }
    
    int size () const
    {
        lines.size();
    }
    
    T& at (const int& index) const
    {
        jassert (index < lines.size());
        
        return lines.at(index);
    }
    
private:
    vector<T> lines;
};


typedef Array<SectorTypes::Runway> Runways;
typedef Array<LowAirwayLine> LowAirways;
typedef Array<HighAirwayLine> HighAirways;


#endif  // SECTORTYPESCONTAINERS_H_INCLUDED
