/*
  ==============================================================================

    SectorContainers.cpp
    Created: 2 Jan 2015 10:34:17pm
    Author:  Jim Mazur

  ==============================================================================
*/

//==============================================================================

//template <class T>
//bool NamedContainer<T>::contains(const T &element) const
//{
//    if (elements.find(element.getName()) == elements.end())
//        return false;
//    
//    return true;
//}
//
//template <class T>
//bool NamedContainer<T>::contains(const juce::String &name) const
//{
//    if (elements.find(name) != elements.end())
//        return true;
//    
//    return false;
//}
//
//template <class T>
//const GeoCoords* NamedContainer<T>::getCoordinateFor(const juce::String &name) const
//{
//    const_elementsIterator it = elements.find(name);
//    
//    if (it == elements.end())
//        return nullptr;
//    
//    std::map<int, int> test;
//    test.begin();
//    
//    return it->second.getCoordinate();
//}
//
//template <class T>
//void NamedContainer<T>::add (const T& element)
//{
//    elements[element.getName()] = element;
//}
//
//template <class T>
//uint NamedContainer<T>::size() const
//{
//    return (uint) elements.size();
//}
//
//template <class T>
//T NamedContainer<T>::at (const uint& index)
//{
//    jassert(index < elements.size());
//    
//    elementsIterator it = elements.begin();
//    std::advance(it, index);
//    return it->second;
//}

//==============================================================================

bool NamedColours::contains(const NamedColour &colour) const
{
    if (colours.find(colour.getName()) != colours.end())
        return true;
    
    return false;
}

void NamedColours::add(const NamedColour &colour)
{
    colours[colour.getName()] = colour;
}

int NamedColours::size() const
{
    return colours.size();
}

NamedColour NamedColours::at(const int &index)
{
    jassert(index < colours.size());
    
    map<String, NamedColour>::iterator it;
    it = colours.begin();
    std::advance(it, index);
    
    return it->second;
}

//template <class T>
//SectorDiagram<T>::SectorDiagram()
//:
//name("Unnamed")
//{}
//
//template <class T>
//SectorDiagram<T>::SectorDiagram (const String& name, const T& line)
//:
//name(name)
//{
//    lines.pushback(line);
//}
//
//template <class T>
//void SectorDiagram<T>::addLine (const T& line)
//{
//    lines.pushback(line);
//}
//
//template <class T>
//uint SectorDiagram<T>::size () const
//{
//    lines.size();
//}
//
//template <class T>
//T SectorDiagram<T>::at (const uint& index) const
//{
//    jassert (index < lines.size());
//    
//    return lines.at(index);
//}
//
//template <class T>
//void SectorDiagram<T>::setName(const juce::String &name)
//{
//    this->name = name;
//}
//
//template <class T>
//String SectorDiagram<T>::getName () const
//{
//    return name;
//}

//==============================================================================

class SectorContainerTest   : public UnitTest
{
public:
    SectorContainerTest()   : UnitTest ("Sector functions testing") {}
    
    void runTest()
    {
        {
            beginTest("DefinedColours test");
            // Construction
            NamedColours colours;
            expect(colours.size() == 0);
            expect(colours.contains(NamedColour()) == false);
            
            // Adding
            NamedColour colour1("test name", Colours::blue);
            colours.add(colour1);
            expect(colours.size() == 1);
            expect(colours.contains(colour1) == true);
            expect(colours.at(0) == colour1);
            
            beginTest("Template construction test");
            Fixes fixes;
            VORs vors;
            NDBs ndbs;
            Airports airports;
            expect(fixes.size() == 0);
            
            beginTest("Template add test");
            GeoCoords aCoord("1d2'3.4\"N 1d2'3.4\"W");
            Fix fix("A fix", aCoord);
            fixes.add(fix);
            fixes.add(fix);
            expect(fixes.size() == 1);
            expect(fixes.at(0).getName() == "A fix");
            
            beginTest("Template contains test");
            Fix anotherFix("B fix", GeographicLib::GeoCoords());
            expect(fixes.contains(fix));
            expect(fixes.contains("A fix"));
            expect(!fixes.contains(anotherFix));
            expect(!fixes.contains("A non existant fix"));
            
            beginTest("getCoordinateFor test");
            expect(fixes.getCoordinateFor("A fix").DMSRepresentation() ==  aCoord.DMSRepresentation());
            expect(!fixes.getCoordinateFor("A non existant fix").isValid());
            expect(!vors.getCoordinateFor("sdsd").isValid());
        }
    }
};

class SectorContainerTests : public UnitTest
{
public:
    SectorContainerTests() : UnitTest ("Sector Container Tests") {}
    
    void runTest()
    {
        beginTest("NamedCoordinateContainer test");
        NamedCoordinateContainer<Fix> fixes;
        NamedCoordinateContainer<VOR> vors;
        NamedCoordinateContainer<NDB> ndbs;
        NamedCoordinateContainer<Airport> airports;
        expect(fixes.size() == 0);
        
        GeoCoords aCoord("1d2'3.4\"N 1d2'3.4\"W");
        GeoCoords bCoord("4d3'2.1\"N 4d3'2.1\"W");
        Fix fix("A fix", aCoord);
        Fix fix2("B fix", bCoord);
        
        fixes.add(fix);
        expect(fixes.size() == 1);
        expect(fixes.at(0).getName() == fix.getName());
        
        expect(!fixes.contains(fix2));
        expect(!fixes.contains(fix2.getName()));
        
        fixes.add(fix2);
        expect(fixes.contains(fix2));
        expect(fixes.contains(fix2.getName()));
        
        // GetCoordinateForTest
        expect(fixes.getCoordinateFor("B fix").DMSRepresentation() == bCoord.DMSRepresentation());
        
        // GetNamedCoordinateFor() test
        NamedCoordinate nc(fixes.getNamedCoordinateFor("B fix"));
        expect(nc.hasName());
        expect(nc.getName() == "B fix");
        
        fixes.at(0).setName("A fix!");
        expect(fixes.at(0).getName() == "A fix!");
        
        beginTest("NamedDiagram constructors");
        CoordinateLine line(aCoord, bCoord);
        {
            // Default constructor
            NamedDiagram<CoordinateLine> empty;
            expect(empty.getName().isEmpty());
            expect(empty.size()==0);
            
            // Name and line constructor
            NamedDiagram<CoordinateLine> diagram("A diagram", line);
            expect(diagram.size() == 1);
            diagram.addLine(line);
            expect(diagram.getName() == "A diagram");
            expect(diagram.atLine(0) == line);
            
            // Name only constructor
            NamedDiagram<CoordinateLine> diagram2("A diagram2");
            expect (diagram2.getName() == "A diagram2" && diagram2.size() == 0);
            
            typedef NamedDiagram<CoordinateLine> GeoLineDiagram;
            typedef NamedDiagramsContainer<GeoLineDiagram, CoordinateLine> GeoLineDiagrams;
            
            beginTest("NamedDiagramsContainer constructor tests");
            {
                GeoLineDiagrams glDiagrams;
                expect(glDiagrams.size() == 0);
            }
            
            {
                GeoLineDiagrams glDiagrams("new diagram");
                expect(glDiagrams.size() == 1);
            }
            
            {
                GeoLineDiagrams glDiagrams("new diagram", line);
                expect(glDiagrams.size() == 1);
                expect(glDiagrams.atDiagram(0).getName() == "new diagram");
                // Paranoid testing here
                expect(glDiagrams.atDiagram(0).atLine(0) == line);
                
                beginTest("NamedDiagramContainer addLine tests");
                CoordinateLine line2(bCoord, aCoord);
                glDiagrams.addline("another diagram", line2);
                expect(glDiagrams.size() == 2);
                expect(glDiagrams.atDiagram(1).atLine(0) == line2);
                
                glDiagrams.addline("new diagram", line2);
                expect(glDiagrams.atDiagram(0).atLine(1) == line2);
            }
        }
        
    }
};

static SectorContainerTests sectorContainertests;
static SectorContainerTest sectorTests;
