/*
  ==============================================================================

    SectorContainers.h
    Created: 2 Jan 2015 10:34:17pm
    Author:  Jim Mazur

  ==============================================================================
*/

#ifndef SECTORCONTAINERS_H_INCLUDED
#define SECTORCONTAINERS_H_INCLUDED

#include "JuceHeader.h"
#include "SectorTypes.h"
#include <map>
#include <vector>

namespace sector
{

//==============================================================================
/**
 Template definition for a container based upon a std::map that stores named
 Elements.
 
 This will wrap around Fixes, VORs, NDBs and Airports. The use of a map ensures
 that there will be fast access times as the contains() function will be called
 offten when parsing a SCT file (for named coordinates).
 
 Any class that is used with this template must have a public member function:
 String getName() const;
 Which returns the name of the object.
 */

class NamedColours
{
public:
    bool contains (const String& colour) const;
    
    bool contains (const NamedColour& colour) const;
    
    void add (const NamedColour& colour);
    
    int size () const;
    
    NamedColour at (const int& index);
    
    NamedColour& getReference (const int& index);
    
    NamedColour getNamedColourFor (const String& colourName) const;
    
private:
    std::map<String, NamedColour> colours;
    StringArray order;
};

template <class T>
class NamedCoordinateContainer
{
public:
    bool contains (const T& element) const
    {
        if (elements.find(element.getName()) == elements.end())
            return false;
        
        return true;
    }
    
    bool contains (const String& name) const
    {
        if (elements.find(name) != elements.end())
            return true;
        
        return false;
    }
    
    const Coordinates getCoordinateFor (const String& name) const
    {
        const_elementsIterator it = elements.find(name);
        
        if (it == elements.end())
            return Coordinates();
        
        return it->second.getCoordinate();
    }
    
    NamedCoordinate getNamedCoordinateFor (const String& name) const
    {
        const_elementsIterator it = elements.find(name);
        
        if (it == elements.end())
            return NamedCoordinate();
        
        return it->second;
    }
    
    void add (const T& element)
    {
        if (!contains(element))
            order.add(element.getName());
            
        elements[element.getName()] = element;
        
        if (element.getName().length() > largestNameLength)
            largestNameLength = element.getName().length();
    }
    
    int size() const
    {
        jassert(elements.size() == order.size());
        return (int) elements.size();
    }
    
    bool isEmpty() const
    {
        if (size() > 0)
            return false;
        return true;
    }
    
    T& at (const int& index)
    {
        jassert(index < size());
        
        elementsIterator it = elements.find(order[index]);
        
        jassert(it != elements.end());
        
        return it->second;
    }
    
    int lengthOfLargestName () const
    {
        return largestNameLength;
    }
    
private:
    typedef typename std::map<String, T>::iterator elementsIterator;
    typedef typename std::map<String, T>::const_iterator const_elementsIterator;
    std::map<String, T> elements;
    
    StringArray order;
    
    int largestNameLength = 0;
};

typedef NamedCoordinateContainer<NavAid> Fixes;
typedef NamedCoordinateContainer<RadioNavAid> VORs;
typedef NamedCoordinateContainer<RadioNavAid> NDBs;
typedef NamedCoordinateContainer<Airport> Airports;

//==============================================================================
/**
 NamedDiagram
 
 Template definition for a diagram that has a name and stores lines of linetype
 T. Use this for storing basic diagrams like SIDs and STARs
 
 */

template <class T>
class NamedDiagram
{
public:
    NamedDiagram ()
    {}
    
    ~NamedDiagram ()
    {}
    
    /** Create an empty digram with the name specified */
    NamedDiagram (const String& name)
    :
    name(name)
    {}
    
    /** Create a diagram with the specified name and add a line to it */
    NamedDiagram (const String& name, const T& line)
    :
    name(name)
    {
        addLine(line);
    }

    
    void addLine (const T& line)
    {
        lines.push_back(line);
    }
    
    int size () const
    {
        return lines.size();
    }
    
    const T& atLine (const int& index) const
    {
        jassert ((index >= 0 && index < lines.size()));
        
        return lines.at(index);
    }
    
    T& getReferenceToLine (const int& index)
    {
        jassert ((index >= 0 && index < lines.size()));
        
        return lines.at(index);
    }
    
    void setName (const String& name)
    {
        this->name = name;
    }

    
    String getName() const
    {
        return name;
    }
    
    bool isName(const String& name) const
    {
        return this->name == name ? true : false;
    }
    
private:
    String name;
    std::vector<T> lines;
};

/** Typedefs for Diagrams */
typedef NamedDiagram<sector::Line> NamedLineDiagram;
typedef NamedDiagram<sector::ColouredLine> NamedColouredLineDiagram;


//==============================================================================
/**
 NamedDiagramsContainer
 
 Template definition for a container that stores multiple NamedDiagram containers.
 
 */

template<class TDiagramType, class TLineType>
class NamedDiagramsContainer
{
public:
    NamedDiagramsContainer ()
    {}
    
    NamedDiagramsContainer(const String& diagramName)
    {
        diagrams.push_back(TDiagramType(diagramName));
        
        checkIfLongestName(diagramName);
    }
    
    NamedDiagramsContainer(const String& diagramName, const TLineType& firstLine)
    {
        diagrams.push_back(TDiagramType(diagramName, firstLine));
        
        checkIfLongestName(diagramName);
    }
    
    bool contains (const String& diagramName) const
    {
        return containsName(diagramName);
    }
    
    void startNewDiagram (const String& diagramName, const TLineType& line)
    {
        diagrams.push_back(TDiagramType(diagramName, line));
        
        checkIfLongestName(diagramName);
    }
    
    void addToExistingDiagram (const String& diagramName, const TLineType& line)
    {
        // Is there already a named diagram in the vector?
        auto diagram = findName(diagramName);
        
        if (diagram != nullptr)
            diagram->addLine(line);
        else
            // if not, add it
            diagrams.push_back(TDiagramType(diagramName, line));
        
        checkIfLongestName(diagramName);
    }
    
    void addLine (const TLineType& line)
    {
        diagrams.back().addLine(line);
        
    }
    
    int size() const
    {
        return diagrams.size();
    }
    
    bool isEmpty() const
    {
        if (size() == 0)
            return true;
        return false;
    }
    
    TDiagramType& getReferenceToDiagram (const int index)
    {
        jassert(diagrams.size() > index);
        
        return diagrams.at(index);
    }
    
    const TDiagramType& atDiagram(const int index) const
    {
        jassert(diagrams.size() > index);
        
        return diagrams.at(index);
    }
    
    int lengthOfLongestName() const
    {
        return longestNameLength;
    }
    
    
private:
    bool containsName (const String& diagramName) const
    {
        for (diagramsConstIterator it = diagrams.begin(); it != diagrams.end(); it++)
        {
            if (it->isName(diagramName))
                return true;
        }
        return false;
    }
    
    TDiagramType* findName (const String& diagramName)
    {
        for (diagramsIterator it = diagrams.begin(); it != diagrams.end(); it++)
        {
            if (it->isName(diagramName))
                return &(*it);
        }
        return nullptr;
    }
    
    void checkIfLongestName (const String& diagramName)
    {
        if (diagramName.length() > longestNameLength)
            longestNameLength = diagramName.length();
    }

private:
    typedef typename std::vector<TDiagramType>::const_iterator diagramsConstIterator;
    typedef typename std::vector<TDiagramType>::iterator diagramsIterator;
    std::vector<TDiagramType> diagrams;
    int longestNameLength = 0;
};

typedef NamedDiagramsContainer<NamedLineDiagram, sector::Line> NamedLineDiagramsContainer;
typedef NamedLineDiagramsContainer ArtccContainer;
typedef NamedLineDiagramsContainer AirwaysContainer;

typedef NamedDiagramsContainer<NamedColouredLineDiagram, sector::ColouredLine> NamedColourLineDiagramsContainer;
typedef NamedColourLineDiagramsContainer SID_STAR_Container;

//==============================================================================
/**
 NamedDiagram
 
 Template definition for a diagram that has a name and stores lines of linetype
 T. Use this for storing basic diagrams like SIDs and STARs
 
 */
    
typedef Array<ColouredLine> GeoLinesContainer;


class ColouredCoordinates     : public Array<CoordinatesWithNotes>, public NamedColour
{
public:
    ColouredCoordinates () {}
};
    
typedef ColouredCoordinates Region;
typedef Array<Region> Regions;
    
typedef Array<Runway> Runways;
typedef Array<sector::Label> Labels;

} // end of sector namespace

#endif  // SECTORCONTAINERS_H_INCLUDED

/**
 template <class T>
 class Diagram
 {
 public:
 Diagram () {}
 
 Diagram (const T& line)
 {
 addLine(line);
 }
 
 
 void addLine (const T& line)
 {
 lines.pushback(line);
 }
 
 int size () const
 {
 lines.size();
 }
 
 T& at (const int& index) const
 {
 jassert (index < lines.size());
 
 return lines.at(index);
 }
 
 private:
 std::vector<T> lines;
 };
 
 
 typedef Diagram<ColouredLine> ColouredLineDiagram;
 typedef ColouredLineDiagram GeoDiagramContainer;
 */
