/*
  ==============================================================================

    SectorTypes.h
    Created: 2 Jan 2015 4:05:40pm
    Author:  Jim Mazur

  ==============================================================================
*/

#ifndef SECTORTYPES_H_INCLUDED
#define SECTORTYPES_H_INCLUDED


#include "JuceHeader.h"
#include <GeographicLib/GeoCoords.hpp>



namespace sector
{

//==============================================================================
/**
 SectorFileLine
 
 An object which olds the original string of the line that was parsed during
 reading the Sector. This is mainly used for debugging purposes.
 
*/


class SectorFileLine
{
public:
    SectorFileLine ();
    
    /** Construct this object with a line of text and its line number */
    SectorFileLine (const String& lineFromSectorFile, const int& lineInSectorFile);
    
    bool hasValidSectorFileLine () const;
    
    void setLineFromSectorFile (const String& text);
    
    String getLineFromSectorFile () const;
    
    void setLineNumberInSectorFile (const int& lineNumber);
    
    int getLineNumberInSectorFile () const;

private:
    String text;
    int lineNumber;
};
    
class Coordinates   : public GeographicLib::GeoCoords
{
public:
    /** Default constructor */
    Coordinates ();
    
    /** Construct Coordinates from a GeoCoords object */
    Coordinates (const GeographicLib::GeoCoords& coordinates);
    
    /** Construct Coordinates from a String object */
    Coordinates (const String& coordinates);
    
    /** Construct Coordinates from a lat/lon cooridnate */
    Coordinates (GeographicLib::Math::real lat, GeographicLib::Math::real lon);
    
    /** Operator overloads */
    bool operator== (const Coordinates& other) const;
    bool operator!= (const Coordinates& other) const;
    
    /** Tests if the Coordinates are actually valid */
    bool isValid() const;
};

//==============================================================================
/**
 AlignedNote
 
 A Note that has an alignment to another peice of text. For example an AlignedNote
 might sit above, next (to the right), or below a sector object. 
 
 ;A note above
 EISN   N051.00.00.000 W008.00.00.000 N049.35.00.000 W008.00.00.000 ; right
 ;below
*/

class AlignedNote
{
public:
    enum Alignment {
        right = 0,
        above = 1,
        below = 2
    };
    
    /** Empty aligned right note */
    AlignedNote();
    
    /** Creates AlignedNote with text, sets alignment to right */
    AlignedNote (const String& text);
    
    /** Creates note with text and specified alignment */
    AlignedNote (const String& text, Alignment alignment);
    
    /** Copy constructor */
    AlignedNote (const AlignedNote& other);
    
    /** Converstion constructor */
    operator String() const;
    
    /** Returns of the note is not empty */
    bool hasNote() const;
    
    /** Returns the text of the note */
    String getText() const;
    
    /** Sets the text of the note */
    void setNote (const String& text);
    
    /** Gets the alignment of the text retunrns:
     @return right, above, below
     */
    Alignment getAlignment() const;
    
    /** Sets the alignment of the note */
    void setAlignment (const Alignment& alignment);

private:
    Alignment alignment;
    String text;
};
    

//==============================================================================
/**
 Holds various AlignedNotes. As a sector object can contain diverse notes, such
 as:
 
 ; Shannon Oceanic Transition Area (SOTA)
 EISN   N051.00.00.000 W008.00.00.000 N049.35.00.000 W008.00.00.000 ; Another note
 
 Multiple notes will need to be stored.
 */
    
class Notes
{
public:
    Notes () {}
    
    /** Creates notes with a String. The text will automatically be right aligned */
    Notes (const String& text);
    
    /** Creates notes with a char array */
    Notes (const char* text);
    
    /** Creates notes with an AlignedNote */
    Notes (const AlignedNote& note);
    
    void clear ();
    
    void addNotes (const String& text);
    
    /** Returns if notes are contained in this object */
    bool hasNotes() const;
    
    /** Gets the note */
    String getNotes () const;
    
    /** Gets number of notes stored */
    int numOfNotes () const;
    
    void addPrecedingNotes (const String& text);
    
    bool hasPrecedingNotes () const;
    
    int numOfPrecedingNotes () const;
    
    // Joins notes with line break characters seperating them
    String getPrecedingNotes () const;
    
    // Returns AlignedNote from array, if the index is out-of-range an empty Aligned Note is retruned
    AlignedNote getNoteAt(const int index);
    
private:
    StringArray notes;
    StringArray precedingNotes;
    StringArray notesBelow;
};
    
class NamedObject
{
public:
    /** Creates NamedObject with an empty name */
    NamedObject ();
    
    /** Creates NamedObject with the given name. */
    NamedObject (const String& name);
    
    bool operator== (const NamedObject& other) const;
    bool operator!= (const NamedObject& other) const;
    
    /** Sets the name associated with the object */
    void setName (const String& name);
    
    /** Returns the name that is stored */
    String getName () const;
    
    /** Checks if the name has been set, returns true if the name is not empty. */
    bool hasName () const;
    
    /** Returns if the stored name matches the. */
    bool isName (const String& name) const;
    
private:
    String name;
};
    
class NamedColour  : public Notes, public NamedObject, public SectorFileLine
{
public:
    /** Creates an empty string. */
    NamedColour () {};
    
    /** Creates a NamedColour from a
        @param name     the name of the Colour
        @param colour   the Colour to store
        @param notes     an optional note, or notes to store
     */
    NamedColour (const String& name, const Colour& colour, const Notes& notes = Notes());
    
    /** Creates an unnamed colour */
    NamedColour (const Colour& colour);
    
    /** Compares two Named Colours */
    bool operator== (NamedColour &other) const;
    
    void setNamedColour (const NamedColour& colour);
    
    /** Sets the Colour */
    void setColour (const Colour& colour)       { this->colour = colour; }
    
    /** Gets the Colour stored */
    Colour getColour () const                   { return colour; }
    
private:
    Colour colour;
};

class InfoBlock : public NamedObject
{
public:
    /** Creates an empty string.
     */
    InfoBlock (const String& sectorName = "Unnamed",
                const String& defaultCallsign = "XXX_XXX",
                const String& defaultAirport = "XXXX",
                const Coordinates& defaultCenterPoint = Coordinates(),
                const int& milesPerDegreeLatitude = 0,
                const int& milesPerDegreeLongitude = 0,
                const int& magneticVariation = 0,
                const int& sectorScale = 0)
    :
    NamedObject(sectorName),
    defaultCallsign(defaultCallsign),
    defaultAirport(defaultAirport),
    defaultCenterPoint(defaultCenterPoint),
    milesPerDegreeLatitude(milesPerDegreeLatitude),
    milesPerDegreeLongitude(milesPerDegreeLongitude),
    magneticVariation(magneticVariation),
    sectorScale(sectorScale)
    {};
    
    /** Creates an empty string.
     */
    StringArray getTextBlock() const;
    
    /** Creates an empty string.
     */
    void setDefaultCallsign (const String& defaultCallsign) { this->defaultCallsign = defaultCallsign; }
    
    /** Creates an empty string.
     */
    String getDefaultCallsign () const                      { return defaultCallsign; }
    
    /** Creates an empty string.
     */
    void setDefaultAirport (const String& defaultAirport)   { this->defaultAirport = defaultAirport; }
    
    /** Creates an empty string.
     */
    String getDefaultAirport() const                        { return defaultAirport; }
    
    /** Creates an empty string.
     */
    void setDefaultCenterPoint (const Coordinates& coord )    { this->defaultCenterPoint = coord; }
    
    /** Creates an empty string.
     */
    Coordinates getDefaultCenterPoint () const                { return defaultCenterPoint; }
    
    /** Creates an empty string.
     */
    void setMilesPerDegreeLatitude (const double& miles)       { this->milesPerDegreeLatitude = miles; }
    
    /** Creates an empty string.
     */
    double getMilesPerDegreeLatitude () const                  { return milesPerDegreeLatitude; }
    
    /** Creates an empty string.
     */
    void setMilesPerDegreeLongitude (const double& miles)      { this->milesPerDegreeLongitude = miles; }
    
    /** Creates an empty string.
     */
    double getMilesPerDegreeLongitude () const                 { return milesPerDegreeLongitude; }
    
    /** Creates an empty string.
     */
    void setMagneticVariation (const float& variation )       { this->magneticVariation = variation; }
    
    /** Creates an empty string.
     */
    double getMagneticVariation () const                       { return magneticVariation; }
    
    /** Creates an empty string.
     */
    void setSectorScale (const double& sectorScale )           { this->sectorScale = sectorScale; }
    
    /** Creates an empty string.
     */
    double getSectorScale () const                             { return sectorScale; }
    
private:
    String defaultCallsign;
    String defaultAirport;
    Coordinates defaultCenterPoint;
    double milesPerDegreeLatitude;
    double milesPerDegreeLongitude;
    double magneticVariation;
    double sectorScale;
};

//==============================================================================
/**
 NamedCooridnate
 
 This is a basic class that contains a NamedObject and a Coordinate. This can be
 used for navigation aids like Fixes, which only require a designator and 
 cooordinates. VOR's, NDB's and Airports inherit this object.
 */
class NamedCoordinate : public NamedObject
{
public:
    /** Creates an empty NamedCoordinate with invalid Coordinate and empty name
     */
    NamedCoordinate () {}
    
    /** Creates a NamedCoordiante with a name and location.
        @param name     the Name of the Coordinate
        @param location the Cooridnate location
     */
    NamedCoordinate (const String& name, const Coordinates& location);
    
    /** Creates a NamedCoordinate with no name, just a location */
    NamedCoordinate (const Coordinates& location);
    
    /** Sets the coordinate of the NamedCoordinate */
    void setCoordinate (const Coordinates& cooridnate);
    
    /** Gets the coordinate of the NamedCoordinate */
    Coordinates getCoordinate () const;
    
private:
    Coordinates coordinate;
};

//==============================================================================
/**
 RadioNamedCoordinate
 
 The RadioNamedCoordinate class is used to contain a NavAid plus a frequency for that
 aid. Navigation Aids such as NDBs and VORs would inherit this class and use the
 frequency for the frequency of the aid itself. A airport would inherit this
 class and use the frequency for the tower.
 */
class RadioNamedCoordinate   : public NamedCoordinate
{
public:
    /** Creates an empty string.
     */
    RadioNamedCoordinate () {}
    
    /** Creates an empty string.
     */
    RadioNamedCoordinate (const String& name, const String& frequency, const Coordinates& location)
    :
    NamedCoordinate(name, location),
    frequency(frequency) {}
    
    /** Creates an empty string.
     */
    void setFrequency (const String& frequency)             { this->frequency = frequency; }
    
    /** Creates an empty string.
     */
    String getFrequency () const                            { return frequency; }
    
private:
    String frequency;
};

//==============================================================================
/**
 Airport
 
 Used to describe an airport. Inherits from the RadioNamedCoordinate class and adds
 the airspace identifier.
 */
class Airport       : public RadioNamedCoordinate, public Notes, public SectorFileLine
{
public:
    Airport () : RadioNamedCoordinate(), airspaceClass("?") {}
    
    /** Creates an empty string.
     */
    Airport (const String& name, const String& frequency, const Coordinates& location, const String& airspaceClass, const Notes& note = Notes())
    :
    RadioNamedCoordinate(name, frequency, location),
    Notes(note),
    airspaceClass(airspaceClass) {}
    
    /** Creates an empty string.
     */
    void setAirspaceClass (const String& airspaceClass)     { this->airspaceClass = airspaceClass; }
    
    /** Creates an empty string.
     */
    String getAirspaceClass() const                           { return airspaceClass; }
    
private:
    String airspaceClass;
};

class NavAid   : public NamedCoordinate, public Notes, public SectorFileLine
{
public:
    NavAid ()
    :
    NamedCoordinate(),
    Notes() {}
    
    NavAid (const String& name, const Coordinates& location, const Notes& note = Notes())
    :
    NamedCoordinate(name, location),
    Notes(note) {}
    
};

class RadioNavAid   : public RadioNamedCoordinate, public Notes, public SectorFileLine
{
public:
    RadioNavAid ()
    :
    RadioNamedCoordinate(),
    Notes() {}
    
    RadioNavAid (const String& name, const String& frequency, const Coordinates& location, const Notes& note = Notes())
    :
    RadioNamedCoordinate(name, frequency, location),
    Notes(note) {}
};

//==============================================================================
/**
 Runway
 
 */
class Runway    :   public Notes, public SectorFileLine
{
public:
    //==============================================================================
    /** Creates an empty string. */
    Runway () :
    runwayNumber("000"), runwayNumberOppositeEnd ("000"), magneticHeading(0), magneticHeadingOppositeEnd(0) {}
    
    /** Creates an empty string.
     */
    Runway (const String& runwayNumber, const String& runwayNumberOppositeEnd,
            const int& magneticHeading, const int& magneticHeadingOppositeEnd,
            const Coordinates& runwayStart, const Coordinates& runwayEnd,
            const Notes& note = Notes() )
    :
    Notes(note),
    runwayNumber(runwayNumber),
    runwayNumberOppositeEnd(runwayNumberOppositeEnd),
    magneticHeading(magneticHeading),
    magneticHeadingOppositeEnd(magneticHeadingOppositeEnd),
    runwayStart(runwayStart),
    runwayEnd(runwayEnd) {}
    
    /** Creates an empty string */
    void setRunwayNumber(const String& runwayNumber)            { this->runwayNumber = runwayNumber; }
    
    /** Creates an empty string */
    String getRunwayNumber() const                                   { return runwayNumber; }
    
    /** Creates an empty string */
    void setRunwayNumberOppositeEnd(const String& runwayNumber) { this->runwayNumberOppositeEnd = runwayNumber; }
    
    /** Creates an empty string */
    String getRunwayNumberOppositeEnd() const                   { return runwayNumberOppositeEnd; }
    
    /** Creates an empty string */
    void setMagneticHeading (const int& heading)               { this->magneticHeading = heading; }
    
    /** Creates an empty string */
    int getMagneticHeading () const                            { return magneticHeading; }
    
    /** Creates an empty string */
    void setMagneticHeadingOppositeEnd (const int& heading)    { this->magneticHeadingOppositeEnd = heading; }
    
    /** Creates an empty string */
    int getMagneticHeadingOppositeEnd () const                 { return magneticHeadingOppositeEnd; }
    
    /** Creates an empty string */
    void setRunwayStart (const Coordinates& coordinate)           { this->runwayStart = coordinate; }
    
    /** Creates an empty string */
    Coordinates getRunwayStart() const                            { return runwayStart; }
    
    /** Creates an empty string */
    void setRunwayEnd (const Coordinates& coordinate)             { this->runwayEnd = coordinate; }
    
    /** Creates an empty string */
    Coordinates getRunwayEnd() const                              { return runwayEnd; }
    
private:
    String runwayNumber;
    String runwayNumberOppositeEnd;
    uint16 magneticHeading;
    int magneticHeadingOppositeEnd;
    Coordinates runwayStart;
    Coordinates runwayEnd;
};
    
//==============================================================================
/**
 NamedCoordinateLine class
 
 A simple class that represents a line from two Coordinates
*/

class NamedCoordinateLine
{
public:
    /** Creates an empty GeographicLine with two invalid coordinates */
    NamedCoordinateLine ();
    
    /** Creates an GeographicLine with a start and end point */
    NamedCoordinateLine (const NamedCoordinate& start, const NamedCoordinate& end);
    
    /** Creates an GeographicLine with a start and end point */
    NamedCoordinateLine (const Coordinates& start, const Coordinates&end);
    
    /** Equality tests */
    bool operator== (const NamedCoordinateLine& other) const;
    bool operator!= (const NamedCoordinateLine& other) const;
    
    /** Sets the line from another line */
    void setLine (const NamedCoordinateLine& line);
    
    /** Sets the start of line to a coordinate */
    void setStart (const NamedCoordinate& start);
    
    /** Gets the start coordinate of the line */
    NamedCoordinate getStart() const;
    
    /** Sets the end of the line to coordinate */
    void setEnd (const NamedCoordinate& end);
    
    /** Gets the end coordinate of the line */
    NamedCoordinate getEnd() const;
    
    /** Checks if either of the coordiates is created by a NamedCoordinate */
    bool containsNamedCoordinates() const;
    
private:
    NamedCoordinate start;
    NamedCoordinate end;
};

class Line :            public NamedCoordinateLine, public Notes, public SectorFileLine
{
public:
    Line () {}
    
    /** Creates a Line from a NamedCoordinate start and end point */
    Line (const NamedCoordinate& start, const NamedCoordinate& end)
    :
    NamedCoordinateLine(start, end)
    {}
    
    /** Creates a Line from a NamedCoordinate start and end point along with
     the provided notes*/
    Line (const NamedCoordinate& start, const NamedCoordinate& end, const Notes notes)
    :
    NamedCoordinateLine(start, end),
    Notes(notes)
    {}

};

class NamedLine :       public NamedObject, public Line
{
public:
    NamedLine () {}
    
    NamedLine (const String& name, const NamedCoordinate& start, const NamedCoordinate& end)
    :
    NamedObject(name),
    Line(start, end)
    {}
    
     NamedLine (const String& name, const NamedCoordinate& start, const NamedCoordinate& end, const Notes& notes)
    :
    NamedObject(name),
    Line(start, end, notes)
    {}
};
    
class ColouredLine  :   public Line
{
public:
    ColouredLine ()
    {}
    
    ColouredLine (const NamedCoordinate& start, const NamedCoordinate& end, const Colour& colour)
    :
    Line(start, end),
    namedColour(colour),
    hasColour(true)
    {}
    
    ColouredLine (const NamedCoordinate& start, const NamedCoordinate& end, const Colour& colour, const Notes& notes)
    :
    Line(start, end, notes),
    namedColour(colour),
    hasColour(true)
    {}
    
    void setNamedColour (const NamedColour& colour)
    {
        namedColour = colour;
        hasColour = true;
    }
    
    NamedColour getNamedColour () const
    {
        return namedColour;
    }
    
    bool hasNamedColour () const
    {
        return hasColour;
    }
    
private:
    NamedColour namedColour;
    bool hasColour = false;
};
    
//==============================================================================
/**
 Regions
 */
    
class CoordinatesWithNotes  :   public Coordinates, public Notes, public SectorFileLine
{
public:
    CoordinatesWithNotes () {}
    
    CoordinatesWithNotes (const Coordinates& coords)
    :
    Coordinates(coords)
    {}
    
    CoordinatesWithNotes (const Coordinates& coords, const Notes& notes)
    :
    Coordinates(coords),
    Notes(notes)
    {}
    
};
    
//==============================================================================
/**
 Label
 A label is a some text positioned by a coordinate with a prescribed colour
*/

class Label     :       public NamedObject, public Notes, public SectorFileLine
{
public:
    /** Default constructor */
    Label () {}

    void setCoordinate (const NamedCoordinate& coord)                   { this->coord = coord; }
    
    NamedCoordinate getNamedCoordinate () const                         { return coord; }
    
    void setColour (const NamedColour& colour )                         { this->colour = colour; }
    
    NamedColour getColour () const                                      { return colour; }
    
private:
    NamedCoordinate coord;
    NamedColour colour;
};


} // end of SectorTypes namespace

#endif  // SECTORTYPES_H_INCLUDED
