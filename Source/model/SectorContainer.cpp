/*
  ==============================================================================

    Sector.cpp
    Created: 1 Jan 2015 11:38:46am
    Author:  Jim Mazur

  ==============================================================================
*/


// Required to implement the singleton of the Sector class
//juce_ImplementSingleton(Sector);

//==============================================================================

bool SectorContainer::containsCoordinatesFor(const juce::String &navaid) const
{
    if (fixes.contains(navaid))
        return true;
    
    if (vors.contains(navaid))
        return true;
    
    if (ndbs.contains(navaid))
        return true;
    
    if (airports.contains(navaid))
        return true;
    
    return false;
}


Coordinates SectorContainer::getCoordinatesFor(const String &navaid) const
{
    if (fixes.contains(navaid))
        return fixes.getCoordinateFor(navaid);
    
    if (vors.contains(navaid))
        return vors.getCoordinateFor(navaid);
    
    if (ndbs.contains(navaid))
        return ndbs.getCoordinateFor(navaid);
    
    if (airports.contains(navaid))
        return airports.getCoordinateFor(navaid);
    
    return Coordinates();
}

NamedCoordinate SectorContainer::getNamedCoordinatesFor(const String &navaid) const
{
    if (fixes.contains(navaid))
        return fixes.getNamedCoordinateFor(navaid);
    
    if (vors.contains(navaid))
        return vors.getNamedCoordinateFor(navaid);
    
    if (ndbs.contains(navaid))
        return ndbs.getNamedCoordinateFor(navaid);
    
    if (airports.contains(navaid))
        return airports.getNamedCoordinateFor(navaid);
    
    return NamedCoordinate();
}
