/*
  ==============================================================================

    SectorContainers.cpp
    Created: 2 Jan 2015 10:34:17pm
    Author:  Jim Mazur

  ==============================================================================
*/

#include "SectorContainers.h"

namespace sector
{

bool NamedColours::contains(const juce::String &colour) const
{
    if (colours.find(colour.toLowerCase()) != colours.end())
        return true;
    
    return false;
}
    
bool NamedColours::contains(const NamedColour &colour) const
{
    if (colours.find(colour.getName().toLowerCase()) != colours.end())
        return true;
    
    return false;
}

void NamedColours::add(const NamedColour &colour)
{
    if (!contains(colour))
        order.add(colour.getName().toLowerCase());
    
    colours[colour.getName().toLowerCase()] = colour;
}

int NamedColours::size() const
{
    jassert(colours.size() == order.size());
    return colours.size();
}

NamedColour NamedColours::at(const int &index)
{
    jassert(index < colours.size());

    std::map<String, NamedColour>::const_iterator it = colours.find(order[index]);
    
    jassert(it != colours.end());
    
//    std::map<String, NamedColour>::iterator it;
//    it = colours.begin();
//    std::advance(it, index);
    
    return it->second;
}

NamedColour& NamedColours::getReference(const int &index)
{
    jassert(index < colours.size());
    
    std::map<String, NamedColour>::iterator it = colours.find(order[index]);
    
    jassert(it != colours.end());
    
    //    std::map<String, NamedColour>::iterator it;
    //    it = colours.begin();
    //    std::advance(it, index);
    
    return it->second;
}
    
NamedColour NamedColours::getNamedColourFor(const juce::String &colourName) const
{
    std::map<String, NamedColour>::const_iterator it = colours.find(colourName.toLowerCase());
    if (it == colours.end())
        return NamedColour();
    
    return it->second;
}


//==============================================================================

class SectorContainerTest   : public UnitTest
{
public:
    SectorContainerTest()   : UnitTest ("Sector functions testing") {}
    
    void runTest()
    {
        {
            beginTest("DefinedColours test");
            // Construction
            NamedColours colours;
            expect(colours.size() == 0);
            expect(colours.contains(NamedColour()) == false);
            
            // Adding
            NamedColour colour1("testname", Colours::blue);
            colours.add(colour1);
            expect(colours.size() == 1);
            expect(colours.contains(colour1) == true);
            expect(colours.contains("TESTNAME")); // Case test
            expect(colours.at(0) == colour1);
            
            beginTest("Template construction test");
            Fixes fixes;
            VORs vors;
            NDBs ndbs;
            Airports airports;
            expect(fixes.size() == 0);
            
            beginTest("Template add test");
            GeographicLib::GeoCoords aCoord("1d2'3.4\"N 1d2'3.4\"W");
            NavAid fix("A fix", aCoord);
            fixes.add(fix);
            fixes.add(fix);
            expect(fixes.size() == 1);
            expect(fixes.at(0).getName() == "A fix");
            
            beginTest("Template contains test");
            NavAid anotherFix("B fix", GeographicLib::GeoCoords());
            expect(fixes.contains(fix));
            expect(fixes.contains("A fix"));
            expect(!fixes.contains(anotherFix));
            expect(!fixes.contains("A non existant fix"));
            
            beginTest("getCoordinateFor test");
            expect(fixes.getCoordinateFor("A fix").DMSRepresentation() ==  aCoord.DMSRepresentation());
            expect(!fixes.getCoordinateFor("A non existant fix").isValid());
            expect(!vors.getCoordinateFor("sdsd").isValid());
        }
    }
};

class NamedCoordinateContainerTest : public UnitTest
{
private:
    NamedCoordinate coordinate1a;
    NamedCoordinate coordinate1b;
    NamedCoordinate coordinate2a;
    NamedCoordinate coordinate2b;
    
    NavAid navaid1;
    NavAid navaid2;
    
    AlignedNote note1;
    AlignedNote note2;
    
    NamedLine line1;
    NamedLine line2;
    
public:
    NamedCoordinateContainerTest()
    :
    UnitTest ("NamedCoordinateContainer Tests"),
    coordinate1a("KC1a", Coordinates(11.11, 22.22)),
    coordinate1b("KC1b", Coordinates(33.33, 44.44)),
    navaid1("navaid1", coordinate1a.getCoordinate()),
    navaid2("navaid2", coordinate1b.getCoordinate())
    {}
    
    void runTest()
    {
        beginTest("Constructor tests");
        NamedCoordinateContainer<NavAid> fixes;
        NamedCoordinateContainer<RadioNavAid> vors;
        NamedCoordinateContainer<Airport> airports;
        expect(fixes.size() == 0);
        
        beginTest("Add tests");
        {
            fixes.add(navaid1);
            expect(fixes.size() == 1);
            expect(fixes.at(0).getName() == navaid1.getName());
        }
        
        beginTest("Contains tests");
        {
            expect(!fixes.contains(navaid2));
            expect(!fixes.contains(navaid2.getName()));
            
            fixes.add(navaid2);
            expect(fixes.contains(navaid2));
            expect(fixes.contains(navaid2.getName()));
        }
        
        beginTest("getCoordinateFor tests");
        {
            expect(fixes.getCoordinateFor("navaid1") == navaid1.getCoordinate());
            expect(fixes.getCoordinateFor("non coord") == Coordinates());
            expect(fixes.getNamedCoordinateFor("navaid1").getCoordinate() == navaid1.getCoordinate());
            expect(fixes.getNamedCoordinateFor("non coord") == NamedCoordinate());
        }
        
        beginTest("at tests");
        {
            expect(fixes.at(0) == navaid1);
            expect(fixes.at(1) == navaid2);
        }
    }
};


class NamedDiagramTests :   public UnitTest
{
private:
    NamedCoordinate coordinate1a;
    NamedCoordinate coordinate1b;
    NamedCoordinate coordinate2a;
    NamedCoordinate coordinate2b;
    
    NavAid navaid1;
    NavAid navaid2;
    
    AlignedNote note1;
    AlignedNote note2;
    
    NamedLine line1;
    NamedLine line2;
public:
    NamedDiagramTests()
    :
    UnitTest ("NamedDiagramTests Tests"),
    coordinate1a("KC1a", Coordinates(11.11, 22.22)),
    coordinate1b("KC1b", Coordinates(33.33, 44.44)),
    coordinate2a("KC2a", Coordinates(00.11, 00.22)),
    coordinate2b("KC2b", Coordinates(00.33, 00.44)),
    navaid1("navaid1", coordinate1a.getCoordinate()),
    navaid2("navaid2", coordinate1b.getCoordinate()),
    note1("note1"),
    note2("note2"),
    line1("line1", coordinate1a, coordinate1b),
    line2("line2", coordinate2a, coordinate2b)
    {}
    
    void runTest()
    {
        using sector::Line;
        
        beginTest("Construction tests");
        {
            NamedDiagram<sector::Line> lineNamedDiagram;
            expect(lineNamedDiagram.getName() == String::empty);
            expect(lineNamedDiagram.size() == 0);
        }
        
        {
            NamedDiagram<sector::Line> lineNamedDiagram("diagram");
            expect(lineNamedDiagram.getName() == "diagram");
            expect(lineNamedDiagram.size() == 0);
        }
        
        Line line(coordinate1a, coordinate1b);
        NamedDiagram<Line> lineNamedDiagram("diagram", line);
        
        {
            expect(lineNamedDiagram.getName() == "diagram");
            expect(lineNamedDiagram.size() == 1);
            expect(lineNamedDiagram.atLine(0) == line);
        }
        
        beginTest("addLine tests");
        {
            Line line2(coordinate1a, coordinate1b);
            lineNamedDiagram.addLine(line2);
            expect(lineNamedDiagram.size() == 2);
            expect(lineNamedDiagram.atLine(1) == line2);
        }
        
    }
};
    
class NamedDiagramsContainerTests   : public UnitTest
{
private:
    NamedCoordinate coordinate1a;
    NamedCoordinate coordinate1b;
    NamedCoordinate coordinate2a;
    NamedCoordinate coordinate2b;
    NamedCoordinate coordinate3a;
    NamedCoordinate coordinate3b;
    
    NavAid navaid1;
    NavAid navaid2;
    
    AlignedNote note1;
    AlignedNote note2;
    
    NamedLine line1a;
    NamedLine line1b;
    NamedLine line2;
public:
    NamedDiagramsContainerTests()
    :
    UnitTest ("NamedDiagramsContainer Tests"),
    coordinate1a("KC1a", Coordinates(11.11, 22.22)),
    coordinate1b("KC1b", Coordinates(33.33, 44.44)),
    coordinate2a("KC2a", Coordinates(00.11, 00.22)),
    coordinate2b("KC2b", Coordinates(00.33, 00.44)),
    coordinate3a("KC3a", Coordinates(11.00, 22.00)),
    coordinate3b("KC3b", Coordinates(33.00, 44.00)),
    line1a("line1", coordinate1a, coordinate1b),
    line1b("line1", coordinate3a, coordinate3b),
    line2("line2", coordinate2a, coordinate2b)
    {}
    
    void runTest()
    {
        using sector::Line;
        
        beginTest("Constructor Tests");
        {
            NamedDiagramsContainer<NamedLineDiagram, Line> container;
            expect(container.size() == 0);
        }
        {
             NamedDiagramsContainer<NamedLineDiagram, Line> container("name");
            expect(container.size() == 1);
        }
        
        NamedDiagramsContainer<NamedLineDiagram, Line> container(line1a.getName(), line1a);
        {
            expect(container.size() == 1);
            expect(container.atDiagram(0).size() == 1);
            expect(container.atDiagram(0).atLine(0) == line1a);
            expect(container.atDiagram(0).atLine(0) != line1b);
        }
        
        beginTest("contains tests");
        {
            expect(container.contains(line1a.getName()));
            expect(!container.contains(line2.getName()));
        }
        
        beginTest("addLine tests");
        {
            container.addToExistingDiagram(line1b.getName(), line1b);
            expect(container.size() == 1);
            container.addToExistingDiagram(line2.getName(), line2);
            expect(container.contains(line2.getName()));
            expect(container.atDiagram(1).atLine(0) == line2);
        }
        
        
    }
    
};

static NamedDiagramTests namedDiagramTests;
static NamedCoordinateContainerTest namedCoordinateContainerTest;
static NamedDiagramsContainerTests namedDiagramsContainerTests;
static SectorContainerTest sectorTests;

} // end of sector namespace

