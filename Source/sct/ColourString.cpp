/*
  ==============================================================================

    ColourString.cpp
    Created: 7 Feb 2015 10:06:02am
    Author:  Jim Mazur

  ==============================================================================
*/

#include "ColourString.h"
#include "StringFunctions.h"

//==============================================================================
// ColourString
//==============================================================================

ColourString::ColourString (const Colour& colour)
:
isValidated(true)
{
    this->colour = colour;
}

//==============================================================================

ColourString::ColourString (const String& colour)
:
isValidated(false)
{
    // Validate the string fits the Sector formating
    
    // is it actually a number
    if (!isNumber(colour))
        return;
    
    // is it within the number limits?
    uint32 intColor = colour.getIntValue();
    if (intColor > 16777215)
        return;
    
    isValidated = true;
    
    uint8 blue     = (intColor / (256 * 256));
    uint8 green    = (intColor - (blue * (256 * 256))) / 256;
    uint8 red      = (intColor - (blue * (256 * 256)) - (green * 256));
    
    this->colour = Colour(red, green, blue);
}

//==============================================================================

String ColourString::getAsSectorFormatted() const
{
    return (String) ((colour.getBlue() * 65536) + (colour.getGreen() * 256) + colour.getRed());
}

//==============================================================================

Colour ColourString::getColour() const
{
    return colour;
}

//==============================================================================
// Unit Tests
//==============================================================================

bool ColourString::isValid() const
{
    return isValidated;
}

class ColourStringTest      : public UnitTest
{
public:
    ColourStringTest()      : UnitTest("ColourString class tests") {}
    
    void runTest()
    {
        beginTest("ColourString() tests");
        // isValid tests
        expect(ColourString("255").isValid() == true);
        expect(ColourString("16777216").isValid() == false);
        expect(ColourString(Colours::blue).isValid() == true);
        
        // Constructor and formatting tests
        expect(ColourString(Colours::red).getAsSectorFormatted() == "255");
        expect(ColourString(Colours::green).getAsSectorFormatted() == "32768");
        expect(ColourString(Colours::blue).getAsSectorFormatted() == "16711680");
        expect(ColourString("16711680").getColour() == Colours::blue);
        expect(ColourString("32768").getColour() == Colours::green);
        expect(ColourString("255").getColour() == Colours::red);
    }
};

static ColourStringTest colourStringTest;