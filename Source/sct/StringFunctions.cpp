/*
  ==============================================================================

    StringFunctions.cpp
    Created: 31 Dec 2014 11:48:26am
    Author:  Jim Mazur

  ==============================================================================
*/

#include "StringFunctions.h"

//==============================================================================

bool isNumber(const String& string)
{
    if (string.containsOnly("0123456789"))
        return true;
    
    return false;
}

bool isNotNumber(const String& string)
{
    return !isNumber(string);
}

StringArray getWhitespaceSeparatedItems (const String& sourceString)
{
    StringArray s;
    s.addTokens (sourceString, " \t", StringRef());
    s.trim();
    s.removeEmptyStrings();
    return s;
}

StringArray getCommaPeriodSeperatedItems (const String& sourceString)
{
    StringArray s;
    s.addTokens(sourceString, ",.", StringRef());
    s.trim();
    s.removeEmptyStrings();
    return s;
}

//==============================================================================
// Unit Tests
//==============================================================================

class StringFunctionTests : public UnitTest
{
public:
    StringFunctionTests () : UnitTest ("String Function Tests") {}
    
    void runTest()
    {
        beginTest("isNumber() test");
        {
            expect(isNumber("1234567890"));
            expect(!isNumber("abcdefg"));
            expect(!isNumber("12345a"));
        }
        
        beginTest("getWhitespaceSeparatedItems()");
        {
            {
                StringArray sa = getWhitespaceSeparatedItems(" first    second third ");
                expectEquals(sa[0], String("first"));
                expectEquals(sa[1], String("second"));
                expectEquals(sa[2], String("third"));
            }
        }
        
        beginTest("getCommaPeriodSeperatedItems");
        {
            StringArray sa = getCommaPeriodSeperatedItems("  123.abc.456,def  ");
            expectEquals(sa[0], String("123"));
            expectEquals(sa[1], String("abc"));
            expectEquals(sa[2], String("456"));
            expectEquals(sa[3], String("def"));
        }
    }
};

static StringFunctionTests stringFunctionTests;