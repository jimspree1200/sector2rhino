//
//  SCTwriter.cpp
//  fstream
//
//  Created by James Mazur on 12/24/12.
//  Copyright (c) 2012 James Mazur. All rights reserved.
//

#include "SCTwriter.h"
#include "Application.h"

using namespace std;

void writeSCTFile (std::string sFileName) {
    std::ofstream SCTOutputFile ("test.txt");
    
    if (SCTOutputFile.is_open()) {
        SCTOutputFile << writeDefineColors();
        SCTOutputFile << writeInfo();
        SCTOutputFile << writeAirports();
        SCTOutputFile << writeRunways();
        SCTOutputFile << writeVORs();
        SCTOutputFile << writeNDBs();
        SCTOutputFile << writeFixes();
        SCTOutputFile << writeAirways_ARTCC();
        SCTOutputFile << writeSIDs_STARs_GEO();
        SCTOutputFile << writeRegions();
    }
}


std::string writeDefineColors () {
    std::string sOutString;
    std::ostringstream outStream;
    
    std::string sName;
    cRGBColor cColor;
    std::string sColor;
    
    outStream << writeSeperator();
    
    outStream << ";Color Definitions" << endl;
    
    for (int i = 0; i < Sector2RhinoApplication::getSectorOld().countDefinedColor(); i++) {
        Sector2RhinoApplication::getSectorOld().returnDefinedColor(i, sName, cColor);
        sColor = int_2_string(RGB2SCTcolor(cColor));
        outStream << "#define " << sName << " " << sColor << std::endl;
    }
    
    outStream << writeSeperator();
    
    sOutString = outStream.str();
    return sOutString;
}

std::string writeInfo(){
    std::string sOutString;
    std::ostringstream outStream;
    std::string sCurrentLine;
    
    outStream << "[INFO]\n";
    
    for (int i = 0; i < Sector2RhinoApplication::getSectorOld().countInfoLines() ; i++) {
        outStream << Sector2RhinoApplication::getSectorOld().returnInfoLine(i) << std::endl;
    }
    
    outStream << writeSeperator();
    
    sOutString = outStream.str();
    return sOutString;
}

std::string writeAirports () {
    std::string sOutString;
    std::ostringstream outStream;
    
    std::string sAirportName;
    std::string sAirportFreq;
    std::string sAirportClass;
    std::string sNote;
    
    GeographicLib::GeoCoords coord;
    
    outStream << "[AIRPORT]" << endl;
    
    for (int i = 0; i < Sector2RhinoApplication::getSectorOld().countAirports(); i++) {
        Sector2RhinoApplication::getSectorOld().returnAirport(i, sAirportName, sAirportFreq, coord, sAirportClass, sNote);
        outStream << sAirportName << " " << sAirportFreq << " " <<ConvertCoord2SCT(coord) << " " << sAirportClass;
        if (sNote != "") outStream << " ;" << sNote;
        outStream << "" << endl;
    }
    
    outStream << writeSeperator();
    
    sOutString = outStream.str();
    return sOutString;
}

std::string writeRunways () {
    std::string sOutString;
    std::ostringstream outStream;
    
    std::string sName1;
    std::string sName2;
    std::string sHeading1;
    std::string sHeading2;
    std::string sNote;
    
    GeographicLib::GeoCoords gCoord1;
    GeographicLib::GeoCoords gCoord2;
    
    outStream << "[RUNWAY]" << endl;
    
    for (int i = 0; i < Sector2RhinoApplication::getSectorOld().countRunways(); i++) {
        Sector2RhinoApplication::getSectorOld().returnRunway(i, sName1, sName2, sHeading1, sHeading2, gCoord1, gCoord2, sNote);
        outStream << sName1 << " " << sName2 << " " << sHeading1 << " " << sHeading2 << " " << ConvertCoord2SCT(gCoord1) << " " << ConvertCoord2SCT(gCoord2);
        if (sNote != "") outStream << " ;" << sNote;
        outStream << "" << endl;
    }
    
    outStream << writeSeperator();
    
    sOutString = outStream.str();
    return sOutString;
}

std::string writeVORs () {
    std::string sOutString;
    std::ostringstream outStream;
    
    std::string sName;
    std::string sFreq;
    std::string sNote;
    //RadioNav eRadioNav;
    
    GeographicLib::GeoCoords gCoord;
    
    outStream << "[VOR]" << endl;
    
    for (int i = 0; i < Sector2RhinoApplication::getSectorOld().countVORs(); i++) {
        //Sector.returnVORNDB(i, sName, eRadioNav, sFreq, gCoord, sNote);
        Sector2RhinoApplication::getSectorOld().returnVOR(i, sName, sFreq, gCoord, sNote);
        outStream << sName << " " << sFreq << " " << ConvertCoord2SCT(gCoord);
        if (sNote != "") outStream << " ;" << sNote;
        outStream << "" << endl;
    }
    
    outStream << writeSeperator();
    
    sOutString = outStream.str();
    return sOutString;
}

std::string writeNDBs () {
    std::string sOutString;
    std::ostringstream outStream;
    
    std::string sName;
    std::string sFreq;
    std::string sNote;
    //RadioNav eRadioNav;
    
    GeographicLib::GeoCoords gCoord;
    
    outStream << "[NDB]" << endl;
    
    for (int i = 0; i < Sector2RhinoApplication::getSectorOld().countNDBs(); i++) {
        //Sector.returnVORNDB(i, sName, eRadioNav, sFreq, gCoord, sNote);
        Sector2RhinoApplication::getSectorOld().returnNDB(i, sName, sFreq, gCoord, sNote);
        outStream << sName << " " << sFreq << " " << ConvertCoord2SCT(gCoord);
        if (sNote != "") outStream << " ;" << sNote;
        outStream << "" << endl;
    }
    
    outStream << writeSeperator();
    
    sOutString = outStream.str();
    return sOutString;
}

std::string writeFixes () {
    std::string sOutString;
    std::ostringstream outStream;
    
    std::string sName;
    std::string sNote;
    
    GeographicLib::GeoCoords gCoord;
    
    outStream << "[FIXES]" << endl;
    
    for (int i = 0; i < Sector2RhinoApplication::getSectorOld().countFixes(); i++) {
        Sector2RhinoApplication::getSectorOld().returnFix(i, sName, gCoord, sNote);
        outStream << sName << " " << ConvertCoord2SCT(gCoord);
        if (sNote != "") outStream << " ;" << sNote;
        outStream << "" << endl;
    }
    
    outStream << writeSeperator();
    
    sOutString = outStream.str();
    return sOutString;
}

std::string writeAirways_ARTCC () {
    std::string sOutString;
    std::ostringstream outStream;
    
    std::string sName;
    std::string sNote;
    
    GeographicLib::GeoCoords gCoord1;
    GeographicLib::GeoCoords gCoord2;
    
    std::string sHeader;
    
    BoundryOrAir eBoundryOrAir;
    
    //int nLineCount;
    
    for (int j = 0; j < 5; j++) {
        if (j == 0) {
            sHeader = "[LOW AIRWAY]";
            eBoundryOrAir = LOW_AIRWAY;
        }
        if (j == 1) {
            sHeader = "[HIGH AIRWAY]";
            eBoundryOrAir = HIGH_AIRWAY;
        }
        if (j == 2) {
            sHeader = "[ARTCC]";
            eBoundryOrAir = ARTCC;
        }
        if (j == 3) {
            sHeader = "[ARTCC LOW]";
            eBoundryOrAir = ARTCC_LOW;
        }
        if (j == 4) {
            sHeader = "[ARTCC HIGH]";
            eBoundryOrAir = ARTCC_HIGH;
        }
        //nLineCount = Sector.countARTCCorAirwary(eBoundryOrAir);
        
        outStream << sHeader << endl;
        
        for (int i = 0; i < Sector2RhinoApplication::getSectorOld().countARTCCorAirwary(eBoundryOrAir); i++) {
            for (int j = 0; j < Sector2RhinoApplication::getSectorOld().countARTCCorAirwaryLines(eBoundryOrAir, i); j++) {
            Sector2RhinoApplication::getSectorOld().returnARTCCorAirway(i, j, eBoundryOrAir, sName, gCoord1, gCoord2, sNote);
            outStream << sName << " " << ConvertCoord2SCT(gCoord1) << " " <<ConvertCoord2SCT(gCoord2);
            if (sNote != "") outStream << " ;" << sNote;
            outStream << "" << endl;
            }
        }
        
        outStream << writeSeperator();
        
    }
    
    sOutString = outStream.str();
    return sOutString;
}

std::string writeSIDs_STARs_GEO () {
    std::string sOutString;
    std::ostringstream outStream;
    
    std::string sName;
    std::string sNote;
    std::string sColorName;
    
    GeographicLib::GeoCoords gCoord1;
    GeographicLib::GeoCoords gCoord2;
    
    cRGBColor cColor;
    
    int nDiagramCount = Sector2RhinoApplication::getSectorOld().countDiagrams(SID_hack) + Sector2RhinoApplication::getSectorOld().countDiagrams(STAR) + Sector2RhinoApplication::getSectorOld().countDiagrams(GEO);
    int nLineCount = 0;
    
    DiagramType eDiagramType;
    DiagramType eRefDiagramType;
    
    std::string sHeader;
    
    for (int k = 0; k<3; k++) {
        if (k == 0) {
            sHeader = "[SID]";
            eRefDiagramType = SID_hack;
        }
        if (k == 1) {
            sHeader = "[STAR]";
            eRefDiagramType = STAR;
        }
        if (k == 2) {
            sHeader = "[GEO]";
            eRefDiagramType = GEO;
        }
        
        outStream << sHeader << endl;
        
        for (int i = 0; i < nDiagramCount; i++) {
            Sector2RhinoApplication::getSectorOld().returnDiagramInfo(i, sName, eDiagramType);
            if (eDiagramType != eRefDiagramType) continue;
            
            //write Diagram name Bradley_9________
            if (eDiagramType != GEO) outStream << sName;
            if (eDiagramType != GEO) outStream << returnSpaces((int)sName.size());
            Sector2RhinoApplication::getSectorOld().returnDiagramLine(i, 0, &gCoord1, &gCoord2, &cColor, &sName, &sNote);
            
            //write coordinates
            outStream << ConvertCoord2SCT(gCoord1) << " " << ConvertCoord2SCT(gCoord2) << " ";
            
            //Write colors
            if (Sector2RhinoApplication::getSectorOld().lookupColor(cColor, sColorName)) {
                if (sColorName == "NULLColor") {}
                else outStream << sColorName;
            }
            else outStream << RGB2SCTcolor(cColor);
            
            //write notes
            if (sNote != "") outStream << " ;" << sNote;
            outStream << "" << endl;
            
            nLineCount = Sector2RhinoApplication::getSectorOld().countLinesInDiagram(i);
            
            for (int j = 1; j < nLineCount; j++) {
                Sector2RhinoApplication::getSectorOld().returnDiagramLine(i, j, &gCoord1, &gCoord2, &cColor, &sName, &sNote);
                
                //SIDs and STARS get spaces
                if (eDiagramType != GEO) outStream << returnSpaces(0);
                
                //Write coordinates
                outStream << ConvertCoord2SCT(gCoord1) << " " << ConvertCoord2SCT(gCoord2) << " ";
                
                //Write colors
                if (Sector2RhinoApplication::getSectorOld().lookupColor(cColor, sColorName)) {
                    if (sColorName == "NULLColor") {}
                    else outStream << sColorName;
                }
                else outStream << RGB2SCTcolor(cColor);
                //Write Notes
                if (sNote != "") outStream << " ;" << sNote;
                outStream << "\n";
            }
            outStream << "" << endl;
        }
        
        outStream << writeSeperator();
        
    }
    
    sOutString = outStream.str();
    return sOutString;
}

std::string writeRegions () {
    Sector2RhinoApplication::getSectorOld().sortRegionsByDepth();
    
    std::string sOutString;
    std::ostringstream outStream;
    
    cRGBColor cColor;
    std::string sNote;
    std::string sColorName;
    int nLineCount;
    
    GeographicLib::GeoCoords gCoord;
    double dDepth;
    
    outStream << "[REGIONS]" << endl;
    outStream << "; Note: The depth= value controls the Z coordinate once the SCT file is imported into Rhino, and conversly keeps the right drawing order once saved again." << endl << "" << endl;
    
    for (int i = 0; i < Sector2RhinoApplication::getSectorOld().returnRegionCount(); i++) {
        Sector2RhinoApplication::getSectorOld().returnRegionInfo(i, nLineCount, cColor, sNote);
        //gCoord = Sector.returnRegionCoord(i, 0);
        Sector2RhinoApplication::getSectorOld().returnRegionCoord(i, 0, gCoord, dDepth);
        // Write color
        if (Sector2RhinoApplication::getSectorOld().lookupColor(cColor, sColorName)) {
            outStream << sColorName;
            outStream << returnSpaces((int)sColorName.length());
            
        }
        else {
            outStream << RGB2SCTcolor(cColor);
            std::string tempString = int_2_string(RGB2SCTcolor(cColor));
            outStream << returnSpaces((int)tempString.length());
        }
        
        // Write coords
        outStream << ConvertCoord2SCT(gCoord);
        
        //Write Notes
        if ((dDepth != 0.0) || (sNote != "")) outStream << " ;";
        if (dDepth != 0.0) outStream << " depth=" << dbl_2_string(dDepth);
        if (sNote != "") outStream << " " << sNote;
        outStream << "" << endl;
        
        for (int j = 1; j < nLineCount; j++) {
            gCoord = Sector2RhinoApplication::getSectorOld().returnRegionCoord(i, j);
            outStream << returnSpaces(0);
            outStream << ConvertCoord2SCT(gCoord);
            outStream << "" << endl;
        }
        
    }
    
    sOutString = outStream.str();
    return sOutString;
}

std::string writeSeperator () {
    std::string sReturn;
    sReturn = "\n;================================================================================= \n\n";
    return sReturn;
}

std::string returnSpaces (int nWordSize) {
    //std::string outString = "";
    if (nWordSize >= 26) return "";
    size_t i = 26 - nWordSize;
    string outString(i, ' ');
    return outString;
}


