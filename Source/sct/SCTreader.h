//
//  SCTreader.h
//  fstream
//
//  Created by James Mazur on 12/8/12.
//  Copyright (c) 2012 James Mazur. All rights reserved.
//

#ifndef __fstream__SCTreader__
#define __fstream__SCTreader__

#include "SCTobj.h"
#include "helpers/GeoConvert.h"
#include <fstream>
#include <vector>
#include <string>
#include <iostream>
#include <GeographicLib/GeoCoords.hpp>

using namespace std;

void readSecFile (std::string file);

void processHeader (std::string sCurrentFileLine, std::ifstream &fStreamIn);

void processInfo (std::ifstream &fStreamIn, std::string &sReturnCurrentLine);

bool processDefine (std::string sCurrentFileLine);

void processAirport (std::ifstream &fStreamIn, std::string &sReturnCurrentLine);

void processRunway (std::ifstream &fStreamIn, std::string &sReturnCurrentLine);

void processFIXES (std::ifstream &fStreamIn, std::string &sReturnCurrentLine);

void processVOR (std::ifstream &fStreamIn, std::string &sReturnCurrentLine);

void processNDB (std::ifstream &fStreamIn, std::string &sReturnCurrentLine);

void processARTCCorAirway (std::ifstream &fStreamIn, BoundryOrAir eBoundryOrAirType, std::string &sReturnCurrentLine);

void processSIDSTAR (std::ifstream &fStreamIn, DiagramType eDiagramType, std::string &sReturnCurrentLine);

void processGEO (std::ifstream &fStreamIn, std::string &sReturnCurrentLine);

void processREGIONS (std::ifstream &fStreamIn, std::string &sReturnCurrentLine);

bool processSIDSTARLine ( std::string inString, std::string &sName, GeographicLib::GeoCoords &coord1, GeographicLib::GeoCoords &coord2, cRGBColor &outColor, std::string &outNotes );

bool extractFixes ( std::string inString, std::string &outName, GeographicLib::GeoCoords &outCoord, std::string &sNote);

bool extractAirport ( std::string inString, std::string &sOutName, std::string &sOutFreq,GeographicLib::GeoCoords &outCoord1, std::string &sClass, std::string &sNote);

bool extractRunway ( std::string inString, std::string &sOutName1, std::string &sOutName2, std::string &sOutHeading1, std::string &sOutHeading2, GeographicLib::GeoCoords &outCoord1, GeographicLib::GeoCoords &outCoord2, std::string &sNote);

bool extractARTCCorAirway ( std::string inString, std::string &sOutName, GeographicLib::GeoCoords &outCoord1, GeographicLib::GeoCoords &outCoord2, std::string &sNote);

bool extractVORNDB ( std::string inString, std::string &sOutName, std::string &sOutFreq,GeographicLib::GeoCoords &outCoord1, std::string &sNote);

bool extractGEO ( std::string inString, GeographicLib::GeoCoords &outCoord1, GeographicLib::GeoCoords &outCoord2, cRGBColor &outColor, std::string &sNote);

bool extractREGION (std::string inString, bool &hasColor, GeographicLib::GeoCoords &outCoord1, double &dDepth, cRGBColor &outColor, std::string &sNote );

bool hasData (std::string inString);

bool isHeader (std::string sInputLine);

bool quadCoordTest (std::vector<std::string> vStrings, int i);

// Helper Functions

//vector<string> split(string sInput, char delim, int rep);

void delEndLine (std::string &sInputString);

void printSeperator(std::string sInput);

bool isCoord (std::string &inString);

bool isFix (std::string sInString);

bool isFix (std::string sInString, GeographicLib::GeoCoords &rCoord);

bool isEmptyChar (char inChar);

//std::string int_2_string (int n);

#endif /* defined(__fstream__SCTreader__) */
