//
//  SCTwriter.h
//  fstream
//
//  Created by James Mazur on 12/24/12.
//  Copyright (c) 2012 James Mazur. All rights reserved.
//

#ifndef __fstream__SCTwriter__
#define __fstream__SCTwriter__

#include <iostream>
#include <fstream>
#include <sstream>
#include "SCTobj.h"
#include "SCTreader.h"

// Evil Globals
//extern SCTman Sector;

void writeSCTFile (std::string sFileName);
std::string writeInfo ();
std::string writeDefineColors ();
std::string writeAirports ();
std::string writeRunways ();
std::string writeVORs ();
std::string writeNDBs ();
std::string writeFixes ();
std::string writeAirways_ARTCC ();
std::string writeSIDs_STARs_GEO ();
std::string writeRegions ();

std::string writeSeperator ();
std::string returnSpaces (int nWordSize);

#endif /* defined(__fstream__SCTwriter__) */
