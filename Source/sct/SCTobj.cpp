//
//  SCTobj.cpp
//  fstream
//
//  Created by James Mazur on 12/4/12.
//  Copyright (c) 2012 James Mazur. All rights reserved.
//

#include "SCTobj.h"


// Section Coordinates

cRGBColor::cRGBColor () {
    r = 0;
    g = 0;
    b = 0;
}

cRGBColor::cRGBColor (int red, int green, int blue) {
    r = red;
    g = green;
    b = blue;
}

void cRGBColor::set(int red, int green, int blue) {
    r = red;
    g = green;
    b = blue;
}

bool cRGBColor::operator==(const cRGBColor &color) {
    if (r == color.r &&
        g == color.g &&
        b == color.b
        ) {
        return true;
    }
    return false;
}

// Sector line class

void cSCTline::addLine(GeographicLib::GeoCoords *inCoord1, GeographicLib::GeoCoords *inCoord2, cRGBColor *inColor, std::string *inNote) {
    gCoord1 = *inCoord1;
    gCoord2 = *inCoord2;
    cColor = *inColor;
    sNote = *inNote;
}

void cSCTline::returnLine(GeographicLib::GeoCoords *rCoord1, GeographicLib::GeoCoords *rCoord2, cRGBColor *rColor, std::string *rNote) {
    *rCoord1 = gCoord1;
    *rCoord2 = gCoord2;
    *rColor = cColor;
    *rNote = sNote;
}

std::string cBoundryOrAir::getName() {
    return sName;
}

void cBoundryOrAir::setName(std::string name) {
    sName = name;
}

void cBoundryOrAir::addLine(GeographicLib::GeoCoords inCoord1,GeographicLib::GeoCoords inCoord2, cRGBColor inColor, std::string inNote) {
    cSCTline cEmptyLine;
    vLines.push_back(cEmptyLine);
    vLines.back().addLine(&inCoord1, &inCoord2, &inColor, &inNote);
}

void cBoundryOrAir::returnLine (int index, GeographicLib::GeoCoords *rCoord1, GeographicLib::GeoCoords *rCoord2, cRGBColor *rColor, std::string *rNote) {
    
    vLines.at(index).returnLine(rCoord1, rCoord2, rColor, rNote);
    
}

int cBoundryOrAir::countLines() {
    return (int) vLines.size();
}

// Sector Section

void cSCTDiagrams::addLine(GeographicLib::GeoCoords *inCoord1, GeographicLib::GeoCoords *inCoord2, cRGBColor *inColor, std::string *inNote) {
    cSCTline *cEmptyLine;
    cEmptyLine = new cSCTline;
    vDiagramLines.push_back(*cEmptyLine);
    vDiagramLines.back().addLine(inCoord1, inCoord2, inColor, inNote);
    delete cEmptyLine;
}

void cSCTDiagrams::returnLine (int index, GeographicLib::GeoCoords *rCoord1, GeographicLib::GeoCoords *rCoord2, cRGBColor *rColor, std::string *rNote) {
    
    vDiagramLines.at(index).returnLine(rCoord1, rCoord2, rColor, rNote);
    
}

void cSCTDiagrams::setName(std::string name) {
    sDiagramName = name;
}

void cSCTDiagrams::setType(DiagramType inDiagramType) {
    eDiagramType = inDiagramType;
}

std::string cSCTDiagrams::getName() {
    return sDiagramName;
}

DiagramType cSCTDiagrams::getType() {
    return eDiagramType;
}

int cSCTDiagrams::countLines() {
    return (int) vDiagramLines.size();
}

// Sector Manager

SCTman::SCTman () {
    dRegionZdepth = 0.0;
    setNULLcolor(50, 50, 50);
    dRegionZdepthIncrement = 0.2;
}

void SCTman::addInfoLine(std::string sInputLine) {
    vInfo.push_back(sInputLine);
}

std::string SCTman::returnInfoLine(int nLineIndex) {
    return vInfo.at(nLineIndex);
}

int SCTman::countInfoLines() {
    return (int) vInfo.size();
}

void SCTman::addDiagramLine (GeographicLib::GeoCoords *inCoord1, GeographicLib::GeoCoords *inCoord2, std::string *sName, cRGBColor *inColor, DiagramType *inDiagramType, std::string *sNoteIn) {
    
    // New sector
    if (vDiagrams.size() == 0) {
        cSCTDiagrams cEmptyDiagram;
        vDiagrams.push_back(cEmptyDiagram);
        vDiagrams.back().addLine(inCoord1, inCoord2, inColor, sNoteIn);
        vDiagrams.back().setName(*sName);
        vDiagrams.back().setType(*inDiagramType);
        
        //return;
    }
    else {
        // Check if new sector name
        for (int i = 0; i < vDiagrams.size(); i++) {
            if (vDiagrams.at(i).getName().compare(*sName) == 0) {
                vDiagrams.at(i).addLine(inCoord1, inCoord2, inColor, sNoteIn);
                vDiagrams.at(i).setType(*inDiagramType);
                return;
            }
        }
        
        // New Sector
        cSCTDiagrams cEmptyDiagram;
        vDiagrams.push_back(cEmptyDiagram);
        vDiagrams.back().addLine(inCoord1, inCoord2, inColor, sNoteIn);
        vDiagrams.back().setName(*sName);
        vDiagrams.back().setType(*inDiagramType);
        
    }
    
    //std::cout << "========================================================" << std::endl;
    //if (inDiagramType == SID) std::cout << "SID Diagram ";
    //if (inDiagramType == STAR) std::cout << "STAR Diagram ";
    //if (inDiagramType == GEO) std::cout << "GEO Diagram ";
    //std::cout << "[addDiagramLine] adding Line: " << sName << " @ " << inCoord1.DMSRepresentation(2, 0, '.') << " - " << inCoord2.DMSRepresentation(2, 0, '.') << " (" << inColor.r << "," << inColor.g << "," << inColor.b << ")";
    //if (sNoteIn != "") std::cout << ", Note: " << sNoteIn;
    //std::cout << std::endl;
    
}
 
void SCTman::returnDiagramLine (int diagramIndex, int lineIndex, GeographicLib::GeoCoords *rCoord1, GeographicLib::GeoCoords *rCoord2, cRGBColor *rColor, std::string *rName, std::string *rNote) {
    
    vDiagrams.at(diagramIndex).returnLine(lineIndex, rCoord1, rCoord2, rColor, rNote);
    *rName = vDiagrams.at(diagramIndex).getName();
}

int SCTman::countDiagrams(DiagramType type) {
    int count = 0;
    for (int i = 0; i < vDiagrams.size(); i++) {
        if (vDiagrams.at(i).getType() == type ) {
            count++;
        }
    }
    return count;
}

int SCTman::countLinesInDiagram (int diagramIndex) {
    return vDiagrams.at(diagramIndex).countLines();
}

void SCTman::returnDiagramInfo (int diagramIndex, std::string &sDiagramName, DiagramType &eDiagramType, int &nDiagramLineCount) {
    eDiagramType = vDiagrams.at(diagramIndex).getType();
    sDiagramName = vDiagrams.at(diagramIndex).getName();
    nDiagramLineCount = vDiagrams.at(diagramIndex).countLines();
}

void SCTman::returnDiagramInfo (int diagramIndex, std::string &sDiagramName, DiagramType &eDiagramType) {
    eDiagramType = vDiagrams.at(diagramIndex).getType();
    sDiagramName = vDiagrams.at(diagramIndex).getName();
}

void SCTman::returnDiagramInfo (int diagramIndex, std::string &sDiagramName) {
    sDiagramName = vDiagrams.at(diagramIndex).getName();
}

void SCTman::addDefinedColor (std::string sName, cRGBColor color) {
    if (sName.at(sName.size()-1) == '\r') {
        sName.erase(sName.size()-1);
    }
    
    if (vDefinedColors.size() == 0) {
        cDefinedColor emptyDefineColor;
        emptyDefineColor.sName  = sName;
        emptyDefineColor.cColor = color;
        vDefinedColors.push_back(emptyDefineColor);
        //std::cout << "[addDefinedColor] Starting color vector with: " << sName << " (" << color.r << "," << color.g << "," << color.b << ")" << std::endl;
        return;
    }
    else {
        for (int i = 0; i < vDefinedColors.size(); i++) {
            if (vDefinedColors.at(i).sName == sName) {
                std::cout << "[addDefinedColor] Color already exists: " << sName << " (" << color.r << "," << color.g << "," << color.b << ")" << std::endl;
                return;
            }
        }
    }
    
    cDefinedColor emptyDefineColor;
    emptyDefineColor.sName = sName;
    emptyDefineColor.cColor = color;
    vDefinedColors.push_back(emptyDefineColor);
    //std::cout << "[addDefinedColor] Adding new color: " << sName << " (" << color.r << "," << color.g << "," << color.b << ")" << std::endl;
}

cRGBColor SCTman::returnDefinedColor (std::string sDefinedName) {
    for (int i = 0; i < vDefinedColors.size(); i++) {
        if (vDefinedColors.at(i).sName == sDefinedName) {
            //std::cout << "Color found:  " << sDefinedName << std::endl;
            return vDefinedColors.at(i).cColor;
        }
    }
    //std::cout << "[ReturnDefinedColor] Color not found: " << sDefinedName << std::endl;
    cRGBColor returnNULLColor;
    
    if (vDefinedColors.at(0).sName != "NULLColor") {
        setNULLcolor(1, 2, 3);
    }
    
    return vDefinedColors.at(0).cColor; //0 index should be NULLColor
}

cRGBColor SCTman::returnDefaultColor () {
    if (vDefinedColors.at(0).sName != "NULLColor") {
        setNULLcolor(1, 2, 3);
    }
    
    return vDefinedColors.at(0).cColor; //0 index should be NULLColor
}

bool SCTman::returnDefinedColor(int nIndex, std::string &sName, cRGBColor &rColor) {
    if (nIndex+1 > vDefinedColors.size()) return false;
    sName = vDefinedColors.at(nIndex).sName;
    rColor = vDefinedColors.at(nIndex).cColor;
    return true;
}

int SCTman::countDefinedColor() {
    return (int) vDefinedColors.size();
}

void SCTman::setNULLcolor(int r, int g, int b) {
    cRGBColor cColor;
    cColor.r = r;
    cColor.g = g;
    cColor.b = b;
    cDefinedColor emptyDefineColor;
    emptyDefineColor.cColor = cColor;
    emptyDefineColor.sName = "NULLColor";
    std::vector<cDefinedColor>::iterator it = vDefinedColors.begin();
    vDefinedColors.insert(it, emptyDefineColor);
}

bool SCTman::lookupColor(cRGBColor cColor, std::string &sName) {
    //Could 
    for (int i = 0; i < vDefinedColors.size(); i++) {
        if (vDefinedColors.at(i).cColor == cColor) {
            sName = vDefinedColors.at(i).sName;
            return true;
        }
    }
    
    return false;
}


// Fixes Section

void SCTman::addFix(std::string sFixNameIn, GeographicLib::GeoCoords inCoord, std::string sNoteIn) {
    
    if (mFixes.find(sFixNameIn) != mFixes.end()) {
        std::cout << "[addFix] Fix already exists: " << sFixNameIn << std::endl;
        return;
    }
    
    cFix emptyFix;
    emptyFix.gCoord = inCoord;
    emptyFix.sNote = sNoteIn;
    
    mFixes[sFixNameIn]=emptyFix;
    
    //std::cout << "[addFix] adding Fix: " << sFixNameIn << " @ " << inCoord.DMSRepresentation(2, 0, '.');
    //if (sNoteIn != "") std::cout << ", Note: " << sNoteIn;
    //std::cout << std::endl;
    
    return;
    
}

bool SCTman::returnFix(std::string sFixName, GeographicLib::GeoCoords &rCoord) {

    std::map<std::string, cFix>::iterator it;
    it = mFixes.find(sFixName);
    
    if (it != mFixes.end()) {
        rCoord = it->second.gCoord;
        return true;
    }
    
    return false;
}
 
bool SCTman::returnFix(int nIndex, std::string &rFixName, GeographicLib::GeoCoords &rCoord, std::string &rNote) {

    if (nIndex > mFixes.size()-1) return false;
    
    std::map<std::string, cFix>::iterator it;
    it = mFixes.begin();
    std::advance(it, nIndex);
    
    rFixName = it->first;
    rCoord = it ->second.gCoord;
    rNote = it->second.sNote;
    
    return true;
    
}

bool SCTman::fixExists (std::string sName) {
    if (mFixes.find(sName) != mFixes.end()) {
        return true;
    }
    return false;
}
int SCTman::countFixes() {
    //return (int) vFixes.size();
    return (int) mFixes.size();
}

// VOR & NDB Section

void SCTman::addVORNDB (RadioNav eNavType, std::string sVORNameIn, std::string sFreqIn, GeographicLib::GeoCoords inCoord, std::string sNoteIn) {
    std::string sNavType;
    
    std::map<std::string, cVORNDB> *VORNDBpnt;
    if (eNavType == VOR_old) {
        VORNDBpnt = &mVORs;
        sNavType = "VOR";
    }
    else if (eNavType == NDB_old) {
        VORNDBpnt = &mNDBs;
        sNavType = "NDB";
    }
    else return;
    
    if (VORNDBpnt->find(sVORNameIn) != VORNDBpnt->end()) {
        std::cout << "[add" << sNavType << "] VOR/NDB already exists: " << sVORNameIn << std::endl;
    }
    
    cVORNDB emptyVORNDB;
    emptyVORNDB.sFreq = sFreqIn;
    emptyVORNDB.gCoord = inCoord;
    emptyVORNDB.sNote = sNoteIn;
    
    (*VORNDBpnt)[sVORNameIn] = emptyVORNDB;

    //std::cout << "[add" << sNavType << "] adding " << sNavType << ": " << sVORNameIn << " @ " << inCoord.DMSRepresentation(2, 0, '.');
    //if (sNoteIn != "") std::cout << ", Note: " << sNoteIn;
    //std::cout << std::endl;

    return;
}

bool SCTman::returnVORNDB(std::string sVORNDBName, RadioNav &eNavType, std::string &sFreqOut, GeographicLib::GeoCoords &outCoord) {
    
    std::map<std::string, cVORNDB>::iterator it;
    it = mVORs.find(sVORNDBName);
    if (it != mVORs.end()) {
        eNavType = VOR_old;
        sFreqOut = it->second.sFreq;
        outCoord = it->second.gCoord;
        return true;
    }
    it = mNDBs.find(sVORNDBName);
    if (it != mNDBs.end()) {
        eNavType = NDB_old;
        sFreqOut = it->second.sFreq;
        outCoord = it->second.gCoord;
        return true;
    }
    
    return false;
    
}

bool SCTman::returnVOR(int nIndex, std::string &sVORNDBName, std::string &sFreqOut, GeographicLib::GeoCoords &outCoord, std::string &sOutNote) {
    if (nIndex > mVORs.size()-1) return false;
    
    std::map<std::string, cVORNDB>::iterator it;
    it = mVORs.begin();
    std::advance(it, nIndex);
    
    sVORNDBName = it->first;
    sFreqOut = it->second.sFreq;
    outCoord = it->second.gCoord;
    sOutNote = it->second.sNote;
    return true;
    
}

bool SCTman::returnNDB(int nIndex, std::string &sVORNDBName, std::string &sFreqOut, GeographicLib::GeoCoords &outCoord, std::string &sOutNote) {
    if (nIndex > mNDBs.size()-1) return false;
    
    std::map<std::string, cVORNDB>::iterator it;
    it = mNDBs.begin();
    std::advance(it, nIndex);
    
    sVORNDBName = it->first;
    sFreqOut = it->second.sFreq;
    outCoord = it->second.gCoord;
    sOutNote = it->second.sNote;
    return true;
}

bool SCTman::VORExists(std::string sName) {
    if (mVORs.find(sName) != mVORs.end()) {
        return true;
    }
    return false;
}

bool SCTman::NDBExists(std::string sName) {
    if (mNDBs.find(sName) != mNDBs.end()) {
        return true;
    }
    return false;
}

int SCTman::countVORs() {
    return (int) mVORs.size();
}

int SCTman::countNDBs() {
    return (int) mNDBs.size();
}

// Airport Section

void SCTman::addAirport (std::string sAirportNameIn, std::string sFreqIn, GeographicLib::GeoCoords inCoord, std::string sClassIn, std::string sNoteIn) {
    
    if (mAirports.find(sAirportNameIn) != mAirports.end()) {
        return;
    }
    
    cAirport tempAirport;
    tempAirport.sFreq = sFreqIn;
    tempAirport.gCoord = inCoord;
    tempAirport.sClass = sClassIn;
    tempAirport.sNote = sNoteIn;
    
    mAirports[sAirportNameIn]=tempAirport;
    
    //std::cout << "[addAirport] adding Airport: " << sAirportNameIn << " (" << sFreqIn << ") " << "(" << sClassIn << ")" << " @ " << inCoord.DMSRepresentation(2, 0, '.');
    //if (sNoteIn != "") std::cout << ", Note: " << sNoteIn;
    //std::cout << std::endl;
    
    return;    
}


bool SCTman::returnAirport (std::string sAirportNameIn, std::string &sFreqOut, GeographicLib::GeoCoords &outCoord, std::string &sClassOut) {
    
    std::map<std::string, cAirport>::iterator it;
    it = mAirports.find(sAirportNameIn);
    
    if (it != mAirports.end()) {
        sFreqOut = it->second.sFreq;
        outCoord = it->second.gCoord;
        sClassOut = it->second.sClass;
        return true;
    }
    
    return false;
}
 
bool SCTman::returnAirport (int nIndex, std::string &sAirportNameOut, std::string &sFreqOut, GeographicLib::GeoCoords &outCoord, std::string &sClassOut, std::string &sNoteOut) {
    
    if (nIndex > mAirports.size()-1) return false;
    
    std::map<std::string, cAirport>::iterator it;
    it = mAirports.begin();
    std::advance(it, nIndex);
    
    sAirportNameOut = it->first;
    sFreqOut = it->second.sFreq;
    outCoord = it->second.gCoord;
    sClassOut = it->second.sClass;
    sNoteOut = it->second.sNote;
    
    return true;
}

bool SCTman::airportExists (const std::string *sName) {
    if (mAirports.find(*sName) != mAirports.end()) {
        return true;
    }
    return false;
}

int SCTman::countAirports () {
    return (int) mAirports.size();
}

// Region Section

void SCTman::startRegion (cRGBColor cRegionColor, GeographicLib::GeoCoords gCoord, std::string sNoteIn) {
    cRegion cNewRegion;
    cNewRegion.cColor = cRegionColor;
    cNewRegion.vCoord.push_back(gCoord);
    cNewRegion.sNote = sNoteIn;
    cNewRegion.dZdepth = dRegionZdepth - dRegionZdepthIncrement;
    dRegionZdepth -= dRegionZdepthIncrement;
    vRegions.push_back(cNewRegion);
    
    //std::cout << "========================================================" << std::endl;
    //std::cout << "[startRegion] Color: (" << cRegionColor.r << "," << cRegionColor.g << "," << cRegionColor.b << ") @ " << gCoord.DMSRepresentation(2, 0, '.');
    //if (sNoteIn != "") std::cout << ", Note: " << sNoteIn;
    //std::cout << std::endl;
}

void SCTman::startRegion (cRGBColor cRegionColor, GeographicLib::GeoCoords gCoord, double dZdepth, std::string sNoteIn) {
    cRegion cNewRegion;
    cNewRegion.cColor = cRegionColor;
    cNewRegion.vCoord.push_back(gCoord);
    cNewRegion.sNote = sNoteIn;
    cNewRegion.dZdepth = dZdepth;
    if (dZdepth < dRegionZdepth) dRegionZdepth = dZdepth;
    vRegions.push_back(cNewRegion);
    
    //std::cout << "========================================================" << std::endl;
    //std::cout << "[startRegion] Color: (" << cRegionColor.r << "," << cRegionColor.g << "," << cRegionColor.b << ") @ " << gCoord.DMSRepresentation(2, 0, '.');
    //if (sNoteIn != "") std::cout << ", Note: " << sNoteIn;
    //std::cout << std::endl;
}

bool SCTman::addToRegion(GeographicLib::GeoCoords gCoord) {
    vRegions.back().vCoord.push_back(gCoord);
    //std::cout << "[addToRegion] Coordinate: " << gCoord.DMSRepresentation(2, 0, '.') << std::endl;
    return true;
}

int SCTman::returnRegionCount () {
    return (int) vRegions.size();
}

void SCTman::returnRegionInfo (int nIndex, int &nCount, cRGBColor &rColor, std::string &sNoteOut) {
    nCount = (int) vRegions.at(nIndex).vCoord.size();
    rColor = vRegions.at(nIndex).cColor;
    sNoteOut = vRegions.at(nIndex).sNote;
}

GeographicLib::GeoCoords SCTman::returnRegionCoord (int nIndexRegion, int nIndexRegCoord) {
    return vRegions.at(nIndexRegion).vCoord.at(nIndexRegCoord);
}

bool SCTman::returnRegionCoord(int nIndexRegion, int nIndexRegCoord, GeographicLib::GeoCoords &gCoord, double &zDepth) {
    
    if (nIndexRegion >= vRegions.size()) {
        return false;
    }
    if (nIndexRegCoord >= vRegions.at(nIndexRegion).vCoord.size()) {
        return false;
    }
    gCoord = vRegions.at(nIndexRegion).vCoord.at(nIndexRegCoord);
    zDepth = vRegions.at(nIndexRegion).dZdepth;
    return true;
}

bool SCTman::sortRegionsByDepth() {
    for (int i = 0; i < vRegions.size(); i++) {
        //double dTemp = vRegions.at(i).dZdepth;
        for (int j = i + 1; j < vRegions.size(); j++) {
            if (vRegions.at(j).dZdepth < vRegions.at(i).dZdepth) {
                std::swap(vRegions[j], vRegions[i]);
            }
        }
    }
    
    return true;
}


void SCTman::addRunway (std::string sName1, std::string sName2, std::string sHeading1, std::string sHeading2, GeographicLib::GeoCoords gCoord1, GeographicLib::GeoCoords gCoord2, std::string sNote) {
    
    if (vRunways.size() == 0) {
        cRunway newRunway;
        newRunway.sName1 = sName1;
        newRunway.sName2 = sName2;
        newRunway.sHeading1 = sHeading1;
        newRunway.sHeading2 = sHeading2;
        newRunway.gCoord1 = gCoord1;
        newRunway.gCoord2 = gCoord2;
        newRunway.sNote = sNote;
        vRunways.push_back(newRunway);
    }
    
    else {
        /*
        for (int i = 0; i < vRunways.size(); i++) {
            if (
                (vRunways.at(i).sName1 == sName1) &&
                (vRunways.at(i).sName2 == sName2) &&
                (vRunways.at(i).sHeading1 == sHeading1) &&
                (vRunways.at(i).sHeading1 == sHeading2) &&
                (vRunways.at(i).gCoord1 == gCoord1)
                ) {
                std::cout << "[addAirport] duplicate Airport: " << sAirportNameIn << std::endl;
                return;
            }
        }
        */
        cRunway newRunway;
        newRunway.sName1 = sName1;
        newRunway.sName2 = sName2;
        newRunway.sHeading1 = sHeading1;
        newRunway.sHeading2 = sHeading2;
        newRunway.gCoord1 = gCoord1;
        newRunway.gCoord2 = gCoord2;
        newRunway.sNote = sNote;
        vRunways.push_back(newRunway);
    }
    
    //std::cout << "[addRunway] adding Runway: " << sName1 << "/" << sName2 << " " << sHeading1 << "/" << sHeading2 << " @ " << gCoord1.DMSRepresentation(2, 0, '.') << "/" << gCoord2.DMSRepresentation(2, 0, '.');
    //if (sNote != "") std::cout << ", Note: " << sNote;
    //std::cout << std::endl;
    
    return;
}

bool SCTman::returnRunway (int nIndex, std::string &sName1, std::string &sName2, std::string &sHeading1, std::string &sHeading2, GeographicLib::GeoCoords &gCoord1, GeographicLib::GeoCoords &gCoord2, std::string &sNote) {
    
    if (vRunways.size() < nIndex+1) {
        return false;
    }
    
    sName1 = vRunways.at(nIndex).sName1;
    sName2 = vRunways.at(nIndex).sName2;
    sHeading1 = vRunways.at(nIndex).sHeading1;
    sHeading2 = vRunways.at(nIndex).sHeading2;
    gCoord1 = vRunways.at(nIndex).gCoord1;
    gCoord2 = vRunways.at(nIndex).gCoord2;
    sNote = vRunways.at(nIndex).sNote;
    
    return true;
}

int SCTman::countRunways() {
    return (int) vRunways.size();
}

void SCTman::addARTCCorAirway (BoundryOrAir eBoundryOrAir, std::string sName, GeographicLib::GeoCoords gCoord1, GeographicLib::GeoCoords gCoord2, std::string sNote) {
    
    //TODO: add support of color here
    cRGBColor tempColor;
    
    std::vector<cBoundryOrAir> *pntVBoundryORAir;
    
    if (eBoundryOrAir == ARTCC) pntVBoundryORAir = &vARTCC;
    else if (eBoundryOrAir == ARTCC_HIGH) pntVBoundryORAir = &vARTCC_High;
    else if (eBoundryOrAir == ARTCC_LOW) pntVBoundryORAir = &vARTCC_Low;
    else if (eBoundryOrAir == LOW_AIRWAY) pntVBoundryORAir = &vAirway_Low;
    else if (eBoundryOrAir == HIGH_AIRWAY) pntVBoundryORAir = &vAirway_High;
    else return;
    
    for (int i = 0; i < pntVBoundryORAir->size(); i++) {
        if (pntVBoundryORAir->at(i).getName() == sName) {
            pntVBoundryORAir->at(i).addLine(gCoord1, gCoord2, tempColor, sNote);
            return;
        }
    }
    
    cBoundryOrAir newBoundryOrAir;
    pntVBoundryORAir->push_back(newBoundryOrAir);
    pntVBoundryORAir->back().setName(sName);
    pntVBoundryORAir->back().addLine(gCoord1, gCoord2, tempColor, sNote);
    
    
}

bool SCTman::returnARTCCorAirway (int nIndex, int nLineIndex, BoundryOrAir eBoundryOrAir, std::string &sName, GeographicLib::GeoCoords &gCoord1, GeographicLib::GeoCoords &gCoord2, std::string &sNote) {
    std::vector<cBoundryOrAir> *pntVBoundryORAir;
    
    if (eBoundryOrAir == ARTCC) pntVBoundryORAir = &vARTCC;
    else if (eBoundryOrAir == ARTCC_HIGH) pntVBoundryORAir = &vARTCC_High;
    else if (eBoundryOrAir == ARTCC_LOW) pntVBoundryORAir = &vARTCC_Low;
    else if (eBoundryOrAir == LOW_AIRWAY) pntVBoundryORAir = &vAirway_Low;
    else if (eBoundryOrAir == HIGH_AIRWAY) pntVBoundryORAir = &vAirway_High;
    else return false;
    
    cRGBColor tempcolor; //TODO: Insert color
    sName = pntVBoundryORAir->at(nIndex).getName();
    pntVBoundryORAir->at(nIndex).returnLine(nLineIndex, &gCoord1, &gCoord2, &tempcolor, &sNote);
    
    return true;
}

int SCTman::countARTCCorAirwary(BoundryOrAir eBoundryOrAir) {
    std::vector<cBoundryOrAir> *pntVBoundryORAir;
    
    if (eBoundryOrAir == ARTCC) pntVBoundryORAir = &vARTCC;
    else if (eBoundryOrAir == ARTCC_HIGH) pntVBoundryORAir = &vARTCC_High;
    else if (eBoundryOrAir == ARTCC_LOW) pntVBoundryORAir = &vARTCC_Low;
    else if (eBoundryOrAir == LOW_AIRWAY) pntVBoundryORAir = &vAirway_Low;
    else if (eBoundryOrAir == HIGH_AIRWAY) pntVBoundryORAir = &vAirway_High;
    else return false;
    
    return (int) pntVBoundryORAir->size();
}

int SCTman::countARTCCorAirwaryLines(BoundryOrAir eBoundryOrAir, int nIndex) {
    std::vector<cBoundryOrAir> *pntVBoundryORAir;
    
    if (eBoundryOrAir == ARTCC) pntVBoundryORAir = &vARTCC;
    else if (eBoundryOrAir == ARTCC_HIGH) pntVBoundryORAir = &vARTCC_High;
    else if (eBoundryOrAir == ARTCC_LOW) pntVBoundryORAir = &vARTCC_Low;
    else if (eBoundryOrAir == LOW_AIRWAY) pntVBoundryORAir = &vAirway_Low;
    else if (eBoundryOrAir == HIGH_AIRWAY) pntVBoundryORAir = &vAirway_High;
    
    return pntVBoundryORAir->at(nIndex).countLines();
}

void SCTman::size() {
    size_t SCTlineSize;
    SCTlineSize = (sizeof(GeographicLib::GeoCoords)*2) + sizeof(cRGBColor) + sizeof(std::string);
    size_t DiagramSize;
    DiagramSize = SCTlineSize * vDiagrams.size();
    std::cout << DiagramSize << std::endl;
}
