/*
  ==============================================================================

    CoordinatesString.h
    Created: 7 Feb 2015 10:05:36am
    Author:  Jim Mazur

  ==============================================================================
*/

#ifndef COORDINATESSTRING_H_INCLUDED
#define COORDINATESSTRING_H_INCLUDED


#include "../../JuceLibraryCode/JuceHeader.h"
#include "../model/SectorTypes.h"

//==============================================================================
/**
 CoordinatesString class
 
 A class for working with coordinates. Has provisions for verifying that the
 string is a valid Sector formatted coordinate. And also converts between the
 Coordinates class type and Sector formated coordinate strings.
 
 */
class CoordinatesString
{
public:
    /** Creates an invalid / empty CoordinatesString. */
    CoordinatesString();
    
    /** Destructor . */
    ~CoordinatesString();
    
    /** Creates a CoordinatesString from a sector formatted string.
     
     The String must beformatted according to the standard sector formatting
     and contain two coordinates, with latitude first and longitude second:
     
     N041.56.18.800 W072.40.57.000
     */
    CoordinatesString (const String& sectorFormattedCoordinatePair);
    
    /** Creates a CoordinatesString from latitude and longitude string.
     
     The strings must beformatted according to the standard sector formatting:
     N041.56.18.800 W072.40.57.000
     */
    CoordinatesString (const String& latitude, const String& longitude);
    
    /** Creates a CoordinatesString from Coordinate */
    CoordinatesString (const sector::Coordinates& coordinates);
    
    /** Returns true if the CoordinatesString contains valid coordinates */
    bool isValid() const;
    
    /** Returns coordinates as Coordinates type */
    sector::Coordinates toCoordinates();
    
    /** Returns coordinates as a sector formatted string */
    String toSectorCoordinates();
    
    /** Returns latitude coordinate as a sector formatted string */
    String toSectorLatitude();
    
    /** Returns longitude coordinate as a sector formatted string */
    String toSectorLongitude();
    
    /** Checks that the passed coordinate is either a latitude or longitude format  */
    static bool isValidLatitudeOrLongitude (const String& sectorFormattedCoordinate);
    
    /** Checks if the string is a pair of sector formated latitude and longitude */
    static bool isValidCoordinate (const String& coordinates);
    
    /** Checks that the string is a sector formatted latitude  */
    static bool isValidLatitudeCoordinate (const String& sectorFormattedCoordinate);
    
    /** Checks that the string is a sector formatted longitude  */
    static bool isValideLongitudeCoordinate (const String& sectorFormattedCoordinate);
    
private:
    /** Checks that the string has correct amount of periods and contains only numbers  */
    static bool hasNumbersAndPeriods (const String& sectorFormattedCoordinate);
    
    bool isValidGeoCoordinates(const sector::Coordinates& coords) const;
    
    /** Internally converts the Coordinates to a sector formated Coordinate  */
    void createStringFromGeoCoordinates();
    
    void createGeoCoordinatesFromString();
    
    Result tryUsingVRC_Conversion();
    
    double DMSToDec (const String& coord);
    
    ScopedPointer<String> SectorCoordinates;
    ScopedPointer<sector::Coordinates> GeoCoordinates;
};

#endif  // COORDINATESSTRING_H_INCLUDED
