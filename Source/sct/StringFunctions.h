/*
  ==============================================================================

    StringFunctions.h
    Created: 31 Dec 2014 11:48:26am
    Author:  Jim Mazur

  ==============================================================================
*/

#ifndef STRINGFUNCTIONS_H_INCLUDED
#define STRINGFUNCTIONS_H_INCLUDED

#include "JuceHeader.h"

bool isNumber(const String& string);

bool isNotNumber (const String& string);

StringArray getWhitespaceSeparatedItems (const String& sourceString);

StringArray getCommaPeriodSeperatedItems (const String& sourceString);


#endif  // STRINGFUNCTIONS_H_INCLUDED
