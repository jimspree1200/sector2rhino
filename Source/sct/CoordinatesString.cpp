/*
  ==============================================================================

    CoordinatesString.cpp
    Created: 7 Feb 2015 10:05:36am
    Author:  Jim Mazur

  ==============================================================================
*/

#include "CoordinatesString.h"
#include "StringFunctions.h"
#include "../Logger/SectorLogger.h"
#include <GeographicLib/DMS.hpp>
#include <exception>
#include <string>

using namespace sector;

//==============================================================================
// CoordinatesString
//==============================================================================

CoordinatesString::CoordinatesString()
{}

CoordinatesString::~CoordinatesString()
{
    SectorCoordinates = nullptr;
    GeoCoordinates = nullptr;
}

CoordinatesString::CoordinatesString (const String& coordinates)
{
    if (!isValidCoordinate(coordinates))
        return;
    
    this->SectorCoordinates = new String(coordinates);
}

CoordinatesString::CoordinatesString (const String& latitude, const String& longitude)
{
    if (!isValidCoordinate(latitude + " " + longitude))
        return;
    
    this->SectorCoordinates = new String(latitude + " " + longitude);
}

CoordinatesString::CoordinatesString (const Coordinates& coordinates)
{
    if (!isValidGeoCoordinates(coordinates))
        return;
    
    this->GeoCoordinates = new Coordinates(coordinates);
}

//==============================================================================

bool CoordinatesString::isValid() const
{
    if (SectorCoordinates != nullptr || GeoCoordinates != nullptr)
        return true;
    
    return false;
}

//==============================================================================

Coordinates CoordinatesString::toCoordinates()
{
    if (!isValid())
        return Coordinates();
    
    if (GeoCoordinates == nullptr)
        createGeoCoordinatesFromString();
    
    return *GeoCoordinates;
}

String CoordinatesString::toSectorCoordinates()
{
    if (!isValid())
        return String();
    
    if (SectorCoordinates == nullptr)
        createStringFromGeoCoordinates();
    
    return *SectorCoordinates;
}

String CoordinatesString::toSectorLatitude()
{
    if (!isValid())
        return String();
    
    StringArray coordinates = getWhitespaceSeparatedItems(toSectorCoordinates());
    
    return coordinates[0];
}

String CoordinatesString::toSectorLongitude()
{
    if (!isValid())
        return String();
    
    StringArray coordinates = getWhitespaceSeparatedItems(toSectorCoordinates());
    
    return coordinates[1];
}

//==============================================================================

bool CoordinatesString::isValidLatitudeOrLongitude(const String &sectorFormattedCoordinate)
{
    if (isValidLatitudeCoordinate(sectorFormattedCoordinate) || isValideLongitudeCoordinate(sectorFormattedCoordinate))
        return true;
    
    return false;
}

bool CoordinatesString::isValidCoordinate(const String& coordinate)
{
    StringArray words = getWhitespaceSeparatedItems(coordinate);
    
    // Must be two words
    if (words.size() != 2)
        return false;
    
    if (!isValidLatitudeCoordinate(words[0]))
        return false;
    
    if (!isValideLongitudeCoordinate(words[1]))
        return false;
    
    return true;
}

bool CoordinatesString::isValidLatitudeCoordinate(const String &sectorFormattedCoordinate)
{
    String coordinate = sectorFormattedCoordinate.trim();
    
    if (!coordinate.startsWithChar('N') &&
        !coordinate.startsWithChar('n') &&
        !coordinate.startsWithChar('S') &&
        !coordinate.startsWithChar('s'))
        return false;
    
    if (!hasNumbersAndPeriods(coordinate))
        return false;
    
    return true;
}

bool CoordinatesString::isValideLongitudeCoordinate(const String &sectorFormattedCoordinate)
{
    String coordinate = sectorFormattedCoordinate.trim();
    
    if (!coordinate.startsWithChar('E') &&
        !coordinate.startsWithChar('e') &&
        !coordinate.startsWithChar('W') &&
        !coordinate.startsWithChar('w'))
        return false;
    
    if (!hasNumbersAndPeriods(coordinate))
        return false;
    
    return true;
}

//==============================================================================

bool CoordinatesString::hasNumbersAndPeriods(const String &sectorFormattedCoordinate)
{
    //    // Should be 14 characters
    //    if (sectorFormattedCoordinate.length() != 14)
    //        return false;
    
    // Check for 3 periods or commas
    // N042.14.25.000
    //     ^  ^  ^
    
    String coordinate = sectorFormattedCoordinate.substring(1);
    
    // Make sure we are dealing with only numbers
    if (isNotNumber(coordinate.removeCharacters(".,")))
        return false;
    
    StringArray DMS;
    DMS.addTokens(coordinate, ".,", StringRef());
    
    if (DMS.size() < 3)
        return false;
    
    
    int characterIndex = 0;
    
    for (int i = 0; i < 3; i++)
    {
        characterIndex = sectorFormattedCoordinate.indexOfAnyOf(".,", characterIndex);
        
        if (characterIndex == -1)
            return false;
    }
    
    // Strip away N,E,S,W and remove '.' then check for only numbers
    if (isNotNumber(sectorFormattedCoordinate.substring(1).removeCharacters(".,")))
        return false;
    
    return true;
}

bool CoordinatesString::isValidGeoCoordinates(const Coordinates &coord) const
{
    if (coord.Latitude() == GeographicLib::Math::NaN())
        return false;
    
    return true;
}

void CoordinatesString::createStringFromGeoCoordinates()
{
    // Should only be called when a valid GeoCoord has been stored
    jassert(GeoCoordinates != nullptr);
    jassert(SectorCoordinates == nullptr);
    
    if (!GeoCoordinates->isValid())
    {
        GeoCoordinates->Reset(0.0, 0.0);
    }
    
    // Geographic Lib will always return a fixed amount of decimal places for
    // latitude and longitude. Therefore we must adjust for the appropriate 0's for
    // the SCT file. For instance:
    //
    // 1d3'1.1"N 1d1'1.001"W - passed to GeoCoord function
    // 01d03'01.100"N 001d01'01.001"W - returned from DMSRepresentation(2)
    // 01.03.01.100N 001.01.01.001W - returned from DMSRepresentation(2, false, '.')
    //
    // Therefore we assume always to recieve:
    // nn.nn.nn.nnnC nnn.nn.nn.nnnC
    // 0123456789012345678901234567
    //
    // For nice SCT formatting we would like the string to convert to:
    // C0nn.nn.nn.nnn Cnnn.nn.nn.nnn
    
    // nn.nn.nn.nnnC nnn.nn.nn.nnnC
    // 0123456789012345678901234567
    String coordinates(GeoCoordinates->DMSRepresentation(2, false, '.'));
    
    String cardinal(juce::String::charToString(coordinates[12]));
    
    // nn.nn.nn.nnnC nnn.nn.nn.nnnC
    //             ^
    // 0123456789012345678901234567
    coordinates = coordinates.removeCharacters(cardinal);
    
    // nn.nn.nn.nnn nnn.nn.nn.nnnC
    // 0123456789012345678901234567
    
    
    coordinates = cardinal + coordinates;
    
    // Cnn.nn.nn.nnn nnn.nn.nn.nnnC
    // 0123456789012345678901234567
    coordinates = coordinates.substring(0, 1) + "0" + coordinates.substring(1);
    
    // C0nn.nn.nn.nnn nnn.nn.nn.nnnC
    //                             ^
    // 01234567890123456789012345678
    cardinal = juce::String::charToString(coordinates[28]);
    
    coordinates = coordinates.dropLastCharacters(1);
    // C0nn.nn.nn.nnn nnn.nn.nn.nnn
    // 0123456789012345678901234567
    
    coordinates = coordinates.substring(0, 15) + cardinal + coordinates.substring(15);
    // C0nn.nn.nn.nnn Cnnn.nn.nn.nnn
    // 0123456789012345678901234567
    
    SectorCoordinates = new String(coordinates);
}

//==============================================================================

void CoordinatesString::createGeoCoordinatesFromString()
{
    jassert(SectorCoordinates != nullptr);
    jassert(GeoCoordinates == nullptr);
    
    String sectorcoords = SectorCoordinates->replaceCharacter(',', '.');
    
    StringArray coordinates = getWhitespaceSeparatedItems(sectorcoords);
    
    for (int i = 0; i < 2; i++) {
        // W074.42.52.128 - SCT formated coordinate
        String* coordinate = &coordinates.getReference(i);
        
        // W074d42.52.128
        *coordinate = coordinate->replaceSection(coordinate->indexOf("."), 1, "d");
        
        // W074d42'52.128
        *coordinate = coordinate->replaceSection(coordinate->indexOf("."), 1, "'");
        
        // W074d42'52.128"
        *coordinate = *coordinate + "\"";
        
        // W074d42'52.128"W
        // coord.append(coord, 1); // <-doesn't work in VS2015!
        *coordinate = *coordinate + (*coordinate)[0]; // <- work around
        
        // 074d42'52.128"W - Ready to be parsed by GeographicLib
        *coordinate = coordinate->substring(1);
    }
    
    try
    {
        GeoCoordinates = new Coordinates(coordinates.joinIntoString(" ").toStdString());
    }
    catch (const std::exception& e)
    {
        DBG("Not valid coordinate: " + *SectorCoordinates);
        DBG(e.what());
        
        SectorLogger::addError(*SectorCoordinates);
        SectorLogger::addError("Could not validate coordinate: " + String(e.what()));
        
        if (tryUsingVRC_Conversion().wasOk())
        {
            SectorLogger::addInfo("Here is the proper coordinate:");
            SectorLogger::addInfo(toSectorCoordinates());
        }
        else
            GeoCoordinates = new Coordinates(0.0, 0.0);
    }
    
}

Result CoordinatesString::tryUsingVRC_Conversion()
{
    StringArray coordinates = getWhitespaceSeparatedItems(*SectorCoordinates);
    
    double lat = DMSToDec(coordinates[0]);
    if (lat == 0.0)
        return Result::fail("Could not parse latitude coordinate: " + coordinates[0]);
    
    double lon = DMSToDec(coordinates[1]);
    if (lon == 0.0)
        return Result::fail("Could not parse longitude coordinate: " + coordinates[1]);
    
    try
    {
        GeoCoordinates = new Coordinates(lat, lon);
        // Update the SectorCoordinates string to follow the correct translation
        *SectorCoordinates = CoordinatesString(*GeoCoordinates).toSectorCoordinates();
    }
    catch (const std::exception& e)
    {
        return Result::fail(e.what());
    }
    
    return Result::ok();
}

double CoordinatesString::DMSToDec(const juce::String &coordinate)
{
    // Original source code from VRC used with permission from:
    // Ross Carlson / www.metacraft.com
    
    std::string coord = coordinate.toStdString();
    static std::string deg;
    static std::string min;
    static std::string sec;
    static std::string secWhole;
    static std::string secDec;
    
    // Make sure we got something.
    if (coord.empty()) return 0.0;
    
    // Find out which side of the axis we're on, then strip out the N, S, E or W.
    bool neg = false;
    if (coord.find_first_of("SWsw") != std::string::npos) neg = true;
    coord = coord.substr(1);
    
    // Split into degrees, minutes, seconds.
    std::string::size_type pt1 = 0;
    std::string::size_type pt2 = 0;
    std::string::size_type pt3 = 0;
    
    // Find each decimal-delimited part.
    pt1 = coord.find_first_of(".,");
    if (pt1 != std::string::npos) {
        deg = coord.substr(0, pt1);
    } else {
        return 0.0;
    }
    pt2 = coord.find_first_of(".,", pt1 + 1);
    if (pt2 != std::string::npos) {
        min = coord.substr(pt1 + 1, (pt2 - pt1) - 1);
    } else {
        return 0.0;
    }
    pt3 = coord.find_first_of(".,", pt2 + 1);
    if (pt3 != std::string::npos) {
        secWhole = coord.substr(pt2 + 1, (pt3 - pt2) - 1);
    } else {
        return 0.0;
    }
    if (pt3 < (coord.length() - 1)) {
        secDec = coord.substr(pt3 + 1);
    } else {
        return 0.0;
    }
    
    // Convert to numeric values.
    sec = secWhole + "." + secDec;
    int degrees = atoi(deg.c_str());
    int minutes = atoi(min.c_str());
    float seconds = atof(sec.c_str());
    
    // Do the math.
    double result = (double)degrees;
    result += (double)minutes / 60.0;
    result += (double)seconds / 3600.0;
    
    // Negate (if necessary) the value and return it.
    return neg ? (result * -1.0) : result;
}

//==============================================================================
// Unit Tests
//==============================================================================

class CoordinatesStringTest : public UnitTest
{
private:
    const GeographicLib::Math::real lat;
    const GeographicLib::Math::real lon;
    const String sectorFormatted;
    const String geolibFormatted;
    const Coordinates coordinate;
    
public:
    CoordinatesStringTest()
    :
    UnitTest ("CoordinatesString class tests"),
    lat(12.12345),
    lon(-21.65432),
    sectorFormatted("N012.07.24.420 W021.39.15.552"),
    geolibFormatted("12d07'24.420\"N 021d39'15.552\"W"),
    coordinate(geolibFormatted)
    {}
    
    void runTest()
    {
        try
        {
            GeographicLib::DMS::flag f;
            GeographicLib::DMS::Decode("S920d30'40.5\"", f);
            DBG("hi");
        }
        catch (const GeographicLib::GeographicErr &e)
        {
            DBG(e.what());
        }
        
        
        
        beginTest("Constructor tests");
        {
            CoordinatesString cs1(sectorFormatted);
            CoordinatesString cs2(coordinate);
        }
        
        beginTest("isValid tests");
        {
            expect(CoordinatesString("N044.38.00.290 W071.11.10.068").isValid());
            expect(!CoordinatesString().isValid());
            
            // isLatitude tests
            {
                expect(CoordinatesString::isValidLatitudeCoordinate("N044.38.00.290")); // Cardinal tests
                expect(CoordinatesString::isValidLatitudeCoordinate("S044.38.00.290")); // Cardinal tests
                expect(CoordinatesString::isValidLatitudeCoordinate("n044.38.00.290")); // Cardinal tests
                expect(CoordinatesString::isValidLatitudeCoordinate("N004.438.00.290")); // Cardinal tests
                
                expect(CoordinatesString::isValidLatitudeCoordinate("N4.3.0.2")); // Mising numbers
                
                expect(!CoordinatesString::isValidLatitudeCoordinate("W044.38.00.290")); // longitude not latitude
                expect(!CoordinatesString::isValidLatitudeCoordinate("N044.38.00.29x")); // letter!
            }
            {
                expect(CoordinatesString::isValideLongitudeCoordinate("W071.11.10.068"));
                expect(CoordinatesString::isValideLongitudeCoordinate("w071.11.10.068"));
                expect(CoordinatesString::isValideLongitudeCoordinate("W071.11.10.068"));
                expect(CoordinatesString::isValideLongitudeCoordinate("w071.11.10.068"));
            }
            
            {
                expect(CoordinatesString::isValidLatitudeOrLongitude("W071.11.10.068"));
                expect(CoordinatesString::isValidLatitudeOrLongitude("N071.11.10.068"));
            }
            
            {
                expect(CoordinatesString::isValidCoordinate("N044.38.00.290 W071.11.10.068"));
                expect(CoordinatesString::isValidCoordinate(" N044.38.00.290  W071.11.10.068 "));
                expect(!CoordinatesString::isValidCoordinate("N044.38.00.290 "));
            }
        }
        
        beginTest("Conversion Tests");
        {
            CoordinatesString cs1(sectorFormatted);
            expect(cs1.toCoordinates() == coordinate);
            
            CoordinatesString cs2((Coordinates(geolibFormatted)));
            expect(cs2.toSectorCoordinates() == sectorFormatted);
        }
        
        // getLatitude() test
        CoordinatesString latlon("N044.38.00.290 W071.11.10.068");
        expectEquals(latlon.toSectorLatitude(), String("N044.38.00.290"));
        
        // getLongitude() test
        expectEquals(latlon.toSectorLongitude(), String("W071.11.10.068"));
    }
};

static CoordinatesStringTest coordinatesStringTest;