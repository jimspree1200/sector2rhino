/*
  ==============================================================================

    SectorFileParser.cpp
    Created: 10 Jan 2015 9:53:48pm
    Author:  Jim Mazur

  ==============================================================================
*/

#include "SectorFileParser.h"
#include "Application.h"
#include "CoordinatesString.h"

using namespace sector;

SectorFileParser::SectorFileParser(File sector)
:
Thread("Sector Parser Thread"),
sector(&Sector2RhinoApplication::getSector()),
sectorFile(sector),
firstPass(true)
{
    streamTotalLength = -1;
    startThread();
}

SectorFileParser::SectorFileParser(File sector, sector::SectorContainer* sectorToAddTo)
:
Thread("Sector Parser Thread"),
sector(sectorToAddTo),
sectorFile(sector),
firstPass(true)
{
    streamTotalLength = -1;
    startThread();
}

//==============================================================================

SectorFileParser::~SectorFileParser()
{
    stopThread(1000);
}

//==============================================================================

void SectorFileParser::run()
{
    parseFile();
}

//==============================================================================

void SectorFileParser::setFile(File sector)
{
    sectorFile = sector;
}

//==============================================================================

File SectorFileParser::getFile()
{
    return sectorFile;
}

//==============================================================================

Result SectorFileParser::parseFile()
{
    if (!sectorFile.existsAsFile())
        return Result::fail(sectorFile.getFullPathName() + " is not a valid file");
    
    sectorFileStream = new FileInputStream(sectorFile);
    if (!sectorFileStream->openedOk())
        return Result::fail(sectorFile.getFullPathName() + " could not open");
    
    lineNumber = 0;
    
    parseNotes();
    
    // First pass
    SectorLogger::addInfo("==== FIRST PASS ====");
    while (!sectorFileStream->isExhausted() && !threadShouldExit())
    {
        parseHeader();
    }
    
    sectorFileStream->setPosition(0);
    firstPass = false;
    lineNumber = 0;
    
    SectorLogger::addInfo("==== SECOND PASS ====");
    
    while (!sectorFileStream->isExhausted() && !threadShouldExit())
    {
        parseHeader();
    }
    
    return Result::ok();
}

//==============================================================================

float SectorFileParser::getProgress()
{
    const ScopedLock sl (streamLock);
    
    if (streamTotalLength <= 0)
        streamTotalLength = sectorFileStream->getTotalLength();
    
    return (float) sectorFileStream->getPosition() / (float) streamTotalLength;
}

//==============================================================================

bool SectorFileParser::getNextLine()
{
    const ScopedLock sl (streamLock);
    
    if (threadShouldExit())
        return false;
    
    if (sectorFileStream->isExhausted())
    {
        SectorLogger::addInfo("End of File");
        return false;
    }
    
    lineNumber++;
    
    String lineRead = sectorFileStream->readNextLine();
    
    if (!CharPointer_ASCII::isValidString (lineRead.toUTF8(), std::numeric_limits<int>::max()))
    {
        SectorLogger::addError("Line: " + String(lineNumber) + " - Non UTF-8 Characters found (please resave as UTF-8 encoding in a text editor) to avoid this error");
        lineRead = String::empty;
    }
    
    if (sector != nullptr)
        currentLine = SectorString(lineRead, sector);
    else
        currentLine = SectorString(lineRead);
    
    if (currentLine == "                          N040.30.00.909 W096.60.15.999 N040.29.32.909 W096.58.40.999 ; W/E")
        DBG("Break here");
    
    streamPosition = sectorFileStream->getPosition();
    
    return true;
}


//==============================================================================

bool SectorFileParser::isDefineOrSectionHeader()
{
    if (currentLine.isNamedColour() || currentLine.isSectionHeader())
        return true;
    
    return false;
}

//==============================================================================

void SectorFileParser::parseAsNotesWhile (SectorStringBoolFunction function)
{
    notes.clear();
    
    std::function<bool()> func = std::bind(function, currentLine);
    
    while (!func())
    {
        notes += currentLine + newLine;
        getNextLine();
        
        func = std::bind(function, currentLine);
    }
}

//==============================================================================

template <typename T>
void SectorFileParser::parseSingleLineAndAddToSector(T, SectorStringIsFunction isFunction, SectorStringToFunction<T> getFunction, SectorAddFunction<T> addFunction)
{
    if (!getNextLine())
        return;
    
    std::function<bool()> is_T = std::bind(isFunction, std::ref(currentLine));
    
    String notes;
    
    while (!currentLine.isSectionHeader())
    {
        if (threadShouldExit())
            return;
        
        // Get preceeding notes
        notes.clear();
        while (currentLine.isWhitespaceOrComment())
        {
            notes += currentLine + newLine;
            if (!getNextLine())
                return;
        }
        
        if (currentLine.isSectionHeader())
        {
            return;
        }
        
        else if (is_T())
        {
            // Get the Type from the Sector String
            auto to_T = std::bind(getFunction, currentLine);
            T t = to_T();
            
            // Add notes
            if (notes.isNotEmpty())
                t.addPrecedingNotes(notes);

            // Add the line from the sector file used to create this object
            t.setLineFromSectorFile (currentLine);
            t.setLineNumberInSectorFile (lineNumber);
            
            // Add the Type to the Sector container
            auto add_T_toSector = std::bind(addFunction, sector, std::placeholders::_1);
            add_T_toSector(t);
        }
        
        else
        {
            SectorLogger::addError("Line " + String (lineNumber) + " :" + currentLine + newLine);
        }
        
        if (!getNextLine())
            return;
    }
}

void SectorFileParser::parseSIDorSTAR(const SectorString::SectionHeaderType& currentHeader)
{
    if (!getNextLine())
        return;
    
    SID_STAR_Container* container;
    if (currentHeader == SectorString::SectionHeaderType::SID)
        container = sector->getSIDs();
    else if (currentHeader == SectorString::SectionHeaderType::STAR)
        container = sector->getSTARs();
    else jassertfalse;
    
    String notes;
    
    while (!currentLine.isSectionHeader())
    {
        if (threadShouldExit())
            return;
        
        notes.clear();
        
        while (currentLine.isWhitespaceOrComment())
        {
            notes += currentLine + newLine;
            if (!getNextLine())
                return;
        }
        
        if (currentLine.isSectionHeader())
            return;
        
        if (currentLine.isNew_SID_STAR() || currentLine.is_SID_STAR_Line())
        {
            ColouredLine cl (currentLine.to_SID_Star_Line());
            
            if (notes.isNotEmpty())
                cl.addPrecedingNotes(notes);
            
            // Add the line from the sector file used to create this object
            cl.setLineFromSectorFile (currentLine);
            cl.setLineNumberInSectorFile (lineNumber);
            
            if (currentLine.isNew_SID_STAR())
                container->startNewDiagram(currentLine.getNew_SID_STAR_Name(), cl);
            else
                container->addLine(cl);
            
        
               // SectorLogger::addInfo("No named diagram to add this line to: " + currentLine);
        }
        
        else
            SectorLogger::addError("Line " + String (lineNumber) + " :" + currentLine + newLine);
        
        if (!getNextLine())
            return;
    }
}

void SectorFileParser::parseRegions()
{
    if (!getNextLine())
        return;
    
    Regions* regions = sector->getRegions();
    
    String notes;
    
    while (!currentLine.isSectionHeader())
    {
        if (threadShouldExit())
            return;
        
        notes.clear();
        
        while (currentLine.isWhitespaceOrComment())
        {
            notes += currentLine + newLine;
            if (!getNextLine())
                return;
        }
        
        if (currentLine.isSectionHeader())
            return;
        
        if (currentLine.isRegionCoordinate() || currentLine.isRegionStart())
        {
            if (currentLine.isRegionStart())
            {
                Region region;
                region.setNamedColour(currentLine.getRegionStartColour());
                region.add(currentLine.toRegionCoordinates());
                regions->add(region);
            }
            else
            {
                if (regions->size() == 0)
                {
                    SectorLogger::addError("Trying to add line while no colour has been set in region");
                }
                else
                {
                    Region* currentRegion = &regions->getReference(regions->size()-1);
                    currentRegion->add(currentLine.toRegionCoordinates());
                }
            }
        }
        else
            SectorLogger::addError("Line " + String (lineNumber) + " :" + currentLine + newLine);
        
        if (!getNextLine())
            return;
    }
}

//==============================================================================

void SectorFileParser::parseUntilNextHeader()
{
    if (!getNextLine())
        return;
    
    while (!currentLine.isSectionHeader())
    {
        if (!getNextLine())
            return;
    }
}

//==============================================================================

void SectorFileParser::parseNotes()
{
    String sectorNotes;
    
    // Process file up until section header or a colour define is found
    while (!isDefineOrSectionHeader())
    {
        sectorNotes += currentLine + newLine;
        getNextLine();
    }
    
    sector->setNotes(sectorNotes);
}

//==============================================================================

void SectorFileParser::parseInfo()
{
    sector::InfoBlock info;
    
    StringArray infoStrings;
    
    // Let's make sure there are no comments ahead of the actual data
    while (getNextLine())
    {
       if (!currentLine.isWhitespaceOrComment())
           break;
    }
    
    for (int i = 0; i < 9; i++)
    {
        if (isDefineOrSectionHeader() || currentLine.isWhitespaceOrComment())
        {
            SectorLogger::addError("Incomplete Sector [INFO] block");
            break;
        }
        infoStrings.add(currentLine);
        
        if (!getNextLine())
            return;
    }
    
    info.setName(infoStrings[0]);
    info.setDefaultCallsign(infoStrings[1]);
    info.setDefaultAirport(infoStrings[2]);
    info.setDefaultCenterPoint(CoordinatesString(infoStrings[3] + " " + infoStrings[4]).toCoordinates());
    info.setMilesPerDegreeLatitude(infoStrings[5].getDoubleValue());
    info.setMilesPerDegreeLongitude(infoStrings[6].getDoubleValue());
    info.setMagneticVariation(infoStrings[7].getDoubleValue());
    info.setSectorScale(infoStrings[8].getDoubleValue());
    
    sector->addInfo(info);
    
    return;
}

//==============================================================================

void SectorFileParser::parseHeader()
{
    if (currentLine.isNamedColour() && !isFirstPass())
    {
        //parseDefines();
        SectorLogger::addInfo("INFO: Adding define");
        
        SectorStringIsFunction isFunc = &SectorString::isNamedColour;
        SectorStringToFunction<NamedColour> toFunc = &SectorString::toNamedColour;
        SectorAddFunction<NamedColour> addFunc = &SectorContainer::add;
        
        parseSingleLineAndAddToSector(NamedColour(), isFunc, toFunc, addFunc);
        
        SectorLogger::addInfo("Found " + (String) sector->getAirportss()->size() + " Colours");
    }
    
    switch (currentLine.getHeaderType())
    {
        case SectorString::SectionHeaderType::Info:
        {
            if (isSecondPass())
            {
                SectorLogger::addInfo("INFO: Entering INFO Header");
                parseInfo();
                break;
            }
            else
                parseUntilNextHeader();
            
            break;
        }
            
        case SectorString::SectionHeaderType::Airport:
        {
            if (isFirstPass())
            {
                SectorLogger::addInfo("INFO: Entering Airport Header");
                
                SectorStringIsFunction isFunc = &SectorString::isAirport;
                SectorStringToFunction<Airport> toFunc = &SectorString::toAirport;
                SectorAddFunction<Airport> addFunc = &SectorContainer::addAirport;
                
                parseSingleLineAndAddToSector(Airport(), isFunc, toFunc, addFunc);
                
                SectorLogger::addInfo("Found " + (String) sector->getAirportss()->size() + " Airports");
                break;
            }
            else
                parseUntilNextHeader();
            
            break;
        }
            
        case SectorString::SectionHeaderType::Runway:
        {
            if (isSecondPass())
            {
                SectorLogger::addInfo("INFO: Entering Runway Header");
                
                SectorStringIsFunction isFunc = &SectorString::isRunway;
                SectorStringToFunction<Runway> toFunc = &SectorString::toRunway;
                SectorAddFunction<Runway> addFunc = &SectorContainer::addRunway;
                
                parseSingleLineAndAddToSector(Runway(), isFunc, toFunc, addFunc);
                
                SectorLogger::addInfo("Found " + (String) sector->getRunways()->size() + " Runways");
            }
            else
                parseUntilNextHeader();
            
            break;
        }
            
        case SectorString::SectionHeaderType::VOR:
        {
            if (isFirstPass())
            {
                SectorLogger::addInfo("INFO: Entering VOR Header");
                
                SectorStringIsFunction isFunc = &SectorString::isVOR;
                SectorStringToFunction<RadioNavAid> toFunc = &SectorString::toRadioNavAid;
                SectorAddFunction<RadioNavAid> addFunc = &SectorContainer::addVOR;
                
                parseSingleLineAndAddToSector(RadioNavAid(), isFunc, toFunc, addFunc);
                
                SectorLogger::addInfo("Found " + (String) sector->getVORs()->size() + " VORs");
                break;
            }
            else
                parseUntilNextHeader();
            
            break;
        }
            
        case SectorString::SectionHeaderType::NDB:
        {
            if (isFirstPass())
            {
                SectorLogger::addInfo("INFO: Entering NDB Header");
                
                SectorStringIsFunction isFunc = &SectorString::isNDB;
                SectorStringToFunction<RadioNavAid> toFunc = &SectorString::toRadioNavAid;
                SectorAddFunction<RadioNavAid> addFunc = &SectorContainer::addNDB;
                
                parseSingleLineAndAddToSector(RadioNavAid(), isFunc, toFunc, addFunc);
                
                SectorLogger::addInfo("Found " + (String) sector->getNDBs()->size() + " NDBs");
                break;
            }
            else
                parseUntilNextHeader();
            
            break;
        }
            
        case SectorString::SectionHeaderType::Fixes:
        {
            if (isFirstPass())
            {
                SectorLogger::addInfo("INFO: Entering Fixes Header");
                
                SectorStringIsFunction isFunc = &SectorString::isFix;
                SectorStringToFunction<NavAid> toFunc = &SectorString::toNavAid;
                SectorAddFunction<NavAid> addFunc = &SectorContainer::addFix;
                
                parseSingleLineAndAddToSector(NavAid(), isFunc, toFunc, addFunc);
                
                SectorLogger::addInfo("Found " + (String) sector->getFixes()->size() + " Fixes");
                break;
            }
            else
                parseUntilNextHeader();
            
            break;
        }
            
        case SectorString::SectionHeaderType::LowAirway:
        {
            if (isSecondPass())
            {
                SectorLogger::addInfo("INFO: Entering LowAirway Header");
                
                SectorStringIsFunction isFunc = &SectorString::isAirway;
                SectorStringToFunction<NamedLine> toFunc = &SectorString::toAirwayLine;
                SectorAddFunction<NamedLine> addFunc = &SectorContainer::addLowAirayLine;
                
                parseSingleLineAndAddToSector(NamedLine(), isFunc, toFunc, addFunc);
                
                SectorLogger::addInfo("Found " + (String) sector->getLowAirways()->size() + " low airways");
                break;
            }
            else
                parseUntilNextHeader();
            
            break;
        }
            
        case SectorString::SectionHeaderType::HighAirway:
        {
            if (isSecondPass())
            {
                SectorLogger::addInfo("INFO: Entering HighAirway Header");
                
                SectorStringIsFunction isFunc = &SectorString::isAirway;
                SectorStringToFunction<NamedLine> toFunc = &SectorString::toAirwayLine;
                SectorAddFunction<NamedLine> addFunc = &SectorContainer::addHighAirwayLine;
                
                parseSingleLineAndAddToSector(NamedLine(), isFunc, toFunc, addFunc);
                
                SectorLogger::addInfo("Found " + (String) sector->getHighAirways()->size() + " high airways");
                break;
            }
            else
                parseUntilNextHeader();
            
            break;
        }
            
        case SectorString::SectionHeaderType::ARTCC:
        {
            if (isSecondPass())
            {
                SectorLogger::addInfo("INFO: Entering ARTCC Header");
                
                SectorStringIsFunction isFunc = &SectorString::isArtcc;
                SectorStringToFunction<NamedLine> toFunc = &SectorString::toArtccLine;
                SectorAddFunction<NamedLine> addFunc = &SectorContainer::addArtccBoundaryLine;
                
                parseSingleLineAndAddToSector(NamedLine(), isFunc, toFunc, addFunc);
                
                SectorLogger::addInfo("Found " + (String) sector->getArtccBoundaries()->size() + " boundaries");                break;
            }
            else
                parseUntilNextHeader();
            
            break;
        }
            
        case SectorString::SectionHeaderType::ARTCC_High:
        {
            if (isSecondPass())
            {
                SectorLogger::addInfo("INFO: Entering ARTCC High Header");
                
                SectorStringIsFunction isFunc = &SectorString::isArtcc;
                SectorStringToFunction<NamedLine> toFunc = &SectorString::toArtccLine;
                SectorAddFunction<NamedLine> addFunc = &SectorContainer::addArtccHighBoundaryLine;
                
                parseSingleLineAndAddToSector(NamedLine(), isFunc, toFunc, addFunc);
                
                SectorLogger::addInfo("Found " + (String) sector->getArtccHighBoundaries()->size() + " high boundaries");
                break;
            }
            else
                parseUntilNextHeader();
            
            break;
        }
            
        case SectorString::SectionHeaderType::ARTCC_Low:
        {
            if (isSecondPass())
            {
                SectorLogger::addInfo("INFO: Entering ARTCC Low Header");
                
                SectorStringIsFunction isFunc = &SectorString::isArtcc;
                SectorStringToFunction<NamedLine> toFunc = &SectorString::toArtccLine;
                SectorAddFunction<NamedLine> addFunc = &SectorContainer::addArtccLowBoundaryLine;
                
                parseSingleLineAndAddToSector(NamedLine(), isFunc, toFunc, addFunc);
                
                SectorLogger::addInfo("Found " + (String) sector->getArtccLowBoundaries()->size() + " low boundaries");
                break;
            }
            else
                parseUntilNextHeader();
            
            break;
        }
            
        case SectorString::SectionHeaderType::SID:
        {
            if (isSecondPass())
            {
                SectorLogger::addInfo("INFO: Entering SID Header");
                parseSIDorSTAR(SectorString::SectionHeaderType::SID);
                SectorLogger::addInfo("Found " + (String) sector->getSIDs()->size() + " SID diagrams");
                break;
            }
            else
                parseUntilNextHeader();
            
            break;
        }
            
        case SectorString::SectionHeaderType::STAR:
        {
            if (isSecondPass())
            {
                SectorLogger::addInfo("INFO: Entering SID Header");
                parseSIDorSTAR(SectorString::SectionHeaderType::STAR);
                SectorLogger::addInfo("Found " + (String) sector->getSTARs()->size() + " STAR diagrams");
                break;
            }
            else
                parseUntilNextHeader();
            
            break;
        }
            
        case SectorString::SectionHeaderType::Geo:
        {
            if (isSecondPass())
            {
                SectorLogger::addInfo("INFO: Entering Geo Header");
                
                SectorStringIsFunction isFunc = &SectorString::isGeoLine;
                SectorStringToFunction<ColouredLine> toFunc = &SectorString::toGeoLine;
                SectorAddFunction<ColouredLine> addFunc = &SectorContainer::addGeoLine;
                
                parseSingleLineAndAddToSector(ColouredLine(), isFunc, toFunc, addFunc);
                
                SectorLogger::addInfo("Found " + (String) sector->getGeoLines()->size() + " Geo lines");
                break;
            }
            else
            {
                parseUntilNextHeader();
            }
        }
            
        case SectorString::SectionHeaderType::Regions:
        {
            if (isSecondPass())
            {
                SectorLogger::addInfo("INFO: Entering Regions Header");
                
                parseRegions();
                
                SectorLogger::addInfo("Found " + (String) sector->getRegions()->size() + " Regions");
                break;
            }
            else
                parseUntilNextHeader();
            
            break;
        }
            
        case SectorString::SectionHeaderType::Labels:
        {
            if (isSecondPass())
            {
                SectorLogger::addInfo("INFO: Entering Labels Header");
                
                SectorStringIsFunction isFunc = &SectorString::isLabel;
                SectorStringToFunction<sector::Label> toFunc = &SectorString::toLabel;
                SectorAddFunction<sector::Label> addFunc = &SectorContainer::addLabel;
                
                parseSingleLineAndAddToSector(sector::Label(), isFunc, toFunc, addFunc);
                
                SectorLogger::addInfo("Found " + (String) sector->getLabels()->size() + " Labels");
                break;
            }
            else
                parseUntilNextHeader();
            
            break;
        }
            
        default:
        {
            if (!getNextLine())
                return;
            break;
        }
    }
}

bool SectorFileParser::isFirstPass() const
{
    return firstPass;
}

bool SectorFileParser::isSecondPass() const
{
    return !isFirstPass();
}

// From IntroJucer code:
// Copyright (c) 2013 - Raw Material Software Ltd.

String SectorFileParser::stringLiteral (const String& text, int maxLineLength)
{
    if (text.isEmpty())
        return "String::empty";
    
    StringArray lines;
    
    {
        String::CharPointerType t (text.getCharPointer());
        bool finished = t.isEmpty();
        
        while (! finished)
        {
            for (String::CharPointerType startOfLine (t);;)
            {
                switch (t.getAndAdvance())
                {
                    case 0:     finished = true; break;
                    case '\n':  break;
                    case '\r':  if (*t == '\n') ++t; break;
                    default:    continue;
                }
                
                lines.add (String (startOfLine, t));
                break;
            }
        }
    }
    
    if (maxLineLength > 0)
    {
        for (int i = 0; i < lines.size(); ++i)
        {
            String& line = lines.getReference (i);
            
            if (line.length() > maxLineLength)
            {
                const String start (line.substring (0, maxLineLength));
                const String end (line.substring (maxLineLength));
                line = start;
                lines.insert (i + 1, end);
            }
        }
    }
    
    for (int i = 0; i < lines.size(); ++i)
        lines.getReference(i) = CppTokeniserFunctions::addEscapeChars (lines.getReference(i));
    
    lines.removeEmptyStrings();
    
    for (int i = 0; i < lines.size(); ++i)
        lines.getReference(i) = "\"" + lines.getReference(i) + "\"";
    
    String result (lines.joinIntoString (newLine));
    
    if (! CharPointer_ASCII::isValidString (text.toUTF8(), std::numeric_limits<int>::max()))
        result = "CharPointer_UTF8 (" + result + ")";
    
    return result;
}

class SectorFileParserUnitTests : public UnitTest
{
private:
    File tempSectorFile;
public:
    SectorFileParserUnitTests() : UnitTest("SectorFileParser class tests") {}
    
    void initialise()
    {
        tempSectorFile = File::getSpecialLocation(File::userDesktopDirectory).getChildFile("TempSector.sct");
        
        if (tempSectorFile.existsAsFile())
            tempSectorFile.deleteFile();
        
        {
            // Copy the binary data into a string
            String sctString ((const char*) BinaryData::ZMP_CTR_Test);
            
            // Write the temporary file
            FileOutputStream fo (tempSectorFile);
            fo.writeString(sctString);
        }
        
    }
    
    void shutdown()
    {
        if (tempSectorFile.existsAsFile())
            tempSectorFile.deleteFile();
    }
    
    void runTest()
    {
        sector::SectorContainer sector;
        SectorFileParser parser(tempSectorFile, &sector);
		parser.waitForThreadToExit(-1);
    }
    
};

static SectorFileParserUnitTests sectorFileParserUnitTests;
