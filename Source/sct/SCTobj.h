//
//  SCTobj.h
//  fstream
//
//  Created by James Mazur on 12/4/12.
//  Copyright (c) 2012 James Mazur. All rights reserved.
//

#ifndef __fstream__SCTobj__
#define __fstream__SCTobj__

#include <iostream>
#include <string>
#include <vector>
#include <map>
#include <GeographicLib/GeoCoords.hpp>

enum SCTtype {
    geo,
    diagram
};

enum DiagramType {
    SID_hack = 1,
    STAR = 2,
    GEO = 4,
    };

enum RadioNav {
    VOR_old = 1,
    NDB_old = 2,
};

enum BoundryOrAir {
    ARTCC = 1,
    ARTCC_HIGH = 2,
    ARTCC_LOW = 3,
    LOW_AIRWAY = 4,
    HIGH_AIRWAY = 5,
};

class cRGBColor {
public:
    cRGBColor();
    cRGBColor(int r, int g, int b);
    void set(int red, int green, int blue);
    bool operator == (const cRGBColor &);
    int r;
    int b;
    int g;
};

struct cFix {
    GeographicLib::GeoCoords gCoord;
    std::string sNote;
};

struct cVORNDB {
    GeographicLib::GeoCoords gCoord;
    std::string sFreq;
    std::string sNote;
};

struct cAirport {
    GeographicLib::GeoCoords gCoord;
    std::string sFreq;
    std::string sClass;
    std::string sNote;
};

struct cRunway {
    GeographicLib::GeoCoords gCoord1;
    GeographicLib::GeoCoords gCoord2;
    std::string sName1;
    std::string sName2;
    std::string sHeading1;
    std::string sHeading2;
    std::string sNote;
};

struct cDefinedColor {
    std::string sName;
    cRGBColor cColor;
};

struct cRegion {
    std::vector<GeographicLib::GeoCoords> vCoord;
    cRGBColor cColor;
    std::string sNote;
    double dZdepth;
};

class cSCTline {
    GeographicLib::GeoCoords gCoord1;
    GeographicLib::GeoCoords gCoord2;
    cRGBColor cColor;
    std::string sNote;
    
public:
    void addLine(GeographicLib::GeoCoords *inCoord1,GeographicLib::GeoCoords *inCoord2, cRGBColor *inColor, std::string *inNote);
    void returnLine(GeographicLib::GeoCoords *rCoord1, GeographicLib::GeoCoords *rCoord2, cRGBColor *rColor, std::string *rNote);
};

class cBoundryOrAir {
    std::vector<cSCTline> vLines;
    std::string sName;
    std::string sNote;
public:
    std::string getName();
    void setName(std::string name);
    void addLine(GeographicLib::GeoCoords inCoord1,GeographicLib::GeoCoords inCoord2, cRGBColor inColor, std::string inNote);
    void returnLine (int index, GeographicLib::GeoCoords *rCoord1, GeographicLib::GeoCoords *rCoord2, cRGBColor *rColor, std::string *rNote);
    int countLines ();
};

class cSCTDiagrams {
    std::vector<cSCTline> vDiagramLines;
    std::string sDiagramName;
    DiagramType eDiagramType;
public:
    void addLine(GeographicLib::GeoCoords *inCoord1, GeographicLib::GeoCoords *inCoord2, cRGBColor *inColor, std::string *inNote);
    void returnLine (int index, GeographicLib::GeoCoords *rCoord1, GeographicLib::GeoCoords *rCoord2, cRGBColor *rColor, std::string *rNote);
    void setName(std::string name);
    std::string getName();
    void setType(DiagramType inDiagramType);
    DiagramType getType ();
    int countLines ();
};

class SCTman {
    std::vector<cSCTDiagrams> vDiagrams;
    std::map<std::string, cAirport> mAirports;
    std::vector<cDefinedColor> vDefinedColors;
    std::vector<std::string> vInfo;
    std::map<std::string, cFix> mFixes;
    std::map<std::string, cVORNDB> mVORs;
    std::map<std::string, cVORNDB> mNDBs;
    std::vector<cRegion> vRegions;
    std::vector<cRunway> vRunways;
    std::vector<cBoundryOrAir> vARTCC;
    std::vector<cBoundryOrAir> vARTCC_High;
    std::vector<cBoundryOrAir> vARTCC_Low;
    std::vector<cBoundryOrAir> vAirway_High;
    std::vector<cBoundryOrAir> vAirway_Low;
    
    double dRegionZdepth;
    double dRegionZdepthIncrement;
    
public:
    SCTman();
    void addInfoLine (std::string sInputLine);
    std::string returnInfoLine (int nLineIndex);
    int countInfoLines ();
    
    void addDiagramLine (GeographicLib::GeoCoords *inCoord1, GeographicLib::GeoCoords *inCoord2, std::string *sName, cRGBColor *inColor, DiagramType *inDiagramType, std::string *sNoteIn);
    void returnDiagramLine (int diagramIndex, int lineIndex, GeographicLib::GeoCoords *rCoord1, GeographicLib::GeoCoords *rCoord2, cRGBColor *rColor, std::string *rName, std::string *rNote);
    
    int countLinesInDiagram (int diagramIndex);

    void returnDiagramInfo (int diagramIndex, std::string &sDiagramName, DiagramType &eDiagramType, int &nDiagramLineCount);
    void returnDiagramInfo (int diagramIndex, std::string &sDiagramName, DiagramType &eDiagramType);
    void returnDiagramInfo (int diagramIndex, std::string &sDiagramName);
    int countDiagrams (DiagramType type);
    
    void addDefinedColor (std::string sName, cRGBColor color);
    cRGBColor returnDefinedColor (std::string sDefinedName);
    bool returnDefinedColor (int nIndex, std::string &sName, cRGBColor &rColor);
    int countDefinedColor ();
    bool lookupColor (cRGBColor cColor, std::string &sName);
    void setNULLcolor (int r, int g, int b);
    cRGBColor returnDefaultColor();
    
    void setInfo (std::string sName, std::string sDefaultCallsign, std::string sDefaultAirport, std::string sDefaultLat, std::string sDefaultLong, int nNMperDegLat, int nNMperDegLon, int nMagVaration, int nSecScale);

    void addFix (std::string sFixNameIn, GeographicLib::GeoCoords inCoordLat, std::string sNoteIn);
    bool returnFix(std::string sFixName, GeographicLib::GeoCoords &rCoord);
    bool returnFix(int nIndex, std::string &rFixName, GeographicLib::GeoCoords &rCoord, std::string &rNote);
    bool fixExists(std::string sName);
    int countFixes();
    
    void addVORNDB (RadioNav eNavType, std::string sVORNameIn, std::string sFreqIn, GeographicLib::GeoCoords inCoord, std::string sNoteIn);
    bool returnVORNDB(std::string sVORNDBName, RadioNav &eNavType, std::string &sFreqOut, GeographicLib::GeoCoords &outCoord);
    bool returnVOR(int nIndex, std::string &sVORNDBName, std::string &sFreqOut, GeographicLib::GeoCoords &outCoord, std::string &sOutNote);
    bool returnNDB(int nIndex, std::string &sVORNDBName, std::string &sFreqOut, GeographicLib::GeoCoords &outCoord, std::string &sOutNote);
    bool VORExists(std::string sName);
    bool NDBExists(std::string sName);
    int countVORs();
    int countNDBs();
    
    void addAirport (std::string sAirportNameIn, std::string sFreqIn, GeographicLib::GeoCoords inCoord, std::string sClassIn, std::string sNoteIn);
    bool returnAirport (std::string sAirportNameIn, std::string &sFreqOut, GeographicLib::GeoCoords &outCoord, std::string &sClassOut);
    bool returnAirport (int nIndex, std::string &sFreqOut, std::string &sAirportNameOut, GeographicLib::GeoCoords &outCoord, std::string &sClassOut, std::string &sNoteOut);
    bool airportExists (const std::string *sName);
    int countAirports ();
    
    void startRegion (cRGBColor cRegionColor, GeographicLib::GeoCoords gCoord, double dDepth, std::string sNoteIn);
    void startRegion (cRGBColor cRegionColor, GeographicLib::GeoCoords gCoord, std::string sNoteIn);
    bool addToRegion (GeographicLib::GeoCoords gCoord);

    int returnRegionCount ();
    void returnRegionInfo (int nIndex, int &nCount, cRGBColor &rColor, std::string &sNoteOut);
    GeographicLib::GeoCoords returnRegionCoord (int nIndexRegion, int nIndexRegCoord);
    bool returnRegionCoord(int nIndexRegion, int nIndexRegCoord, GeographicLib::GeoCoords &gCoord, double &zDepth);
    void swapRegions (std::vector<cRegion> *region1, std::vector<cRegion> *region2);
    bool sortRegionsByDepth ();
    
    void addRunway (std::string sName1, std::string sName2, std::string sHeading1, std::string sHeading2, GeographicLib::GeoCoords gCoord1, GeographicLib::GeoCoords gCoord2, std::string sNote);
    bool returnRunway (int nIndex, std::string &sName1, std::string &sName2, std::string &sHeading1, std::string &sHeading2, GeographicLib::GeoCoords &gCoord1, GeographicLib::GeoCoords &gCoord2, std::string &sNote);
    int countRunways ();
    
    void addARTCCorAirway (BoundryOrAir eBoundryOrAir, std::string sName, GeographicLib::GeoCoords gCoord1, GeographicLib::GeoCoords gCoord2, std::string sNote);
    bool returnARTCCorAirway (int nIndex, int nLineIndex, BoundryOrAir eBoundryOrAir, std::string &sName, GeographicLib::GeoCoords &gCoord1, GeographicLib::GeoCoords &gCoord2, std::string &sNote);
    int countARTCCorAirwary (BoundryOrAir eBoundryOrAir);
    int countARTCCorAirwaryLines (BoundryOrAir eBoundryOrAir, int nIndex);
    void size();

};


#endif /* defined(__fstream__SCTobj__) */
