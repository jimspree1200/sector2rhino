//
//  SCTreader.cpp
//  fstream
//
//  Created by James Mazur on 12/8/12.
//  Copyright (c) 2012 James Mazur. All rights reserved.
//

#include "SCTreader.h"
#include "Application.h"


using namespace std;

//extern SCTman Sector;

int nCurrentFileLineNumber = 0;
int nNext10 = 10;

void incrementLineNumber () {
    nCurrentFileLineNumber++;
    if (nCurrentFileLineNumber < nNext10) {
        return;
    }
    std::cout << "." << std::flush;
    nNext10 += 10;
}

void readSecFile (std::string file) {
    
    //open file
    std::ifstream secFile(file.c_str(), std::ifstream::in);
    
    std::string sCurrentFileLine;
   
    std::cout << "\nStarting processing of SCT file \n";
    
    while(std::getline(secFile, sCurrentFileLine)) {
        incrementLineNumber();
        processDefine(sCurrentFileLine);
        if (isHeader(sCurrentFileLine)) {
            processHeader(sCurrentFileLine, secFile);
        }
    }
    std::cout << "\nDone processing SCT file. \n";
    secFile.close();
    
}

void processHeader (std::string sCurrentFileLine, std::ifstream &fStreamIn) {
    start:
        
    if (sCurrentFileLine.find("[INFO]") != string::npos) {
        printSeperator("Processing [INFO] Section");
        processInfo (fStreamIn, sCurrentFileLine);
        goto start;
    }
    else if (sCurrentFileLine.find("[AIRPORT]") != string::npos) {
        printSeperator("Processing [AIRPORT] Section");
        processAirport(fStreamIn, sCurrentFileLine);
        goto start;
    }
    else if (sCurrentFileLine.find("[RUNWAY]")!= string::npos) {
        printSeperator("Processing [RUNWAY] Section");
        processRunway(fStreamIn, sCurrentFileLine);
        goto start;
    }
    else if (sCurrentFileLine.find("[VOR]")!= string::npos) {
        printSeperator("Processing [VOR] Section");
        processVOR(fStreamIn, sCurrentFileLine);
        goto start;
    }
    else if (sCurrentFileLine.find("[NDB]")!= string::npos) {
        printSeperator("Processing [NDB] Section");
        processNDB(fStreamIn, sCurrentFileLine);
        
        goto start;
    }
    else if (sCurrentFileLine.find("[FIXES]")!= string::npos) {
        printSeperator("Processing [FIXES] Section");
        processFIXES(fStreamIn, sCurrentFileLine);
        goto start;
    }
    else if (sCurrentFileLine.find("[LOW AIRWAY]")!= string::npos) {
        printSeperator("Processing [LOW AIRWARY] Section");
        processARTCCorAirway(fStreamIn, LOW_AIRWAY, sCurrentFileLine);
        goto start;
    }
    else if (sCurrentFileLine.find("[HIGH AIRWAY]")!= string::npos) {
        printSeperator("Processing [HIGH AIRWARY] Section");
        processARTCCorAirway(fStreamIn, HIGH_AIRWAY, sCurrentFileLine);
        goto start;
    }
    else if (sCurrentFileLine.find("[ARTCC]")!= string::npos) {
        printSeperator("Processing [ARTCC] Section");
        processARTCCorAirway(fStreamIn, ARTCC, sCurrentFileLine);
        goto start;
    }
    else if (sCurrentFileLine.find("[ARTCC HIGH]")!= string::npos) {
        printSeperator("Processing [ARTCC HIGH] Section");
        processARTCCorAirway(fStreamIn, ARTCC_HIGH, sCurrentFileLine);
        goto start;
    }
    else if (sCurrentFileLine.find("[ARTCC LOW]")!= string::npos) {
        printSeperator("Processing [ARTCC LOW] Section");
        processARTCCorAirway(fStreamIn, ARTCC_LOW, sCurrentFileLine);
        goto start;
    }
    else if (sCurrentFileLine.find("[SID]")!= string::npos) {
        printSeperator("Processing [SID] Section");
        processSIDSTAR(fStreamIn, SID_hack, sCurrentFileLine);
        goto start;
    }
    else if (sCurrentFileLine.find("[STAR]")!= string::npos) {
        printSeperator("Processing [STAR] Section");
        processSIDSTAR(fStreamIn, STAR, sCurrentFileLine);
        goto start;
    }
    else if (sCurrentFileLine.find("[GEO]")!= string::npos) {
        printSeperator("Processing [GEO] Section");
        processGEO(fStreamIn, sCurrentFileLine);
        goto start;
    }
    else if (sCurrentFileLine.find("[REGIONS]")!= string::npos) {
        printSeperator("Processing [REGIONS] Section");
        processREGIONS(fStreamIn, sCurrentFileLine);
        return;
        goto start;
    }
    else return;
}

bool processDefine (std::string sCurrentFileLine) {
    char cCurrentChar;
    for (int i = 0; i < sCurrentFileLine.length(); i++) {
        cCurrentChar = sCurrentFileLine.at(i);
        if (isEmptyChar(cCurrentChar)) {
            continue;
        }
        else if (cCurrentChar == '#') {
            delEndLine(sCurrentFileLine);
            vector<string> splitStrings = split_space_tab(sCurrentFileLine);
            Sector2RhinoApplication::getSectorOld().addDefinedColor(splitStrings.at(1), SCTcolor2RGB(splitStrings.at(2)));
            return true;
        }
        else {
            return false;
        }
    }
    return false;
}

void processInfo (std::ifstream &fStreamIn, std::string &sReturnCurrentLine) {
    std::string sCurrentFileLine;
    
    while(std::getline(fStreamIn, sCurrentFileLine)) {
        incrementLineNumber();
        if (isHeader(sCurrentFileLine) == false && hasData(sCurrentFileLine)) {
            Sector2RhinoApplication::getSectorOld().addInfoLine(sCurrentFileLine);
        }
        else if (isHeader(sCurrentFileLine) == false) {
            continue;
        }
        else {
            sReturnCurrentLine = sCurrentFileLine;
            return;
        }
    }
    
    sReturnCurrentLine = sCurrentFileLine;
    return;
    
}

void processAirport (std::ifstream &fStreamIn, std::string &sReturnCurrentLine) {
    string sCurrentFileLine;
    string sName = "";
    string sFreq = "";
    string sClass = "";
    string sNote = "";
    
    GeographicLib::GeoCoords coord;
    
    while(std::getline(fStreamIn, sCurrentFileLine)) {
        incrementLineNumber();
        if (isHeader(sCurrentFileLine) == false && hasData(sCurrentFileLine)) {
            if (extractAirport(sCurrentFileLine, sName, sFreq, coord, sClass, sNote)) {
                Sector2RhinoApplication::getSectorOld().addAirport(sName, sFreq, coord, sClass, sNote);
            }
        }
        else if (isHeader(sCurrentFileLine) == false) {
            continue;
        }
        else {
            sReturnCurrentLine = sCurrentFileLine;
            return;
        }
    }
    
    sReturnCurrentLine = sCurrentFileLine;
    return;
    
}

void processRunway (std::ifstream &fStreamIn, std::string &sReturnCurrentLine) {
    string sCurrentFileLine;
    
    string sName1 = "";
    string sName2 = "";
    string sHeading1 = "";
    string sHeading2 = "";
    string sNote = "";
    
    GeographicLib::GeoCoords coord1;
    GeographicLib::GeoCoords coord2;
    
    while(std::getline(fStreamIn, sCurrentFileLine)) {
        incrementLineNumber();
        if (isHeader(sCurrentFileLine) == false && hasData(sCurrentFileLine)) {
            if (extractRunway(sCurrentFileLine, sName1, sName2, sHeading1, sHeading2, coord1, coord2, sNote)) {
                Sector2RhinoApplication::getSectorOld().addRunway(sName1, sName2, sHeading1, sHeading2, coord1, coord2, sNote);
            }
        }
        else if (isHeader(sCurrentFileLine) == false) {
            continue;
        }
        else {
            sReturnCurrentLine = sCurrentFileLine;
            return;
        }
    }
    
    sReturnCurrentLine = sCurrentFileLine;
    return;
    
}

void processVOR (std::ifstream &fStreamIn, std::string &sReturnCurrentLine) {
    string sCurrentFileLine;
    string sName = "";
    string sFreq = "";
    string sNote = "";
    GeographicLib::GeoCoords coord;
    
    while(std::getline(fStreamIn, sCurrentFileLine)) {
        incrementLineNumber();
        if (isHeader(sCurrentFileLine) == false && hasData(sCurrentFileLine)) {
            if (extractVORNDB(sCurrentFileLine, sName, sFreq, coord, sNote)) {
                Sector2RhinoApplication::getSectorOld().addVORNDB(RadioNav::VOR_old, sName, sFreq, coord, sNote);
            }
        }
        else if (isHeader(sCurrentFileLine) == false) {
            continue;
        }
        else {
            sReturnCurrentLine = sCurrentFileLine;
            return;
        }
    }
    
    sReturnCurrentLine = sCurrentFileLine;
    return;
    
}

void processNDB (std::ifstream &fStreamIn, std::string &sReturnCurrentLine) {
    string sCurrentFileLine;
    string sName = "";
    string sFreq = "";
    string sNote = "";
    GeographicLib::GeoCoords coord;
    
    while(std::getline(fStreamIn, sCurrentFileLine)) {
        incrementLineNumber();
        if (isHeader(sCurrentFileLine) == false && hasData(sCurrentFileLine)) {
            if (extractVORNDB(sCurrentFileLine, sName, sFreq, coord, sNote)) {
                Sector2RhinoApplication::getSectorOld().addVORNDB(RadioNav::NDB_old, sName, sFreq, coord, sNote);
            }
        }
        else if (isHeader(sCurrentFileLine) == false) {
            continue;
        }
        else {
            sReturnCurrentLine = sCurrentFileLine;
            return;
        }
    }
    
    sReturnCurrentLine = sCurrentFileLine;
    return;
    
}

void processFIXES (std::ifstream &fStreamIn, std::string &sReturnCurrentLine) {
    string sCurrentFileLine;
    string sName = "";
    GeographicLib::GeoCoords coord1;
    string sNote ="";
    
    while(std::getline(fStreamIn, sCurrentFileLine)) {
        incrementLineNumber();
        if (isHeader(sCurrentFileLine) == false && hasData(sCurrentFileLine)) {
            if (extractFixes(sCurrentFileLine, sName, coord1, sNote)) {
                Sector2RhinoApplication::getSectorOld().addFix(sName, coord1, sNote);
            }
        }
        else if (isHeader(sCurrentFileLine) == false) {
            continue;
        }
        else {
            sReturnCurrentLine = sCurrentFileLine;
            return;
        }
    }
    
    sReturnCurrentLine = sCurrentFileLine;
    return;
    
}

void processARTCCorAirway (std::ifstream &fStreamIn, BoundryOrAir eBoundryOrAirType, std::string &sReturnCurrentLine) {
    string sCurrentFileLine;
    
    string sName = "";
    string sNote;
    GeographicLib::GeoCoords coord1;
    GeographicLib::GeoCoords coord2;
    
    while(std::getline(fStreamIn, sCurrentFileLine)) {
        incrementLineNumber();
        if (isHeader(sCurrentFileLine) == false && hasData(sCurrentFileLine)) {
            
            if (extractARTCCorAirway(sCurrentFileLine, sName, coord1, coord2, sNote)) {
                Sector2RhinoApplication::getSectorOld().addARTCCorAirway(eBoundryOrAirType, sName, coord1, coord2, sNote);
            }
            
        }
        else if (isHeader(sCurrentFileLine) == false) {
            continue;
        }
        else {
            sReturnCurrentLine = sCurrentFileLine;
            return;
        }
    }
    
    sReturnCurrentLine = sCurrentFileLine;
    return;
    
}

void processSIDSTAR (std::ifstream &fStreamIn, DiagramType eDiagramType, std::string &sReturnCurrentLine) {
    string sCurrentFileLine;
    
    string sName = "Unnamed";
    //string sCoords;
    string sNote;
    cRGBColor cColor;
    
    //DiagramType eDiagramType = SID;
    GeographicLib::GeoCoords coord1;
    GeographicLib::GeoCoords coord2;
    
    while(std::getline(fStreamIn, sCurrentFileLine)) {
        incrementLineNumber();
        if (isHeader(sCurrentFileLine) == false && hasData(sCurrentFileLine)) {
            
            if (processSIDSTARLine(sCurrentFileLine, sName, coord1, coord2, cColor, sNote)) {
            Sector2RhinoApplication::getSectorOld().addDiagramLine(&coord1, &coord2, &sName, &cColor, &eDiagramType, &sNote);
            }
            
        }
        else if (isHeader(sCurrentFileLine) == false) {
            continue;
        }
        else {
            sReturnCurrentLine = sCurrentFileLine;
            return;
        }
    }
    
    sReturnCurrentLine = sCurrentFileLine;
    return;
    
}

void processGEO (std::ifstream &fStreamIn, std::string &sReturnCurrentLine) {
    string sCurrentFileLine;

    GeographicLib::GeoCoords coord1;
    GeographicLib::GeoCoords coord2;
    cRGBColor color;
    std::string sNote = "";
    
    while(std::getline(fStreamIn, sCurrentFileLine)) {
        incrementLineNumber();
        if (isHeader(sCurrentFileLine) == false && hasData(sCurrentFileLine)) {
            if (extractGEO(sCurrentFileLine, coord1, coord2, color, sNote)) {
                std::string sName = "Geo";
                DiagramType eDiagramType = GEO;
                Sector2RhinoApplication::getSectorOld().addDiagramLine(&coord1, &coord2, &sName, &color, &eDiagramType, &sNote);
            }
        }
        else if (isHeader(sCurrentFileLine) == false) {
            continue;
        }
        else {
            sReturnCurrentLine = sCurrentFileLine;
            return;
        }
    }
    
    sReturnCurrentLine = sCurrentFileLine;
    return;
}

void processREGIONS (std::ifstream &fStreamIn, std::string &sReturnCurrentLine) {
    string sCurrentFileLine;
    
    GeographicLib::GeoCoords coord1;
    double dDepth = 0.0;
    cRGBColor color;
    bool hasColor;
    std::string sNote;
    
    while(std::getline(fStreamIn, sCurrentFileLine)) {
        incrementLineNumber();
        if (isHeader(sCurrentFileLine) == false && hasData(sCurrentFileLine)) {
            if (extractREGION(sCurrentFileLine, hasColor, coord1, dDepth, color, sNote)) {
                if (hasColor) {
                    Sector2RhinoApplication::getSectorOld().startRegion(color, coord1, dDepth, sNote);
                    continue;
                }
                Sector2RhinoApplication::getSectorOld().addToRegion(coord1);
                continue;
            }
        }
        else if (isHeader(sCurrentFileLine) == false) {
            continue;
        }
        else {
            sReturnCurrentLine = sCurrentFileLine;
            return;
        }
    }
    sReturnCurrentLine = sCurrentFileLine;
    return;
}

bool processSIDSTARLine ( std::string inString, std::string &sName, GeographicLib::GeoCoords &coord1, GeographicLib::GeoCoords &coord2, cRGBColor &outColor, std::string &outNotes ) {
    
    //std::cout << "[extractSID/STAR] Input: " << inString << endl;
    
    int nWordCount = 0;
    string sNametemp = "";
    GeographicLib::GeoCoords gTempCoord;
    string sCoords = "";
    string sCoordsTemp = "";
    string sNumCheck;
    outNotes = "";
    outColor = Sector2RhinoApplication::getSectorOld().returnDefaultColor();
    delEndLine(inString);
    
    //std::cout << "Input line: " << inString << std::endl;
    vector<string> vCoords;
    vector<string> splitStrings = split_space_tab(inString);
    
    if (splitStrings.at(0) == "HSKEL") {
        std::cout << "STOP!";
    }
    
    int j = (int) splitStrings.size();
    for (int i = 0; i < j; i++) {
        
        // Parse SID/STAR Name directly if not valid coord
        if ((isCoord(splitStrings.at(i)) == false) && (isFix(splitStrings.at(i)) == false) && (nWordCount == 0)) {
            if (sNametemp.length() == 0) {
                sNametemp = splitStrings.at(i);
                continue;
            }
            sNametemp = sNametemp + " " + splitStrings.at(i);
            continue;
        }
        
        // In case a valid coord is in the name, this function will allow the name name to grow until quadCoord finds that the coordinate is part of thte LAST 4 valid coords
        if ( nWordCount == 0 && splitStrings.size() > (4 + i) ) {
            if (quadCoordTest(splitStrings, i)) {
                if (sNametemp.length() == 0) {
                    sNametemp = splitStrings.at(i);
                    continue;
                }
                sNametemp = sNametemp + " " + splitStrings.at(i);
                continue;
            }
        }
        
        if (nWordCount < 4) {
            // Parse standard coordinate
            if (isCoord(splitStrings.at(i))) {
                vCoords.push_back(splitStrings.at(i));
                nWordCount++;
                continue;
            }
            
            // Parse fix by looking up
            else if (isFix(splitStrings.at(i), gTempCoord)) {
                
                sCoordsTemp = ConvertCoord2SCT(gTempCoord);
                
                if (nWordCount == 0 || nWordCount == 2) {
                    vCoords.push_back(sCoordsTemp.substr(0, 14));
                    nWordCount++;
                    continue;
                }
                //Sector.returnFix(splitStrings.at(i), gTempCoord);
                vCoords.push_back(sCoordsTemp.substr(15, 14));
                nWordCount++;
                continue;
            }
        }

        
        // Parse Color
        else if (nWordCount == 4 && splitStrings.at(i).at(0) != ';') {
            nWordCount++;
            sNumCheck = splitStrings.at(i);
            
            std::string::const_iterator it = sNumCheck.begin();
            while (it != sNumCheck.end() ) //&& std::isdigit(*it)) ++it; // keep iterating until letter found ... removed isdigit for MSVS
            if (it == sNumCheck.end()) {
                // number
                outColor = SCTcolor2RGB(sNumCheck);
                continue;
            }
            else {
                // Define color
                outColor = Sector2RhinoApplication::getSectorOld().returnDefinedColor(sNumCheck);
                continue;
            }
        }
        
        // Parse notes / comments
        else if (nWordCount == 5 || splitStrings.at(i).at(0) == ';') {
            nWordCount++;
            outNotes = splitStrings.at(i);
            if (outNotes.at(0) == ';') { // remove ";"
                outNotes.erase(0, 1);
            }
            continue;
        }
        
        else if (nWordCount >= 6) { // more notes
            nWordCount++;
            outNotes = outNotes + " " + splitStrings.at(i);
            continue;
        }
    }
    
    // Only update name of SID if name was found
    if (sNametemp.length() != 0) {
        sName = sNametemp;
    }
    
    // Saftey check
    if (vCoords.size() < 4) {
        std::cout << "Line: " << nCurrentFileLineNumber << " - Not enough coordinates found: " << inString << std::endl;
        return false;
    }
    
    // Could be more robust in the future, but this should keep ghost coords out
    if (vCoords.at(0).find("000.00.00.000") != std::string::npos) {
        //std::cout << "[extractCoords] 000.00.00.000 coords found, ignoring line" << endl;
        return false;
    }
    
    coord1 = ConvertSCT2Coord(vCoords.at(0) + " " + vCoords.at(1));
    coord2 = ConvertSCT2Coord(vCoords.at(2) + " " + vCoords.at(3));
    
    //std::cout <<
    //"Name: " << sName << endl <<
    //"Coords: " << sCoords << endl <<
    //"Color: " << outColor.r << "," << outColor.g << "," << outColor.b << endl <<
    //"Note: " << outNotes << endl;
    
    return true;
    
}

bool quadCoordTest (std::vector<std::string> vStrings, int i) {
    std::vector<std::string> splitStrings = vStrings;
    std::string sNametemp;
    
    //bool isCoord1;
    //bool isCoord2;
    //bool isCoord3;
    //bool isCoord4;
    //bool isFix1;
    //bool isFix2;
    //bool isFix3;
    //bool isFix4;
    
    for (int k = i; k < splitStrings.size() - 4; k++) {
        //isCoord1 = isCoord(splitStrings.at(k+1));
        //isCoord1 = isCoord(splitStrings.at(k+2));
        //isCoord1 = isCoord(splitStrings.at(k+3));
        //isCoord1 = isCoord(splitStrings.at(k+4));
        
        
        if (
            ( isCoord(splitStrings.at(k+1)) || isFix(splitStrings.at(k+1)) ) &&
            ( isCoord(splitStrings.at(k+2)) || isFix(splitStrings.at(k+2)) ) &&
            ( isCoord(splitStrings.at(k+3)) || isFix(splitStrings.at(k+3)) ) &&
            ( isCoord(splitStrings.at(k+4)) || isFix(splitStrings.at(k+4)) )
            ) {
            if (i <= k) {
                return true;
            }
        }
    }
    return false;
}

bool extractFixes ( std::string inString, std::string &outName, GeographicLib::GeoCoords &outCoord, std::string &sNote) {
    
    sNote = "";
    
    delEndLine(inString);
    
    vector<string> splitStrings = split_space_tab(inString);
    
    if (isCoord(splitStrings.at(0)) ) {
            std::cout << "[extractFix] Input: " << inString << endl;
            std::cout << "[extractFix] coordinate found premature" << endl;
            return false;
    }
    outName = splitStrings.at(0);
    
    if ( !(isCoord(splitStrings.at(1))) || !(isCoord(splitStrings.at(2)))) {
        std::cout << "[extractFix] Input: " << inString << endl;
        std::cout << "[extractFix] not valid coordinate found" << endl;
        return false;
    }
    outCoord = ConvertSCT2Coord(splitStrings.at(1) + " " + splitStrings.at(2));
    
    if (splitStrings.size() > 3) {
        if (splitStrings.at(3).at(0) == ';') {
            splitStrings.at(3).erase(0, 1);
        }
        
        for (int i = 3; i < splitStrings.size(); i++) {
            if (sNote == "") sNote = splitStrings.at(i);
            else sNote = sNote + " " + splitStrings.at(i);
        }
    }
    
    return true;
}

bool extractAirport ( std::string inString, std::string &sOutName, std::string &sOutFreq,GeographicLib::GeoCoords &outCoord1, std::string &sClass, std::string &sNote) {
    
    sNote = "";
    
    delEndLine(inString);
    
    vector<string> splitStrings = split_space_tab(inString);
    
    if (splitStrings.size() < 5) {
        std::cout << "[extractAIRPORT] Input: " << inString << endl;
        std::cout << "[extractAIRPORT] Missing info in line" << std::endl;
        return false;
    }
    
    if (isCoord(splitStrings.at(0)) ) {
        std::cout << "[extractAIRPORT] Input: " << inString << endl;
        std::cout << "[extractAIRPORT] coordinate found premature" << endl;
        return false;
    }
    sOutName = splitStrings.at(0);
    
    sOutFreq = splitStrings.at(1);
    
    if ( !(isCoord(splitStrings.at(2))) || !(isCoord(splitStrings.at(3)))) {
        std::cout << "[extractAIRPORT] Input: " << inString << endl;
        std::cout << "[extractAIRPORT] no valid coordinate found" << endl;
        return false;
    }
    outCoord1 = ConvertSCT2Coord(splitStrings.at(2) + " " + splitStrings.at(3));
    
    sClass = splitStrings.at(4);
    
    if (splitStrings.size() > 5) {
        if (splitStrings.at(5).at(0) == ';') {
            splitStrings.at(5).erase(0, 1);
        }
        
        for (int i = 5; i < splitStrings.size(); i++) {
            if (sNote == "") sNote = splitStrings.at(i);
            else sNote = sNote + " " + splitStrings.at(i);
        }
    }
    
    //std::cout << "[extractAirport] " << sOutName << " " << outCoord1.DMSRepresentation(2, 0, '.') << " " << sClass << " " << sNote << endl;
    
    return true;
}

bool extractRunway ( std::string inString, std::string &sOutName1, std::string &sOutName2, std::string &sOutHeading1, std::string &sOutHeading2, GeographicLib::GeoCoords &outCoord1, GeographicLib::GeoCoords &outCoord2, std::string &sNote ) {
    
    sNote = "";
    
    delEndLine(inString);
    
    vector<string> splitStrings = split_space_tab(inString);
    
    if (splitStrings.size() < 8) {
        std::cout << "[extractRUNWAY] Input: " << inString << endl;
        std::cout << "[extractRUNWAY] Missing info in line" << std::endl;
        return false;
    }
    
    if (isCoord(splitStrings.at(0)) || isCoord(splitStrings.at(1)) ) {
        std::cout << "[extractRUNWAY] Input: " << inString << endl;
        std::cout << "[extractRUNWAY] coordinate found premature" << endl;
        return false;
    }
    sOutName1 = splitStrings.at(0);
    
    sOutName2 = splitStrings.at(1);
    
    sOutHeading1 = splitStrings.at(2);
    
    sOutHeading2 = splitStrings.at(3);
    
    if ( !(isCoord(splitStrings.at(4)))
        || !(isCoord(splitStrings.at(5)))
        || !(isCoord(splitStrings.at(6)))
        || !(isCoord(splitStrings.at(7)))
        ) {
        std::cout << "[extractRUNWAY] Input: " << inString << endl;
        std::cout << "[extractRUNWAY] not valid coordinate found" << endl;
        return false;
    }
    outCoord1 = ConvertSCT2Coord(splitStrings.at(4) + " " + splitStrings.at(5));
    outCoord2 = ConvertSCT2Coord(splitStrings.at(6) + " " + splitStrings.at(7));
    
    if (splitStrings.size() > 8) {
        if (splitStrings.at(8).at(0) == ';') {
            splitStrings.at(8).erase(0, 1);
        }
        sNote = splitStrings.at(8);
        for (int i = 9; i < splitStrings.size(); i++) {
            sNote = sNote + " " + splitStrings.at(i);
        }
    }
    
    //std::cout << "[extractAirport] " << sOutName << " " << outCoord1.DMSRepresentation(2, 0, '.') << " " << sClass << " " << sNote << endl;
    
    return true;
}

bool extractARTCCorAirway ( std::string inString, std::string &sOutName, GeographicLib::GeoCoords &outCoord1, GeographicLib::GeoCoords &outCoord2, std::string &sNote) {

    GeographicLib::GeoCoords gTempCoord;
    std::string sCoord;
    std::string sCoordTemp;
    sNote = "";
    
    delEndLine(inString);
    
    vector<string> splitStrings = split_space_tab(inString);
    
    if (splitStrings.size() < 5) {
        std::cout << "[extractARTCCorAirway] Input: " << inString << endl;
        std::cout << "[extractARTCCorAirway] Missing info in line" << std::endl;
        return false;
    }
    
    if (isCoord(splitStrings.at(0)) || isFix(splitStrings.at(0)) ) {
        std::cout << "[extractARTCCorAirway] Input: " << inString << endl;
        std::cout << "[extractARTCCorAirway] coordinate found premature" << endl;
        return false;
    }
    
    sOutName = splitStrings.at(0);
    
    //Break if no coords in next 4 words
    if (!( isCoord(splitStrings.at(1)) || isFix(splitStrings.at(1)) ) ||
        !( isCoord(splitStrings.at(2)) || isFix(splitStrings.at(2)) ) ||
        !( isCoord(splitStrings.at(3)) || isFix(splitStrings.at(3)) ) ||
        !( isCoord(splitStrings.at(4)) || isFix(splitStrings.at(4)) )
        ) {
        std::cout << "[extractARTCCorAirway] Input: " << inString << endl;
        std::cout << "[extractARTCCorAirway] Error: Non-valid coordinate found";
        return false;
    }
    
    // Get first coord pair
    if ((isFix(splitStrings.at(1), gTempCoord))) {
        sCoordTemp = ConvertCoord2SCT(gTempCoord);
        sCoord = sCoordTemp.substr(0, 14);
    }
    else {
        sCoord = splitStrings.at(1);
    }
    
    if ((isFix(splitStrings.at(2), gTempCoord))) {
        sCoordTemp = ConvertCoord2SCT(gTempCoord);
        sCoord += " " + sCoordTemp.substr(15, 14);
    }
    else {
        sCoord += " " + splitStrings.at(2);
    }
    
    if (sCoord.find("000.00.00.000") != std::string::npos) {
        std::cout << "[extractCoords] 000.00.00.000 coords found, ignoring line" << endl;
        return false;
    }
    
    outCoord1 = ConvertSCT2Coord(sCoord);
    
    // Get second coord pair
    sCoord = "";

    if ((isFix(splitStrings.at(3), gTempCoord))) {
        sCoordTemp = ConvertCoord2SCT(gTempCoord);
        sCoord = sCoordTemp.substr(0, 14);
    }
    else {
        sCoord = splitStrings.at(3);
    }
    
    if ((isFix(splitStrings.at(4), gTempCoord))) {
        sCoordTemp = ConvertCoord2SCT(gTempCoord);
        sCoord += " " + sCoordTemp.substr(15, 14);
    }
    else {
        sCoord += " " + splitStrings.at(4);
    }
    
    if (sCoord.find("000.00.00.000") != std::string::npos) {
        std::cout << "[extractCoords] 000.00.00.000 coords found, breaking parsing" << endl;
        return false;
    }
    
    outCoord2 = ConvertSCT2Coord(sCoord);
    
    //Get Note
    if (splitStrings.size() > 5) {
        if (splitStrings.at(5).at(0) == ';') {
            splitStrings.at(5).erase(0, 1);
        }
        sNote = splitStrings.at(5);
        for (int i = 6; i < splitStrings.size(); i++) {
            sNote = sNote + " " + splitStrings.at(i);
        }
    }

    return true;
}



bool extractVORNDB ( std::string inString, std::string &sOutName, std::string &sOutFreq,GeographicLib::GeoCoords &outCoord1, std::string &sNote) {
    
    sNote = "";
    
    delEndLine(inString);
    
    vector<string> splitStrings = split_space_tab(inString);
    
    if (isCoord(splitStrings.at(0)) ) {
        std::cout << "[extractVORNDB] Input: " << inString << endl;
        std::cout << "[extractVORNDB] coordinate found premature" << endl;
        return false;
    }
    sOutName = splitStrings.at(0);
    
    sOutFreq = splitStrings.at(1);
    
    if ( !(isCoord(splitStrings.at(2))) || !(isCoord(splitStrings.at(3)))) {
        std::cout << "[extractVORNDB] Input: " << inString << endl;
        std::cout << "[extractVORNDB] not valid coordinate found" << endl;
        return false;
    }
    outCoord1 = ConvertSCT2Coord(splitStrings.at(2) + " " + splitStrings.at(3));
    
    if (splitStrings.size() > 4) {
        if (splitStrings.at(4).at(0) == ';') {
            splitStrings.at(4).erase(0, 1);
        }
        
        for (int i = 4; i < splitStrings.size(); i++) {
            if (sNote == "") sNote = splitStrings.at(i);
            else sNote = sNote + " " + splitStrings.at(i);
        }
    }

    //std::cout << "[extractVORNDB] " << sOutName << ":" << " " << sOutFreq << " " << outCoord1.DMSRepresentation(2, 0, '.') << " " << sNote << endl;
    
    return true;
}

bool extractGEO ( std::string inString, GeographicLib::GeoCoords &outCoord1, GeographicLib::GeoCoords &outCoord2, cRGBColor &outColor, std::string &sNote) {
    
    
    
    sNote = "";
    
    delEndLine(inString);
    
    vector<string> splitStrings = split_space_tab(inString);
    
    if (splitStrings.size() < 2) {
        std::cout << "[extractGEO] Input: " << inString << endl;
        std::cout << "[extractGEO] Missing info in line" << std::endl;
        return false;
    }
    
    outCoord1 = ConvertSCT2Coord(splitStrings.at(0) + " " + splitStrings.at(1));
    outCoord2 = ConvertSCT2Coord(splitStrings.at(2) + " " + splitStrings.at(3));
    
    if (splitStrings.size() > 3)
    {
        string sNumCheck = splitStrings.at(4);
        
        std::string::const_iterator it = sNumCheck.begin();
        while (it != sNumCheck.end()) // && std::isdigit(*it)) ++it; // keep iterating until letter found. removed is digit for MSVS
        if (it == sNumCheck.end()) {
            // number
            outColor = SCTcolor2RGB(sNumCheck);
        }
        else {
            // Define color
            outColor = Sector2RhinoApplication::getSectorOld().returnDefinedColor(sNumCheck);
        }
    }
    
    if (splitStrings.size() > 5) {
        if (splitStrings.at(5).at(0) == ';') {
            splitStrings.at(5).erase(0, 1);
        }
        
        for (int i = 5; i < splitStrings.size(); i++) {
            if (sNote == "") sNote = splitStrings.at(i);
            else sNote = sNote + " " + splitStrings.at(i);
        }
    }
    
    
    //std::cout << "[extractGEO] " << outCoord1.DMSRepresentation(2, 0, '.') << " " << outCoord2.DMSRepresentation(2, 0, '.') << " " << outColor.r << "," << outColor.g << "," << outColor.b << " " << sNote << endl;
    
    return true;
}

bool extractREGION (std::string inString, bool &hasColor, GeographicLib::GeoCoords &outCoord1, double &dDepth, cRGBColor &outColor, std::string &sNote ) {
    delEndLine(inString);
    
   // std::cout << "[extractREGION] Input: " << inString << endl;
    
    sNote = ""; 
    
    dDepth = 0.0;
    
    vector<std::string> splitStrings = split_space_tab(inString);
    
    // Check if first two words are coords / return them if they are
    if ((isFix(splitStrings.at(0)) || isCoord(splitStrings.at(0)) ) &&
        ( isFix(splitStrings.at(1)) || isCoord(splitStrings.at(1))) ) {
        hasColor = false;
        outCoord1 = ConvertSCT2Coord(splitStrings.at(0) + " " + splitStrings.at(1));
        return true;
    }
    
    {
        string sNumCheck = splitStrings.at(0);
        std::string::const_iterator it = sNumCheck.begin();
        while (it != sNumCheck.end()) // && std::isdigit(*it)) ++it; // keep iterating until letter found
        if (it == sNumCheck.end()) {
            // number
            outColor = SCTcolor2RGB(sNumCheck);
        }
        else {
            // Define color
            outColor = Sector2RhinoApplication::getSectorOld().returnDefinedColor(sNumCheck);
        }
    }
    
    hasColor=true;
    if ( (isFix(splitStrings.at(1)) || isCoord(splitStrings.at(1)) ) &&
          ( isFix(splitStrings.at(2)) || isCoord(splitStrings.at(2))) ) {
        outCoord1 = ConvertSCT2Coord(splitStrings.at(1) + " " + splitStrings.at(2));
        
        // changes ";note" to "note"
        if (splitStrings.size() > 3) {
            if (splitStrings.at(3).at(0) == ';') {
                splitStrings.at(3).erase(0, 1);
            }
            
            for (int i = 3; i < splitStrings.size(); i++) {
                if (splitStrings.at(i).compare(0, 6, "depth=") == 0) {
                    std::string sTemp;
                    size_t sLocation = 6;
                    sTemp = splitStrings.at(i).substr(sLocation);
                    dDepth = atof(sTemp.c_str());
                    continue;
                }
                if (sNote == "") sNote = splitStrings.at(i);
                else sNote = sNote + " " + splitStrings.at(i);
            }
        }
        
        return true;
    }
    
    
    return false;
}

bool hasData (std::string inString) {
    char cCurrentChar;
    for (int i = 0; i < inString.length(); i++) {
        cCurrentChar = inString.at(i);
        if (isEmptyChar(cCurrentChar)) {
            continue;
        }
        else if (cCurrentChar != '\r' && cCurrentChar != ';') {
            return true;
        }
        else {
            return false;
        }
    }
    return false;
}

bool isHeader (std::string sInputLine) {
    for (int i = 0; i < sInputLine.length(); i++) {
        char cChar = sInputLine.at(i);
        
        if (isEmptyChar(cChar)) {
            //cout << "Space found" << endl;
            continue;
        }
        else if (cChar == '[') {
            return true;
        }
        else return false;
    }
    return false;
}

cRGBColor SCTcolor2RGB (std::string sInput) {
    cRGBColor returnRGB;
    
    int conv;
    conv = atoi(sInput.c_str());
    
    returnRGB.b = (conv / 65536);
    returnRGB.g = (conv - (returnRGB.b * 65536)) / 256;
    returnRGB.r = (conv - (returnRGB.b * 65536) - (returnRGB.g * 256));
    
    return returnRGB;
}

int RGB2SCTcolor (cRGBColor inColor) {
    //(BLUE x 65536) + (GREEN x 256) + RED
    int r = (inColor.b * 65536) + (inColor.g * 256) + inColor.r;
    return r;
}

GeographicLib::GeoCoords ConvertSCT2Coord (std::string sInput) {
    
    std::vector<string> vSplitStrings = split_space_tab(sInput);
    
    size_t firstPeriod = vSplitStrings.at(0).find_first_of(".");
    size_t secondPeriod = vSplitStrings.at(0).find_first_of(".", firstPeriod +1);
    vSplitStrings.at(0).replace(firstPeriod, 1, "d");
    vSplitStrings.at(0).replace(secondPeriod, 1, "'");
    vSplitStrings.at(0).insert(vSplitStrings.at(0).end(), '\"');
    vSplitStrings.at(0).insert(vSplitStrings.at(0).end(), vSplitStrings.at(0).at(0));
    vSplitStrings.at(0).erase(0,1);
    
    firstPeriod = vSplitStrings.at(1).find_first_of(".");
    secondPeriod = vSplitStrings.at(1).find_first_of(".", firstPeriod +1);
    vSplitStrings.at(1).replace(firstPeriod, 1, "d");
    vSplitStrings.at(1).replace(secondPeriod, 1, "'");
    vSplitStrings.at(1).insert(vSplitStrings.at(1).end(), '\"');
    vSplitStrings.at(1).insert(vSplitStrings.at(1).end(), vSplitStrings.at(1).at(0));
    vSplitStrings.at(1).erase(0,1);
    
    //Replace degrees, minutes
    /*
    sInput.replace(4, 1, "d");
    sInput.replace(7, 1, "'");
    
    sInput.replace(19, 1, "d");
    sInput.replace(22, 1, "'");
    
    sInput.insert(14, ("\"" + sInput.substr(0,1)));
    sInput.erase(0,1);
    
    sInput.insert(30, "\"" + sInput.substr(16, 1));
    sInput.erase(16,1);
     */
    
    //std::cout << "inCoord:  " << sInput << endl;
    sInput = vSplitStrings.at(0) + " " + vSplitStrings.at(1);
    
    GeographicLib::GeoCoords outCoord(sInput);
    
    //std::cout << "outCoord: " << outCoord.DMSRepresentation(2) << endl;
    
    return outCoord;
}

std::string ConvertCoord2SCT (GeographicLib::GeoCoords gCoordIn) {
    //std::cout << gCoordIn.DMSRepresentation(2, 0, '.') << endl;
    // 42d15'57.000"N 070d06'11.000"W
    // 000000000011111111112222222222
    // 012345678901234567890123456789
    //std::string coord1 = gCoordIn.DMSRepresentation(2, 0, '.');
    //std::string coord2;
    size_t cFind = 0;
    
    vector<std::string> vSplitStrings;
    
    vSplitStrings = split_space_tab(gCoordIn.DMSRepresentation(2, 0, '.'));
    
    string sTempcoord;
    
    string sTemp;
    
    for (int i = 0; i < 2; i++) {
        sTempcoord = vSplitStrings.at(i);
        
        sTemp = sTempcoord.at(sTempcoord.length()-1);
        sTempcoord.insert(0, sTemp);
        sTempcoord.erase(sTempcoord.length()-1);
        
        cFind = sTempcoord.find('.', 0);
        
        while (cFind != 4) {
            sTempcoord.insert(1, "0");
            cFind = sTempcoord.find('.', 0);
        }
        
        cFind = sTempcoord.find('.', 5);
        
        while (cFind != 7) {
            sTempcoord.insert(5, "0");
            cFind = sTempcoord.find('.', 5);
        }
        
        cFind = sTempcoord.find('.', 8);
        
        while (cFind != 10) {
            sTempcoord.insert(8, "0");
            cFind = sTempcoord.find('.', 8);
        }
        
        while (sTempcoord.length() < 14) {
            sTempcoord.append("0");
        }
        vSplitStrings.at(i) = sTempcoord;
        
    }
    
    return vSplitStrings.at(0) + " " + vSplitStrings.at(1);
}


/*
vector<string> split(string sInput, char delim, int rep) {
    vector<string> flds;
    string work = sInput;
    string buf = "";
    int i = 0;
    while (i < work.length()) {
        if (work[i] != ' ' && work[i] != '\t')
            buf += work[i];
        else if (rep == 1) {
            flds.push_back(buf);
            buf = "";
        } else if (buf.length() > 0) {
            flds.push_back(buf);
            buf = "";
        }
        i++;
    }
    if (!buf.empty())
        flds.push_back(buf);
    return flds;
}
*/

void delEndLine (std::string &sInputString) {
    if (sInputString.at(sInputString.size()-1) == '\r') {
        sInputString.erase(sInputString.size()-1);
    }
}

void printSeperator (std::string sInput) {
    std::cout << endl;
    std::cout << "========================================================" << std::endl;
    std::cout << sInput << std::endl;
    std::cout << "========================================================" << std::endl;
}

bool isCoord (std::string &inString) {
    
    size_t cFind = 0;
    
    // Shortest coord is NX.X.X.X
    if (inString.length() < 8) return false;
    
    // Check if first Char is valid directional, then check for 3 "."
    {
        if (inString.at(0) == 'N' ||
            inString.at(0) == 'n' ||
            inString.at(0) == 'S' ||
            inString.at(0) == 's' ||
            inString.at(0) == 'E' ||
            inString.at(0) == 'e' ||
            inString.at(0) == 'W' ||
            inString.at(0) == 'w')
        {
            for (int i = 0; i < 3; i++) {
                cFind = inString.find('.', cFind);
                if (cFind == std::string::npos) return false;
                cFind = cFind + 1;
            }
        }
        else return false;
    }
    
    //Make char Capital if not
    {
        if (inString.at(0) == 'n') inString.replace(0, 1, "N");
        if (inString.at(0) == 's') inString.replace(0, 1, "S");
        if (inString.at(0) == 'e') inString.replace(0, 1, "E");
        if (inString.at(0) == 'w') inString.replace(0, 1, "W");
    }
    
    //Add additional 0's to make NXXX.XX.XX.XXX
    {
        cFind = inString.find('.', 0);
        
        while (cFind != 4) {
            inString.insert(1, "0");
            cFind = inString.find('.', 0);
        }
        
        cFind = inString.find('.', 5);
        
        while (cFind != 7) {
            inString.insert(5, "0");
            cFind = inString.find('.', 5);
        }
        
        cFind = inString.find('.', 8);
        
        while (cFind != 10) {
            inString.insert(8, "0");
            cFind = inString.find('.', 8);
        }
        
        while (inString.length() < 14) {
            inString.append("0");
        }
    }
    
    std::string::const_iterator it = inString.begin() + 1;
    while (it != inString.end())// && (std::isdigit(*it) || *it == '.')) ++it; // keep iterating until letter found
    if (it != inString.end()) {
        cout << "ERROR: Letter found in coord" << endl;
        return false;
    }

    return true;

}

bool isFix (std::string sInString) {
    GeographicLib::GeoCoords rCoord;
    //RadioNav rnavtemp;
    std::string sfreqtemp;
    
    if (Sector2RhinoApplication::getSectorOld().fixExists(sInString) ||
        Sector2RhinoApplication::getSectorOld().VORExists(sInString) ||
        Sector2RhinoApplication::getSectorOld().NDBExists(sInString) ||
        Sector2RhinoApplication::getSectorOld().airportExists(&sInString)) return true;
    return false;
}

bool isFix (std::string sInString, GeographicLib::GeoCoords &rCoord) {
    //GeographicLib::GeoCoords rCoord;
    RadioNav rnavtemp;
    std::string sfreqtemp;
    
    if (Sector2RhinoApplication::getSectorOld().returnFix(sInString, rCoord)) {
        return true;
    }
    else if (Sector2RhinoApplication::getSectorOld().returnVORNDB(sInString, rnavtemp, sfreqtemp, rCoord) ) {
        return true;
    }
    else if (Sector2RhinoApplication::getSectorOld().returnAirport(sInString, sfreqtemp, rCoord, sfreqtemp)) {
        return true;
    }
    return false;
}

bool isEmptyChar (char inChar) {
    if (inChar == ' ') {
        return true;
    }
    if (inChar == '\t') {
        return true;
    }
    else
        return false;
}
