/*
  ==============================================================================

    SectorFileParser.h
    Created: 10 Jan 2015 9:53:48pm
    Author:  Jim Mazur

  ==============================================================================
*/

#ifndef SECTORFILEPARSER_H_INCLUDED
#define SECTORFILEPARSER_H_INCLUDED

#include "JuceHeader.h"
#include "../sct/SectorString.h"
#include "../model/Sector.h"

class SectorFileParser : public Thread
{
public:
    SectorFileParser (File sector);
    
    SectorFileParser (File sector, sector::SectorContainer* sectorToAddTo);
    
    ~SectorFileParser();
    
    void setFile (File sector);
    
    File getFile ();
    
    Result parseFile();
    
    float getProgress();
    
    void run();
    
private:
    bool getNextLine();
    
    bool isDefineOrSectionHeader();
    
    typedef bool (SectorString::*SectorStringBoolFunction)() const;
    
    void parseAsNotesWhile (SectorStringBoolFunction function);
    
    template<typename T>
    void parseSingleLine(T, SectorStringBoolFunction);
    
    using SectorStringIsFunction = bool (SectorString::*)() const;
    
    template<typename T>
    using SectorStringToFunction = T (SectorString::*)() const;
    
    template<typename T>
    using SectorAddFunction = void (sector::SectorContainer::*)(const T&);
    
    template<typename T>
    void parseSingleLineAndAddToSector(T, SectorStringIsFunction isFunction, SectorStringToFunction<T> getFunction, SectorAddFunction<T> addFunction);
    
    void parseSIDorSTAR(const SectorString::SectionHeaderType &currentHeader);
    
    void parseRegions();
    
    void parseUntilNextHeader();
    
    void parseNotes();
    
    void parseInfo();
    
    void parseHeader();
    
    bool isFirstPass() const;
    
    bool isSecondPass() const;
    
    String stringLiteral (const String& text, int maxLineLength);
    
    sector::SectorContainer* sector;
    
    String notes;
    
    File sectorFile;
    SectorString currentLine;
    int lineNumber;
    ScopedPointer<FileInputStream> sectorFileStream;
    int64 streamTotalLength;
    int64 streamPosition;
    CriticalSection streamLock;
    
    bool firstPass;
};


#endif  // SECTORFILEPARSER_H_INCLUDED
