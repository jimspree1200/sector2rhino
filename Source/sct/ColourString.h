/*
  ==============================================================================

    ColourString.h
    Created: 7 Feb 2015 10:06:02am
    Author:  Jim Mazur

  ==============================================================================
*/

#ifndef COLOURSTRING_H_INCLUDED
#define COLOURSTRING_H_INCLUDED

#include "../../JuceLibraryCode/JuceHeader.h"

class ColourString
{
public:
    ColourString(const Colour& colour);
    
    ColourString(const String& colour);
    
    String getAsSectorFormatted() const;
    
    Colour getColour() const;
    
    bool isValid() const;
    
private:
    Colour colour;
    bool isValidated;
};


#endif  // COLOURSTRING_H_INCLUDED
