/*
  ==============================================================================

    SectorFileExporter.h
    Created: 12 Feb 2015 9:29:05pm
    Author:  Jim Mazur

  ==============================================================================
*/

#ifndef SECTORFILEEXPORTER_H_INCLUDED
#define SECTORFILEEXPORTER_H_INCLUDED

#include "../../JuceLibraryCode/JuceHeader.h"
#include "Sector.h"

//namespace sector
//{
//    class SectorContainer;
//    class RadioNavAid;
//    typedef NamedLineDiagram;
//}

class SectorFileExporter    :   public Thread
{
public:
    SectorFileExporter (const File& fileToWrtite);
    
    void run ();
    
private:
    void writeSector();
    
    void writeNotes();
    void writeDefines();
    void writeInfo();
    
    void writeVORs();
    void writeNDBs();
    void writeRadioNavAid(const sector::RadioNavAid& navaid, const int& namelength);
    
    void writeAirports();
    void writeRunways();
    void writeFixes();
    
    void writeARTCC();
    void writeARTCC_Highs();
    void writeARTCC_Lows();
    void writeHighAirways();
    void writeLowAirways();
    void writeNamedLineDiagrams (const sector::NamedLineDiagramsContainer& container);
    void writeSID_Diagrams ();
    void writeSTAR_Diagrams ();
    void writeSID_STAR_Diagrams (const sector::SID_STAR_Container& container);
    void writeGeoLines ();
    void writeRegions ();
    void writeLabel ();
    
    void writeNewLine ();
    void writeLine (const String& textToWrite);
    void writeText (const String& textToWrite);
    
    String addTrailingSpaces (const String& text, const int& fillUntilTotalCharAre);
    String addLeadingZeros (const String& text, const int& fillUntilTotalLength);
    String formatSectorLine (const sector::Line& line);
    String formatSectorColour (const sector::NamedColour& colour);
    
private:
    sector::SectorContainer* sector;
    ScopedPointer<FileOutputStream> filestream;
    
};


#endif  // SECTORFILEEXPORTER_H_INCLUDED
