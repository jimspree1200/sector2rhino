/*
  ==============================================================================

    SectorFileExporter.cpp
    Created: 12 Feb 2015 9:29:05pm
    Author:  Jim Mazur

  ==============================================================================
*/

#include "SectorFileExporter.h"
#include "../model/Sector.h"
#include "../Logger/SectorLogger.h"
#include "Application.h"
#include "ColourString.h"
#include "CoordinatesString.h"

using namespace sector;

SectorFileExporter::SectorFileExporter (const File& fileToWrite)
:
Thread("Sector File Exporter Thread")
{
    sector = &Sector2RhinoApplication::getSector();
    
    filestream = new FileOutputStream(fileToWrite);
    if (!filestream->openedOk())
    {
        SectorLogger::addError("Could not open file: " + fileToWrite.getFullPathName());
        return;
    }
    
    startThread();
}

void SectorFileExporter::run()
{
    writeSector();
}

void SectorFileExporter::writeSector()
{
    SectorLogger::addInfo("Starting to write Sector...");
    writeNotes();
    writeDefines();
    writeInfo();
    writeVORs();
    writeNDBs();
    writeAirports();
    writeRunways();
    writeFixes();
    writeARTCC();
    writeARTCC_Highs();
    writeARTCC_Lows();
    writeHighAirways();
    writeLowAirways();
    writeSID_Diagrams();
    writeSTAR_Diagrams();
    writeGeoLines();
    writeRegions();
    writeLabel();
}

void SectorFileExporter::writeNotes()
{
    writeText(sector->getNotes());
}

void SectorFileExporter::writeDefines()
{
    NamedColours* colours = sector->getNamedColours();
    
    if (colours->size() == 0)
        return;
    
    SectorLogger::addInfo("Writing colour definitions...");
    
    for (int i = 0; i < colours->size(); i++)
    {
        NamedColour colour = colours->at(i);
        
        if (colour.hasPrecedingNotes())
            writeText(colour.getPrecedingNotes());
        
        String line;
        line << "#define ";
        line << colour.getName() + " ";
        line << ColourString(colour.getColour()).getAsSectorFormatted();
        
        if (colour.hasNotes())
            line << " ; " + colour.getNotes();
        
        writeLine(line);
    }
    
}

void SectorFileExporter::writeInfo()
{
    InfoBlock* info = sector->getInfo();
    
    CoordinatesString coords(info->getDefaultCenterPoint());
    
    writeNewLine();
    writeLine("[INFO]");
    writeLine(info->getName());
    writeLine(info->getDefaultCallsign());
    writeLine(info->getDefaultAirport());
    writeLine(coords.toSectorLatitude());
    writeLine(coords.toSectorLongitude());
    writeLine(String(info->getMilesPerDegreeLatitude()));
    writeLine(String(info->getMilesPerDegreeLongitude()));
    writeLine(String(info->getMagneticVariation()));
    writeLine(String(info->getSectorScale()));
    
    writeNewLine();
}

void SectorFileExporter::writeVORs()
{
    VORs* vors = sector->getVORs();
    
    if (vors->size() == 0)
        return;
    
    SectorLogger::addInfo("Writing VORs...");
    
    writeLine("[VOR]");
    
    const int length = vors->lengthOfLargestName();
    
    for (int i = 0; i < vors->size(); i++)
    {
        writeRadioNavAid(vors->at(i), length);
    }
    
    writeNewLine();
}

void SectorFileExporter::writeNDBs()
{
    NDBs* ndbs = sector->getNDBs();
    
    if (ndbs->size() == 0)
        return;
    
    SectorLogger::addInfo("Writing NDBs...");

    writeLine("[NDB]");
    
    const int length = ndbs->lengthOfLargestName();
    
    for (int i = 0; i < ndbs->size(); i++)
    {
        writeRadioNavAid(ndbs->at(i), length);
    }
    
    writeNewLine();
}

void SectorFileExporter::writeRadioNavAid(const sector::RadioNavAid &navaid, const int& namelength)
{
    if (navaid.hasPrecedingNotes())
        writeText(navaid.getPrecedingNotes());
    
    String line;
    line << addTrailingSpaces(navaid.getName(), namelength) << " ";
    line << addTrailingSpaces(navaid.getFrequency(), 7) << " ";
    line << CoordinatesString(navaid.getCoordinate()).toSectorCoordinates();
    if (navaid.hasNotes())
        line << " ; " + navaid.getNotes();
    
    writeLine(line);
}

void SectorFileExporter::writeAirports()
{
    Airports* airports = sector->getAirportss();
    
    if (airports->size() == 0)
        return;
    
    SectorLogger::addInfo("Writing Airports...");
    
    writeLine("[AIRPORT]");
    
    const int length = airports->lengthOfLargestName();
    
    for (int i = 0; i < airports->size(); i++)
    {
        Airport airport = airports->at(i);
        
        if (airport.hasPrecedingNotes())
            writeText(airport.getPrecedingNotes());
        
        String line;
        line << addTrailingSpaces(airport.getName(), length) << " ";
        line << addTrailingSpaces(airport.getFrequency(), 7) << " ";
        line << CoordinatesString(airport.getCoordinate()).toSectorCoordinates() + " ";
        line << airport.getAirspaceClass();
        
        if (airport.hasNotes())
            line << " ; " + airport.getNotes();
        
        writeLine(line);
    }
    
    writeNewLine();
}

void SectorFileExporter::writeRunways()
{
    Runways* runways = sector->getRunways();
    
    if (runways->size() == 0)
        return;
    
    SectorLogger::addInfo("Writing Runways...");
    
    writeLine("[RUNWAY]");
    
    for (int i = 0; i < runways->size(); i++) {
        const Runway* runway = &runways->getReference(i);
        
        if (runway->hasPrecedingNotes())
            writeText(runway->getPrecedingNotes());
        
        String line;
        line << addTrailingSpaces(runway->getRunwayNumber(), 3) << " ";
        line << addTrailingSpaces(runway->getRunwayNumberOppositeEnd(), 3) << " ";
        line << addTrailingSpaces(String(runway->getMagneticHeading()), 3) << " ";
        line << addTrailingSpaces(String(runway->getMagneticHeadingOppositeEnd()), 3) << " ";
        line << CoordinatesString(runway->getRunwayStart()).toSectorCoordinates() << " ";
        line << CoordinatesString(runway->getRunwayEnd()).toSectorCoordinates() << " ";
        
        if (runway->hasNotes())
            line << " ; " + runway->getNotes();
        
        writeLine(line);
    }
    
    writeNewLine();
}

void SectorFileExporter::writeFixes()
{
    Fixes* fixes = sector->getFixes();
    
    if (fixes->size() == 0)
        return;
    
    SectorLogger::addInfo("Writing Fixes...");
    
    writeLine("[FIXES]");
    
    const int nameLength = fixes->lengthOfLargestName();
    const NavAid* fix;
    
    for (int i = 0; i < fixes->size(); i++)
    {
        fix = &fixes->at(i);
        
        if (fix->hasPrecedingNotes())
            writeText(fix->getPrecedingNotes());
        
        String line;
        line << addTrailingSpaces(fix->getName(), nameLength) << " ";
        line << CoordinatesString(fix->getCoordinate()).toSectorCoordinates() << " ";
        
        if (fix->hasNotes())
            line << " ; " << fix->getNotes();
        
        writeLine(line);
    }
    writeNewLine();
}

void SectorFileExporter::writeARTCC()
{
    ArtccContainer* container = sector->getArtccBoundaries();
    
    if (container->size() == 0)
        return;
    
    SectorLogger::addInfo("Writing ARTCC Boundaries...");
    
    writeLine("[ARTCC]");
    
    writeNamedLineDiagrams(*container);
}

void SectorFileExporter::writeARTCC_Highs()
{
    ArtccContainer* container = sector->getArtccHighBoundaries();
    
    if (container->size() == 0)
        return;
    
    SectorLogger::addInfo("Writing ARTCC High Boundaries...");
    
    writeLine("[ARTCC HIGH]");
    
    writeNamedLineDiagrams(*container);
}

void SectorFileExporter::writeARTCC_Lows()
{
    ArtccContainer* container = sector->getArtccLowBoundaries();
    
    if (container->size() == 0)
        return;
    
    SectorLogger::addInfo("Writing ARTCC Low Boundaries...");
    
    writeLine("[ARTCC LOW]");
    
    writeNamedLineDiagrams(*container);
}

void SectorFileExporter::writeHighAirways()
{
    ArtccContainer* container = sector->getHighAirways();
    
    if (container->size() == 0)
        return;
    
    SectorLogger::addInfo("Writing High Airways...");
    
    writeLine("[HIGH AIRWAY]");
    
    writeNamedLineDiagrams(*container);
}

void SectorFileExporter::writeLowAirways()
{
    ArtccContainer* container = sector->getLowAirways();
    
    if (container->size() == 0)
        return;
    
    SectorLogger::addInfo("Writing ARTCC Boundaries...");
    
    writeLine("[LOW AIRWAY]");
    
    writeNamedLineDiagrams(*container);
}

void SectorFileExporter::writeNamedLineDiagrams(const sector::NamedLineDiagramsContainer &container)
{
    for (int i = 0; i < container.size(); i++)
    {
        const NamedLineDiagram* diagram = &container.atDiagram(i);
        String name = addTrailingSpaces(diagram->getName(), 26);
        
        for (int i = 0; i < diagram->size(); i++)
        {
            const sector::Line* sectorline = &diagram->atLine(i);
            
            if (sectorline->hasPrecedingNotes())
                writeText(sectorline->getPrecedingNotes());
            
            String line;
            
            line << name << " ";
            
            line << formatSectorLine(*sectorline);
            
            if (sectorline->hasNotes())
                line << " ; " << sectorline->getNotes();
            
            writeLine(line);
        }
    }
    writeNewLine();
}

void SectorFileExporter::writeSID_Diagrams()
{
    SID_STAR_Container* container = sector->getSIDs();
    
    if (container->size() == 0)
        return;
    
    SectorLogger::addInfo("Writing SID Diagrams...");
    
    writeLine("[SID]");
    
    writeSID_STAR_Diagrams(*container);
}

void SectorFileExporter::writeSTAR_Diagrams()
{
    SID_STAR_Container* container = sector->getSTARs();
    
    if (container->size() == 0)
        return;
    
    SectorLogger::addInfo("Writing STAR Diagrams...");
    
    writeLine("[STAR]");
    
    writeSID_STAR_Diagrams(*container);
}

void SectorFileExporter::writeSID_STAR_Diagrams(const sector::SID_STAR_Container &container)
{
    for (int i = 0; i < container.size(); i++)
    {
        const NamedColouredLineDiagram* diagram = &container.atDiagram(i);
        
        for (int j = 0; j < diagram->size(); j++)
        {
            const ColouredLine* sectorLine = &diagram->atLine(j);
            
            if (sectorLine->hasPrecedingNotes())
                writeText(sectorLine->getPrecedingNotes());
            
            String line;
            
            // Only include name on first line
            if ( j == 0 )
                line << addTrailingSpaces(diagram->getName(), 26) << " ";
            else
                line << addTrailingSpaces(String::empty, 26) << " ";
            
            line << formatSectorLine(*sectorLine);
            
            if (sectorLine->hasNamedColour())
            {
                NamedColour colour = sectorLine->getNamedColour();
                line << " " << formatSectorColour(colour);
            }
            
            if (sectorLine->hasNotes())
                line << " ; " << sectorLine->getNotes();
            
            writeLine(line);
        }
    }
}

void SectorFileExporter::writeGeoLines()
{
    const GeoLinesContainer* container = sector->getGeoLines();
    
    if (container->size() == 0)
        return;
    
    SectorLogger::addInfo("Writing GEO Diagrams...");
    
    writeLine("[GEO]");
    
    for (int i = 0; i < container->size(); i++)
    {
        const ColouredLine* cl = &container->getReference(i);
        
        if (cl->hasPrecedingNotes())
            writeText(cl->getPrecedingNotes());
        
        String line;
        line << formatSectorLine(*cl);
        line << " " << formatSectorColour(cl->getNamedColour());
        
        if (cl->hasNotes())
            line << " ; " << cl->getNotes();
        
        writeLine(line);
    }
}

void SectorFileExporter::writeRegions()
{
    const Regions* container = sector->getRegions();
    
    if (container->size() == 0)
        return;
    
    SectorLogger::addInfo("Writing Regions...");
    
    writeLine("[REGIONS]");
    
    for (int i = 0; i < container->size(); i++)
    {
        const Region* region = &container->getReference(i);
        
        for (int j = 0; j < region->size(); j++)
        {
            const CoordinatesWithNotes* coords = &region->getReference(j);
            
            if (coords->hasPrecedingNotes())
                writeText(coords->getPrecedingNotes());
            
            String line;
            
            if (j == 0)
            {
                String colour = formatSectorColour(region->getColour());
                line << addTrailingSpaces(colour, 26) << " ";
            }
            else
                line << addTrailingSpaces(String::empty, 26) << " ";
            
            line << CoordinatesString(*coords).toSectorCoordinates();
            
            if (coords->hasNotes())
                line << " ; " << coords->getNotes();
            
            writeLine(line);
        }
        
    }
}


void SectorFileExporter::writeLabel()
{
    const Labels* container = sector->getLabels();
    
    if (container->size() == 0)
        return;
    
    SectorLogger::addInfo("Writing Labels...");
    
    writeLine("[LABELS]");
    
    for (int i = 0; i < container->size(); i++)
    {
        const sector::Label* label = &container->getReference(i);
        
        if (label->hasPrecedingNotes())
            writeText(label->getPrecedingNotes());
        
        String line;
        line << label->getName() << " ";
        line << CoordinatesString(label->getNamedCoordinate().getCoordinate()).toSectorCoordinates() << " ";
        line << formatSectorColour(label->getColour());
        
        if (label->hasNotes())
            line << " " << label->getNotes();
        
        writeLine(line);
    }
}

void SectorFileExporter::writeNewLine()
{
    filestream->writeText(newLine, false, false);
}

void SectorFileExporter::writeLine(const juce::String &textToWrite)
{
    filestream->writeText(textToWrite, false, false);
    writeNewLine();
}

void SectorFileExporter::writeText(const juce::String &textToWrite)
{
    filestream->writeText(textToWrite, false, false);
}

String SectorFileExporter::addTrailingSpaces(const juce::String &text, const int &fillUntilTotalCharAre)
{
    if (text.length() >= fillUntilTotalCharAre)
        return text;
    
    String s = text;

    while (s.length() < fillUntilTotalCharAre)
        s.append(" ", 1);
    
    return s;
}

String SectorFileExporter::addLeadingZeros(const juce::String &text, const int &fillUntilTotalLength)
{
    if (text.length() >= fillUntilTotalLength)
        return text;
    
    String s = text;
    
    while (s.length() < fillUntilTotalLength)
        s = "0" + s;
    
    return s;
}

String SectorFileExporter::formatSectorLine(const sector::Line &line)
{
    String s;
    
    if (line.getStart().hasName())
        s << addTrailingSpaces(line.getStart().getName(), 14) << " "
        << addTrailingSpaces(line.getStart().getName(), 14) << " ";
    else
        s << CoordinatesString(line.getStart().getCoordinate()).toSectorCoordinates()
        << " ";
    
    if (line.getEnd().hasName())
        s << addTrailingSpaces(line.getEnd().getName(), 14) << " "
        << addTrailingSpaces(line.getEnd().getName(), 14) << " ";
    else
        s << CoordinatesString(line.getEnd().getCoordinate()).toSectorCoordinates()
        << " ";
    
    return s;
}

String SectorFileExporter::formatSectorColour(const sector::NamedColour &colour)
{
    if (colour.hasName())
    {
        return colour.getName();
    }
    else
    {
        return ColourString(colour.getColour()).getAsSectorFormatted();
    }
}

