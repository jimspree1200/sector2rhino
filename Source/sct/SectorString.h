/*
  ==============================================================================

    SectorString.h
    Created: 7 Feb 2015 10:06:28am
    Author:  Jim Mazur

  ==============================================================================
*/

#ifndef SECTORSTRING_H_INCLUDED
#define SECTORSTRING_H_INCLUDED

#include "../../JuceLibraryCode/JuceHeader.h"
#include "../model/SectorTypes.h"

// Forward declaration
namespace sector
{
    class SectorContainer;
}

class SectorString : public String
{
public:
    SectorString ();
    
    SectorString (const String& string);
    
    SectorString (const String& string, const bool& useStrongTypes);
    
    SectorString (const String& string, const sector::SectorContainer* sector);
    
    //==============================================================================
    // Constructors from SectorTypes...
    
    SectorString(const sector::Coordinates& coord);
    
    SectorString(const sector::NamedColour& colour);
    
    SectorString(const sector::InfoBlock& info);
    
    SectorString(const sector::Airport& airport);
    
    SectorString(const sector::RadioNavAid& navaid);
    
    SectorString(const sector::NavAid& fix);
    
    
    //==============================================================================
    // Conversion operators..
    
    operator sector::NamedColour () const;
    operator sector::Airport () const;
    operator sector::Runway () const;
    operator sector::RadioNavAid () const;
    operator sector::NavAid() const;
    
    //==============================================================================
    // General purpose functions...
    
    bool usingStrongTypes() const;
    
    /** Checks if the String is a whitespace or comment only */
    bool isWhitespaceOrComment() const;
    
    /** Tests if the SectorString containts notes. Notes always start with a ';'
     character. Will only return true if the note delimiter is followed by a word
     or words:
     ";" = false
     ";word" = true
     "; word" = true
     */
    bool hasNotes() const;
    
    /** Returns the notes contained in the Sector String */
    String getNotes() const;
    
    /** Returns string without notes */
    String getWithoutNotes () const;
    
    //==============================================================================
    // Coordinate functions...
    
    /** Counts the occurances of coordinates within the Sector String
     
     This function will also not count any coordinates that exist as notes, for
     example:
     
     N044.38.00.290 W071.11.10.068 ; was: N044.38.00.291 W071.11.10.062
     
     will return 2.
     */
    int numberOfCoordinates() const;
    
    typedef Array<sector::NamedCoordinate> NamedCoordinateArray;
    
    /** Gets all the known coordinates from this SectorString, does not return
     coordinates that exist as notes
     */
    NamedCoordinateArray getAllCoordinates () const;
    
    /** Gets the last two coordinates as a Line from this SectorString.
     Does not include coordinates from the notes.
     */
    sector::NamedCoordinateLine getLastOccuranceOfLine () const;
    
    //==============================================================================
    // Section header functions...
    
    /** Tests if the SectorString is a section header (e.g. [INFO], [AIRPORT, etc...) */
    bool isSectionHeader();
    
    /** Enumerator referencing different Section Header types */
    enum class SectionHeaderType
    {
        None,
        Info,
        VOR,
        NDB,
        Airport,
        Runway,
        Fixes,
        ARTCC,
        ARTCC_High,
        ARTCC_Low,
        SID,
        STAR,
        LowAirway,
        HighAirway,
        Geo,
        Regions,
        Labels
    };
    
    /** Returns the header type, if the Sector String is a section header. Test for this with isSectionHeader() */
    SectionHeaderType getHeaderType();
    
    //==============================================================================
    // NamedColour functions...
    
    /** Tests if the SectorString is a defined colour. A defined Colour must have the syntax of:
     #define colourname 123
     */
    bool isNamedColour() const;
    
    /** Returns a NamedColour object, if the SectorString is formed as a #define */
    sector::NamedColour toNamedColour() const;
    
    //==============================================================================
    // Airport functions..
    
    /** Tests if the string contains a valid formated airport. A airport is formatted as:
     KBDL 120.300 N041.56.18.800 W072.40.57.000 C (plus optional notes)
     */
    bool isAirport() const;
    
    /** Returns a Airport object, if the SectorString is formed as a #define */
    sector::Airport toAirport() const;
    
    //==============================================================================
    // Runway functions..
    bool isRunway() const;
    
    sector::Runway toRunway() const;
    
    //==============================================================================
    // Radio NavAid functions..
    bool isVOR() const;
    
    bool isNDB() const;
    
    sector::RadioNavAid toRadioNavAid() const;
    
    //==============================================================================
    // NDB functions..
    bool isFix() const;
    
    sector::NavAid toNavAid() const;
    
    //==============================================================================
    // Airway functions..
    bool isAirway() const;
    
    String getAirwayName() const;
    
    sector::NamedLine toAirwayLine() const;
    
    //==============================================================================
    // ARTCC functions..
    bool isArtcc() const;
    
    String getArtccName() const;
    
    sector::NamedLine toArtccLine () const;
    
    //==============================================================================
    // SID / Star functions..
    
    bool isNew_SID_STAR() const;
    
    bool is_SID_STAR_Line() const;
    
    String getNew_SID_STAR_Name() const;
    
    sector::ColouredLine to_SID_Star_Line() const;
    
    //==============================================================================
    // Geography functions
    
    bool isGeoLine () const;
    
    sector::ColouredLine toGeoLine() const;
    
    //==============================================================================
    // Region functions
    
    bool isRegionStart() const;
    
    bool isRegionCoordinate() const;
    
    sector::NamedColour getRegionStartColour() const;
    
    sector::CoordinatesWithNotes toRegionCoordinates() const;
    
    //==============================================================================
    // Label functions
    
    bool isLabel() const;
    
    sector::Label toLabel () const;
    
    
private:
    /** Tests if the text is a valid frequency, it must contain a '.' and besides that only numbers. */
    bool isFrequency(const String& text) const;
    
    String getFirstWord() const;
    
    bool startsWithWhitespace () const;
    
    //==============================================================================
    
    bool isCoordinate (const String& text) const;
    
    bool isLatitudeCoordinate (const String& text) const;
    
    bool isLongitudeCoordinate (const String& text) const;
    
    bool isNamedCoordinate (const String& text) const;
    
    bool isCoordinates (const String& latitude, const String& longitude) const;
    
    bool isCoordinatePair(const String& text) const;
    
    bool isNamedCoordinatePair(const String& text) const;
    
    //==============================================================================
    
    bool isNameAndTwoCoordinates() const;
    
    String getLatitudeFor (const String& navaid) const;
    
    String getLongitudeFor (const String& navaid) const;
    
    sector::NamedCoordinate getNamedCoordinateFor (const String& sectorCoordinateLat, const String& sectorCoordinateLon) const;
    
    //==============================================================================
    
    bool isColour (const String& text) const;
    
    bool isNamedColour (const String& text) const;
    
    sector::NamedColour getNamedColourFor (const String& text) const;
    
private:
    const sector::SectorContainer* sector;
    bool strongTypes;
};


#endif  // SECTORSTRING_H_INCLUDED
