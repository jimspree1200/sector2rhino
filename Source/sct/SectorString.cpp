/*
  ==============================================================================

    SectorString.cpp
    Created: 7 Feb 2015 10:06:28am
    Author:  Jim Mazur

  ==============================================================================
*/

#include "SectorString.h"
#include "StringFunctions.h"
#include "ColourString.h"
#include "CoordinatesString.h"
#include "../model/Sector.h"
#include "../Logger/SectorLogger.h"

using namespace sector;

//==============================================================================
// SectorString
//==============================================================================

SectorString::SectorString()
:
sector(nullptr),
strongTypes(false)
{}

SectorString::SectorString (const String& string)
:
String(string),
sector(nullptr),
strongTypes(false)
{}

SectorString::SectorString (const String& string, const bool& useStrongTypes)
:
String(string),
sector(nullptr),
strongTypes(useStrongTypes)
{}

SectorString::SectorString (const String& string, const SectorContainer* sector)
:
String(string),
sector(sector),
strongTypes(false)
{}

bool SectorString::usingStrongTypes() const
{
    return strongTypes;
}

bool SectorString::isWhitespaceOrComment() const
{
    if (!this->containsNonWhitespaceChars())
        return true;
    
    if (this->trimStart().startsWithChar(';'))
        return true;
    
    return false;
}

SectorString::SectorString(const Coordinates& coord)
{
    
}

//==============================================================================

bool SectorString::hasNotes() const
{
    if (!this->containsChar(';'))
        return false;
    
    String trimmed = this->trim();
    
    int remainingCharacters = trimmed.substring(trimmed.indexOfChar(';')).length();
    
    // don't count the ';'
    remainingCharacters--;
    
    if (remainingCharacters)
        return true;
    
    return false;
}

String SectorString::getNotes() const
{
    jassert(hasNotes());
    
    if (!hasNotes())
        return String();
    
    String trimmed = this->trim();
    
    return trimmed.substring(trimmed.indexOfChar(';') + 1).trimStart();
}

String SectorString::getWithoutNotes() const
{
    if (hasNotes())
        return this->substring(0, this->indexOfChar(';')).trim();
    
    return *this;
}

//==============================================================================

int SectorString::numberOfCoordinates() const
{
    if (isWhitespaceOrComment())
        return 0;
    
    return getAllCoordinates().size();
}

SectorString::NamedCoordinateArray SectorString::getAllCoordinates() const
{
    NamedCoordinateArray array;
    
    if (isWhitespaceOrComment())
        return array;
    
    StringArray words = getWhitespaceSeparatedItems(getWithoutNotes());
    
    while (words.size() >= 2)
    {
        if (isLatitudeCoordinate(words[0]) && isLongitudeCoordinate(words[1]))
        {
            NamedCoordinate nc = getNamedCoordinateFor(words[0], words[1]);
            array.add(nc);
            words.removeRange(0, 2);
        }
        else
            words.remove(0);
    }
    
    return array;
}



sector::NamedCoordinateLine SectorString::getLastOccuranceOfLine() const
{
    jassert(numberOfCoordinates() >= 2);
    
    using sector::NamedCoordinateLine;
    
    StringArray words = getWhitespaceSeparatedItems(getWithoutNotes());
    
    // Not enough words to be an actualy lat lon lat lon sequence
    if (words.size() < 4)
        return NamedCoordinateLine();
    
    NamedCoordinateArray array(getAllCoordinates());
    
    if (array.size() < 2)
        return NamedCoordinateLine();
    
    int endIndex = array.size() - 1;
    
    NamedCoordinateLine ncl;
    ncl.setStart(array[endIndex - 1]);
    ncl.setEnd(array[endIndex]);
    
    return ncl;
}

//==============================================================================

bool SectorString::isSectionHeader()
{
    if (this->isWhitespaceOrComment())
        return false;
    
    if (!this->trimStart().startsWithChar('['))
        return false;
    
    if (getHeaderType() == SectionHeaderType::None)
        return false;
    
    return true;
}

SectorString::SectionHeaderType SectorString::getHeaderType()
{
    String text(this->trim());
    text = text.toUpperCase();
    
    if (text.startsWith("[INFO]"))          return SectionHeaderType::Info;
    if (text.startsWith("[VOR]"))           return SectionHeaderType::VOR;
    if (text.startsWith("[NDB]"))           return SectionHeaderType::NDB;
    if (text.startsWith("[AIRPORT]"))       return SectionHeaderType::Airport;
    if (text.startsWith("[RUNWAY]"))        return SectionHeaderType::Runway;
    if (text.startsWith("[FIXES]"))         return SectionHeaderType::Fixes;
    if (text.startsWith("[ARTCC]"))         return SectionHeaderType::ARTCC;
    if (text.startsWith("[ARTCC HIGH]"))    return SectionHeaderType::ARTCC_High;
    if (text.startsWith("[ARTCC LOW]"))     return SectionHeaderType::ARTCC_Low;
    if (text.startsWith("[SID]"))           return SectionHeaderType::SID;
    if (text.startsWith("[STAR]"))          return SectionHeaderType::STAR;
    if (text.startsWith("[LOW AIRWAY]"))    return SectionHeaderType::LowAirway;
    if (text.startsWith("[HIGH AIRWAY]"))   return SectionHeaderType::HighAirway;
    if (text.startsWith("[GEO]"))           return SectionHeaderType::Geo;
    if (text.startsWith("[REGIONS]"))       return SectionHeaderType::Regions;
    if (text.startsWith("[LABELS]"))        return SectionHeaderType::Labels;
    
    return SectionHeaderType::None;
}

//==============================================================================

bool SectorString::isNamedColour() const
{
    if (this->isWhitespaceOrComment())
        return false;
    
    if (!this->trimStart().startsWithChar('#'))
        return false;
    
    StringArray words = getWhitespaceSeparatedItems(getWithoutNotes());
    
    // Must be three words in a #define (e.g. #define Black 0)
    if (words.size() < 3)
        return false;
    
    if (words[0] != "#define")
        return false;
    
    if (usingStrongTypes())
    {
        // Make sure that that the color is represented numerically
        if (!isNumber(words[2]))
            return false;
        
        // Make sure the color is within valid range of a 24bit int ((BLUE x 65536) + (GREEN x 256) + RED)
        if (words[2].getIntValue() > 16777215)
            return false;
    }
    
    return true;
    
}

NamedColour SectorString::toNamedColour() const
{
    NamedColour colour;
    
    StringArray words = getWhitespaceSeparatedItems(getWithoutNotes());
    
    colour.setName(words[1]);
    colour.setColour(ColourString(words[2]).getColour());
    
    if (hasNotes())
        colour.addNotes(getNotes());
    
    return colour;
}

SectorString::operator NamedColour() const
{
    return this->toNamedColour();
}

//==============================================================================

bool SectorString::isAirport() const
{
    if (this->isWhitespaceOrComment())
        return false;
    
    StringArray words = getWhitespaceSeparatedItems(getWithoutNotes());
    
    // Airport requires 5 words, e.g. KBDL 120.300 N041.56.18.800 W072.40.57.000 C
    if (words.size() < 4)
        return false;
    
    // VRC allows airport class to be obmitted
    if (usingStrongTypes())
    {
        if (words.size() < 5)
            return false;
    }
    
    // Next two words should be coordinates, e.g. 120.300 N041.56.18.800 W072.40.57.000
    if (!isCoordinates(words[2], words[3]))
        return false;
    
    if (usingStrongTypes())
    {
        // Airspace class should only be one character
        if (words[4].length() != 1)
            return false;
    }
    
    return true;
}

SectorString::operator Airport() const
{
    return this->toAirport();
}

Airport SectorString::toAirport() const
{
    Airport airport;
    
    StringArray words = getWhitespaceSeparatedItems(getWithoutNotes());
    
    airport.setName(words.getReference(0));
    airport.setFrequency(words.getReference(1));
    airport.setCoordinate(CoordinatesString(words.getReference(2), words.getReference(3)).toCoordinates());
    
    if (words.size() > 4)
        airport.setAirspaceClass(words.getReference(4).toUpperCase());
    else
        airport.setAirspaceClass("X");
    
    
    if (hasNotes())
    {
        airport.addNotes(getNotes());
    }
    
    return airport;
}

//==============================================================================

bool SectorString::isRunway() const
{
    if (this->isWhitespaceOrComment())
        return false;
    
    StringArray words = getWhitespaceSeparatedItems(getWithoutNotes());
    
    // Reference:
    // 02  20  000 000 N041.44.36.974 W072.38.50.204 N041.43.53.883 W072.38.59.351 ;note
    // 0   1   2   3   4              5              6              7              8...
    
    if (words.size() < 8)
        return false;
    
    if (usingStrongTypes())
    {
        // Headings should be numbers
        if (isNotNumber(words[2]) || isNotNumber(words[3]))
            return false;
    }
    
    // First coordinate pair check (start of runway)
    if (!isCoordinates(words[4], words[5]))
        return false;
    
    // Second coordinate pair check (end of runway)
    if (!isCoordinates(words[6], words[7]))
        return false;
    
    return true;
}

Runway SectorString::toRunway() const
{
    Runway runway;
    
    StringArray words = getWhitespaceSeparatedItems(getWithoutNotes());
    
    // Reference:
    // 02  20  000 000 N041.44.36.974 W072.38.50.204 N041.43.53.883 W072.38.59.351 ;note
    // 0   1   2   3   4              5              6              7              8
    /*
     0 - Runway number with an optional L, R, or C for Left, Right or Center.
     1 - Runway number for the opposite end.
     2 - Magnetic runway heading.
     3 - Magnetic runway heading for the opposite end.
     4 - Latitude of the runway start point.
     5 - Longitude of the runway start point.
     6 - Latitude of the runway end point.
     7 - Longitude of the runway end point.
     */
    
    runway.setRunwayNumber(words[0]);
    runway.setRunwayNumberOppositeEnd(words[1]);
    runway.setMagneticHeading(words[2].getIntValue());
    runway.setMagneticHeadingOppositeEnd(words[3].getIntValue());
    runway.setRunwayStart(CoordinatesString(words[4] + " " + words[5]).toCoordinates());
    runway.setRunwayEnd(CoordinatesString(words[6] + " " + words[7]).toCoordinates());
    
    if (hasNotes())
        runway.addNotes(getNotes());
    
    return runway;
}

SectorString::operator Runway() const
{
    return this->toRunway();
}

//==============================================================================

bool SectorString::isVOR() const
{
    StringArray words = getWhitespaceSeparatedItems(getWithoutNotes());
    
    /** Reference:
     ACK 116.20  N041.16.54.794 W070.01.36.157 ;NANTUCKET VOR/DME
     0   1       2              3              4
     */
    
    // Should be 4 words
    if (words.size() < 4)
        return false;
    
    if (usingStrongTypes())
    {
        // VOR's only have alphanumeric ids and should not be more than 3 letters
        if (isNumber(words[0]) && words[0].length() > 3)
            return false;
        
        // Next word should be a frequency
        if (!isFrequency(words[1]))
            return false;
    }
    
    // Followed by 2 coordinates
    if (!isCoordinates(words[2], words[3]))
        return false;
    
    return true;
}

bool SectorString::isNDB() const
{
    StringArray words = getWhitespaceSeparatedItems(getWithoutNotes());
    
    /** Reference:
     AL    219.000 N042.49.02.280 W073.48.30.636 ;HAWKY
     0     1       2              3              4
     */
    
    // Should be 4 words
    if (words.size() < 4)
        return false;
    
    if (usingStrongTypes())
    {
        // Next word should be a frequency
        if (!isFrequency(words[1]))
            return false;
    }
    
    // Followed by 2 coordinates
    if (!isCoordinates(words[2], words[3]))
        return false;
    
    return true;
}

RadioNavAid SectorString::toRadioNavAid() const
{
    RadioNavAid radioNavAid;
    
    StringArray words = getWhitespaceSeparatedItems(getWithoutNotes());
    
    /** Reference:
     ACK 116.20  N041.16.54.794 W070.01.36.157 ;NANTUCKET VOR/DME
     0   1       2              3              4
     */
    
    radioNavAid.setName(words[0]);
    radioNavAid.setFrequency(words[1]);
    radioNavAid.setCoordinate(CoordinatesString(words[2] + " " + words[3]).toCoordinates());
    
    if (hasNotes())
        radioNavAid.addNotes(getNotes());
    
    return radioNavAid;
}

SectorString::operator RadioNavAid () const
{
    return this->toRadioNavAid();
}

//==============================================================================

bool SectorString::isFix() const
{
    StringArray words = getWhitespaceSeparatedItems(getWithoutNotes());
    
    /** Reference:
     18THR N042.14.25.609 W071.50.20.580  ;note
     0     1              2               3
     */
    
    // Should be 3 words
    if (words.size() < 3)
        return false;
    
    // Followed by 2 coordinates
    if (!isCoordinates(words[1], words[2]))
        return false;
    
    return true;
}

NavAid SectorString::toNavAid() const
{
    NavAid navAid;
    
    StringArray words = getWhitespaceSeparatedItems(getWithoutNotes());
    
    navAid.setName(words[0]);
    navAid.setCoordinate(CoordinatesString(words[1] + " " + words[2]).toCoordinates());
    
    if (hasNotes())
        navAid.addNotes(getNotes());
    
    return navAid;
}

SectorString::operator NavAid() const
{
    return toNavAid();
}

//==============================================================================

bool SectorString::isAirway() const
{
    return isNameAndTwoCoordinates();
}

String SectorString::getAirwayName() const
{
    // Should only be called on an actual Airway
    jassert(isAirway());
    
    return getFirstWord();
}

NamedLine SectorString::toAirwayLine() const
{
    jassert(isAirway());
    
    NamedLine airway;
    airway.setName(getAirwayName());
    airway.setLine(getLastOccuranceOfLine());
    
    if (hasNotes())
        airway.addNotes(getNotes());
    
    return airway;
}

//==============================================================================

bool SectorString::isArtcc() const
{
    return isNameAndTwoCoordinates();
}

String SectorString::getArtccName() const
{
    return getFirstWord();
}

NamedLine SectorString::toArtccLine() const
{
    jassert(isArtcc());
    
    return toAirwayLine();
}

//==============================================================================

bool SectorString::isNew_SID_STAR() const
{
    if (isWhitespaceOrComment())
        return false;
    
    // All names must start with a real char
    if (startsWithWhitespace())
        return false;
    
    if (getWithoutNotes().length() <= 26)
        return false;
    
    StringArray words = getWhitespaceSeparatedItems(getWithoutNotes().substring(26));
    
    if (words.size() < 4)
        return false;
    
    if (usingStrongTypes())
    {
        if (!isCoordinates(words[0], words[1]) && !isCoordinates(words[0], words[1]))
            return false;
    }
    
    return true;
}

bool SectorString::is_SID_STAR_Line() const
{
    if (isWhitespaceOrComment())
        return false;
    
    if (!startsWithWhitespace())
        return false;
    
    StringArray words = getWhitespaceSeparatedItems(getWithoutNotes());
    
    if (words.size() < 4)
    {
        SectorLogger::addError("SID / STAR lines must have 4 space seperated coordinates");
        return false;
    }
    
    if (numberOfCoordinates() < 2)
    {
        SectorLogger::addError("Could not find 2 coordinates to create line");
        return false;
    }
    
    if (!isCoordinates(words[0], words[1]) && !isCoordinates(words[2], words[3]))
        return false;
    
    return true;
}

String SectorString::getNew_SID_STAR_Name() const
{
    jassert(isNew_SID_STAR());
    
    return this->substring(0, 25).trimEnd();
}

sector::ColouredLine SectorString::to_SID_Star_Line() const
{
    jassert(isNew_SID_STAR() || is_SID_STAR_Line());
    
    String text = getWithoutNotes();
    
    // Strip name
    if (isNew_SID_STAR())
        text = text.substring(26);
    
    StringArray words = getWhitespaceSeparatedItems(text);
    
    ColouredLine cl;
    
    cl.setLine(getLastOccuranceOfLine());
    
    // Get colour
    if (words.size() > 4)
    {
        // Is it a defined named colour?
        if (isNamedColour(words[4]))
            cl.setNamedColour(sector->getNamedColourFor(words[4]));
        
        // If not, should be a sector formated colour
        if (isNumber(words[4]))
             cl.setNamedColour(NamedColour(ColourString(words[4]).getColour()));
    }
    
    if (hasNotes())
        cl.addNotes(getNotes());
    
    return cl;
}

//==============================================================================

bool SectorString::isGeoLine() const
{
    if (isWhitespaceOrComment())
        return false;
    
    StringArray words = getWhitespaceSeparatedItems(getWithoutNotes());
    
    if (words.size() < 5)
    {
        SectorLogger::addError("Geographic Lines must have 2 coordinates and a colour");
        return false;
    }
    
    if (!isCoordinates(words[0], words[1]) && !isCoordinates(words[2], words[3]))
    {
        SectorLogger::addError("Could not find 2 coordinates to form line");
        return false;
    }
    
    return true;
}


sector::ColouredLine SectorString::toGeoLine() const
{
    jassert(isGeoLine());
    
    StringArray words = getWhitespaceSeparatedItems(getWithoutNotes());
    
    ColouredLine cl;
    
    cl.setLine(getLastOccuranceOfLine());
    
    // Get colour
    if (words.size() > 4)
    {
        // Is it a defined named colour?
        if (isNamedColour(words[4]))
            cl.setNamedColour(sector->getNamedColourFor(words[4]));
        
        // If not, should be a sector formated colour
        if (isNumber(words[4]))
            cl.setNamedColour(NamedColour(ColourString(words[4]).getColour()));
    }
    
    if (hasNotes())
        cl.addNotes(getNotes());
    
    return cl;
}

//==============================================================================

bool SectorString::isRegionStart() const
{
    if (startsWithWhitespace())
        return false;
    
    StringArray words = getWhitespaceSeparatedItems(getWithoutNotes());
    
    if (words.size() < 3)
        return false;
    
    if (!isNamedColour(words[0]) && !isNumber(words[0]))
    {
        SectorLogger::addError("Could not find colour: " + words[0]);
        return false;
    }
    
    words.remove(0);
    
    if (!isCoordinates(words[0], words[1]))
    {
        SectorLogger::addError("Could not find coordinates to form line");
        return false;
    }
    
    words.removeRange(0, 2);
    
    if (words.size())
        SectorLogger::addWarning("Extra words in region ignored: " + words.joinIntoString(" "));
    
    return true;
}

bool SectorString::isRegionCoordinate() const
{
    if (!startsWithWhitespace())
        return false;
    
    StringArray words = getWhitespaceSeparatedItems(getWithoutNotes());
    
    if (words.size() < 2)
        return false;
    
    if (!isCoordinates(words[0], words[1]))
    {
        SectorLogger::addError("Could not find coordinates to form line");
        return false;
    }
    
    words.removeRange(0, 2);
    
    if (words.size())
        SectorLogger::addWarning("Extra words in region ignored: " + words.joinIntoString(" "));
    
    return true;
}

sector::NamedColour SectorString::getRegionStartColour() const
{
    jassert(isRegionStart());
    
    StringArray words = getWhitespaceSeparatedItems(*this);
    
    return getNamedColourFor(words.getReference(0));
}

sector::CoordinatesWithNotes SectorString::toRegionCoordinates() const
{
    jassert(isRegionStart() || isRegionCoordinate());
    
    StringArray words = getWhitespaceSeparatedItems(getWithoutNotes());
    
    if (isRegionStart())
        words.remove(0);
    
    CoordinatesWithNotes coords(CoordinatesString(words[0] + " " + words[1]).toCoordinates());
    
    if (hasNotes())
        coords.addNotes(getNotes());
    
    return coords;
}

//==============================================================================

bool SectorString::isLabel() const
{
    if (isWhitespaceOrComment())
        return false;
    
    StringArray words;
    words.addTokens(getWithoutNotes(), true);
    words.trim();
    words.removeEmptyStrings();
    
    if (words.size() < 4)
        return false;
    
    if (!words[0].startsWithChar('"') || !words[0].endsWithChar('"'))
        return false;
    
    if (!isCoordinates(words[1], words[2]))
        return false;
    
    if (!isColour(words[3]))
        return false;

    return true;
}

sector::Label SectorString::toLabel () const
{
    jassert(isLabel());
    
    StringArray words;
    words.addTokens(getWithoutNotes(), true);
    words.trim();
    words.removeEmptyStrings();
    
    sector::Label l;
    l.setName(words[0]);
    l.setCoordinate(CoordinatesString(words[1], words[2]).toCoordinates());
    l.setColour(getNamedColourFor(words[3]));
    
    return l;
}

//==============================================================================
// Private Member Functions
//==============================================================================

bool SectorString::isFrequency(const juce::String &text) const
{
    String frequency = text.trim();
    
    frequency = frequency.removeCharacters(".");
    
    // Should only be numbers (123321)
    if (isNotNumber(frequency))
        return false;
    
    return true;
}

String SectorString::getFirstWord() const
{
    if (isWhitespaceOrComment())
        return String();
    
    StringArray words = getWhitespaceSeparatedItems(getWithoutNotes());
    
    if (words.size() == 0)
        return String();
    
    return words[0];
}

bool SectorString::startsWithWhitespace() const
{
    // empty
    if (!this->length())
        return false;
    
    String s = this->substring(0, 1);
    
    return !s.containsNonWhitespaceChars();
}

//==============================================================================

bool SectorString::isCoordinate(const String &text) const
{
    // First check that it isn't already basic text coordinate
    if (CoordinatesString::isValidLatitudeOrLongitude(text))
        return true;
    
    if (isNamedCoordinate(text))
        return true;
    
    return false;
}

bool SectorString::isLatitudeCoordinate(const String &text) const
{
    if (CoordinatesString::isValidLatitudeCoordinate(text))
        return true;
    
    if (isNamedCoordinate(text))
        return true;
    
    return false;
}

bool SectorString::isLongitudeCoordinate(const String &text) const
{
    if (CoordinatesString::isValideLongitudeCoordinate(text))
        return true;
    
    if (isNamedCoordinate(text))
        return true;
    
    return false;
}

bool SectorString::isNamedCoordinate(const String &text) const
{
    if (sector == nullptr)
        return false;
    
    return sector->containsCoordinatesFor(text);
}

bool SectorString::isCoordinates(const String& latitude, const String& longitude) const
{
    if (!isLatitudeCoordinate(latitude) || !isLongitudeCoordinate(longitude))
        return false;
    
    return true;
}

bool SectorString::isCoordinatePair(const String& text) const
{
    StringArray words = getWhitespaceSeparatedItems(getWithoutNotes());
    
    // Must be 4 words
    if (words.size() < 4)
        return false;
    
    if (!isCoordinates(words[0], words[1]))
        return false;
    
    if (!isCoordinates(words[2], words[3]))
        return false;
    
    return true;
}

bool SectorString::isNamedCoordinatePair(const String& text) const
{
    StringArray words = getWhitespaceSeparatedItems(getWithoutNotes());
    
    if (words.size() < 5)
        return false;
    
    words.remove(0);
    
    return isCoordinatePair(words.joinIntoString(" "));
}

//==============================================================================

bool SectorString::isNameAndTwoCoordinates() const
{
    if (isWhitespaceOrComment())
        return false;
    
    StringArray words = getWhitespaceSeparatedItems(getWithoutNotes());
    
    /**
     Reference
     V1-419                   BOS            BOS            WHYBE         WHYBE          ;HOLD
     0                        1              2              3             4              5
     */
    
    if (words.size() < 5)
        return false;
    
    if (!isCoordinates(words[1], words[2]))
        return false;
    
    if (!isCoordinates(words[3], words[4]))
        return false;
    
    return true;
}

String SectorString::getLatitudeFor(const String &navaid) const
{
    return CoordinatesString(sector->getCoordinatesFor(navaid)).toSectorLatitude();
}

String SectorString::getLongitudeFor(const String& navaid) const
{
    return CoordinatesString(sector->getCoordinatesFor(navaid)).toSectorLongitude();
}

NamedCoordinate SectorString::getNamedCoordinateFor(const String& sectorCoordinateLat, const String& sectorCoordinateLon) const
{
    CoordinatesString cs(sectorCoordinateLat, sectorCoordinateLon);
    
    // If the strings are just a standard coordinate string, return an NamedCoordinate with no name specified
    if (cs.isValid())
        return NamedCoordinate(cs.toCoordinates());
    
    // Don't proceed if there is no sector pointer!
    if (sector == nullptr)
        return NamedCoordinate();
    
    // Handle matching named coordinates (i.e. BOS BOS or ATL ATL, this is the typical use case)
    if (sectorCoordinateLat == sectorCoordinateLon)
    {
        if (sector->containsCoordinatesFor(sectorCoordinateLat))
            return sector->getNamedCoordinatesFor(sectorCoordinateLat);
        
        else return NamedCoordinate();
    }
    
    /** Handle the rare case of a Sector where a mixed coordinate (named and sector formated,
     or even two non-matching named coordinates. As example from the ZNY.sct file:
     
     [AIRPORTS]
     B24 0.000 N042.50.37.733 W075.33.41.043 E
     
     [FIXES]
     WEBBB N037.40.17.562 W071.58.55.329
     AZEZU N037.52.28.099 W072.22.43.201
     
     [LOW AIRWAYS]
     B24 WEBBB WEBBB AZEZU AZEZU
     ^   ^  <- would be a valid coordinate pair
     
     */
    {
        String latSectorCoordinate;
        {
            // is a valid navaid with this latitude existing?
            if (sector->containsCoordinatesFor(sectorCoordinateLat))
                latSectorCoordinate = getLatitudeFor(sectorCoordinateLat);
            
            // is it a valid sector formated coordinate?
            if (CoordinatesString::isValidLatitudeCoordinate(sectorCoordinateLat))
                latSectorCoordinate = sectorCoordinateLat;
            
            // If neither return
            else return NamedCoordinate();
        }
        
        String lonSectorCoordinate;
        {
            // is a valid navaid with this latitude existing?
            if (sector->containsCoordinatesFor(sectorCoordinateLon))
                lonSectorCoordinate = getLongitudeFor(sectorCoordinateLon);
            
            // is it a valid sector formated coordinate?
            if (CoordinatesString::isValideLongitudeCoordinate(sectorCoordinateLon))
                lonSectorCoordinate = sectorCoordinateLat;
            
            // If neither return
            else return NamedCoordinate();
        }
        
        return NamedCoordinate(empty, Coordinates(CoordinatesString(latSectorCoordinate, lonSectorCoordinate).toCoordinates()));
    }
    
    return NamedCoordinate();
}

//==============================================================================

bool SectorString::isColour (const String& text) const
{
    if (isNamedColour(text))
        return true;
    
    if (isNumber(text) && text.getIntValue() <= 16777215)
        return true;
    
    return false;
}

bool SectorString::isNamedColour(const String& text) const
{
    if (sector == nullptr)
        return false;
    
    return sector->containsNamedColour(text);
}

sector::NamedColour SectorString::getNamedColourFor(const juce::String &text) const
{
    if (sector != nullptr && isNamedColour(text))
        return sector->getNamedColourFor(text);
    
    // Is it a numberic colour
    if (isNumber(text))
        return NamedColour(ColourString(text).getColour());
    
    return NamedColour();
}

//==============================================================================
// Unit Tests
//==============================================================================

class SectorStringTests   : public UnitTest
{
public:
    
    SectorContainer sector;
    
    SectorStringTests()   : UnitTest ("SectorString class tests")
    {
        // Setup a dummy Sector with some coordinates
        sector.addFix(NavAid("ACELA", Coordinates(12.34, 43.21)));
        sector.addVOR(RadioNavAid("ACK", "116.20", Coordinates(21.43, 34.12)));
        sector.addFix(NavAid("BOS", Coordinates(12.00, 43.00)));
        sector.addFix(NavAid("WHYBE", Coordinates(00.12, 00.43)));
        sector.add(NamedColour("blue", Colour(0, 0, 255)));
    }
    
    void runTest()
    {
        beginTest("Constructors and Operators");
        {
            SectorString ss;
            expect(ss.isEmpty());
            
            ss = SectorString("test string");
            expectEquals((String) ss, String("test string"));
        }
        
        beginTest("General Purpose Functions");
        {
            // isWhiteSpaceOrComment() Tests
            expect(SectorString(String()).isWhitespaceOrComment());         // empty string
            expect(SectorString("\t ").isWhitespaceOrComment());              // whitespace
            expect(SectorString(juce::newLine).isWhitespaceOrComment());    // new line
            expect(SectorString("; a comment").isWhitespaceOrComment());    // sector comment
            expect(!SectorString(" text").isWhitespaceOrComment());         // valid line
            
            // isSectionHeader() Tests
            expect(SectorString("[INFO] other words").isSectionHeader());
            expect(SectorString("  [INFO]   ").isSectionHeader());          // whitespace test
            expect(!SectorString("]INFO] other words").isSectionHeader());
            
            // getSectionHeader() Tests
            expect(SectorString("[ARTCC HIGH]").getHeaderType() == SectorString::SectionHeaderType::ARTCC_High);
            
            // hasNotes() Tests
            expect(!SectorString("text text text").hasNotes());
            expect(!SectorString("text ;").hasNotes());
            expect(!SectorString("text ; ").hasNotes());
            expect(SectorString("text ;note").hasNotes());
            expect(SectorString("text ; note").hasNotes());
            
            // getNotes() Tests
            expectEquals(SectorString("text ;note").getNotes(), String("note"));
            expectEquals(SectorString("text ;note ").getNotes(), String("note"));
            expectEquals(SectorString("text ; note ").getNotes(), String("note"));
            
            // getWithoutNotes() Tests
            expectEquals(SectorString("text ;note").getWithoutNotes(), String("text"));
            expectEquals(SectorString(" text;note").getWithoutNotes(), String("text"));
        }
        
        
        beginTest("Coordinate Functions");
        {
            // numberOfCoordinates() Tests
            expect(SectorString("word word N041.56.18.800 W072.40.57.000 word").numberOfCoordinates() == 1);
            expect(SectorString("word word N041.56.18.800 W072.40.57.000;word").numberOfCoordinates() == 1);
            expect(SectorString("ACK ACK word word", &sector).numberOfCoordinates() == 1);
            expect(SectorString("ACK ACK ACELA ACELA;note", &sector).numberOfCoordinates() == 2);
            expect(SectorString("word word N041.56.18.800 W072.40.57.000 ;word N041.56.18.800 W072.40.57.000").numberOfCoordinates() == 1);
            expect(SectorString("word word N041.56.18.800 W072.40.57.000 word N041.56.18.800 W072.40.57.000").numberOfCoordinates() == 2);
            
            // getAllCoorinates() Tests
            {
                SectorString ss("word ACK ACK word BOS BOS N041.56.18.800 W072.40.57.000; some note ", &sector);
                SectorString::NamedCoordinateArray nca = ss.getAllCoordinates();
                expectEquals(nca[0].getName(), String("ACK"));
                expectEquals(nca[1].getName(), String("BOS"));
                expectEquals((int) nca[2].getCoordinate().Latitude(), 41);
            }
            
            // getLastOccuranceOfLine() Tests
            {
                SectorString test("some diagram name BOS BOS WHYBE WHYBE; hold", &sector);
                sector::NamedCoordinateLine ncl(test.getLastOccuranceOfLine());
                expect(ncl.getStart().getName() == "BOS");
                expect(ncl.getEnd().getName() == "WHYBE");
                
                SectorString test2 ("BOS BOS diagram WHYBE WHYBE BOS BOS ; hold", & sector);
                ncl = test2.getLastOccuranceOfLine();
                expect(ncl.getStart().getName() == "WHYBE");
                expect(ncl.getEnd().getName() == "BOS");
            }
        }
        
        beginTest("NamedColour Functions");
        {
            // isNamedColour() Tests
            expect(SectorString("#define White 16777215").isNamedColour());
            expect(!SectorString("define White 16777215").isNamedColour());      // Missing #
            expect(!SectorString("#Define White 16777215").isNamedColour());     // #Define case sensitive
            expect(!SectorString("#define White 16777xxx", true).isNamedColour());     // letters in color
            expect(!SectorString("#define White 16777216", true).isNamedColour());     // colour out of range
            
            // toNamedColour() Tests
            SectorString defineColour("#define blue 16711680 ; a note");
            expect(defineColour.toNamedColour().getName() == "blue");
            expect(defineColour.toNamedColour().getColour() == Colours::blue);
            expect(defineColour.toNamedColour().getNotes() == "a note");
            
            // Equality test
            NamedColour namedColour("blue", Colours::blue, "; a note");
            expect(NamedColour(defineColour) == namedColour);
        }
        
        beginTest("Airport Functions");
        {
            // isAirport Test
            expect(SectorString("KBDL 120.300 N041.56.18.800 W072.40.57.000 C").isAirport() == true);
            
            // Airport() Test
            Airport airport = SectorString("KBDL 120.300 N041.56.18.800 W072.40.57.000 C");
            expect(airport.getName() == "KBDL");
        }
        
        beginTest("Runway Functions");
        {
            // isRunway() Tests
            expect(SectorString("11  29  123 321 N041.43.59.501 W072.38.57.074 N041.44.03.078 W072.39.27.331").isRunway());
            
            // toRunway() Tests
            Runway runway(SectorString("11  29  123 321 N041.43.59.501 W072.38.57.074 N041.44.03.078 W072.39.27.331;note 1").toRunway());
            expect(runway.getRunwayNumber() == "11");
            expect(runway.getRunwayNumberOppositeEnd() == "29");
            expect(runway.getMagneticHeading() == 123);
            expect(runway.getMagneticHeadingOppositeEnd() == 321);
            expect(CoordinatesString(runway.getRunwayStart()).toSectorCoordinates() == "N041.43.59.501 W072.38.57.074");
            expect(CoordinatesString(runway.getRunwayEnd()).toSectorCoordinates() == "N041.44.03.078 W072.39.27.331");
            expect(runway.getNotes() == "note 1");
        }
        
        beginTest("RadioNavAid Functions");
        {
            // isVOR() Tests
            expect(SectorString("ACK 116.20  N041.16.54.794 W070.01.36.157 ;NANTUCKET VOR/DME").isVOR());
            
            // isNDB() Tests
            expect(SectorString("AL    219.000 N042.49.02.280 W073.48.30.636 ;HAWKY").isNDB());
            
            // toRadioNavAid() Tests
            RadioNavAid vor(SectorString("ACK 116.20  N041.16.54.794 W070.01.36.157 ;NANTUCKET VOR/DME").toRadioNavAid());
            expect(vor.getName() == "ACK");
            expect(vor.getFrequency() == "116.20");
            expect(CoordinatesString(vor.getCoordinate()).toSectorCoordinates() == "N041.16.54.794 W070.01.36.157");
            expect(vor.getNotes() == "NANTUCKET VOR/DME");
        }
        
        beginTest("Fix Functions");
        {
            // isFix() Tests
            expect(SectorString("18THR N042.14.25.609 W071.50.20.580 ; note").isFix());
            
            // toFix() Tests
            NavAid fix(SectorString("18THR N042.14.25.609 W071.50.20.580 ; note").toNavAid());
            expectEquals(fix.getName(), String("18THR"));
            expectEquals(CoordinatesString(fix.getCoordinate()).toSectorCoordinates(),
                         String("N042.14.25.609 W071.50.20.580"));
            expectEquals(fix.getNotes(), String("note"));
        }
        
        beginTest("Airways Functions");
        {
            // isAirway() Tests
            expect(SectorString("V1-419                   BOS            BOS            WHYBE         WHYBE          ;HOLD", &sector).isAirway());
            expect(!SectorString("V1-419                   BOX            BOX            WHYBX         WHYBX          ;HOLD", &sector).isAirway());
            
            // getAirwayName() Test
            expectEquals(SectorString("V1-419                   BOS            BOS            WHYBE         WHYBE          ;HOLD", &sector).getAirwayName(), String ("V1-419"));
            
            // toAirwayLine() Tests
            NamedLine nl(SectorString("V1-419                   BOS            BOS            WHYBE         WHYBE          ;HOLD", &sector).toAirwayLine());
            expectEquals(nl.getName(), String("V1-419"));
            expectEquals(nl.getStart().getName(), String("BOS"));
            expectEquals(nl.getEnd().getName(), String("WHYBE"));
            expectEquals((String) nl.getNotes(), String("HOLD"));
        }
        
        beginTest("ARTCC Functions");
        {
            // isArtcc() Test
            expect(SectorString("ZBW-47BOSOX N042.16.15.000 W072.46.30.000 N042.21.14.000 W072.39.29.000 SECTOR ;A-B").isArtcc());
            
            // getArtccName() Test
            expectEquals(SectorString("ZBW-47BOSOX N042.16.15.000 W072.46.30.000 N042.21.14.000 W072.39.29.000 SECTOR ;A-B").getArtccName(), String("ZBW-47BOSOX"));
            
            // toArtccLine() Test
            NamedLine al = SectorString("ZBW-47BOSOX N042.16.15.000 W072.46.30.000 N043.21.14.000 W073.39.29.000 SECTOR ;A-B").toArtccLine();
            expectEquals(al.getName(), String("ZBW-47BOSOX"));
            expect((int) al.getStart().getCoordinate().Latitude() == 42);
            expect((int) al.getStart().getCoordinate().Longitude() == -72);
            expect((int) al.getEnd().getCoordinate().Latitude() == 43);
            expect((int) al.getEnd().getCoordinate().Longitude() == -73);
            expectEquals(al.getNotes(), String("A-B"));
        }
        
        beginTest("SID STAR Functions");
        {
            expect(SectorString("ZMP 5.2 Eff. 11 FEB 10     N010.00.00.000 W001.00.00.000 N010.00.00.000 W001.00.00.000;note").isNew_SID_STAR());
            expect(SectorString("ZMP 5.2 Eff. 11 FEB 10     N010.00.00.000 W001.00.00.000 N010.00.00.000 W001.00.00.000;note").getNew_SID_STAR_Name() == "ZMP 5.2 Eff. 11 FEB 10");
            
            // Check splitting at right character to get name
            expect(SectorString("1234567890123456789012356789X Y X Y").isNew_SID_STAR());
            expect(SectorString("1234567890123456789012356789X Y X Y").getNew_SID_STAR_Name() == "1234567890123456789012356");
            
            expect(SectorString(" N045.55.41.627 W096.33.51.591 N046.55.41.627 W097.33.51.591 blue").is_SID_STAR_Line());
            expect(!SectorString("N045.55.41.627 W096.33.51.591 N046.55.41.627 W097.33.51.591 blue").is_SID_STAR_Line());
            
            ColouredLine cl = SectorString(" N045.55.41.627 W096.33.51.591 N046.55.41.627 W097.33.51.591 blue;note", &sector).to_SID_Star_Line();
            expect((int) cl.getStart().getCoordinate().Latitude() == 45);
            expect((int) cl.getStart().getCoordinate().Longitude() == -96);
            expect((int) cl.getEnd().getCoordinate().Latitude() == 46);
            expect((int) cl.getEnd().getCoordinate().Longitude() == -97);
            expect(cl.getNamedColour().getColour() == Colour(0, 0, 255));
            expect(cl.getNamedColour().getName() == "blue");
            expect(cl.getNotes() == "note");
        }
        
        beginTest("GeoLine Functions");
        {
            expect(SectorString("N048.38.01.531 W093.05.00.256 N049.38.01.531 W094.04.54.955 blue ; note").isGeoLine());
            expect(!SectorString("N048.38.01.531 W093.05.00.256 N048.38.01.531 W093.04.54.955").isGeoLine());
            
            ColouredLine cl = SectorString("N048.38.01.531 W093.05.00.256 N049.38.01.531 W094.04.54.955 blue ; note", &sector).toGeoLine();
            expect((int) cl.getStart().getCoordinate().Latitude() == 48);
            expect((int) cl.getStart().getCoordinate().Longitude() == -93);
            expect((int) cl.getEnd().getCoordinate().Latitude() == 49);
            expect((int) cl.getEnd().getCoordinate().Longitude() == -94);
            expect(cl.getNamedColour().getColour() == Colour(0, 0, 255));
            expect(cl.getNamedColour().getName() == "blue");
            expect(cl.getNotes() == "note");
        }
        
        beginTest("Label functions");
        {
            expect(SectorString("\"ZYZ BLW FL230\" N46.03.11.637 W84.27.40.250 blue", &sector).isLabel());
            CharPointer_UTF8 text("\"100/70\"\xa0N044.31.28.157 W093.00.43.420 BRVOALT-C");
            //CharPointer_UTF8 toUTF8 ( String("\"100/70\"\xa0N044.31.28.157 W093.00.43.420 BRVOALT-C").toRawUTF8());
            {
                //String x("\"100/70\"\xa0N044.31.28.157 W093.00.43.420 BRVOALT-C");
            }
            //String debug (toUTF8);
            //DBG(debug);
//            String (CharPointer_UTF8("\"100/70\"\xa0N044.31.28.157 W093.00.43.420 BRVOALT-C"));
//            String test(text);
//            SectorString(test).isLabel();
        }
    }
};

static SectorStringTests sectorStringTests;