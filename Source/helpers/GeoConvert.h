//
//  GeoConvert.h
//  fstream
//
//  Created by James Mazur on 12/8/12.
//  Copyright (c) 2012 James Mazur. All rights reserved.
//

#ifndef __fstream__GeoConvert__
#define __fstream__GeoConvert__

#include "sct/SCTobj.h"
#include <iostream>
#include <GeographicLib/TransverseMercator.hpp>
#include <GeographicLib/TransverseMercatorExact.hpp>
#include "GeoConvert.h"
#include <GeographicLib/GeoCoords.hpp>
#include <vector>
#include <string>
#include <exception>

bool Geo2DoubleCoord (GeographicLib::GeoCoords inCoord, double &lat, double &lon);
bool Geo2Mercator (GeographicLib::GeoCoords inCoord, double &x, double &y);
bool Mercator2Geo (double x, double y, GeographicLib::GeoCoords &rCoord);
std::vector<std::string> split(std::string sInput, char delim);
std::vector<std::string> split_space_tab(std::string sInput);
std::vector<std::string> split_line(std::string sInput);

std::string int_2_string (int n);
std::string dbl_2_string (double n);
std::string float_2_string (float n);

cRGBColor SCTcolor2RGB (std::string sInput);
int RGB2SCTcolor (cRGBColor inColor);
GeographicLib::GeoCoords ConvertSCT2Coord (std::string sInput);
std::string ConvertCoord2SCT (GeographicLib::GeoCoords gCoordIn);

#endif /* defined(__fstream__GeoConvert__) */
