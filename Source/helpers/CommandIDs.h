/*
  ==============================================================================

    CommandIDs.h
    Created: 10 Jan 2015 10:24:28pm
    Author:  Jim Mazur

  ==============================================================================
*/

#ifndef COMMANDIDS_H_INCLUDED
#define COMMANDIDS_H_INCLUDED

/**
 A namespace to hold all the possible command IDs.
 */
namespace CommandIDs
{
    enum
    {
        open                    = 0x200010,
        
        parseSector             = 0x200020,

    };
}

namespace CommandCategories
{
    static const char* const general       = "General";
    static const char* const debugging       = "Debugging";
}



#endif  // COMMANDIDS_H_INCLUDED
