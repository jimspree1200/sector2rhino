/*
  ==============================================================================

    CoordinateProjections.h
    Created: 25 Feb 2015 12:28:04am
    Author:  Jim Mazur

  ==============================================================================
*/

#ifndef COORDINATEPROJECTIONS_H_INCLUDED
#define COORDINATEPROJECTIONS_H_INCLUDED

#include "../../JuceLibraryCode/JuceHeader.h"

namespace sector
{
class Coordinates;
}

class CoordinatesProjection
{
public:
    enum ProjectionModel
    {
        DaysMinutesSeconds,
        Mercator
    };
    
    // Defaults to DMS projection model
    CoordinatesProjection ();
    
    CoordinatesProjection (const ProjectionModel& modelToUse);
    
    Result CoordinatesToCartesian (const sector::Coordinates& coordinates, Point<double> &point);
    
    Result CartesianToCoordinates (const Point<double> &point, sector::Coordinates& coordinates);
    
private:
    Result CoordinatesToMercator (const sector::Coordinates& coordinates, Point<double> &point);
    
    Result MercatorToCoordinates (const Point<double> &point, sector::Coordinates& coordinates);
    
    Result CoordinatesToDMS (const sector::Coordinates& coordinates, Point<double> &point);
    
    Result DMSToCoordinates (const Point<double>& point, sector::Coordinates& coordinates);

    ProjectionModel projection;
    bool swapXY;
    
};

#endif  // COORDINATEPROJECTIONS_H_INCLUDED
