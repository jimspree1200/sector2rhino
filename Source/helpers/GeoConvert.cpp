//
//  GeoConvert.cpp
//  fstream
//
//  Created by James Mazur on 12/8/12.
//  Copyright (c) 2012 James Mazur. All rights reserved.
//

#include "GeoConvert.h"

using namespace std;

int UMTZone = -72;

bool Geo2DoubleCoord (GeographicLib::GeoCoords inCoord, double &lat, double &lon) {
     std::string sTempCoord;
    sTempCoord = inCoord.GeoRepresentation(9);
    
    lat = atof(sTempCoord.substr(0,sTempCoord.length()-(sTempCoord.find_first_of(" ")+2)).c_str());
    
    lon = atof(sTempCoord.substr(sTempCoord.find_first_of(" "), sTempCoord.length()-sTempCoord.find_first_of(" ")).c_str());
    
     std::cout.precision(20);
    //std::cout << lat << " " << lon << std::endl;
    
    return true;
}

bool Geo2Mercator (GeographicLib::GeoCoords inCoord, double &x, double &y) {
    std::string sTempCoord;
    double lat;
    double lon;
    
    sTempCoord = inCoord.GeoRepresentation(9);
    
    lat = atof(sTempCoord.substr(0,sTempCoord.length()-(sTempCoord.find_first_of(" ")+2)).c_str());
    
    lon = atof(sTempCoord.substr(sTempCoord.find_first_of(" "), sTempCoord.length()-sTempCoord.find_first_of(" ")).c_str());
    
    //std::cout.precision(20);
    //std::cout << lat << " " << lon << std::endl;
    
    GeographicLib::TransverseMercatorExact proj(GeographicLib::Constants::WGS84_a(), GeographicLib::Constants::WGS84_f(), GeographicLib::Constants::UTM_k0());
    
    proj.Forward(UMTZone, lat, lon, x, y);
    
    
    return true;
}

bool Mercator2Geo (double x, double y, GeographicLib::GeoCoords &rCoord) {
    try {
    GeographicLib::TransverseMercatorExact proj(GeographicLib::Constants::WGS84_a(), GeographicLib::Constants::WGS84_f(), GeographicLib::Constants::UTM_k0());
    double lat = 0, lon = 0;
    proj.Reverse(UMTZone, x, y, lat, lon);
    

    GeographicLib::Math::real rLat = lat;
    GeographicLib::Math::real rLon = lon;
    
    GeographicLib::GeoCoords coord1(rLat, rLon);
    
        if (GeographicLib::Math::isnan(lat) || GeographicLib::Math::isnan(lon)) {
        std::cout << "ERRROR" << std::endl;
        return false;
    }
    
    rCoord = coord1;
}
    catch (const exception& e) {
        cerr << "Caught exception: " << e.what() << "\n";
        return false;
    }

    
    //std::cout << coord1.DMSRepresentation(2, 0, '.') << std::endl;
    
    //std::cout << coord1.DMSRepresentation(2, 0, '.') << std::endl;
    return true;
}

std::vector<std::string> split(std::string sInput, char delim) {
    std::vector<std::string> flds;
    std::string work = sInput;
    std::string buf = "";
    int i = 0;
    while (i < work.length()) {
        if (work[i] != ' ' && work[i] != '\t')
            buf += work[i];
        else if (buf.length() > 0) {
            flds.push_back(buf);
            buf = "";
        }
        i++;
    }
    if (!buf.empty())
        flds.push_back(buf);
    return flds;
}

std::vector<std::string> split_space_tab(std::string sInput) {
    std::vector<std::string> flds;
    std::string work = sInput;
    std::string buf = "";
    int i = 0;
    while (i < work.length()) {
        if (work[i] != ' ' && work[i] != '\t')
            buf += work[i];
        else if (buf.length() > 0) {
            flds.push_back(buf);
            buf = "";
        }
        i++;
    }
    if (!buf.empty())
        flds.push_back(buf);
    return flds;
}

std::vector<std::string> split_line(std::string sInput) {
    std::vector<std::string> flds;
    std::string work = sInput;
    std::string buf = "";
    int i = 0;
    while (i < work.length()) {
        if (work[i] != '\n' && work[i] != '\r')
            buf += work[i];
        else if (buf.length() > 0) {
            flds.push_back(buf);
            buf = "";
        }
        i++;
    }
    if (!buf.empty())
        flds.push_back(buf);
    return flds;
}

std::string int_2_string (int n) {
    
    //std::ostringstream convert;   // stream used for the conversion
    
//    convert << n;      // insert the textual representation of 'Number' in the characters in the stream
//    
//    return convert.str();
	return std::string();
}

std::string dbl_2_string (double n)  {
    //std::ostringstream convert;
//    convert << n;
//    return convert.str();
	return std::string();
}

std::string float_2_string (float n)  {
//    //std::ostringstream convert;
//    convert << n;
//    return convert.str();
	return std::string();
}

/*
 cRGBColor tempColor;
 std::string tempString;
 std::string sTempCoord;
 double dTempCoord1x, dTempCoord1y, dTempCoord2x, dTempCoord2y;
 
 GeographicLib::GeoCoords tempCoord1, tempCoord2;
 
 for (int i = 0; i < Sector.countDiagrams(); i++) {
 for (int j = 0; j < Sector.countLinesInDiagram(i); j++) {
 
 Sector.returnDiagramLine(i, j, &tempCoord1, &tempCoord2, &tempColor, &tempString);
 std::cout.precision(20);
 sTempCoord = tempCoord1.GeoRepresentation(9);
 std::cout << "Geo: " << sTempCoord << std::endl;
 
 dTempCoord1y = atof(sTempCoord.substr(0,sTempCoord.length()-(sTempCoord.find_first_of(" ")+2)).c_str());
 std::cout << "1y : " << dTempCoord1y << std::endl;
 
 dTempCoord1x = atof(sTempCoord.substr(sTempCoord.find_first_of(" "), sTempCoord.length()-sTempCoord.find_first_of(" ")).c_str());
 std::cout << "1x : " << dTempCoord1x << std::endl;
 
 sTempCoord = tempCoord2.GeoRepresentation(9);
 dTempCoord2y = atof(sTempCoord.substr(0,sTempCoord.length()-(sTempCoord.find_first_of(" ")+2)).c_str());
 dTempCoord2x = atof(sTempCoord.substr(sTempCoord.find_first_of(" "), sTempCoord.length()-sTempCoord.find_first_of(" ")).c_str());
 
 std::cout << "2y : " << dTempCoord2y << std::endl;
 
 std::cout << "2x : " << dTempCoord2x << std::endl;
 
 ONX_Model_Object& mo = model.m_object_table.AppendNew();
 mo.m_object = new ON_LineCurve( ON_Line( ON_3dPoint(
 dTempCoord1x,
 dTempCoord1y,
 0.0
 ),
 ON_3dPoint(
 dTempCoord2x,
 dTempCoord2y,
 0.0
 ) ) );
 mo.m_bDeleteObject = true;
 mo.m_attributes.m_layer_index = i;
 mo.m_attributes.m_name = tempString.c_str();
 }
 }
 
 */