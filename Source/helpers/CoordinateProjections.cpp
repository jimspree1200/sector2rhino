/*
  ==============================================================================

    CoordinateProjections.cpp
    Created: 25 Feb 2015 12:28:04am
    Author:  Jim Mazur

  ==============================================================================
*/

#include "CoordinateProjections.h"
#include "../model/SectorTypes.h"
#include "TransverseMercatorExact.hpp"
#include <exception>

using namespace GeographicLib;
using namespace sector;

CoordinatesProjection::CoordinatesProjection ()
:
projection(DaysMinutesSeconds),
swapXY(true)
{}

CoordinatesProjection::CoordinatesProjection (const ProjectionModel& modelToUse)
:
projection(modelToUse),
swapXY(true)
{}

Result CoordinatesProjection::CoordinatesToCartesian(const sector::Coordinates &coordinates, Point<double> &point)
{
    switch (projection) {
        case ProjectionModel::DaysMinutesSeconds:
        {
            return CoordinatesToDMS(coordinates, point);
            break;
        }
            
        case ProjectionModel::Mercator:
        {
            return CoordinatesToMercator(coordinates, point);
            break;
        }
            
        default:
            break;
    }
    return Result::fail("No projection model");
}

Result CoordinatesProjection::CartesianToCoordinates(const Point<double> &point, sector::Coordinates &coordinates)
{
    switch (projection) {
        case ProjectionModel::DaysMinutesSeconds:
        {
            return DMSToCoordinates(point, coordinates);
            break;
        }
            
        case ProjectionModel::Mercator:
        {
            return MercatorToCoordinates(point, coordinates);
            break;
        }
            
        default:
            break;
    }
    return Result::fail("No projection model");
}

Result CoordinatesProjection::CoordinatesToMercator (const sector::Coordinates& coordinates, Point<double> &point)
{
    try
    {
        const TransverseMercatorExact& projection = TransverseMercatorExact::UTM();
    
        // Should think about using the zone around the center point
        const double lon0 = 0.0;
        double x, y;
        
        projection.Forward(lon0, coordinates.Latitude(), coordinates.Longitude(), x, y);
        
        point.setXY(x, y);
        if (swapXY)
            point.setXY(point.getY(), point.getX());
    }
    catch (const std::exception& e)
    {
        String error;
        error   << "Could not forward project coordinate: "
                << coordinates.Latitude() << " (lat) / "
                << coordinates.Longitude() << " (lon)" << newLine
                << "Error: "  << e.what();
        return Result::fail(error);
    }

    return Result::ok();
}

Result CoordinatesProjection::MercatorToCoordinates (const Point<double> &point, sector::Coordinates& coordinates)
{
    try
    {
        const TransverseMercatorExact& projection = TransverseMercatorExact::UTM();
        
        // Should think about using the zone around the center point
        const double lon0 = 0.0;
        
        double lat, lon;
        
        projection.Reverse(lon0, point.getX(), point.getY(), lat, lon);
        
        coordinates.Reset(lat, lon);
    }
    catch (const std::exception& e)
    {
        String error;
        error   << "Could not reverse project coordinate: "
        << point.getX() << " (X) / "
        << point.getY() << " (Y)" << newLine
        << "Error: "  << e.what();
        return Result::fail(error);
    }
    
    return Result::ok();
}

Result CoordinatesProjection::CoordinatesToDMS(const sector::Coordinates &coordinates, Point<double> &point)
{
    if (!coordinates.isValid())
        return Result::fail("Cannot convert invalid coordinates");
    
    point.setXY(coordinates.Latitude(), coordinates.Longitude());
    
    if (swapXY)
        point.setXY(point.getY(), point.getX());
    
    return Result::ok();
}

Result CoordinatesProjection::DMSToCoordinates(const Point<double> &point, sector::Coordinates &coordinates)
{
    Coordinates coords (point.getX(), point.getY());
    
    if (!coords.isValid())
        return Result::fail("Cannot convert invalid coordinates");
    
    coordinates = coords;
    
    return Result::ok();
}