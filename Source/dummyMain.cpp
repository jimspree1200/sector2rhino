/*
  ==============================================================================

    dummyMain.cpp
    Created: 30 Dec 2014 10:34:08pm
    Author:  Jim Mazur

  ==============================================================================
*/

#include "dummyMain.h"
#include "sct/SCTreader.h"
#include "3dm/3DMwriter.h"
#include "model/SectorTypes.h"

void tempMain ()
{
    readSecFile("/Users/jimmazur/Documents/Programming/Projects/sector2rhino/Resources/testSCTfiles/megaSCT.txt");
    write3dm();
}