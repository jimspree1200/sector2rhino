/*
  ==============================================================================

    Application.h
    Created: 30 Dec 2014 10:11:01pm
    Author:  Jim Mazur

  ==============================================================================
*/

#ifndef APPLICATION_H_INCLUDED
#define APPLICATION_H_INCLUDED

#include "helpers/CommandIDs.h"
#include "MainComponent.h"
#include "sct/SectorFileParser.h"
#include "model/Sector.h"
#include "Logger/SectorLogger.h"
#include "sct/SectorFileExporter.h"
#include "3dm/3dmFileExporter.h"

//==============================================================================
class Sector2RhinoApplication  : public JUCEApplication
{
public:
    //==============================================================================
    Sector2RhinoApplication() {}
    
    const String getApplicationName() override       { return ProjectInfo::projectName; }
    const String getApplicationVersion() override    { return ProjectInfo::versionString; }
    bool moreThanOneInstanceAllowed() override       { return true; }
    
    //==============================================================================
    void initialise (const String& commandLine) override
    {
        // Run unit tests
        UnitTestRunner runner;
        runner.setAssertOnFailure(true);
        runner.runAllTests();
        
        initialiseCommandManager();
        
        sector = new sector::SectorContainer();
        
        initialiseLogger();
        
        //tempMain();
        mainWindow = new MainWindow (getApplicationName());
        mainWindow->setResizable(true, false);
        
        SectorLogger::addInfo("Built: " + Time::getCompilationDate().toString(true, true));
        
        
        
        askUserToOpenFile();
        
    }
    
    void initialiseLogger()
    {
        logger = new SectorLogger();
        SectorLogger::setCurrentLogger(logger);
    }
    
    static SectorLogger& getLogger ()
    {
        SectorLogger* logger = Sector2RhinoApplication::getApp().logger;
        jassert(logger != nullptr);
        return *logger;
    }
    
    void initialiseCommandManager()
    {
        commandManager = new ApplicationCommandManager;
        commandManager->registerAllCommandsForTarget(this);
    }
    
    void shutdown() override
    {
        if (fileParser != nullptr && fileParser->isThreadRunning())
            fileParser->stopThread(500);
        File tempSectorFile = File::getSpecialLocation(File::userDesktopDirectory).getChildFile("TempSector.sct");
        SectorFileExporter exporter(tempSectorFile);
        exporter.waitForThreadToExit(-1);
        
        File tempRhinoFile = File::getSpecialLocation(File::userDesktopDirectory).getChildFile("TempSector.3dm");
        RhinoFileExporter rhinoExporter(tempRhinoFile, &getSector());
        rhinoExporter.waitForThreadToExit(-1);
        
        sector = nullptr;

        mainWindow = nullptr; // (deletes our window)
    }
    
    //==============================================================================
    void systemRequestedQuit() override
    {
        // This is called when the app is being asked to quit: you can ignore this
        // request and let the app carry on running, or call quit() to allow the app to close.
        quit();
    }
    
    void anotherInstanceStarted (const String& commandLine) override
    {
        // When another instance of the app is launched while this one is running,
        // this method is invoked, and the commandLine parameter tells you what
        // the other instance's command-line arguments were.
    }
    
    static Sector2RhinoApplication& getApp ()
    {
        Sector2RhinoApplication* const app = dynamic_cast<Sector2RhinoApplication*>(JUCEApplication::getInstance());
        jassert(app != nullptr);
        return *app;
    }
    
    static sector::SectorContainer& getSector()
    {
        sector::SectorContainer* sector = Sector2RhinoApplication::getApp().sector;
        jassert(sector != nullptr);
        return *sector;
    }
    
    //==============================================================================
    /* Command Manager */
    
    ApplicationCommandTarget* getNextCommandTarger ()
    {
        return nullptr;
    }
    
    void getAllCommands (Array <CommandID>& commands) override
    {
        JUCEApplication::getAllCommands (commands);
        
        const CommandID ids[] = {
            CommandIDs::open,
        };
        
        commands.addArray (ids, numElementsInArray (ids));
    }
    
    void getCommandInfo (CommandID commandID, ApplicationCommandInfo& result) override
    {
        switch (commandID)
        {
                
            case CommandIDs::open:
                result.setInfo ("Open...", "Opens a script", CommandCategories::general, 0);
                result.defaultKeypresses.add (KeyPress ('o', ModifierKeys::commandModifier, 0));
                break;
                
            default:
                JUCEApplication::getCommandInfo (commandID, result);
                break;
        }
    }
    
    bool perform (const InvocationInfo& info) override
    {
        switch (info.commandID)
        {
            case CommandIDs::open:                      askUserToOpenFile(); break;
            default:                                    return JUCEApplication::perform (info);
        }
        
        return true;
    }
    
    void askUserToOpenFile()
    {
        FileChooser fc ("Open File");
        
        if (!fc.browseForFileToOpen())
            return;
        
        if (fc.getResult().getFileExtension().toLowerCase() == ".sct2")
        {
            fileParser = new SectorFileParser(fc.getResult());
        }
        
    }
    
    void writeSectorFile ()
    {
        
    }
    
    //==============================================================================
    /*
     This class implements the desktop window that contains an instance of
     our MainContentComponent class.
     */
    class MainWindow    : public DocumentWindow
    {
    public:
        MainWindow (String name)  : DocumentWindow (name,
                                                    Colours::lightgrey,
                                                    DocumentWindow::allButtons)
        {
            setUsingNativeTitleBar (true);
            setContentOwned (new MainContentComponent(), true);
            
            centreWithSize (getWidth(), getHeight());
            setVisible (true);
        }
        
        void closeButtonPressed() override
        {
            // This is called when the user tries to close this window. Here, we'll just
            // ask the app to quit when this happens, but you can change this to do
            // whatever you need.
            JUCEApplication::getInstance()->systemRequestedQuit();
        }
        
        /* Note: Be careful if you override any DocumentWindow methods - the base
         class uses a lot of them, so by overriding you might break its functionality.
         It's best to do all your work in your content component instead, but if
         you really have to override any DocumentWindow methods, make sure your
         subclass also calls the superclass's method.
         */
        
    private:
        JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (MainWindow)
    };
    
    ScopedPointer<ApplicationCommandManager> commandManager;
    
private:
    ScopedPointer<sector::SectorContainer> sector;
    ScopedPointer<SectorLogger> logger;
    ScopedPointer<SectorFileParser> fileParser;
    ScopedPointer<MainWindow> mainWindow;
};


#endif  // APPLICATION_H_INCLUDED
