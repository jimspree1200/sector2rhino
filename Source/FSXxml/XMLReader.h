//
//  XMLReader.h
//  fstream
//
//  Created by James Mazur on 12/26/12.
//  Copyright (c) 2012 James Mazur. All rights reserved.
//

#ifndef __fstream__XMLReader__
#define __fstream__XMLReader__

#include <iostream>
#include "pugixml/pugixml.hpp"
#include <GeographicLib/GeodesicExact.hpp>

#include "helpers/GeoConvert.h"
#include "sct/SCTobj.h"
#include "FSXxml/XMLAirport.h"

//class XMLAirport;

bool readXML (char *xmlDo);

bool processRunway (double dLat, double dLon, double dHeading, double dLength, double dWidth);

//void addHeading (double &heading, double dAdd);
//void addHeading (GeographicLib::Math::real &rHeading, GeographicLib::Math::real rAdd);

#endif /* defined(__fstream__XMLReader__) */
