//
//  XMLAirport.cpp
//  fstream
//
//  Created by James Mazur on 12/27/12.
//  Copyright (c) 2012 James Mazur. All rights reserved.
//

#include "XMLAirport.h"
#include "Application.h"

// Evil Globals!
//extern SCTman Sector;

using std::cout;
using std::endl;

void XMLAirport::addRunway (GeographicLib::GeoCoords gCoord,
                double dHeading,
                double dLength,
                double dWidth,
                int nNumber) {
    cRunways newRunway;
    newRunway._gCoord = gCoord;
    newRunway._dHeading = dHeading;
    newRunway._dLength = dLength;
    newRunway._dWidth = dWidth;
    newRunway._nNumber = nNumber;
    vRunways.push_back(newRunway);
}

void XMLAirport::addTaxiPoint(int nIndex,
                              TaxiPoint_Type eType,
                              Orientation eOrientation,
                              GeographicLib::GeoCoords gCoord) {
    cTaxiPoint newTaxiPoint;
    newTaxiPoint._nIndex = nIndex;
    newTaxiPoint._eType = eType;
    newTaxiPoint._eOrientation = eOrientation;
    newTaxiPoint._gCood = gCoord;
    vTaxiPoints.push_back(newTaxiPoint);
}

void XMLAirport::returnTaxiPoint (int vIndex,
                                  int &nIndex,
                                  TaxiPoint_Type &eType,
                                  Orientation &eOrientation,
                                  GeographicLib::GeoCoords &gCoord) {
    nIndex = vTaxiPoints.at(vIndex)._nIndex;
    eType = vTaxiPoints.at(vIndex)._eType;
    eOrientation = vTaxiPoints.at(vIndex)._eOrientation;
    gCoord = vTaxiPoints.at(vIndex)._gCood;
}

int XMLAirport::countTaxiPoints () {
    return (int) vTaxiPoints.size();
}

void XMLAirport::addTaxiwayParking(int nIndex,
                                GeographicLib::GeoCoords gCoord,
                                double dHeading, float fRadius,
                                Gate_Type eGateType,
                                std::string sName,
                                int nNumber,
                                double dTeeOffSet) {
    cTaxiwayParking newTaxiwayParking;
    newTaxiwayParking._nIndex = nIndex;
    newTaxiwayParking._gCoord = gCoord;
    newTaxiwayParking._dHeading = dHeading;
    newTaxiwayParking._fRadius = fRadius;
    newTaxiwayParking._eGateType = eGateType;
    newTaxiwayParking._sName = sName;
    newTaxiwayParking._nNumber = nNumber;
    newTaxiwayParking._dTeeOffSet = dTeeOffSet;
    vTaxiwayParkings.push_back(newTaxiwayParking);
}

void XMLAirport::returnTaxiwayParking (int nIndex,
                           GeographicLib::GeoCoords &gCoord,
                           double &dHeading,
                           float &fRadius,
                           Gate_Type &eGateType,
                           std::string &sName,
                           int &nNumber,
                           double &dTeeOffSet) {
    gCoord = vTaxiwayParkings.at(nIndex)._gCoord;
    dHeading = vTaxiwayParkings.at(nIndex)._dHeading;
    fRadius = vTaxiwayParkings.at(nIndex)._fRadius;
    eGateType = vTaxiwayParkings.at(nIndex)._eGateType;
    sName = vTaxiwayParkings.at(nIndex)._sName;
    nNumber = vTaxiwayParkings.at(nIndex)._nNumber;
    dTeeOffSet = vTaxiwayParkings.at(nIndex)._dTeeOffSet;
}

int XMLAirport::countTaxiwayParking () {
    return (int) vTaxiwayParkings.size();
}

void XMLAirport::addTaxiName(int nIndex, std::string sName) {
    cTaxiName newTaxiName;
    newTaxiName._nIndex = nIndex;
    newTaxiName._sName = sName;
    vTaxiNames.push_back(newTaxiName);
}

void XMLAirport::addTaxiPaths (TaxiPath_Type eTaxiPath_Type,
                            int nStart,
                            int nEnd,
                            double nWidth) {
    cTaxiPath newTaxiPath;
    newTaxiPath._eTaxiPath_Type = eTaxiPath_Type;
    newTaxiPath._nStart = nStart;
    newTaxiPath._nEnd = nEnd;
    newTaxiPath._nWidth = nWidth;
    vTaxiPaths.push_back(newTaxiPath);
}

void XMLAirport::addApron() {
    cApron emptyApron;
    vAprons.push_back(emptyApron);
}

void XMLAirport::addApronVertext(GeographicLib::GeoCoords gCoord) {
    int nSize = (int) vAprons.size() - 1;
    vAprons.at(nSize)._vGCoords.push_back(gCoord);
}

bool XMLAirport::processAirport() {
    
    for (int i = 0; i < vRunways.size(); i++) {
        processRunway(vRunways.at(i));
    }
    
    for (int i = 0; i < vTaxiPaths.size(); i++) {
        processTaxiPath(vTaxiPaths.at(i));
    }
    
    for (int i = 0; i < vAprons.size(); i++) {
        processArpon(vAprons.at(i));
    }
    
    return true;
}

bool XMLAirport::processRunway (const cRunways &Runway) {
    double dLat = Runway._gCoord.Latitude();
    double dLon = Runway._gCoord.Longitude();
    double dLength = Runway._dLength;
    double dHeading = Runway._dHeading;
    double dWidth = Runway._dWidth;
    double dLat2, dLon2;
    double dLat1, dLon1;
    double dInverseHeading = Runway._dHeading;
    std::string sName = int_2_string(Runway._nNumber);
    
    dInverseHeading = addHeading(dInverseHeading, 180.0);
    
    GeographicLib::GeodesicExact geod(GeographicLib::Constants::WGS84_a(), GeographicLib::Constants::WGS84_f());
    {
        
        double dHalfLength = dLength / 2;
        geod.Direct(dLat, dLon, dInverseHeading, dHalfLength, dLat1, dLon1);
        geod.Direct(dLat1, dLon1, dHeading, dLength, dLat2, dLon2);
        cout << dLat2 << " " << dLon2 << "\n";
        
        GeographicLib::GeoCoords coord1(dLat1, dLon1);
        GeographicLib::GeoCoords coord2(dLat2, dLon2);
        
        std::string sNote;
        sNote = "Length: " + dbl_2_string(dLength) + " Width:" + dbl_2_string(dWidth);
        
        SCTman& Sector = Sector2RhinoApplication::getSectorOld();
        Sector.addRunway("000", "000", "000", "000", coord1, coord2, sNote);
        
        cRGBColor cColor (0, 0, 0);
        double dDepth = 0.0;
        
        createRect(coord1, coord2, dDepth, dWidth, cColor, "newfunction");
        
    }
    return true;
}

bool XMLAirport::processTaxiPath (const cTaxiPath &Taxipath) {
    GeographicLib::GeoCoords gCoord1;
    GeographicLib::GeoCoords gCoord2;
    cRGBColor cColor;
    
    for (int i = 0; i < vTaxiPoints.size(); i++) {
        if (vTaxiPoints.at(i)._nIndex == Taxipath._nStart) {
            gCoord1 = vTaxiPoints.at(i)._gCood;
        }
    }
    
    if (Taxipath._eTaxiPath_Type == Parking) {
        for (int i = 0; i < vTaxiwayParkings.size(); i++) {
            if (vTaxiwayParkings.at(i)._nIndex == Taxipath._nEnd) {
                gCoord2 = vTaxiwayParkings.at(i)._gCoord;
                cColor.set(0,100,0);
            }
        }
    }
    else {
        for (int i = 0; i < vTaxiPoints.size(); i++) {
            if (vTaxiPoints.at(i)._nIndex == Taxipath._nEnd) {
                gCoord2 = vTaxiPoints.at(i)._gCood;
                cColor.set(100,0,0);
            }
        }
    }
    
    std::string sNote;
    if (Taxipath._eTaxiPath_Type == Taxi) sNote = "Taxi";
    else if (Taxipath._eTaxiPath_Type == TaxiPath_Type::Parking) sNote = "Parking";
    else if (Taxipath._eTaxiPath_Type == TaxiPath_Type::Path_normal) sNote = "Path";
    else if (Taxipath._eTaxiPath_Type == TaxiPath_Type::Runway) sNote = "Runway";
    
    sNote = sNote + " [" + int_2_string(Taxipath._nStart) + "]-[" + int_2_string(Taxipath._nEnd) + "] Width:" + dbl_2_string(Taxipath._nWidth);
    
    double dDepth = -0.2; // Set Z point to behind runway
    createRect(gCoord1, gCoord2, dDepth, Taxipath._nWidth, cColor, sNote);
    std::string sName = "FSX Import";
    DiagramType eDiagramType = SID_hack;
    
    SCTman& Sector = Sector2RhinoApplication::getSectorOld();
    Sector.addDiagramLine(&gCoord1, &gCoord2, &sName, &cColor, &eDiagramType, &sNote);
    
    return true;
}

bool XMLAirport::processArpon(const cApron &Apron) {
    cRGBColor cColor(10,10,10);
    double dDepth = -0.4; // Set Z point to behind taxi ways and rwys
    
    SCTman& Sector = Sector2RhinoApplication::getSectorOld();
    Sector.startRegion(cColor, Apron._vGCoords.at(0), dDepth, "Apron");
    
    for (int i = 1; i < Apron._vGCoords.size(); i++) {
        Sector.addToRegion(Apron._vGCoords.at(i));
    }
    Sector.addToRegion(Apron._vGCoords.at(0)); // Connect end to start
    return true;
}

double addHeading (double heading, double dAdd) {
    heading = heading + dAdd;
    if (heading > 360.0) heading = heading - 360.0;
    if (heading < 0.0) heading = heading + 360.0;
    return heading;
}

bool createRect (GeographicLib::GeoCoords coord1, GeographicLib::GeoCoords coord2, double dDepth, double dWidth, cRGBColor cColor, std::string sNote) {
    
    double dHalfWidth = dWidth * 0.5;
    double dHeading;
    double dDistance;
    GeographicLib::GeodesicExact geod(GeographicLib::Constants::WGS84_a(), GeographicLib::Constants::WGS84_f());
    
    geod.Inverse(coord1.Latitude(), coord1.Longitude(), coord2.Latitude(), coord2.Longitude(), dDistance, dHeading);
    
    double dHeadingMinus90 = addHeading(dHeading, -90.0);
    double dHeadingPlus90 = addHeading(dHeading, 90.0);
    
    // coord1x1/y1 --- coord1 ---- coord1x2/y2
    // coord2x1/y1 --- coord2 ---- coord2x2/y2
    
    double coord1_x1, coord1_y1, coord1_x2, coord1_y2;
    geod.Direct(coord1.Latitude(), coord1.Longitude(), dHeadingMinus90, dHalfWidth, coord1_x1, coord1_y1);
    geod.Direct(coord1.Latitude(), coord1.Longitude(), dHeadingPlus90, dHalfWidth, coord1_x2, coord1_y2);
    double coord2_x1, coord2_y1, coord2_x2, coord2_y2;
    geod.Direct(coord2.Latitude(), coord2.Longitude(), dHeadingMinus90, dHalfWidth, coord2_x1, coord2_y1);
    geod.Direct(coord2.Latitude(), coord2.Longitude(), dHeadingPlus90, dHalfWidth, coord2_x2, coord2_y2);
    
    GeographicLib::GeoCoords tempCoord(coord1_x1, coord1_y1);
    
    SCTman& Sector = Sector2RhinoApplication::getSectorOld();
    Sector.startRegion(cColor, tempCoord, dDepth, sNote);
    Sector.addToRegion(coord1);
    tempCoord.Reset(coord1_x2, coord1_y2);
    Sector.addToRegion(tempCoord);
    tempCoord.Reset(coord2_x2, coord2_y2);
    Sector.addToRegion(tempCoord);
    Sector.addToRegion(coord2);
    tempCoord.Reset(coord2_x1, coord2_y1);
    Sector.addToRegion(tempCoord);
    tempCoord.Reset(coord1_x1, coord1_y1);
    Sector.addToRegion(tempCoord);
    
    return true;
}