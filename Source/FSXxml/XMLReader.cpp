//
//  XMLReader.cpp
//  fstream
//
//  Created by James Mazur on 12/26/12.
//  Copyright (c) 2012 James Mazur. All rights reserved.
//
#include "XMLAirport.h"
#include "XMLReader.h"
#include "Application.h"

using std::endl;
using std::cout;
using GeographicLib::Math;

//extern SCTman Sector;
//extern XMLAirport xmlAirport;

bool readXML (char *xmlDoc) {
    
    //XMLAirport xmlAirport;
    
    pugi::xml_document doc;
    
    pugi::xml_parse_result result = doc.load_file(xmlDoc);
    
    std::cout << "Load result: " << result.description() << ", mesh name: " << doc.child("Airport").attribute("ident").value() << std::endl << endl;
    
    XMLAirport& xmlAirport = Sector2RhinoApplication::getXmlAiport();
    xmlAirport.sIdent = doc.child("Airport").attribute("ident").value();
    
    pugi::xml_node AirportNodes = doc.child("Airport");
    
    for (pugi::xml_node AirportNode = AirportNodes.first_child(); AirportNode; AirportNode = AirportNode.next_sibling())
    {
        cout << "AirportNode:" << AirportNode.name() << endl;
        std::string sAirportNode = AirportNode.name();
        
        if (sAirportNode == "Runway") {
            
            cout << "Lat: " << AirportNode.attribute("lat").value() << endl;
            cout << "Lon: " << AirportNode.attribute("lon").value() << endl;
            cout << "Heading: " << AirportNode.attribute("heading").value() << endl;
            cout << "Length: " << AirportNode.attribute("length").value() << endl;
            cout << "Number: " << AirportNode.attribute("number").value() << endl << endl;
            
            double dLat = atof(AirportNode.attribute("lat").value());
            double dLon = atof(AirportNode.attribute("lon").value());
            double dHeading = atof(AirportNode.attribute("heading").value());
            double dLength = atof(AirportNode.attribute("length").value());
            double dWidth = atof(AirportNode.attribute("width").value());
            int nNumber = atoi(AirportNode.attribute("number").value());
            
            //cout.precision(15);
            
            //cout << dHeading << endl;
            
            GeographicLib::GeoCoords gCoord(dLat, dLon);
            
            xmlAirport.addRunway(gCoord, dHeading, dLength, dWidth, nNumber);
            
            //processRunway(dLat, dLon, dHeading, dLength, dWidth);
            /*
            for (pugi::xml_attribute attr = AirportNode.first_attribute(); attr; attr = attr.next_attribute())
            {
                std::cout << " " << attr.name() << "=" << attr.value();
            }
            cout << endl;
             */
        }
        
        else if (sAirportNode == "TaxiwayPoint") {
            
            cout << "Index: " << AirportNode.attribute("index").value() << endl;
            cout << "Type: " << AirportNode.attribute("type").value() << endl;
            cout << "Orientation: " << AirportNode.attribute("orientation").value() << endl;
            cout << "Lat: " << AirportNode.attribute("lat").value() << endl;
            cout << "Lon: " << AirportNode.attribute("lon").value() << endl;
            cout << endl;
            
            int nIndex = atoi(AirportNode.attribute("index").value());
            
            double dLat = atof(AirportNode.attribute("lat").value());
            double dLon = atof(AirportNode.attribute("lon").value());
            
            GeographicLib::GeoCoords gCoord(dLat, dLon);
            
            std::string sValue;
            
            sValue = AirportNode.attribute("type").value();
            TaxiPoint_Type eType;
            if (sValue == "NORMAL") eType = normal_point;
            if (sValue == "ILS_HOLD_SHORT") eType = ils_hold_short;
            if (sValue == "HOLD_SHORT") eType = hold_short;
            
            sValue = AirportNode.attribute("orientation").value();
            Orientation eOrientation;
            if (sValue == "FORWARD") eOrientation = forward;
            else if (sValue == "REVERSE") eOrientation = reverse;
            
            xmlAirport.addTaxiPoint(nIndex, eType, eOrientation, gCoord);
            
        }
        
        else if (sAirportNode == "TaxiwayParking") {
            
            cout << "Index: " << AirportNode.attribute("index").value() << endl;
            cout << "Lat: " << AirportNode.attribute("lat").value() << endl;
            cout << "Lon: " << AirportNode.attribute("lon").value() << endl;
            cout << "Heading: " << AirportNode.attribute("heading").value() << endl;
            cout << "Type: " << AirportNode.attribute("type").value() << endl;
            cout << "Radius: " << AirportNode.attribute("radius").value() << endl;
  
            cout << endl;
            
            int nIndex = atoi(AirportNode.attribute("index").value());
            
            double dLat = atof(AirportNode.attribute("lat").value());
            double dLon = atof(AirportNode.attribute("lon").value());
            double dHeading = atof(AirportNode.attribute("heading").value());
            float dRadius = (float) atof(AirportNode.attribute("radius").value());
            int nNumber = atoi(AirportNode.attribute("number").value());
            double dTeeOffset = atof(AirportNode.attribute("teeOffset1").value());
            
            GeographicLib::GeoCoords gCoord(dLat, dLon);
            
            std::string sValue;
            
            sValue = AirportNode.attribute("type").value();
            Gate_Type eType;
            {
                if (sValue == "GATE_SMALL") eType = Small;
                if (sValue == "GATE_MEDIUM") eType = Medium;
                if (sValue == "GATE_LARGE") eType = Large;
                if (sValue == "RAMP_GA_SMALL") eType = GA_Small;
                if (sValue == "RAMP_GA_MEDIUM") eType = GA_Medium;
                if (sValue == "RAMP_GA_LARGE") eType = GA_Large;
                if (sValue == "RAMP_CARGO") eType = Cargo;
            }
            
            sValue = AirportNode.attribute("name").value();
            
            xmlAirport.addTaxiwayParking(nIndex, gCoord, dHeading, dRadius, eType, sValue, nNumber, dTeeOffset);
        }
        
        else if (sAirportNode == "TaxiName") {
            
            cout << "Index: " << AirportNode.attribute("index").value() << endl;
            cout << "Name: " << AirportNode.attribute("name").value() << endl;
            
            cout << endl;
            
            int nIndex = atoi(AirportNode.attribute("index").value());
            
            std::string sValue;
            
            sValue = AirportNode.attribute("name").value();
            
            xmlAirport.addTaxiName(nIndex, sValue);
        }
        
        else if (sAirportNode == "TaxiwayPath") {
            /*
            for (pugi::xml_attribute attr = AirportNode.first_attribute(); attr; attr = attr.next_attribute())
            {
                std::cout << " " << attr.name() << "=" << attr.value();
            }
            cout << endl;
            */
             
            cout << "Type: " << AirportNode.attribute("type").value() << endl;
            cout << "Start: " << AirportNode.attribute("start").value() << endl;
            cout << "End: " << AirportNode.attribute("end").value() << endl;
            cout << "Width " << AirportNode.attribute("width").value() << endl;
            cout << "Name: " << AirportNode.attribute("name").value() << endl;
            
            int nStart = atoi(AirportNode.attribute("start").value());
            int nEnd = atoi(AirportNode.attribute("end").value());
            double dWidth = atof(AirportNode.attribute("width").value());

            std::string sValue;
            
            sValue = AirportNode.attribute("type").value();
            TaxiPath_Type eType;
            {
                if (sValue == "PATH") eType = Path_normal;
                if (sValue == "PARKING") eType = Parking;
                if (sValue == "RUNWAY") eType = TaxiPath_Type::Runway;
                if (sValue == "TAXI") eType = Taxi;
            }
            
            sValue = AirportNode.attribute("name").value();
            
            xmlAirport.addTaxiPaths(eType, nStart, nEnd, dWidth);
            
        }
        
        else if (sAirportNode == "Aprons") {
            
            pugi::xml_node ApronNodes = doc.child("Airport").child("Aprons");
            
             for (pugi::xml_node ApronNode = ApronNodes.first_child(); ApronNode; ApronNode = ApronNode.next_sibling())
             {
                 std::cout << ApronNode.name();
                 cout << endl;
                 
                 xmlAirport.addApron();
                 
                 pugi::xml_node VertexNodes = ApronNode;
                 for (pugi::xml_node VertexNode = VertexNodes.first_child(); VertexNode; VertexNode = VertexNode.next_sibling()) {
                     std::cout << VertexNode.name() << endl << VertexNode.attribute("lat").value() << endl << VertexNode.attribute("lon").value() << endl;
                     std::string sCoord = VertexNode.attribute("lat").value();
                     sCoord = sCoord + " ";
                     sCoord = sCoord + VertexNode.attribute("lon").value();
                     GeographicLib::GeoCoords gCoord(sCoord);
                     xmlAirport.addApronVertext(gCoord);
                 }
                 
             }
             
             
            
            cout << "Type: " << AirportNode.attribute("type").value() << endl;
            cout << "Start: " << AirportNode.attribute("start").value() << endl;
            cout << "End: " << AirportNode.attribute("end").value() << endl;
            cout << "Width " << AirportNode.attribute("width").value() << endl;
            cout << "Name: " << AirportNode.attribute("name").value() << endl;
            
            int nStart = atoi(AirportNode.attribute("start").value());
            int nEnd = atoi(AirportNode.attribute("end").value());
            double dWidth = atof(AirportNode.attribute("width").value());
            
            std::string sValue;
            
            sValue = AirportNode.attribute("type").value();
            TaxiPath_Type eType;
            {
                if (sValue == "PATH") eType = Path_normal;
                if (sValue == "PARKING") eType = Parking;
                if (sValue == "RUNWAY") eType = TaxiPath_Type::Runway;
                if (sValue == "TAXI") eType = Taxi;
            }
            
            sValue = AirportNode.attribute("name").value();
            
            xmlAirport.addTaxiPaths(eType, nStart, nEnd, dWidth);
            
        }
        
    }
    
    xmlAirport.processAirport();
    
    return true;
}

/*
bool processRunway (double dLat, double dLon, double dHeading, double dLength, double dWidth) {
    double dLat2, dLon2;
    double dLat1, dLon1;
    double dInverseHeading = dHeading;
    
    dInverseHeading = addHeading(dInverseHeading, 180.0);
    
    //if (dHeading > 180.0) dInverseHeading = dHeading - 180.0f;
    //else dInverseHeading = dHeading + 180.0f;
    
    GeographicLib::GeodesicExact geod(GeographicLib::Constants::WGS84_a(), GeographicLib::Constants::WGS84_f());
    // Alternatively: const GeodesicExact& geod = GeodesicExact::WGS84;
    {
        
        double dHalfLength = dLength / 2;
        geod.Direct(dLat, dLon, dInverseHeading, dHalfLength, dLat1, dLon1);
        geod.Direct(dLat1, dLon1, dHeading, dLength, dLat2, dLon2);
        cout << dLat2 << " " << dLon2 << "\n";
        
        GeographicLib::GeoCoords coord1(dLat1, dLon1);
        GeographicLib::GeoCoords coord2(dLat2, dLon2);
        
        Sector.addRunway("testName1", "testName2", "heading1", "heading2", coord1, coord2, "note");
        
        createRect(coord1, coord2, dWidth, "newfunction");
        
    }
    
    {
        double dHalfWidth = dWidth / 2.0;
        double x1, x2, y1, y2;
        double dTempheading = dHeading;
        
        cout.precision(15);
        
        cout << dTempheading << endl;
        addHeading(dTempheading, 90.0);
        addHeading(dTempheading, 90.0);
        addHeading(dTempheading, 90.0);
        addHeading(dTempheading, 90.0);
        cout << dTempheading << endl;
        
        cRGBColor cColor(120, 0, 0);
        
        geod.Direct(dLat1, dLon1, dTempheading, dHalfWidth, x1, y1);
        GeographicLib::GeoCoords coord(x1, y1);
        Sector.startRegion(cColor, coord, "Side 1");
        cout << coord.DMSRepresentation(3, 0, '.') << endl;
        
        dTempheading = addHeading(dTempheading, -90.0);
        geod.Direct(x1, y1, dHeading, dLength, x2, y2);
        coord.Reset(x2, y2);
        Sector.addToRegion(coord);
        cout << coord.DMSRepresentation(3, 0, '.') << endl;
        
        dTempheading = addHeading(dTempheading, -90.0);
        geod.Direct(x2, y2, dTempheading, dWidth, x1, y1);
        coord.Reset(x1, y1);
        Sector.addToRegion(coord);
        cout << coord.DMSRepresentation(3, 0, '.') << endl;
        
        dTempheading = addHeading(dTempheading, -90.0);
        geod.Direct(x1, y1, dTempheading, dLength, x2, y2);
        coord.Reset(x2, y2);
        Sector.addToRegion(coord);
        cout << coord.DMSRepresentation(3, 0, '.') << endl;
        
        dTempheading = addHeading(dTempheading, -90.0);
        geod.Direct(x2, y2, dTempheading, dWidth, x1, y1);
        coord.Reset(x1, y1);
        Sector.addToRegion(coord);
        cout << coord.DMSRepresentation(3, 0, '.') << endl;

    }
    
    {
        double dHalfWidth = dWidth / 2.0;
        double x1d, x2d, y1d, y2d, x1u, x2u, y2u, y1u;
        double dTempheading = dHeading;
        addHeading(dTempheading, 90.0);
        
        cRGBColor cColor(120, 0, 0);
        
        geod.Direct(dLat1, dLon1, dTempheading, dHalfWidth, x1d, y1d);
        GeographicLib::GeoCoords coord(x1d, y1d);
        Sector.startRegion(cColor, coord, "Side 2");
        coord.Reset(dLat1, dLon1);
        Sector.addToRegion(coord);
        cout << coord.DMSRepresentation(3, 0, '.') << endl;
        
        dTempheading = dHeading;
        dTempheading = addHeading(dTempheading, -90.0);
        geod.Direct(dLat1, dLon1, dTempheading, dHalfWidth, x2d, y2d);
        coord.Reset(x2d, y2d);
        Sector.addToRegion(coord);
        cout << coord.DMSRepresentation(3, 0, '.') << endl;
        
        //addHeading(dTempheading, -90.0);
        geod.Direct(dLat2, dLon2, dTempheading, dHalfWidth, x1u, y1u);
        coord.Reset(x1u, y1u);
        Sector.addToRegion(coord);
        coord.Reset(dLat2, dLon2);
        Sector.addToRegion(coord);
        cout << coord.DMSRepresentation(3, 0, '.') << endl;
        
        dTempheading = dHeading;
        dTempheading = addHeading(dTempheading, 90.0);
        geod.Direct(dLat2, dLon2, dTempheading, dHalfWidth, x2u, y2u);
        coord.Reset(x2u, y2u);
        Sector.addToRegion(coord);
        cout << coord.DMSRepresentation(3, 0, '.') << endl;
    
        
    }
    
    
    return true;
}

bool createRect (GeographicLib::GeoCoords coord1, GeographicLib::GeoCoords coord2, double dWidth, std::string sNote) {
    
    cRGBColor cColor(220, 0, 0);
    double dHalfWidth = dWidth * 0.5;
    double dHeading;
    double dDistance;
     GeographicLib::GeodesicExact geod(GeographicLib::Constants::WGS84_a(), GeographicLib::Constants::WGS84_f());
    
    geod.Inverse(coord1.Latitude(), coord1.Longitude(), coord2.Latitude(), coord2.Longitude(), dDistance, dHeading);
    
    double dHeadingMinus90 = addHeading(dHeading, -90.0);
    double dHeadingPlus90 = addHeading(dHeading, 90.0);
    
    // coord1x1/y1 --- coord1 ---- coord1x2/y2
    // coord2x1/y1 --- coord2 ---- coord2x2/y2
    
    double coord1_x1, coord1_y1, coord1_x2, coord1_y2;
    geod.Direct(coord1.Latitude(), coord1.Longitude(), dHeadingMinus90, dHalfWidth, coord1_x1, coord1_y1);
    geod.Direct(coord1.Latitude(), coord1.Longitude(), dHeadingPlus90, dHalfWidth, coord1_x2, coord1_y2);
    double coord2_x1, coord2_y1, coord2_x2, coord2_y2;
    geod.Direct(coord2.Latitude(), coord2.Longitude(), dHeadingMinus90, dHalfWidth, coord2_x1, coord2_y1);
    geod.Direct(coord2.Latitude(), coord2.Longitude(), dHeadingPlus90, dHalfWidth, coord2_x2, coord2_y2);
    
    GeographicLib::GeoCoords tempCoord(coord1_x1, coord1_y1);
    
    Sector.startRegion(cColor, tempCoord, sNote);
    Sector.addToRegion(coord1);
    tempCoord.Reset(coord1_x2, coord1_y2);
    Sector.addToRegion(tempCoord);
    tempCoord.Reset(coord2_x2, coord2_y2);
    Sector.addToRegion(tempCoord);
    Sector.addToRegion(coord2);
    tempCoord.Reset(coord2_x1, coord2_y1);
    Sector.addToRegion(tempCoord);
    tempCoord.Reset(coord1_x1, coord1_y1);
    Sector.addToRegion(tempCoord);

    return true;
}

double addHeading (double heading, double dAdd) {
    heading = heading + dAdd;
    if (heading > 360.0) heading = heading - 360.0;
    if (heading < 0.0) heading = heading + 360.0;
    return heading;
}
*/

//void addHeading (double &heading, double dAdd) {
//    heading = heading + dAdd;
//    if (heading > 360.0) heading = heading - 360.0;
//    if (heading < 0.0) heading = heading + 360.0;
//}
