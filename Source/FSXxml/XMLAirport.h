//
//  XMLAirport.h
//  fstream
//
//  Created by James Mazur on 12/27/12.
//  Copyright (c) 2012 James Mazur. All rights reserved.
//

#ifndef __fstream__XMLAirport__
#define __fstream__XMLAirport__

#include <vector>
#include <string>
#include <iostream>
#include "sct/SCTobj.h"
#include "helpers/GeoConvert.h"
#include <GeographicLib/GeoCoords.hpp>
#include <GeographicLib/GeodesicExact.hpp>
//#include "XMLReader.h"

enum TaxiPoint_Type {
    normal_point = 1,
    hold_short = 2,
    ils_hold_short = 3
};

enum Orientation {
    forward = 0,
    reverse = 1
};

struct cTaxiPoint {
    int _nIndex;
    TaxiPoint_Type _eType;
    Orientation _eOrientation;
    GeographicLib::GeoCoords _gCood;
};

enum Gate_Type {
    GA_Small = 0,
    GA_Medium = 1,
    GA_Large = 2,
    Small = 3,
    Medium = 4,
    Large = 5,
    Cargo = 6,
    Fuel = 7
};

struct cTaxiwayParking {
    int _nIndex;
    GeographicLib::GeoCoords _gCoord;
    double _dHeading;
    float _fRadius;
    Gate_Type _eGateType;
    std::string _sName;
    int _nNumber;
    double _dTeeOffSet;
};

struct cTaxiName {
    int _nIndex;
    std::string _sName;
};

//TODO: put this in namespace to avoid JUCE conflicts (esp path)
enum TaxiPath_Type {
    Taxi = 0,
    Runway = 1,
    Parking = 2,
    Path_normal = 3
};

struct cTaxiPath {
    TaxiPath_Type _eTaxiPath_Type;
    int _nStart;
    int _nEnd;
    double _nWidth;
};

struct cApron {
    std::vector<GeographicLib::GeoCoords> _vGCoords;
};

struct cRunways {
    GeographicLib::GeoCoords _gCoord;
    double _dHeading;
    double _dLength;
    double _dWidth;
    int _nNumber;
};


class XMLAirport {
    
    std::vector<cTaxiPoint> vTaxiPoints;
    std::vector<cTaxiwayParking> vTaxiwayParkings;
    std::vector<cTaxiName> vTaxiNames;
    std::vector<cTaxiPath> vTaxiPaths;
    std::vector<cRunways> vRunways;
    std::vector<cApron> vAprons;
    
public:
    
    std::string sIdent;
    
    void addRunway (GeographicLib::GeoCoords gCoord,
                    double dHeading,
                    double dLength,
                    double dWidth,
                    int nNumber);
    
    void addTaxiPoint (int nIndex,
                       TaxiPoint_Type eType,
                       Orientation eOrientation,
                       GeographicLib::GeoCoords gCoord);
    
    void returnTaxiPoint (int vIndex,
                          int &nIndex,
                          TaxiPoint_Type &eType,
                          Orientation &eOrientation,
                          GeographicLib::GeoCoords &gCoord);
    
    int countTaxiPoints ();
    
    void addTaxiwayParking (int nIndex,
                            GeographicLib::GeoCoords gCoord,
                            double dHeading,
                            float fRadius,
                            Gate_Type eGateType,
                            std::string sName,
                            int nNumber,
                            double dTeeOffSet);
    
    void returnTaxiwayParking (int nIndex,
                               GeographicLib::GeoCoords &gCoord,
                               double &dHeading,
                               float &fRadius,
                               Gate_Type &eGateType,
                               std::string &sName,
                               int &nNumber,
                               double &dTeeOffSet
                               );
    
    int countTaxiwayParking ();
    
    void addTaxiName (int nIndex,
                      std::string sName);
    
    void addTaxiPaths (TaxiPath_Type eTaxiPath_Type,
                       int nStart,
                       int nEnd,
                       double nWidth);
    void addApron ();
    void addApronVertext (GeographicLib::GeoCoords gCoord);
    bool processAirport ();
    bool processRunway (const cRunways &Runway);
    bool processTaxiPath (const cTaxiPath &Taxipath);
    bool processArpon(const cApron &Apron);

};

double addHeading (double heading, double dAdd);
bool createRect (GeographicLib::GeoCoords coord1,
                 GeographicLib::GeoCoords coord2,
                 double dDepth,
                 double dWidth,
                 cRGBColor cColor,
                 std::string sNote);

#endif /* defined(__fstream__XMLAirport__) */
