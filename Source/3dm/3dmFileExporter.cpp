/*
  ==============================================================================

    3dmFileExporter.cpp
    Created: 24 Feb 2015 5:19:05pm
    Author:  Jim Mazur

  ==============================================================================
*/

#include "3dmFileExporter.h"
#include "opennurbs.h"
#include "../model/Sector.h"
#include "3dmIdentifiers.h"

using namespace sector;

RhinoFileExporter::RhinoFileExporter (File fileToWrite, sector::SectorContainer* sectorToRead)
:
Thread("Rhino exporter"),
rhinoFile(fileToWrite),
sector(sectorToRead),
lastLayerIndex(0),
lastMaterialIndex(0)
{    
    if (sector == nullptr)
        return;
    
    startThread();
}

void RhinoFileExporter::run()
{
    if (writeModel().failed())
        return;
    
    writeFile();
}

Result RhinoFileExporter::writeModel()
{
    model = new ONX_Model;
    if (model == nullptr)
        return Result::fail("Could not allocate space for model");
    
    model->m_properties.m_RevisionHistory.NewRevision();
    
    // set application information
    model->m_properties.m_Application.m_application_name = "SCT to openNURBS Converter";
    model->m_properties.m_Application.m_application_URL = "http://www.jimmazur.com";
    model->m_properties.m_Application.m_application_details = "Boston ARTCC";
    
    
    // file settings (units, tolerances, views, ...)
    model->m_settings.m_ModelUnitsAndTolerances.m_unit_system = ON::meters;
    model->m_settings.m_ModelUnitsAndTolerances.m_absolute_tolerance = 0.000000001;
                                                                    // 0.00000055555556
    model->m_settings.m_ModelUnitsAndTolerances.m_angle_tolerance = ON_PI/180.0; // radians
    model->m_settings.m_ModelUnitsAndTolerances.m_relative_tolerance = 0.01; // 1%
    
    model->m_settings.m_RenderSettings.m_background_color = ON_Color(0, 0, 0);
    
    //writeDefaultLayers();
    writeDefines();
    writeAirports();
    writeVORs();
    writeNDBs();
    writeFixes();
    writeRunways();
    writeARTCC();
    writeARTCC_Highs();
    writeARTCC_Lows();
    writeHighAirways();
    writeLowAirways();
    writeSID_Diagrams();
    writeSTAR_Diagrams();
    writeGeoLines();
    writeRegions();
    
    model->m_settings.m_RenderSettings.m_background_color.SetRGB(0, 0, 0);
    
    model->Polish();
    
    return Result::ok();
}

Result RhinoFileExporter::writeFile()
{
    ON_wString log;
    ON_TextLog textlog;
    
    const char* sStartSectionComment = __FILE__ "write_points_example()" __DATE__;
    const char* file = rhinoFile.getFullPathName().toRawUTF8();
    
    bool result = model->Write(file, 0, sStartSectionComment, &textlog);
    
    DBG(String(log));
    
    if (!result)
        return Result::fail("Could not write model");
    
    return Result::ok();
}

void RhinoFileExporter::writeDefines()
{
    NamedColours* colours (sector->getNamedColours());
    
    for (int i = 0; i < colours->size(); i++)
    {
        NamedColour* colour(&colours->getReference(i));
        ON_Material* material = new ON_Material();
        material->m_material_name = colour->getName().toWideCharPointer();
        ON_Color rhinoColor(getAsRhinoColour(colour->getColour()));
        material->m_diffuse = rhinoColor;
        material->m_reflection = rhinoColor;
        material->m_reflectivity = 1.0;
        material->m_bShared = true;
        material->m_material_index = getNextNewMaterialIndex();
        addNotesToModelObject(*colour, material);
        model->m_material_table.Append(*material);
    }
    
    
}

void RhinoFileExporter::writeVORs()
{
    sector::VORs* vors = sector->getVORs();
    
    if (vors->isEmpty())
        return;
    
    createLayer(Rhino_Ids::LayerNames::VORs_NDBs_Fixes_Parent, Colours::black);
    createLayer(Rhino_Ids::LayerNames::VORs, Colours::black);
    setLayerToParent(Rhino_Ids::LayerNames::VORs, Rhino_Ids::LayerNames::VORs_NDBs_Fixes_Parent);
    
    for (int i = 0; i < vors->size(); i++)
    {
        RadioNavAid* navaid = &vors->at(i);
        ONX_Model_Object* modelObject = writeRadioNavAidPoint(navaid);
        addNameToModelObject(*navaid, modelObject);
        setModelObjectLayer(modelObject, "VORs");
    }
    
}

void RhinoFileExporter::writeNDBs()
{
    sector::NDBs* ndbs = sector->getNDBs();
    
    if (ndbs->isEmpty())
        return;
    
    createLayer(Rhino_Ids::LayerNames::VORs_NDBs_Fixes_Parent, Colours::black);
    createLayer(Rhino_Ids::LayerNames::NDBs, Colours::black);
    setLayerToParent(Rhino_Ids::LayerNames::NDBs, Rhino_Ids::LayerNames::VORs_NDBs_Fixes_Parent);
    
    for (int i = 0; i < ndbs->size(); i++)
    {
        RadioNavAid* navaid = &ndbs->at(i);
        ONX_Model_Object* modelObject = writeRadioNavAidPoint(navaid);
        addNameToModelObject(*navaid, modelObject);
        setModelObjectLayer(modelObject, "NDBs");
    }
}

ONX_Model_Object* RhinoFileExporter::writeRadioNavAidPoint(const sector::RadioNavAid* navaid)
{
    ON_Point* point = createNamedCoordinatePoint(*navaid);
    addNotesToModelObject(*navaid, point);
    return addObjectToModel(point);
}

void RhinoFileExporter::writeFixes()
{
    sector::Fixes* fixes = sector->getFixes();
    
    if (fixes->isEmpty())
        return;
    
    createLayer(Rhino_Ids::LayerNames::VORs_NDBs_Fixes_Parent, Colours::black);
    createLayer(Rhino_Ids::LayerNames::Fixes, Colours::black);
    setLayerToParent(Rhino_Ids::LayerNames::Fixes, Rhino_Ids::LayerNames::VORs_NDBs_Fixes_Parent);
    
    for (int i = 0; i < fixes->size(); i++)
    {
        NavAid* fix = &fixes->at(i);
        ON_Point* point = createNamedCoordinatePoint(*fix);
        addNotesToModelObject(*fix, point);
        ONX_Model_Object& mo = model->m_object_table.AppendNew();
        mo.m_object = point;
        mo.m_attributes.m_name = fix->getName().toWideCharPointer();
        mo.m_attributes.m_layer_index = getIndexForLayer("Fixes");
    }
}

ON_Point* RhinoFileExporter::createNamedCoordinatePoint(const sector::NamedCoordinate& coord)
{
    Point<double> point;
    Result r = projection.CoordinatesToCartesian(coord.getCoordinate(), point);
    if (r.failed())
    {
        DBG(r.getErrorMessage());
        return nullptr;
    }
    ON_Point* point2 = new ON_Point(ON_2dPoint(point.getX(), point.getY()));
    
    return point2;
}

void RhinoFileExporter::writeAirports()
{
    sector::Airports* airports = sector->getAirportss();
    
    if (airports->size() == 0)
        return;
    
    createLayer(Rhino_Ids::LayerNames::AirportsRunwaysParent, Colours::black);
    createLayer(Rhino_Ids::LayerNames::Airports, Colours::black);
    setLayerToParent(Rhino_Ids::LayerNames::Airports, Rhino_Ids::LayerNames::AirportsRunwaysParent);
    
    for (int i = 0; i < airports->size(); i++)
    {
        sector::Airport* airport = &airports->at(i);
        ON_Point* point = createNamedCoordinatePoint(*airport);
        point->SetUserString(L"Frequency", airport->getFrequency().toWideCharPointer());
        point->SetUserString(L"Airspace_Class", airport->getAirspaceClass().toWideCharPointer());
        addNotesToModelObject(*airport, point);
        ONX_Model_Object* modelObject = addObjectToModel(point);
        addNameToModelObject(*airport, modelObject);
        setModelObjectLayer(modelObject, "Airports");
    }
}

void RhinoFileExporter::writeRunways()
{
    sector::Runways* runways = sector->getRunways();
    
    if (runways->size() == 0)
        return;
    
    createLayer(Rhino_Ids::LayerNames::AirportsRunwaysParent, Colours::black);
    createLayer(Rhino_Ids::LayerNames::Runways, Colours::black);
    setLayerToParent(Rhino_Ids::LayerNames::Runways, Rhino_Ids::LayerNames::AirportsRunwaysParent);
    
    for (int i = 0; i < runways->size(); i++)
    {
        sector::Runway* runway = &runways->getReference(i);

        Point<double> start;
        projection.CoordinatesToCartesian(runway->getRunwayStart(), start);
        
        Point<double> end;
        projection.CoordinatesToCartesian(runway->getRunwayEnd(), end);
        
        ON_3dPoint s(start.getX(), start.getY(), 0.0);
        ON_3dPoint e(end.getX(), end.getY(), 0.0);
        
        ON_LineCurve* line = new ON_LineCurve(ON_2dPoint(start.getX(), start.getY()), ON_2dPoint(end.getX(), end.getY()));
        addNotesToModelObject(*runway, line);
        
        ONX_Model_Object* modelObject = addObjectToModel(line);
        setModelObjectLayer(modelObject, "Runways");
    }
}

//==============================================================================

void RhinoFileExporter::writeARTCC()
{
    
    writeNamedLineDiagram(Rhino_Ids::LayerNames::ARTCCs,
                          Rhino_Ids::LayerNames::ARTCC_Parent,
                          sector->getArtccBoundaries());
}

void RhinoFileExporter::writeARTCC_Highs()
{
    writeNamedLineDiagram(Rhino_Ids::LayerNames::ARTCC_Highs,
                          Rhino_Ids::LayerNames::ARTCC_Parent,
                          sector->getArtccHighBoundaries());
}

void RhinoFileExporter::writeARTCC_Lows()
{
    writeNamedLineDiagram(Rhino_Ids::LayerNames::ARTCC_Lows,
                          Rhino_Ids::LayerNames::ARTCC_Parent,
                          sector->getArtccLowBoundaries());
}

void RhinoFileExporter::writeHighAirways()
{
    writeNamedLineDiagram(Rhino_Ids::LayerNames::HighAirways,
                          Rhino_Ids::LayerNames::AirwaysParent,
                          sector->getHighAirways());
}

void RhinoFileExporter::writeLowAirways()
{
    writeNamedLineDiagram(Rhino_Ids::LayerNames::LowAirways,
                          Rhino_Ids::LayerNames::AirwaysParent,
                          sector->getLowAirways());
}

void RhinoFileExporter::writeNamedLineDiagram(const juce::String &layerName, const juce::String &parentLayer, const sector::NamedLineDiagramsContainer *container)
{
    if (container->isEmpty())
        return;
    
    createLayer(parentLayer, Colours::aliceblue);
    createLayer(layerName, Colours::aliceblue);
    setLayerToParent(layerName, parentLayer);
    
    for (int i = 0; i < container->size(); i++)
    {
        const String diagramName (container->atDiagram(i).getName());
        
        createLayer(diagramName, Colours::aliceblue);
        setLayerToParent(diagramName, layerName);
        
        for (int j = 0; j < container->atDiagram(i).size(); j++)
        {
            const sector::Line* line = &container->atDiagram(i).atLine(j);
            
            ON_LineCurve* lineCurve = new ON_LineCurve(lineCurveFromSectorLine(*line));
            addNotesToModelObject(*line, lineCurve);
            addSectorFileLine(lineCurve, *line);
            
            ONX_Model_Object* modelObject = addObjectToModel(lineCurve);
            setModelObjectLayer(modelObject, diagramName);
            setName(modelObject, diagramName);
        }
    }
}

//==============================================================================

void RhinoFileExporter::writeSID_Diagrams()
{
    writeSID_STAR_Container(Rhino_Ids::LayerNames::SIDs,
                            Rhino_Ids::LayerNames::SIDs_STARs_Parent,
                            sector->getSIDs());
}

void RhinoFileExporter::writeSTAR_Diagrams()
{
    writeSID_STAR_Container(Rhino_Ids::LayerNames::STARs,
                            Rhino_Ids::LayerNames::SIDs_STARs_Parent,
                            sector->getSTARs());
}

void RhinoFileExporter::writeSID_STAR_Container(const juce::String &containerName, const juce::String &parentLayer, const sector::SID_STAR_Container *container)
{
    if (container->isEmpty())
        return;
    
    createLayer(parentLayer, Colours::blue);
    createLayer(containerName, Colours::blueviolet);
    setLayerToParent(containerName, parentLayer);
    
    for (int i = 0; i < container->size(); i++)
    {
        const String& diagramName (container->atDiagram(i).getName());
        createLayer(diagramName, Colours::blueviolet);
        setLayerToParent(diagramName, containerName);
        
        for (int j = 0; j < container->atDiagram(i).size(); j++) {
            auto* sectorLine = &container->atDiagram(i).atLine(j);
            
            // Create line object
            auto* rhinoLine = new ON_LineCurve(lineCurveFromSectorLine(*sectorLine));
            addSectorFileLine(rhinoLine, *sectorLine);
            addNotesToModelObject(*sectorLine, rhinoLine);
            
            // Add it into the model
            auto* modelObject(addObjectToModel(rhinoLine));
            setModelObjectLayer(modelObject, diagramName);
            if (sectorLine->hasNamedColour())
                addNamedColour(modelObject, sectorLine->getNamedColour());
           
            // Set name
            setName(modelObject, diagramName);
        }
        
    }
}

void RhinoFileExporter::writeGeoLines ()
{
    GeoLinesContainer* container(sector->getGeoLines());
    
    if (container->size() == 0)
        return;
    
    createLayer(Rhino_Ids::LayerNames::Geos, Colours::black);
    
    for (int i = 0; i < container->size(); i++)
    {
        auto* containerLine = &container->getReference(i);
        
        auto* rhinoLine = new ON_LineCurve(lineCurveFromSectorLine(*containerLine));
        addSectorFileLine(rhinoLine, *containerLine);
        addNotesToModelObject(*containerLine, rhinoLine);
        
        // Add to model
        auto* modelObject(addObjectToModel(rhinoLine));
        setModelObjectLayer(modelObject, Rhino_Ids::LayerNames::Geos);
        if (containerLine->hasNamedColour())
            addNamedColour(modelObject, containerLine->getNamedColour());
    }
    
}

void RhinoFileExporter::writeRegions()
{
    const auto* container(sector->getRegions());
    
    if (container->size() == 0)
        return;
    
    createLayer(Rhino_Ids::LayerNames::Regions, Colours::white);
    
    // We will space all diagrams across the z-plane
    const double zAxisIncrements = 1.0 / (double) container->size();
    
    for (int i = 0; i < container->size(); i++)
    {
        auto* region = &container->getReference(i);
        double zAxis = ((double) i) * zAxisIncrements;
        
        createLayer(region->getName(), region->getColour());
        setLayerToParent(region->getName(), Rhino_Ids::LayerNames::Regions);
        
        for (int j = 0; j < region->size()-1; j++)
        {
            auto* regionCoordinate1 (&region->getReference(j));
            auto* regionCoordinate2 (&region->getReference(j+1));
            auto coordinatePoint1 (pointFromCoordinates(*regionCoordinate1));
            auto coordinatePoint2 (pointFromCoordinates(*regionCoordinate2));
            coordinatePoint1.z = zAxis;
            coordinatePoint2.z = zAxis;
            
            auto* rhinoLine = new ON_LineCurve(coordinatePoint1, coordinatePoint2);
            addSectorFileLine(rhinoLine, *regionCoordinate1);
            addNotesToModelObject(*regionCoordinate1, rhinoLine);
            
            auto* modelObject (addObjectToModel(rhinoLine));
            setModelObjectLayer(modelObject, Rhino_Ids::LayerNames::Regions);
            addNamedColour(modelObject, *region);
        }
    }
    
    /**
    for (int i = 0; i < container->size(); i++)
    {
        auto* mesh (new ON_Mesh(0, container->getReference(i).size(), false, false));
        
        for (int j = 0; j < container->getReference(i).size(); j++)
        {
            const auto* coordinate (&container->getReference(i).getReference(j));
            auto point = pointFromCoordinates(*coordinate);
            mesh->SetVertex(j, point);
        }
        
        mesh->ComputeVertexNormals();
        // TODO: calculate faces and set using SetTriangle
        mesh->ComputeFaceNormals();
        mesh->Compact();
        
        auto* modelObject(addObjectToModel(mesh));
        setModelObjectLayer(modelObject, Rhino_Ids::LayerNames::Regions);
        addNamedColour(modelObject, container->getReference(i).getColour());
    }
     */
}

//==============================================================================

ON_3dPoint RhinoFileExporter::pointFromCoordinates (const sector::Coordinates& coordinates)
{
    Point<double> point;
    Result r = projection.CoordinatesToCartesian(coordinates, point);
    
    if (!r)
        return ON_3dPoint(0, 0, 0);
    
    return ON_3dPoint(point.getX(), point.getY(), 0.0);
}

ON_LineCurve RhinoFileExporter::lineCurveFromCoordinates(const sector::Coordinates &start, const sector::Coordinates &end)
{
    return ON_LineCurve(pointFromCoordinates(start), pointFromCoordinates(end));
}

ON_LineCurve RhinoFileExporter::lineCurveFromSectorLine(const sector::Line &line)
{
    return lineCurveFromCoordinates(line.getStart().getCoordinate(), line.getEnd().getCoordinate());
}

ON_Color RhinoFileExporter::getAsRhinoColour(const juce::Colour &colour)
{
    return ON_Color(colour.getRed(), colour.getGreen(), colour.getBlue());
}

//==============================================================================

ONX_Model_Object* RhinoFileExporter::addObjectToModel (ON_Object* object)
{
    ONX_Model_Object* modelObject = &model->m_object_table.AppendNew();
    modelObject->m_object = object;
    return modelObject;
}

void RhinoFileExporter::addNotesToModelObject(const sector::Notes &notes, ON_Object* objectToWriteInto)
{
    if (notes.hasPrecedingNotes())
    {
        bool result = objectToWriteInto->SetUserString(L"PreceedingNotes", notes.getPrecedingNotes().toWideCharPointer());
        jassert(result);
    }
    
    if (notes.hasNotes())
    {
        objectToWriteInto->SetUserString(L"Notes", notes.getNotes().toWideCharPointer());
    }
}

void RhinoFileExporter::addNameToModelObject(const sector::NamedObject objectWithName, ONX_Model_Object *objectToAddTo)
{
    if (!objectWithName.hasName())
        return;
    
    objectToAddTo->m_attributes.m_name = objectWithName.getName().toWideCharPointer();
}

void RhinoFileExporter::addNamedColour(ONX_Model_Object *modelObject, const sector::NamedColour &colour)
{
    setModelObjectColour(modelObject, colour.getColour());
    
    if (colour.hasName())
    {
        modelObject->m_attributes.m_material_index = getIndexOfMaterialName(colour);
        modelObject->m_attributes.SetMaterialSource(ON::object_material_source::material_from_object);
    }
}

void RhinoFileExporter::addSectorFileLine(ON_Object *object, const sector::SectorFileLine &fileLine)
{
    if (!fileLine.hasValidSectorFileLine())
        return;

    object->SetUserString(L"SectorFileString", fileLine.getLineFromSectorFile().toWideCharPointer());
    object->SetUserString(L"SectorLineNumber", String(fileLine.getLineNumberInSectorFile()).toWideCharPointer());
}

void RhinoFileExporter::setName(ONX_Model_Object *modelObject, const juce::String &name)
{
    modelObject->m_attributes.m_name = name.toWideCharPointer();
}

//==============================================================================

void RhinoFileExporter::setModelObjectLayer(ONX_Model_Object *modelObject, const juce::String &layerName)
{
    if (hasLayer(layerName))
        modelObject->m_attributes.m_layer_index = getIndexForLayer(layerName);
}

int RhinoFileExporter::getIndexForLayer(const juce::String &layerName)
{
    auto layer = getLayerFor(layerName);
    
    if (layer == nullptr)
        return -1;
    
    return layer->m_layer_index;
}

bool RhinoFileExporter::hasLayer(const juce::String &layerName)
{
    return getLayerFor(layerName) != nullptr;
}

ON_Layer* RhinoFileExporter::createLayer(const juce::String &layerName, const juce::Colour &layerColour)
{
    ON_Layer newLayer;
    newLayer.SetLayerName(layerName.toWideCharPointer());
    ON_Color colour(layerColour.getRed(), layerColour.getGreen(), layerColour.getBlue());
    newLayer.SetColor(colour);
    return addIfNotAlreadyThere(newLayer);
}

ON_Layer* RhinoFileExporter::getLayerFor(const juce::String &layerName)
{
    for (int i = 0; i < model->m_layer_table.Count(); i++)
    {
        ON_Layer* layer = model->m_layer_table.At(i);
        if (layer->m_name == layerName.toWideCharPointer())
            return layer;
    }
    return nullptr;
}

ON_Layer* RhinoFileExporter::addIfNotAlreadyThere(const ON_Layer& layer)
{
    String name(layer.m_name);
    if (hasLayer(name))
        return getLayerFor(name);
    
    model->m_layer_table.Append(layer);
    
    // Set the layer index
    model->m_layer_table.Last()->m_layer_index = getNextNewLayerIndex();
    
    return model->m_layer_table.Last();
}

int RhinoFileExporter::getNextNewLayerIndex()
{
    return lastLayerIndex++;
}

void RhinoFileExporter::setLayerToParent(const juce::String &child, const juce::String &parent)
{
    model->Polish();
    
    const int parentIndex (getIndexForLayer(parent));
    const int childIndex (getIndexForLayer(child));
    if (parentIndex < 0 || childIndex < 0)
        return;
    
    ON_UUID parentId(model->m_layer_table[parentIndex].m_layer_id);
    model->m_layer_table[childIndex].m_parent_layer_id = parentId;
    
    return;
}

//==============================================================================

void RhinoFileExporter::setModelObjectColour(ONX_Model_Object *modelObject, const juce::Colour &colour)
{
    ON_Color rhinoColour(colour.getRed(), colour.getGreen(), colour.getGreen());
    modelObject->m_attributes.m_color = rhinoColour;
    modelObject->m_attributes.SetColorSource(ON::object_color_source::color_from_object);
}

int RhinoFileExporter::getNextNewMaterialIndex()
{
    return lastMaterialIndex++;
}

bool RhinoFileExporter::hasMaterialForColour(const sector::NamedColour &colour)
{
    if (!colour.hasName())
        return false;
    
    if (getIndexOfMaterialName(colour) < 0)
        return false;
    
    return true;
}

int RhinoFileExporter::getIndexOfMaterialName(const sector::NamedColour &colour)
{
    if (!colour.hasName())
        return -1;
    
    const String name = colour.getName();
    
    for (int i = 0; i < model->m_material_table.SizeOfArray(); i++)
    {
        ON_Material* material(model->m_material_table.At(i));
        if (material->m_material_name == name.toWideCharPointer())
            return material->m_material_index;
    }
    return -1;
}
