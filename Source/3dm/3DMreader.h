//
//  3DMreader.h
//  fstream
//
//  Created by James Mazur on 12/22/12.
//  Copyright (c) 2012 James Mazur. All rights reserved.
//

#ifndef __fstream___DMreader__
#define __fstream___DMreader__

#include <iostream>
#include "opennurbs.h"
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <algorithm>
#include "sct/SCTobj.h"
#include "helpers/GeoConvert.h"
#include <GeographicLib/GeoCoords.hpp>


bool read3dm();

bool readObjectTable (ONX_Model &model);

bool readNotes (ONX_Model &model);

bool processLine_Crv (const ON_LineCurve *line_crv, ON_Layer *mLayer, const ON_3dmObjectAttributes &attr, ONX_Model &model);

bool processPolyline_Crv (const ON_PolylineCurve *pline_crv, ON_Layer *mLayer, const ON_3dmObjectAttributes &attr, ONX_Model &model);

bool processPoint (const ON_Point *pnt, ON_Layer *mLayer, const ON_3dmObjectAttributes &attr, ONX_Model &model);

bool hasParentLayerName (std::string &sParentName, ONX_Model &model, ON_Layer *mLayer);

bool hasColor (cRGBColor &cColor, ON_Layer *mLayer, const ON_3dmObjectAttributes &attr);

bool isChildOfLayer (std::vector<std::string> sSearchName, std::string &sSearchResult, std::vector<std::string> &vFullLayerTree, const ONX_Model &model, const ON_Layer *mLayer);

bool hasName (std::string &sName, const ON_3dmObjectAttributes &attr);

std::string wchar2stirng (const wchar_t *wsName);

std::string wstring2string (ON_wString wString);

#endif /* defined(__fstream___DMreader__) */
