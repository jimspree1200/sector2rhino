    //
//  3DMreader.cpp
//  fstream
//
//  Created by James Mazur on 12/22/12.
//  Copyright (c) 2012 James Mazur. All rights reserved.
//
//

#include "3DMreader.h"
#include "Application.h"

// Evil globals!
//extern SCTman Sector;

bool read3dm() {
    
    ONX_Model model;
    
    ON_TextLog dump;
    
    ON_wString ws_arg = "KBDL.3dm";
    const wchar_t* FileName2 = ws_arg;
    
    FILE* archive_fp = ON::OpenFile( FileName2, L"rb");
    if ( !archive_fp )
    {
        dump.Print("  Unable to open file.\n" );
        return false;
    }
    
    dump.PushIndent();
    
    // create achive object from file pointer
    ON_BinaryFile archive( ON::read3dm, archive_fp );
    
    // read the contents of the file into "model"
    bool rc = model.Read( archive, &dump );
    
    // close the file
    ON::CloseFile( archive_fp );
    
    // print diagnostic
    if ( rc )
        dump.Print("Successfully read.\n");
    else
        dump.Print("Errors during reading.\n");
    
    // see if everything is in good shape
    if ( model.IsValid(&dump) )
    {
        dump.Print("Model is valid.\n");
    }
    else
    {
        dump.Print("Model is not valid.\n");
    }
    
    readNotes(model);
    std::cout << "WTF" << std::endl;
    readObjectTable(model);
    
    // destroy this model
    model.Destroy();
    
    
    return rc;
}

bool readNotes (ONX_Model &model) {
//    ON_wString wNotes = model.m_properties.m_Notes.m_notes;
//    std::string sNotes = wstring2string(wNotes);
//    std::istringstream ssNotes(sNotes);
//    //ssNotes << sNotes;
//    std::string sCurrentLine;
//    std::vector<std::string> vSplitStrings;
//    cRGBColor cColor;
//    
//    while (std::getline(ssNotes, sCurrentLine)) {
//        std::cout << sCurrentLine << std::endl;
//        if (sCurrentLine.find("[INFO]") != std::string::npos) {
//            std::cout << "[INFO] Section Found" << std::endl;
//            for (int i = 0; i < 8; i++) {
//                std::getline(ssNotes, sCurrentLine);
//                std::cout << "[INFO] adding:" << sCurrentLine << std::endl;
//                Sector.addInfoLine(sCurrentLine);
//            }
//        }
//        if (sCurrentLine.find("#") != std::string::npos) {
//            vSplitStrings = split_space_tab(sCurrentLine);
//            cColor = SCTcolor2RGB(vSplitStrings.at(2));
//            Sector.addDefinedColor(vSplitStrings.at(1), cColor);
//        }
//        
//    }
//    //wStringStream >> sNotes;
//    std::vector<std::string> vSplitLines = split_line(sNotes);
//    
//    //for (int i = 0; i < vSplitLines.size(); i++) {
//    //    std::cout << vSplitLines.at(i);
//    //}
//    
//    std::cout << sNotes;
    return true;
}

bool readObjectTable (ONX_Model &model) {
    
    for (int i = 0; i < model.m_object_table.Count(); i++)
    {
        // Get a model object
        const ONX_Model_Object& model_obj = model.m_object_table[i];
        
        // Get the geometry of the object
        const ON_Object* obj = model_obj.m_object;
        if( 0 == obj )
            continue;
        
        // Get the attributes of the object
        const ON_3dmObjectAttributes attr = model_obj.m_attributes;
        
        // Get the layer of the object
        ON_Layer *mLayer;
        mLayer = model.m_layer_table.At(attr.m_layer_index);
        
        // Is the geometry a curve?
        const ON_Curve* crv = ON_Curve::Cast( obj );
        if( crv )
        {
            // Is the curve a line?
            const ON_LineCurve* line_crv = ON_LineCurve::Cast( crv );
            if( line_crv )
            {
                processLine_Crv(line_crv, mLayer, attr, model);
                continue;
            }
            
            // Is the curve a polyline?
            const ON_PolylineCurve* pline_crv = ON_PolylineCurve::Cast( crv );
            if( pline_crv )
            {
                //processLine_PlineCrv(pline_crv, mLayer, attr, model);
                processPolyline_Crv(pline_crv, mLayer, attr, model);
                continue;
            }
            
            // Is the curve an arc (or circle)?
            const ON_ArcCurve* arc_crv = ON_ArcCurve::Cast( crv );
            if( arc_crv )
            {
                // TODO: process arc curve - conversion is supported, doubt it!
                continue;
            }
            
            // Is the curve a polycurve?
            const ON_PolyCurve* poly_crv = ON_PolyCurve::Cast( crv );
            if( poly_crv )
            {
                // TODO: process poly curve
                continue;
            }
            
            // Is the curve a NURBS curve?
            const ON_NurbsCurve* nurbs_crv = ON_NurbsCurve::Cast( crv );
            if( nurbs_crv )
            {
                // TODO: process NURBS curve
                continue;
            }
        }
        
        const ON_Point* pnt = ON_Point::Cast( obj);
        if ( pnt ) {
            std::cout << "Point found" << std::endl;
            processPoint(pnt, mLayer, attr, model);
        }
    }
    return true;
}

bool processPoint (const ON_Point *pnt, ON_Layer *mLayer, const ON_3dmObjectAttributes &attr, ONX_Model &model) {
    
    //Get Object Name;
    std::string sName = ""; // Set default name
    hasName(sName, attr);
    std::vector<std::string> splitStrings = split_space_tab(sName);
    
    std::string sNote = "";
    
    std::string sLayerName = wchar2stirng(mLayer->LayerName());
    
    GeographicLib::GeoCoords coord;
    Mercator2Geo(pnt->point.x, pnt->point.y, coord);
    
    std::string sParentName;
    
    if (sLayerName == "Airports") {
        
        if (splitStrings.size() < 3) {
            std::cout << "[ERROR] - Not enough parameters for airport" << std::endl;
            return false;
        }
        if (splitStrings.size() > 3) {
            sNote = splitStrings.at(3);
            for (int i = 4; i < splitStrings.size(); i++) {
                sNote = sNote + " " + splitStrings.at(i);
            }
        }
        Mercator2Geo(pnt->point.x, pnt->point.y, coord);
        
        SCTman& Sector = Sector2RhinoApplication::getSectorOld();
        Sector.addAirport(splitStrings.at(0), splitStrings.at(1), coord, splitStrings.at(2), sNote);
        return true;
    }
    
    if (sLayerName == "VORs") {
        
        if (splitStrings.size() < 2) {
            std::cout << "[ERROR] - Not enough parameters for VOR" << std::endl;
            return false;
        }
        if (splitStrings.size() > 2) {
            sNote = splitStrings.at(2);
            for (int i = 3; i < splitStrings.size(); i++) {
                sNote = sNote + " " + splitStrings.at(i);
            }
        }
        
        SCTman& Sector = Sector2RhinoApplication::getSectorOld();
        Sector.addVORNDB(RadioNav::VOR_old, splitStrings.at(0), splitStrings.at(1), coord, sNote);
        return true;
    }
    
    if (sLayerName == "NDBs") {
        if (splitStrings.size() < 2) {
            std::cout << "[ERROR] - Not enough parameters for NDB" << std::endl;
            return false;
        }
        if (splitStrings.size() > 2) {
            sNote = splitStrings.at(2);
            for (int i = 3; i < splitStrings.size(); i++) {
                sNote = sNote + " " + splitStrings.at(i);
            }
        }
        
        SCTman& Sector = Sector2RhinoApplication::getSectorOld();
        Sector.addVORNDB(RadioNav::NDB_old, splitStrings.at(0), splitStrings.at(1), coord, sNote);
        return true;
    }
    
    if (sLayerName == "Fixes") {
        if (splitStrings.size() < 1) {
            std::cout << "[ERROR] - Not enough parameters for Fix" << std::endl;
            return false;
        }
        if (splitStrings.size() > 1) {
            sNote = splitStrings.at(1);
            for (int i = 2; i < splitStrings.size(); i++) {
                sNote = sNote + " " + splitStrings.at(i);
            }
        }
        
        SCTman& Sector = Sector2RhinoApplication::getSectorOld();
        Sector.addFix(splitStrings.at(0), coord, sNote);
        return true;
    }
    
    
    
    std::cout << "[ERROR] - Point not found on valid layer, skipping" << std::endl;
    return false;
}

bool processLine_Crv (const ON_LineCurve *line_crv, ON_Layer *mLayer, const ON_3dmObjectAttributes &attr, ONX_Model &model) {
    
    std::string sNote;
    
    //Get Object Name;
    std::string sObjName = ""; // Set default name
    hasName(sObjName, attr);
    
    std::vector<std::string> splitStrings = split_space_tab(sObjName);
    
    //std::string sLayerName = wchar2stirng(mLayer->LayerName());
    
    // Get start and end points, test if they are valid coords
    GeographicLib::GeoCoords coord1;
    GeographicLib::GeoCoords coord2;
    ON_3dPoint ptStart = line_crv->ON_LineCurve::PointAtStart();
    ON_3dPoint ptEnd = line_crv->ON_LineCurve::PointAtEnd();
    if (!Mercator2Geo(ptStart.x, ptStart.y, coord1) ||
        !Mercator2Geo(ptEnd.x, ptEnd.y, coord2) ) {
        return false;
    }
    
    // Check if obj has color
    cRGBColor cColor;
    hasColor(cColor, mLayer, attr);
    
    // Recursivley search layer tree to find valid root layer, if no result found, terminate
    std::string sSearchResult;
    std::string sSearchNames[9] = {"Runways", "Low Airways", "High Airways", "ARTCC", "ARTCC Low", "ARTCC High", "SIDs", "STARs", "Geography"};
    std::vector<std::string> vSearchNames(&sSearchNames[0], &sSearchNames[0] + 9);
    std::vector<std::string> vFullLayerTree;
    if (!isChildOfLayer(vSearchNames, sSearchResult, vFullLayerTree, model, mLayer)) return false;
    
    // Turn vector of tree names into one string
    std::string sTree = "";
    for (int i = 1; i < vFullLayerTree.size(); i++) {
        sTree = sTree + "/" + vFullLayerTree.at(i);
    }
    
    
    // Handle adding to SCT obj based on root layer name
    if (sSearchResult == "STARs" || sSearchResult == "SIDs" || sSearchResult == "Geography") {
        
        DiagramType eDiagramType;
        if (sSearchResult == "STARs") eDiagramType = STAR;
        else if (sSearchResult == "SIDs") eDiagramType = SID_hack;
        else if (sSearchResult == "Geography") eDiagramType = GEO;
        
        // Properly space tree name from obj name
        if (sTree != "" && sObjName != "") sTree += " ";
        sNote = sTree + sObjName;
        
        SCTman& Sector = Sector2RhinoApplication::getSectorOld();
        Sector.addDiagramLine(&coord1, &coord2, &vFullLayerTree.at(0), &cColor, &eDiagramType, &sNote);
        
        return true;
    }
    
    else if (sSearchResult == "Runways") {
        
        // First four words are rwy descriptors, notes should follow
        if (splitStrings.size() > 4) {
            sNote = splitStrings.at(4);
            for (int i = 5; i < splitStrings.size(); i++) {
                sNote = sNote + " " + splitStrings.at(i);
            }
        }
        
        // Properly space tree name from obj name
        if (sTree != "" && sObjName != "") sTree += " ";
        sNote = sTree + sNote;
        
        SCTman& Sector = Sector2RhinoApplication::getSectorOld();
        Sector.addRunway(splitStrings.at(0), splitStrings.at(1), splitStrings.at(2), splitStrings.at(3), coord1, coord2, sNote);
        
        return true;
    }
    
    else if (sSearchResult == "Low Airways" ||
             sSearchResult == "High Airways" ||
             sSearchResult == "ARTCC" ||
             sSearchResult == "ARTCC Low" ||
             sSearchResult == "ARTCC High") {
        
        BoundryOrAir eBoundryOrAir;
        if (sSearchResult == "Low Airways") eBoundryOrAir = LOW_AIRWAY;
        if (sSearchResult == "High Airways") eBoundryOrAir = HIGH_AIRWAY;
        if (sSearchResult == "ARTCC") eBoundryOrAir = ARTCC;
        if (sSearchResult == "ARTCC Low") eBoundryOrAir = ARTCC_LOW;
        if (sSearchResult == "ARTCC High") eBoundryOrAir = ARTCC_HIGH;
        
        // Any words after first word are notes
        if (splitStrings.size() > 0) {
            sNote = splitStrings.at(0);
            for (int i = 1; i < splitStrings.size(); i++) {
                sNote = sNote + " " + splitStrings.at(i);
            }
        }
        
        // Properly space tree name from obj name
        if (sTree != "" && sObjName != "") sTree += " ";
        sNote = sTree + sNote;
        
        SCTman& Sector = Sector2RhinoApplication::getSectorOld();
        Sector.addARTCCorAirway(eBoundryOrAir, vFullLayerTree.at(0), coord1, coord2, sNote);
        
        return true;
    }
    
    //shouldnt be needed as isChildOfLayer should already catch non matching root names
    else return false;
    
}

bool processPolyline_Crv (const ON_PolylineCurve *pline_crv, ON_Layer *mLayer, const ON_3dmObjectAttributes &attr, ONX_Model &model) {
    
    SCTman& Sector = Sector2RhinoApplication::getSectorOld();
    
    std::string sNote;
    
    //Get Object Name;
    std::string sObjName = ""; // Set default name
    hasName(sObjName, attr);
    
    std::vector<std::string> splitStrings = split_space_tab(sObjName);
    
    //std::string sLayerName = wchar2stirng(mLayer->LayerName());
    
    // Get start and end points, test if they are valid coords
    GeographicLib::GeoCoords coord1;
    GeographicLib::GeoCoords coord2;
    ON_3dPoint ptStart;
    ON_3dPoint ptEnd;
    
    // Check if obj has color
    cRGBColor cColor;
    hasColor(cColor, mLayer, attr);
    
    // Recursivley search layer tree to find valid root layer, if no result found, terminate
    std::string sSearchResult;
    std::string sSearchNames[10] = {"Runways", "Low Airways", "High Airways", "ARTCC", "ARTCC Low", "ARTCC High", "SIDs", "STARs", "Geography", "Regions"};
    std::vector<std::string> vSearchNames(&sSearchNames[0], &sSearchNames[0] + 10);
    std::vector<std::string> vFullLayerTree;
    if (!isChildOfLayer(vSearchNames, sSearchResult, vFullLayerTree, model, mLayer)) return false;
    
    // Turn vector of tree names into one string
    std::string sTree = "";
    for (int i = 1; i < vFullLayerTree.size(); i++) {
        sTree = sTree + "/" + vFullLayerTree.at(i);
    }
    
    
    // Handle adding to SCT obj based on root layer name
    if (sSearchResult == "STARs" || sSearchResult == "SIDs" || sSearchResult == "Geography") {
        
        DiagramType eDiagramType;
        if (sSearchResult == "STARs") eDiagramType = STAR;
        else if (sSearchResult == "SIDs") eDiagramType = SID_hack;
        else if (sSearchResult == "Geography") eDiagramType = GEO;
        
        // Properly space tree name from obj name
        if (sTree != "" && sObjName != "") sTree += " ";
        sNote = sTree + sObjName;
        
        for (int i = 0 ; i < (pline_crv->m_pline.PointCount()-1); i++) {
            ptStart = pline_crv->m_pline[i];
            ptEnd = pline_crv->m_pline[i+1];
            if (Mercator2Geo(ptStart.x, ptStart.y, coord1) && Mercator2Geo(ptEnd.x, ptEnd.y, coord2) )
            {
                Sector.addDiagramLine(&coord1, &coord2, &vFullLayerTree.at(0), &cColor, &eDiagramType, &sNote);
            }
        }
        
        return true;
    }

    else if (sSearchResult == "Low Airways" ||
             sSearchResult == "High Airways" ||
             sSearchResult == "ARTCC" ||
             sSearchResult == "ARTCC Low" ||
             sSearchResult == "ARTCC High") {
        
        BoundryOrAir eBoundryOrAir;
        if (sSearchResult == "Low Airways") eBoundryOrAir = LOW_AIRWAY;
        if (sSearchResult == "High Airways") eBoundryOrAir = HIGH_AIRWAY;
        if (sSearchResult == "ARTCC") eBoundryOrAir = ARTCC;
        if (sSearchResult == "ARTCC Low") eBoundryOrAir = ARTCC_LOW;
        if (sSearchResult == "ARTCC High") eBoundryOrAir = ARTCC_HIGH;
        
        // Any words after first word are notes
        if (splitStrings.size() > 1) {
            sNote = splitStrings.at(1);
            for (int i = 2; i < splitStrings.size(); i++) {
                sNote = sNote + " " + splitStrings.at(i);
            }
        }
        
        // Properly space tree name from obj name
        if (sTree != "" && sObjName != "") sTree += " ";
        sNote = sTree + sNote;
        
        for (int i = 0 ; i < (pline_crv->m_pline.Count()-1); i++) {
            ptStart = pline_crv->m_pline[i];
            ptEnd = pline_crv->m_pline[i+1];
            if ( Mercator2Geo(ptStart.x, ptStart.y, coord1) && Mercator2Geo(ptEnd.x, ptEnd.y, coord2))
                Sector.addARTCCorAirway(eBoundryOrAir, splitStrings.at(0), coord1, coord2, sNote);
        }
        
        return true;
    }
    
    else if (sSearchResult == "Regions") {
        
        // Properly space tree name from obj name
        if (sTree != "" && sObjName != "") sTree += " ";
        sNote = sTree + sObjName;
        
        ptStart = pline_crv->m_pline[0];
        if (Mercator2Geo(ptStart.x, ptStart.y, coord1)) Sector.startRegion(cColor, coord1, ptStart.z, sNote);
        for (int i = 1 ; i < pline_crv->m_pline.PointCount()-1; i++) {
            ptStart = pline_crv->m_pline[i];
            if (Mercator2Geo(ptStart.x, ptStart.y, coord1)) Sector.addToRegion(coord1);
        }
        return true;
        
        return true;
    }
    
    //shouldnt be needed as isChildOfLayer should already catch non matching root names
    else return false;
    
}

bool hasParentLayerName (std::string &sParentName, ONX_Model &model, ON_Layer *mLayer) {
    
   // model.IsValid();
    
    ON_wString wsParentLayerName;
    
    if (ON_UuidIsNotNil(mLayer->m_parent_layer_id)) {
        for (int i = 0; i < model.m_layer_table.Count(); i++) {
            if (0 == ON_UuidCompare(mLayer->m_parent_layer_id, model.m_layer_table[i].m_layer_id)) {
                wsParentLayerName = model.m_layer_table[i].m_name;
            }
        }
        wchar_t *wcParentLayerName = wsParentLayerName.Array();
        ON_wString wsChildLayerName = mLayer->LayerName();
        wchar_t *wcChildLayerName = wsChildLayerName.Array();
        std::wcout << wcParentLayerName << " is Parent Name of " << wcChildLayerName << std::endl;
        sParentName = wchar2stirng(wsParentLayerName);
        return true;
    } 

    return false;
    
}

bool hasColor (cRGBColor &cColor, ON_Layer *mLayer, const ON_3dmObjectAttributes &attr) {
    //Get Object Color
    ON_Color mColor;
    
    if (attr.ColorSource() == ON::color_from_layer) {
        mColor = mLayer->Color();
        cColor.r = mColor.Red();
        cColor.g = mColor.Green();
        cColor.b = mColor.Blue();
        return true;
    }
    if (attr.ColorSource() == ON::color_from_object) {
        mColor = attr.m_color;
        cColor.r = mColor.Red();
        cColor.g = mColor.Green();
        cColor.b = mColor.Blue();
        return true;
    }
    else return false;
}

bool isChildOfLayer (std::vector<std::string> sSearchName, std::string &sSearchResult, std::vector<std::string> &vFullLayerTree, const ONX_Model &model, const ON_Layer *mLayer) {
    
    ON_wString wsParentLayerName;
    
    ON_UUID currentParentUUID = mLayer->m_parent_layer_id;
    
    if (ON_UuidIsNil(currentParentUUID)) {
        wsParentLayerName = mLayer->m_name;
    }
    
    vFullLayerTree.clear();
    vFullLayerTree.push_back(wstring2string(mLayer->m_name));

    while (ON_UuidIsNotNil(currentParentUUID)) {
        for (int i = 0; i < model.m_layer_table.Count(); i++) {
            
            if (0 == ON_UuidCompare(currentParentUUID, model.m_layer_table[i].m_layer_id)) {
                
                if (ON_UuidIsNotNil(model.m_layer_table[i].m_parent_layer_id)) {
                    vFullLayerTree.push_back(wstring2string(model.m_layer_table[i].m_name));
                    currentParentUUID = model.m_layer_table[i].m_parent_layer_id;
                    break;
                }
                else {
                    wsParentLayerName = model.m_layer_table[i].m_name;
                    currentParentUUID = model.m_layer_table[i].m_parent_layer_id;
                }
                
            }
        }
    }
    
    for (size_t i = 0; i < sSearchName.size(); i++) {
        ON_wString currentSearchString = sSearchName.at(i).c_str();
        if (currentSearchString == wsParentLayerName) {
            std::reverse(vFullLayerTree.begin(), vFullLayerTree.end());
            sSearchResult = sSearchName.at(i).c_str();
            return true;
        }
    }
    
    vFullLayerTree.clear();
    return false;
    
}

bool hasName (std::string &sName, const ON_3dmObjectAttributes &attr) {
    if (attr.m_name.IsEmpty() == false) {
        //const wchar_t *wsName = attr.m_name.Array();
        sName = wstring2string(attr.m_name);
        //sName = wchar2stirng(wsName);
        return true;
    }
    else return false;
}

std::string wchar2stirng (const wchar_t *wsName) {
    char cName[40];
    size_t ret;
    
    ret = wcstombs(cName, wsName, sizeof(cName));
    if (ret==32) cName[31]='\0';
    std::string sName = cName;
    return sName;
}

std::string wstring2string (ON_wString wString)
{
    std::string rString;
    for (int i = 0; i < wString.Length(); i++) {
        char nChar = std::cin.narrow(wString.GetAt(i), ' ');
        if (nChar == '\r') nChar = '\n';
        rString.push_back(nChar);
    }
    return rString;
}