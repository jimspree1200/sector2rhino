/*
  ==============================================================================

    3dmFileExporter.h
    Created: 24 Feb 2015 5:19:05pm
    Author:  Jim Mazur

  ==============================================================================
*/

#ifndef RHINOFILEEXPORTER_H_INCLUDED
#define RHINOFILEEXPORTER_H_INCLUDED

#include "../../JuceLibraryCode/JuceHeader.h"
#include "../helpers/CoordinateProjections.h"
#include "../model/Sector.h"

class ONX_Model;
class ON_Layer;
class ON_Color;
class ONX_Model_Object;
class ON_Object;
class ON_Point;
class ON_3dPoint;
class ON_LineCurve;

class RhinoFileExporter :   public Thread
{
public:
    RhinoFileExporter(File fileToWrite, sector::SectorContainer* sectorToRead);
    
    void run ();
    
private:
    enum LayerIndexes
    {
        Airport,
        Runway,
        VOR,
        NDB,
        Fixes,
        LowAirways,
        HighAirways,
        Artcc,
        ArtccHigh,
        ArtccLow,
        SID,
        Star,
        Geo,
        Regions,
        Labels
    };
    
    Result writeModel();
    Result writeFile();
    
    void writeNotes();
    void writeDefines();
    void writeInfo();
    
    void writeAirports();
    
    void writeVORs();
    void writeNDBs();
    void writeFixes();
    ONX_Model_Object* writeRadioNavAidPoint (const sector::RadioNavAid* navaid);
    ON_Point* createNamedCoordinatePoint(const sector::NamedCoordinate& coord);
    
    void writeRunways();
    
    void writeARTCC();
    void writeARTCC_Highs();
    void writeARTCC_Lows();
    void writeHighAirways();
    void writeLowAirways();
    void writeNamedLineDiagram(const String& layerName, const String& parentLayer, const sector::NamedLineDiagramsContainer* container);
    
    void writeSID_Diagrams ();
    void writeSTAR_Diagrams ();
    void writeSID_STAR_Container (const String& containerName, const String& parentLayer, const sector::SID_STAR_Container* container);
    
    void writeGeoLines ();
    void writeRegions ();
    void writeLabel ();
    
    // Conversions
    ON_3dPoint pointFromCoordinates (const sector::Coordinates& coordinates);
    
    ON_LineCurve lineCurveFromCoordinates (const sector::Coordinates& start, const sector::Coordinates& end);
    
    ON_LineCurve lineCurveFromSectorLine (const sector::Line& line);
    
    ON_Color getAsRhinoColour (const Colour& colour);
    
    // Adding and modifying objects
    ONX_Model_Object* addObjectToModel (ON_Object* object);
    void addNotesToModelObject (const sector::Notes& notes, ON_Object* objectToWriteInto);
    void addNameToModelObject (const sector::NamedObject objectWithName, ONX_Model_Object* objectToAddTo);
    void addNamedColour (ONX_Model_Object* modelObject, const sector::NamedColour& colour);
    void addSectorFileLine (ON_Object* object, const sector::SectorFileLine& fileLine);
    void setModelObjectLayer (ONX_Model_Object* modelObject, const String& layerName);
    void setModelObjectColour (ONX_Model_Object* modelObject, const Colour& colour);
    void setName (ONX_Model_Object* modelObject, const String& name);
    
    // Layers
    int getIndexForLayer(const String& layerName);
    bool hasLayer (const String& layerName);
    ON_Layer* createLayer (const String& layerName, const Colour& layerColour);
    ON_Layer* getLayerFor (const String& layerName);
    ON_Layer* addIfNotAlreadyThere (const ON_Layer& layer);
    int getNextNewLayerIndex();
    void setLayerToParent (const String& child, const String& parent);
    
    // Colours
    int getNextNewMaterialIndex();
    bool hasMaterialForColour (const sector::NamedColour& colour);
    int getIndexOfMaterialName (const sector::NamedColour& colour);
    
private:
    File rhinoFile;
    sector::SectorContainer* sector;
    
    CoordinatesProjection projection;
    
    ScopedPointer<ONX_Model> model;
    int lastLayerIndex;
    int lastMaterialIndex;
};

#endif  // 3DMFILEEXPORTER_H_INCLUDED
