/* $NoKeywords: $ */
/*
//
// Copyright (c) 1993-2011 Robert McNeel & Associates. All rights reserved.
// OpenNURBS, Rhinoceros, and Rhino3D are registered trademarks of Robert
// McNeel & Assoicates.
//
// THIS SOFTWARE IS PROVIDED "AS IS" WITHOUT EXPRESS OR IMPLIED WARRANTY.
// ALL IMPLIED WARRANTIES OF FITNESS FOR ANY PARTICULAR PURPOSE AND OF
// MERCHANTABILITY ARE HEREBY DISCLAIMED.
//				
// For complete openNURBS copyright information see <http://www.opennurbs.org>.
//
////////////////////////////////////////////////////////////////
*/


#include "3DMwriter.h"
#include <stdlib.h>
#include "Application.h"

#define Airport_layerIndex 1
#define Runway_layerIndex 2
#define VOR_layerIndex 3
#define NDB_layerIndex 4
#define Fixes_layerIndex 5
#define LowAirway_layerIndex 6
#define HighAirway_layerIndex 7
#define ARTCC_layerIndex 8
#define ARTCCLow_layerIndex 9
#define ARTCCHigh_layerIndex 10
#define SID_layerIndex 11
#define STAR_layerIndex 12
#define GEO_layerInex 13
#define Regions_layerInex 14
//
#define layerCount 15
//
#define FSXTopLayer_layerIndex 15
#define FSXParking_layerIndex 16
#define FSXTaxiPoints_layerIndex 17


#define UTMZone -72

int nTopLayerCount = 0;
int nCurrentLayerIndex = layerCount;

int nLowAltAirwayLayersCount = 0;
int nHighAltAirwayLayersCount = 0;
int nARTCClayersCount = 0;
int nARTCCLowLayersCount = 0;
int nARTCCHighLayersCount = 0;
int nSIDlayersCount = 0;
int nSTARlayersCount = 0;
int nFSXparkinglayersCount = 0;
int nFSXtaxiwayPointsCount = 0;



// Evil globals!
//extern SCTman Sector;
//extern XMLAirport xmlAirport;

int objCount = 0;


static bool write_Model( FILE* fp, int version, ON_TextLog& error_log )
{
    // Write Properties

    // example demonstrates how to write a NURBS curve, line, and circle
    ONX_Model model;
    
    // file properties (notes, preview image, revision history, ...)
    
    // set revision history information
    model.m_properties.m_RevisionHistory.NewRevision();
    
    // set application information
    model.m_properties.m_Application.m_application_name = "SCT to openNURBS Converter";
    model.m_properties.m_Application.m_application_URL = "http://www.jimmazur.com";
    model.m_properties.m_Application.m_application_details = "Boston ARTCC";
    
    
    // file settings (units, tolerances, views, ...)
    model.m_settings.m_ModelUnitsAndTolerances.m_unit_system = ON::meters;
    model.m_settings.m_ModelUnitsAndTolerances.m_absolute_tolerance = 0.00001;
    model.m_settings.m_ModelUnitsAndTolerances.m_angle_tolerance = ON_PI/180.0; // radians
    model.m_settings.m_ModelUnitsAndTolerances.m_relative_tolerance = 0.01; // 1%
    
    ON_3dmRenderSettings renderSettings;
    
    model.m_settings.m_RenderSettings.m_background_color.SetRGB(0, 0, 0);
    
    write_Notes(model);
    
    write_layers(model);
    
    write_Fixes(model);
    write_Airports(model);
    write_Runways(model);
    write_NDBVOR(model);
    write_ARTCC_Airways(model);
    write_SIDSTAR(model, SID_hack);
    write_SIDSTAR(model, STAR);
    write_GEO(model);
    write_REGIONS(model);
    
    XMLAirport& xmlAirport = Sector2RhinoApplication::getXmlAiport();
    
    if (xmlAirport.countTaxiwayParking() > 0) write_FSXTaxiwayParkingPoints(model);
    
    if (xmlAirport.countTaxiPoints() > 0) write_FSXTaxiPoints(model);
    
    //int error_cout;
    //ON_SimpleArray<int> error_warnings;
    //model.Audit(true, &error_cout, &error_log, &error_warnings);
    model.Polish();
    
    write_SetLayerHierarchy(model);
    
    // start section comments
    const char* sStartSectionComment = __FILE__ "write_curves_example()" __DATE__;
    
    ON_BinaryFile archive( ON::write3dm, fp ); // fp = pointer from fopoen(...,"wb")
    
    // Set uuid's, indices, etc.
    //model.Polish();
    
    // writes model to archive
    bool ok = model.Write(archive, version, sStartSectionComment, &error_log );
    
    return ok;
    
}

bool write_Notes (ONX_Model &model) {
    // some notes
    ON_wString sNotes;
    std::string sTemp;
    std::string sName;
    cRGBColor cColor;
    int nColor;
    
    SCTman& Sector = Sector2RhinoApplication::getSectorOld();
    
    if (Sector.countInfoLines()) {
        sNotes = "[INFO]\n";
        for (int i = 0; i < Sector.countInfoLines(); i++) {
            //sTemp = " " + Sector.returnInfoLine(i);
            sNotes += Sector.returnInfoLine(i).c_str();
        }
    }
    
    sNotes += "\n";
    
    if (Sector.countDefinedColor()) {
        sNotes += "\n";
        for (int i = 0; i < Sector.countDefinedColor(); i++) {
            Sector.returnDefinedColor(i, sName, cColor);
            sTemp = "#define " + sName + " ";
            nColor = RGB2SCTcolor(cColor);
            sTemp += int_2_string(nColor);
            sTemp += "\n";
            sNotes += sTemp.c_str();
        }
    }
    
    
    model.m_properties.m_Notes.m_notes = sNotes;
    model.m_properties.m_Notes.m_bVisible = true;
    return true;
}

bool write_layers (ONX_Model &model) {
    
    // Write standard layers
    
    ON_Layer layer[15];
    
    layer[0].SetLayerName("Default");
    layer[0].SetVisible(true);
    layer[0].SetLocked(false);
    layer[0].SetColor( ON_Color(0,0,0) );
    nTopLayerCount++;
    
    layer[1].SetLayerName("Airports");
    layer[1].SetVisible(true);
    layer[1].SetLocked(false);
    layer[1].SetLayerIndex(Airport_layerIndex);
    layer[1].SetColor( ON_Color(0,0,0) );
    nTopLayerCount++;
    
    layer[2].SetLayerName("Runways");
    layer[2].SetVisible(true);
    layer[2].SetLocked(false);
    layer[2].SetLayerIndex(Runway_layerIndex);
    layer[2].SetColor( ON_Color(0,0,0) );
    nTopLayerCount++;
    
    layer[3].SetLayerName("VORs");
    layer[3].SetVisible(true);
    layer[3].SetLocked(false);
    layer[3].SetLayerIndex(VOR_layerIndex);
    layer[3].SetColor( ON_Color(0,255,0) );
    nTopLayerCount++;
    
    layer[4].SetLayerName("NDBs");
    layer[4].SetVisible(true);
    layer[4].SetLocked(false);
    layer[4].SetLayerIndex(1);
    layer[4].SetColor( ON_Color(0,255,0) );
    nTopLayerCount++;
    
    layer[5].SetLayerName("Fixes");
    layer[5].SetVisible(true);
    layer[5].SetLocked(false);
    layer[5].SetLayerIndex(Fixes_layerIndex);
    layer[5].SetColor( ON_Color(0,0,255) );
    nTopLayerCount++;
    
    layer[6].SetLayerName("Low Airways");
    layer[6].SetVisible(true);
    layer[6].SetLocked(false);
    layer[6].SetLayerIndex(LowAirway_layerIndex);
    layer[6].SetColor( ON_Color(0,0,255) );
    nTopLayerCount++;
    
    layer[7].SetLayerName("High Airways");
    layer[7].SetVisible(true);
    layer[7].SetLocked(false);
    layer[7].SetLayerIndex(HighAirway_layerIndex);
    layer[7].SetColor( ON_Color(0,0,255) );
    nTopLayerCount++;
    
    layer[8].SetLayerName("ARTCC");
    layer[8].SetVisible(true);
    layer[8].SetLocked(false);
    layer[8].SetLayerIndex(ARTCC_layerIndex);
    layer[8].SetColor( ON_Color(0,0,255) );
    nTopLayerCount++;
    
    layer[9].SetLayerName("ARTCC Low");
    layer[9].SetVisible(true);
    layer[9].SetLocked(false);
    layer[9].SetLayerIndex(ARTCCLow_layerIndex);
    layer[9].SetColor( ON_Color(0,0,255) );
    nTopLayerCount++;
    
    layer[10].SetLayerName("ARTCC High");
    layer[10].SetVisible(true);
    layer[10].SetLocked(false);
    layer[10].SetLayerIndex(ARTCCHigh_layerIndex);
    layer[10].SetColor( ON_Color(0,0,255) );
    nTopLayerCount++;
    
    layer[11].SetLayerName("SIDs");
    layer[11].SetVisible(true);
    layer[11].SetLocked(false);
    layer[11].SetLayerIndex(SID_layerIndex);
    layer[11].SetColor( ON_Color(0,0,255) );
    nTopLayerCount++;
    
    layer[12].SetLayerName("STARs");
    layer[12].SetVisible(true);
    layer[12].SetLocked(false);
    layer[12].SetLayerIndex(STAR_layerIndex);
    layer[12].SetColor( ON_Color(0,0,255) );
    nTopLayerCount++;
    
    layer[13].SetLayerName("Geography");
    layer[13].SetVisible(true);
    layer[13].SetLocked(false);
    layer[13].SetLayerIndex(GEO_layerInex);
    layer[13].SetColor( ON_Color(0,0,255) );
    nTopLayerCount++;
    
    layer[14].SetLayerName("Regions");
    layer[14].SetVisible(true);
    layer[14].SetLocked(false);
    layer[14].SetLayerIndex(Regions_layerInex);
    layer[14].SetColor( ON_Color(0,0,255) );
    nTopLayerCount++;
    
    model.m_layer_table.Append(layer[0]);
    model.m_layer_table.Append(layer[1]);
    model.m_layer_table.Append(layer[2]);
    model.m_layer_table.Append(layer[3]);
    model.m_layer_table.Append(layer[4]);
    model.m_layer_table.Append(layer[5]);
    model.m_layer_table.Append(layer[6]);
    model.m_layer_table.Append(layer[7]);
    model.m_layer_table.Append(layer[8]);
    model.m_layer_table.Append(layer[9]);
    model.m_layer_table.Append(layer[10]);
    model.m_layer_table.Append(layer[11]);
    model.m_layer_table.Append(layer[12]);
    model.m_layer_table.Append(layer[13]);
    model.m_layer_table.Append(layer[14]);
    
    nCurrentLayerIndex = nTopLayerCount;
    
    XMLAirport& xmlAirport = Sector2RhinoApplication::getXmlAiport();
    
    if ((xmlAirport.countTaxiwayParking() > 0) ||
        (xmlAirport.countTaxiPoints() > 0)) {
        ON_Layer FSX_Toplayer;
        FSX_Toplayer.SetLayerName("FSX XML");
        FSX_Toplayer.SetVisible(true);
        FSX_Toplayer.SetLocked(true);
        FSX_Toplayer.SetLayerIndex(FSXTopLayer_layerIndex);
        FSX_Toplayer.SetColor(ON_Color(0,0,0));
        model.m_layer_table.Append(FSX_Toplayer);
        
        nCurrentLayerIndex++;
    }
    
    if (xmlAirport.countTaxiwayParking() > 0) {
        ON_Layer TaxiwayParkingLayer;
        TaxiwayParkingLayer.SetLayerName("Parking Points");
        TaxiwayParkingLayer.SetVisible(true);
        TaxiwayParkingLayer.SetLocked(true);
        TaxiwayParkingLayer.SetLayerIndex(FSXParking_layerIndex);
        TaxiwayParkingLayer.SetColor( ON_Color(0,0,0) );
        model.m_layer_table.Append(TaxiwayParkingLayer);
        
        nFSXparkinglayersCount++;
        nCurrentLayerIndex++;
    }
    
    if (xmlAirport.countTaxiPoints() > 0) {
        ON_Layer TaxiPointsLayer;
        TaxiPointsLayer.SetLayerName("Taxi Points");
        TaxiPointsLayer.SetVisible(true);
        TaxiPointsLayer.SetLocked(true);
        TaxiPointsLayer.SetLayerIndex(FSXTaxiPoints_layerIndex);
        TaxiPointsLayer.SetColor( ON_Color(0,0,0) );
        model.m_layer_table.Append(TaxiPointsLayer);
        
        nFSXtaxiwayPointsCount++;
        nCurrentLayerIndex++;
    }
    
    return true;
}

bool write_Airports (ONX_Model &model) {
    SCTman& Sector = Sector2RhinoApplication::getSectorOld();
    int nAirportCount = Sector.countAirports();
    double x1 = 0, y1= 0;
    std::string sAirportName, sFreq, sClass;
    std::string sNote = "";
    
    GeographicLib::GeoCoords coord1;
    
    for (int i = 0; i < nAirportCount; i++) {
        Sector.returnAirport(i, sAirportName, sFreq, coord1, sClass, sNote);
        Geo2Mercator(coord1, x1, y1);
        
        sAirportName = sAirportName + " " + sFreq + " " + sClass;
        if (sNote != "") sAirportName = sAirportName + " " + sNote;
        const char *cAirportName = sAirportName.c_str();
        
        objCount++;
        std::cout << "[" << objCount << "] " << x1 << " " << y1 << " " << cAirportName << std::endl;
        
       // ON_Point point1(ON_3dPoint( x1, y1, 0.0 ));
        ONX_Model_Object& mo = model.m_object_table.AppendNew();
        mo.m_object = new ON_Point(ON_3dPoint( x1, y1, 0.0 ));;
        mo.m_bDeleteObject = false; // point1 is on the stack
        mo.m_attributes.m_layer_index = Airport_layerIndex;
        mo.m_attributes.m_name = cAirportName;

    }
    
    return true;
}


bool write_Runways (ONX_Model &model) {
    SCTman& Sector = Sector2RhinoApplication::getSectorOld();
    int nObjectCount = Sector.countRunways();
    double x1 = 0, y1= 0, x2 = 0, y2 = 0;
    std::string sName1, sName2, sHeading1, sHeading2;
    std::string sNote = "";
    GeographicLib::GeoCoords coord1;
    GeographicLib::GeoCoords coord2;
    
    for (int i = 0; i < nObjectCount; i++) {
        Sector.returnRunway(i, sName1, sName2, sHeading1, sHeading2, coord1, coord2, sNote);
        Geo2Mercator(coord1, x1, y1);
        Geo2Mercator(coord2, x2, y2);
        
        sName1 = sName1 + " "  + sName2 + " " + sHeading1 + " " + sHeading2;
        if (sNote != "") sName1 = sName1 + " " + sNote;
        const char *cName = sName1.c_str();
        
        objCount++;
        //std::cout << "[" << objCount << "] " << x1 << " " << y1 << " " << cName << std::endl;
        
        ONX_Model_Object& mo = model.m_object_table.AppendNew();
        mo.m_object = new ON_LineCurve( ON_Line( ON_3dPoint(x1,y1,0), ON_3dPoint(x2,y2,0) ) );
        mo.m_bDeleteObject = true;
        mo.m_attributes.m_layer_index = Runway_layerIndex;
        mo.m_attributes.m_name = cName;
        mo.m_attributes.SetColorSource(ON::ObjectColorSource(0));
        //mo.m_attributes.m_color = ( ON_Color(cColor.r, cColor.g, cColor.b));
        //mo.m_attributes.m_plot_color = ( ON_Color(cColor.r, cColor.g, cColor.b));
        
    }
    
    return true;
}

bool write_NDBVOR (ONX_Model &model) {
    
    SCTman& Sector = Sector2RhinoApplication::getSectorOld();
    int nObjectCount;
    double x = 0, y= 0;
    std::string sName, sFreq;
    std::string sNote = "";
    GeographicLib::GeoCoords coord;
    RadioNav eNavType;
    
    for (int i = 0; i < 2; i++) {
        if (i == 0) {
            eNavType = RadioNav::VOR_old;
            nObjectCount = Sector.countVORs();
        }
        if (i == 1) {
            eNavType = RadioNav::NDB_old;
            nObjectCount = Sector.countNDBs();
        }
        for (int j = 0; j < nObjectCount; j++) {
            //Sector.returnVORNDB(i, sName, eNavType, sFreq, coord, sNote);
            if (eNavType == RadioNav::VOR_old
                ) Sector.returnVOR(j, sName, sFreq, coord, sNote);
            if (eNavType == RadioNav::NDB_old) Sector.returnNDB(j, sName, sFreq, coord, sNote);
            Geo2Mercator(coord, x, y);
            
            sName = sName + " "  + sFreq;
            if (sNote != "") sName = sName + " " + sNote;
            const char *cName = sName.c_str();
            
            objCount++;
            //std::cout << "[" << objCount << "] " << x1 << " " << y1 << " " << cName << std::endl;
            
            ONX_Model_Object& mo = model.m_object_table.AppendNew();
            mo.m_object = new ON_Point(ON_3dPoint( x, y, 0.0 ));;
            mo.m_bDeleteObject = false; // point1 is on the stack
            if (eNavType == RadioNav::VOR_old) mo.m_attributes.m_layer_index = VOR_layerIndex;
            if (eNavType == RadioNav::NDB_old) mo.m_attributes.m_layer_index = NDB_layerIndex;
            mo.m_attributes.m_name = cName;
            
        }
    }
    
    
    return true;
}

bool write_Fixes (ONX_Model &model) {
    
    SCTman& Sector = Sector2RhinoApplication::getSectorOld();
    int nAirportCount = Sector.countFixes();
    double x1 = 0;
    double y1= 0;
    std::string sFixName;
    std::string sNote ="";
    std::string sTempString;
    GeographicLib::GeoCoords coord1;
    
    for (int i = 0; i < nAirportCount; i++) {
        Sector.returnFix(i, sFixName, coord1, sNote);
        Geo2Mercator(coord1, x1, y1);
        
        if (sNote != "") sFixName = sFixName + " " + sNote;
        const char *cName = sFixName.c_str();
        
        objCount++;
        //std::cout << "[" << objCount << "] " << x1 << " " << y1 << " " << cName << std::endl;
        
        // ON_Point point1(ON_3dPoint( x1, y1, 0.0 ));
        ONX_Model_Object& mo = model.m_object_table.AppendNew();
        mo.m_object = new ON_Point(ON_3dPoint( x1, y1, 0.0 ));;
        mo.m_bDeleteObject = false; // point1 is on the stack
        mo.m_attributes.m_layer_index = Fixes_layerIndex;
        mo.m_attributes.m_name = cName;
        
    }
    
    return true;
}

bool write_SIDSTAR (ONX_Model &model, DiagramType eDiagramSearchType) {
    SCTman& Sector = Sector2RhinoApplication::getSectorOld();
    int nDiagramCount = Sector.countDiagrams(SID_hack) + Sector.countDiagrams(STAR) + Sector.countDiagrams(GEO);
    double x1 = 0, x2 = 0;
    double y1 = 0, y2 = 0;
    std::string sDiagramName;
    std::string sLineName;
    std::string sNote = "";
    GeographicLib::GeoCoords coord1, coord2;
    cRGBColor cColor;
    DiagramType eDiagramType;
    ON_Layer DiagramLayer;
    
    for (int i = 0; i < nDiagramCount; i++) {
        Sector.returnDiagramInfo(i, sDiagramName, eDiagramType);
        if (eDiagramType != eDiagramSearchType) {
            continue;
        }
        const char *cDiagramName = sDiagramName.c_str();
        
        std::cout << " Creating [" << nCurrentLayerIndex << "] " << sDiagramName << std::endl;
        
        DiagramLayer.SetLayerName(cDiagramName);
        DiagramLayer.SetVisible(true);
        DiagramLayer.SetLocked(false);
        DiagramLayer.SetLayerIndex(nCurrentLayerIndex);
        DiagramLayer.SetColor( ON_Color(0,0,255) );
        model.m_layer_table.Append(DiagramLayer);
        
        for (int j = 0; j < Sector.countLinesInDiagram(i); j++) {
            
            Sector.returnDiagramLine(i, j, &coord1, &coord2, &cColor, &sDiagramName, &sNote);
            Geo2Mercator(coord1, x1, y1);
            Geo2Mercator(coord2, x2, y2);
            
            objCount++;
            
            const char *cNote = sNote.c_str();
            
            std::cout << "[" << objCount << "] " << x1 << " " << y1 << " " << x2 << " " << y2 << " " << cColor.r << "," << cColor.g << " " << cColor.b << " " << sNote << std::endl;
            
            ONX_Model_Object& mo = model.m_object_table.AppendNew();
            mo.m_object = new ON_LineCurve( ON_Line( ON_3dPoint(x1,y1,0), ON_3dPoint(x2,y2,0) ) );
            mo.m_bDeleteObject = true;
            mo.m_attributes.m_layer_index = nCurrentLayerIndex;
            mo.m_attributes.m_name = cNote;
            mo.m_attributes.SetColorSource(ON::ObjectColorSource(1));
            mo.m_attributes.m_color = ( ON_Color(cColor.r, cColor.g, cColor.b));
            mo.m_attributes.m_plot_color = ( ON_Color(cColor.r, cColor.g, cColor.b));
        }
        
        // Set Layer counts
        if (eDiagramSearchType == SID_hack) nSIDlayersCount++;
        if (eDiagramSearchType == STAR) nSTARlayersCount++;
        nCurrentLayerIndex ++;
        
    }
    return true;
}

bool write_ARTCC_Airways (ONX_Model &model) {
    double x1 = 0, x2 = 0;
    double y1 = 0, y2 = 0;
    std::string sName;
    std::string sLineName;
    std::string sNote;
    GeographicLib::GeoCoords coord1, coord2;
    BoundryOrAir eBoundryOrAir;
    ON_Layer DiagramLayer;
    int nLayerIndex;
    
    for (int i = 0; i < 5; i++) {
        if (i == 0) {
            eBoundryOrAir = LOW_AIRWAY;
            nLayerIndex = LowAirway_layerIndex;
        }
        else if (i == 1) {
            eBoundryOrAir = HIGH_AIRWAY;
            nLayerIndex = HighAirway_layerIndex;
        }
        else if (i == 2) {
            eBoundryOrAir = ARTCC;
            nLayerIndex = ARTCC_layerIndex;
        }
        else if (i == 3) {
            eBoundryOrAir = ARTCC_LOW;
            nLayerIndex = ARTCCLow_layerIndex;
        }
        else if (i == 4) {
            eBoundryOrAir = ARTCC_HIGH;
            nLayerIndex = ARTCCHigh_layerIndex;
        }
        
        SCTman& Sector = Sector2RhinoApplication::getSectorOld();
        int nDiagramCount = Sector.countARTCCorAirwary(eBoundryOrAir);
        
        // TODO: Add layers for each similar name
        
        for (int j = 0; j < nDiagramCount; j++) {
            
            Sector.returnARTCCorAirway(j, 0, eBoundryOrAir, sName, coord1, coord2, sNote); 
            
            const char *cDiagramName = sName.c_str();
            DiagramLayer.SetLayerName(cDiagramName);
            DiagramLayer.SetVisible(true);
            DiagramLayer.SetLocked(true);
            DiagramLayer.SetLayerIndex(nCurrentLayerIndex);
            DiagramLayer.SetColor( ON_Color(128,128,128));
            model.m_layer_table.Append(DiagramLayer);
            
            int nLineCount = Sector.countARTCCorAirwaryLines(eBoundryOrAir, j);
            
            for (int k = 0; k < nLineCount; k++) {
                //Sector.returnARTCCorAirway(j, eBoundryOrAir, sName, coord1, coord2, sNote);
                Sector.returnARTCCorAirway(j, k, eBoundryOrAir, sName, coord1, coord2, sNote);
                Geo2Mercator(coord1, x1, y1);
                Geo2Mercator(coord2, x2, y2);
                
                objCount++;
                //if (sNote != "") sName =+ " " + sNote;
                const char *cOBJName = sNote.c_str();
                
                ONX_Model_Object& mo = model.m_object_table.AppendNew();
                mo.m_object = new ON_LineCurve( ON_Line( ON_3dPoint(x1,y1,0), ON_3dPoint(x2,y2,0) ) );
                mo.m_bDeleteObject = true;
                mo.m_attributes.m_layer_index = nCurrentLayerIndex;
                mo.m_attributes.m_name = cOBJName;
                mo.m_attributes.SetColorSource(ON::ObjectColorSource(1));
            }
            
            nCurrentLayerIndex++;
            if (eBoundryOrAir == LOW_AIRWAY) nLowAltAirwayLayersCount++;
            if (eBoundryOrAir == HIGH_AIRWAY) nHighAltAirwayLayersCount++;
            if (eBoundryOrAir == ARTCC) nARTCClayersCount++;
            if (eBoundryOrAir == ARTCC_LOW) nARTCCLowLayersCount++;
            if (eBoundryOrAir == ARTCC_HIGH) nARTCCHighLayersCount++;
            
        }
        
    }
    
    return true;
}

bool write_GEO (ONX_Model &model) {
    SCTman& Sector = Sector2RhinoApplication::getSectorOld();
    int nDiagramCount = Sector.countDiagrams(SID_hack) + Sector.countDiagrams(STAR) + Sector.countDiagrams(GEO);
    double x1 = 0, x2 = 0;
    double y1 = 0, y2 = 0;
    std::string sDiagramName;
    std::string sLineName;
    std::string sNote;
    GeographicLib::GeoCoords coord1, coord2;
    cRGBColor cColor;
    DiagramType eDiagramType;
    ON_Layer DiagramLayer;
    
    for (int i = 0; i < nDiagramCount; i++) {
        Sector.returnDiagramInfo(i, sDiagramName, eDiagramType);
        
        if (eDiagramType != GEO) {
            continue;
        }
        
        for (int j = 0; j < Sector.countLinesInDiagram(i); j++) {
            
            Sector.returnDiagramLine(i, j, &coord1, &coord2, &cColor, &sLineName, &sNote);
            Geo2Mercator(coord1, x1, y1);
            Geo2Mercator(coord2, x2, y2);
            
            objCount++;
            
            const char *cNote = sNote.c_str();
            
            std::cout << "[" << objCount << "] " << x1 << " " << y1 << " " << x2 << " " << y2 << " " << cColor.r << "," << cColor.g << " " << cColor.b << " " << cNote << std::endl;
            
            ONX_Model_Object& mo = model.m_object_table.AppendNew();
            mo.m_object = new ON_LineCurve( ON_Line( ON_3dPoint(x1,y1,0), ON_3dPoint(x2,y2,0) ) );
            mo.m_bDeleteObject = true;
            mo.m_attributes.m_layer_index = GEO_layerInex;
            mo.m_attributes.m_name = cNote;
            mo.m_attributes.SetColorSource(ON::ObjectColorSource(1));
            mo.m_attributes.m_color = ( ON_Color(cColor.r, cColor.g, cColor.b));
            mo.m_attributes.m_plot_color = ( ON_Color(cColor.r, cColor.g, cColor.b));
        }
        
    }
    return true;
}

bool write_REGIONS (ONX_Model &model) {

    
    
    //ON_Polyline plyLine2;
    
    SCTman& Sector = Sector2RhinoApplication::getSectorOld();
    
    int nRegionCrvCount = 0;
    int nRegionCount = Sector.returnRegionCount();
    cRGBColor cColor;
    GeographicLib::GeoCoords gCoord;
    double x = 0, y = 0;
    std::string sNote = "";
    
    for (int i = 0; i < nRegionCount; i++) {
        
        ON_3dPoint Point;
        ON_Polyline plyLine;
        plyLine.Destroy();
        sNote = "";
        double zDepth;
        
        Sector.returnRegionInfo(i, nRegionCrvCount, cColor, sNote);
        for (int j = 0; j < nRegionCrvCount; j++) {
            //gCoord = Sector.returnRegionCoord(i, j);
            Sector.returnRegionCoord(i, j, gCoord, zDepth);
            Geo2Mercator(gCoord, x, y);
            Point.Set(x, y, zDepth);
            plyLine.Append(Point);
        }
        
        const char *cNote = sNote.c_str();
        
        objCount++;
        
        ON_PolylineCurve plyLineCrv(plyLine);
        
        ONX_Model_Object& mo = model.m_object_table.AppendNew();
        //mo.m_object = new ON_PolylineCurve(plyLine);
        mo.m_object = new ON_PolylineCurve(plyLineCrv);
        mo.m_bDeleteObject = true;
        mo.m_attributes.m_layer_index = Regions_layerInex;
        mo.m_attributes.m_name = cNote;
        mo.m_attributes.SetColorSource(ON::ObjectColorSource(1));
        mo.m_attributes.m_color = ( ON_Color(cColor.r, cColor.g, cColor.b));
        mo.m_attributes.m_plot_color = ( ON_Color(cColor.r, cColor.g, cColor.b));
        
        //create hatch
        const ON_Curve* crv = ON_Curve::Cast(&plyLineCrv);
        ON_Plane plane;
        plane.CreateFromPoints(ON_3dPoint(0,0,zDepth), ON_3dPoint(1, 0,zDepth), ON_3dPoint(0,1,zDepth));
        ON_SimpleArray<const ON_Curve *> loops;
        loops.Append(crv);
        ON_Hatch hatch;
        hatch.Create(plane, loops, 1, 0.0, 1.0);
        
        //add hatch
        ONX_Model_Object& mo2 = model.m_object_table.AppendNew();
        mo2.m_object = new ON_Hatch(hatch);
        mo2.m_bDeleteObject = true;
        mo2.m_attributes.m_layer_index = Regions_layerInex;
        mo2.m_attributes.m_name = cNote;
        mo2.m_attributes.SetColorSource(ON::ObjectColorSource(1));
        mo2.m_attributes.m_color = ( ON_Color(cColor.r, cColor.g, cColor.b));
        mo2.m_attributes.m_plot_color = ( ON_Color(cColor.r, cColor.g, cColor.b));
        
    }
    
    return true;
}

bool write_FSXTaxiwayParkingPoints (ONX_Model &model) {
    
    XMLAirport& xmlAirport = Sector2RhinoApplication::getXmlAiport();
    
    int nCount = xmlAirport.countTaxiwayParking();
    double x1 = 0;
    double y1= 0;
    
    std::string sName;
    double dHeading;
    float fRadius;
    Gate_Type eGateType;
    std::string sGateName;
    int nNumber;
    double dTeeOffset;
    std::string sNote ="";
    
    GeographicLib::GeoCoords gCoord;
    
    for (int i = 0; i < nCount; i++) {
        xmlAirport.returnTaxiwayParking(i, gCoord, dHeading, fRadius, eGateType, sGateName, nNumber, dTeeOffset);
        Geo2Mercator(gCoord, x1, y1);
        
        sName = int_2_string(nNumber);
        {
            std::string sTempString;
            if (eGateType == Small) sTempString = "Small";
            else if (eGateType == Medium) sTempString = "Medium";
            else if (eGateType == Large) sTempString = "Large";
            else if (eGateType == GA_Small) sTempString = "GA_Small";
            else if (eGateType == GA_Medium) sTempString = "GA_Medium";
            else if (eGateType == GA_Large) sTempString = "GA_Large";
            else if (eGateType == Cargo) sTempString = "Cargo";
            else if (eGateType == Fuel) sTempString = "Fuel";
            sName += " " + sTempString;
        }
        
        sName += " Heading:" + dbl_2_string(dHeading) +
        " Radius:" + float_2_string(fRadius) +
        " Name:" + sGateName +
        " TeeOffset:" + dbl_2_string(dTeeOffset);
        
        const char *cName = sName.c_str();
        
        objCount++;
        //std::cout << "[" << objCount << "] " << x1 << " " << y1 << " " << cName << std::endl;
        
        // ON_Point point1(ON_3dPoint( x1, y1, 0.0 ));
        ONX_Model_Object& mo = model.m_object_table.AppendNew();
        mo.m_object = new ON_Point(ON_3dPoint( x1, y1, 0.0 ));;
        mo.m_bDeleteObject = false; // point1 is on the stack
        mo.m_attributes.m_layer_index = FSXParking_layerIndex;
        mo.m_attributes.m_name = cName;
        
    }
    
    return true;
}

bool write_FSXTaxiPoints (ONX_Model &model) {
    
    XMLAirport& xmlAirport = Sector2RhinoApplication::getXmlAiport();
    
    int nCount = xmlAirport.countTaxiPoints();
    double x1 = 0;
    double y1= 0;
    
    int nIndex;
    std::string sName;
    TaxiPoint_Type eType;
    Orientation eOrientation;
    std::string sNote ="";
    
    GeographicLib::GeoCoords gCoord;
    
    for (int i = 0; i < nCount; i++) {
        xmlAirport.returnTaxiPoint(i, nIndex, eType, eOrientation, gCoord);
        Geo2Mercator(gCoord, x1, y1);
        sName = "[" + int_2_string(nIndex) + "] ";
        
        {
            std::string sTempString;
            if (eType == normal_point) sTempString = "Normal";
            else if (eType == hold_short) sTempString = "Hold Short";
            else if (eType == ils_hold_short) sTempString = "ILS Hold Short";
            else sTempString = "Unknown Type";
            sName += "Type:" + sTempString;
        }
        
        {
            std::string sTempString;
            if (eOrientation == 0) sTempString = "Forward";
            else sTempString = "Reverse";
            sName += " Orientation:" + sTempString;
        }
        
        const char *cName = sName.c_str();
        
        objCount++;
        //std::cout << "[" << objCount << "] " << x1 << " " << y1 << " " << cName << std::endl;
        
        // ON_Point point1(ON_3dPoint( x1, y1, 0.0 ));
        ONX_Model_Object& mo = model.m_object_table.AppendNew();
        mo.m_object = new ON_Point(ON_3dPoint( x1, y1, 0.0 ));;
        mo.m_bDeleteObject = false; // point1 is on the stack
        mo.m_attributes.m_layer_index = FSXTaxiPoints_layerIndex;
        mo.m_attributes.m_name = cName;
        
    }
    
    return true;
}

bool write_SetLayerHierarchy (ONX_Model &model) {
    
    XMLAirport& xmlAirport = Sector2RhinoApplication::getXmlAiport();
    
    nCurrentLayerIndex = nTopLayerCount + nFSXparkinglayersCount;
    //int nSIDcount = Sector.countDiagrams(SID);
    //int nSTARcount = Sector.countDiagrams(STAR);
    
    for (int i = 0; i < nLowAltAirwayLayersCount; i++) {
        model.m_layer_table[nCurrentLayerIndex].m_parent_layer_id = model.m_layer_table[LowAirway_layerIndex].m_layer_id;
        nCurrentLayerIndex++;
    }
    for (int i = 0; i < nHighAltAirwayLayersCount; i++) {
        model.m_layer_table[nCurrentLayerIndex].m_parent_layer_id = model.m_layer_table[HighAirway_layerIndex].m_layer_id;
        nCurrentLayerIndex++;
    }
    for (int i = 0; i < nARTCClayersCount; i++) {
        model.m_layer_table[nCurrentLayerIndex].m_parent_layer_id = model.m_layer_table[ARTCC_layerIndex].m_layer_id;
        nCurrentLayerIndex++;
    }
    for (int i = 0; i < nARTCCLowLayersCount; i++) {
        model.m_layer_table[nCurrentLayerIndex].m_parent_layer_id = model.m_layer_table[ARTCCLow_layerIndex].m_layer_id;
        nCurrentLayerIndex++;
    }
    for (int i = 0; i < nARTCCHighLayersCount; i++) {
        model.m_layer_table[nCurrentLayerIndex].m_parent_layer_id = model.m_layer_table[ARTCCHigh_layerIndex].m_layer_id;
        nCurrentLayerIndex++;
    }
    for (int i = 0; i < nSIDlayersCount; i++) {
        model.m_layer_table[nCurrentLayerIndex].m_parent_layer_id = model.m_layer_table[SID_layerIndex].m_layer_id;
        nCurrentLayerIndex++;
    }
    for (int i = 0; i < nSTARlayersCount; i++) {
        model.m_layer_table[nCurrentLayerIndex].m_parent_layer_id = model.m_layer_table[STAR_layerIndex].m_layer_id;
        nCurrentLayerIndex++;
    }
    if (xmlAirport.countTaxiwayParking() > 0) {
        model.m_layer_table[FSXParking_layerIndex].m_parent_layer_id = model.m_layer_table[FSXTopLayer_layerIndex].m_layer_id;
        nCurrentLayerIndex++;
    }
    if (xmlAirport.countTaxiPoints() > 0) {
        model.m_layer_table[FSXTaxiPoints_layerIndex].m_parent_layer_id = model.m_layer_table[FSXTopLayer_layerIndex].m_layer_id;
        nCurrentLayerIndex++;
    }
    
    return true;
}

int write3dm ()
{
    bool rc;
    const char* filename;
    
    ON::Begin();
    // If you want to learn to write b-rep models, first work through
    // this example paying close attention to write_trimmed_surface_example(),
    // then examime example_brep.cpp.
    
    // The OpenNURBS toolkit will write version 2 and 3 and read
    // version 1, 2 and 3 of the 3DM file format.
    //
    // version 1 is the legacy Rhino I/O tookit format and was used by Rhino 1.x.
    // version 2 is the OpenNURBS format (released 1 July 2000) and is used by Rhino 2.x
    // version 3 is the OpenNURBS format (released 1 November 2002) and is used by Rhino 3.x
    // version 4 is the OpenNURBS format (released September 2006) and is used by Rhino 4.x
    // version 5 is the OpenNURBS format (released September 2009) and is used by Rhino 5.x
    
    // version to write
    int version = 0; // version will be ON_BinaryArchive::CurrentArchiveVersion()
    
    // errors printed to stdout
    ON_TextLog error_log;
    
    // messages printed to stdout
    ON_TextLog message_log;
    
    // errors logged in text file
    //FILE* error_log_fp = ON::OpenFile("error_log.txt","w");
    //ON_TextLog error_log(error_log_fp);
    
    /*
    filename = "my_points.3dm";
    FILE* fp = ON::OpenFile( filename, "wb" );
    rc = write_points_example( fp, version, error_log );
    ON::CloseFile( fp );
    if (rc)
        message_log.Print("Successfully wrote %s.\n",filename);
    else
        message_log.Print("Errors while writing %s.\n",filename);
    */
     
    filename = "my_curves.3dm";
    FILE* fp = ON::OpenFile( filename, "wb" );
    rc = write_Model( fp, version, error_log );
    ON::CloseFile( fp );
    if (rc)
        message_log.Print("Successfully wrote %s.\n",filename);
    else
        message_log.Print("Errors while writing %s.\n",filename);
    
    ON::End();
    
    return 0;
}

