//
//  3DMwriter.h
//  fstream
//
//  Created by James Mazur on 12/10/12.
//  Copyright (c) 2012 James Mazur. All rights reserved.
//

#ifndef fstream__DMwriter_h
#define fstream__DMwriter_h

#include "opennurbs.h"
//#include "example_ud.h"
#include "sct/SCTobj.h"
#include <string>
#include <GeographicLib/TransverseMercatorExact.hpp>
#include <GeographicLib/GeoCoords.hpp>
#include "helpers/GeoConvert.h"
#include "sct/SCTreader.h"
#include "FSXxml/XMLAirport.h"


static bool write_Model( FILE* fp, int version, ON_TextLog& error_log );

static bool write_curves_example( FILE* fp, int version, ON_TextLog& error_log );

bool write_Notes (ONX_Model &model);

bool write_layers (ONX_Model &model);

bool write_Fixes (ONX_Model &model);

bool write_Airports (ONX_Model &model);

bool write_Runways (ONX_Model &model);

bool write_NDBVOR (ONX_Model &model);

bool write_ARTCC_Airways (ONX_Model &model);

bool write_SIDSTAR (ONX_Model &model, DiagramType eDiagramSearchType);

bool write_GEO (ONX_Model &model);

bool write_REGIONS (ONX_Model &model);

bool write_FSXTaxiwayParkingPoints (ONX_Model &model);

bool write_FSXTaxiPoints (ONX_Model &model);

bool write_SetLayerHierarchy (ONX_Model &model);

int write3dm ();



#endif
