/*
  ==============================================================================

    3dmIdentifiers.h
    Created: 1 Mar 2015 4:49:13pm
    Author:  Jim Mazur

  ==============================================================================
*/

#ifndef _3DMIDENTIFIERS_H_INCLUDED
#define _3DMIDENTIFIERS_H_INCLUDED

#include "../../JuceLibraryCode/JuceHeader.h"

namespace Rhino_Ids
{
    class LayerNames
    {
    public:
        static const String
        AirportsRunwaysParent,
        Airports,
        Runways,
        VORs_NDBs_Fixes_Parent,
        VORs,
        NDBs,
        Fixes,
        ARTCC_Parent,
        ARTCCs,
        ARTCC_Highs,
        ARTCC_Lows,
        AirwaysParent,
        HighAirways,
        LowAirways,
        SIDs_STARs_Parent,
        SIDs,
        STARs,
        Geos,
        Regions,
        Labels;
    };
}

const String Rhino_Ids::LayerNames::AirportsRunwaysParent ("Airports and Runways");
const String Rhino_Ids::LayerNames::Airports ("Airports");
const String Rhino_Ids::LayerNames::Runways ("Runways");
const String Rhino_Ids::LayerNames::VORs_NDBs_Fixes_Parent ("Navigation Aids");
const String Rhino_Ids::LayerNames::VORs ("VORs");
const String Rhino_Ids::LayerNames::NDBs ("NDBs");
const String Rhino_Ids::LayerNames::Fixes ("Fixes");
const String Rhino_Ids::LayerNames::ARTCC_Parent ("ARTCC Boundaries");
const String Rhino_Ids::LayerNames::ARTCCs ("ARTCCs");
const String Rhino_Ids::LayerNames::ARTCC_Highs ("ARTCC Highs");
const String Rhino_Ids::LayerNames::ARTCC_Lows ("ARTCC Lows");
const String Rhino_Ids::LayerNames::AirwaysParent ("Airways");
const String Rhino_Ids::LayerNames::HighAirways ("High Airways");
const String Rhino_Ids::LayerNames::LowAirways ("Low Airways");
const String Rhino_Ids::LayerNames::SIDs_STARs_Parent ("SID / STAR Diagrams");
const String Rhino_Ids::LayerNames::SIDs ("SIDs");
const String Rhino_Ids::LayerNames::STARs ("STARs");
const String Rhino_Ids::LayerNames::Geos ("Geography");
const String Rhino_Ids::LayerNames::Regions ("Regions");
const String Rhino_Ids::LayerNames::Labels ("Labels");

#endif  // 3DMIDENTIFIERS_H_INCLUDED
