/*
  ==============================================================================

    SectorLogger.h
    Created: 12 Feb 2015 10:20:31am
    Author:  Jim Mazur

  ==============================================================================
*/

#ifndef SECTORLOGGER_H_INCLUDED
#define SECTORLOGGER_H_INCLUDED

#include "../../JuceLibraryCode/JuceHeader.h"


//==============================================================================
/**
 An Event to be stored in the SectorLogger. Each event contains a
 timestamp, severity level (info, warning, error) and a message that can be stored
 */

class LogMessage
{
public:
    //==============================================================================
    /**
     Different severity levels for the event.
     */
    enum EventSeverity
    {
        Info,
        Warning,
        Error
    };
    
    //==============================================================================
    /** Default constructor, uses default system time, severity of info and no
     message.
     */
    LogMessage();
    
    LogMessage (const String& message);
    
    LogMessage (const EventSeverity& severity, const String& message);
    
    /** Constructs an event with the specified timestamp, severity level and message
     */
    LogMessage(Time timeStamp, EventSeverity eventType, String message);
    
    /** Copy constructor and assignment operator. */
    LogMessage(const LogMessage&);
    LogMessage& operator=( const LogMessage& other);
    
    /** Get the time of the event. */
    Time getTime() const                            { return m_timeStamp; };
    
    /** Get the severity of the event. */
    EventSeverity getSeverity() const               { return m_eventSeverity; };
    
    /** Get the message of the event. */
    String getMessage() const                       { return m_eventMessage; };
    
private:
    //==============================================================================
    Time m_timeStamp;
    EventSeverity m_eventSeverity;
    String m_eventMessage;
};

//==============================================================================
/**
 A logger for the AngelScriptEngine. Simply add events to the enginve by using
 the addEvent function.
 
 To receive events from the logger in an object, simply derive the object using
 the LogMessageListner object.
 */

class SectorLogger : public ChangeBroadcaster
{
public:
    static void setCurrentLogger (SectorLogger* const newLogger) noexcept;
    
    static SectorLogger* getCurrentLogger()  noexcept;
    //==============================================================================
    /** Add a new event to the logger. */
    static void addEvent (const LogMessage& event);
    
    /** Add a new event to the logger as an info. */
    static void addEvent (const String& message);
    
    static void addInfo (const String& message);
    
    static void addWarning (const String& message);
    
    static void addError (const String& message);
    
    /** Get all events in the logger. */
    Array<LogMessage> getAllEvents();
    
    /** Get events since a certain timestamp. */
    Array<LogMessage> getEventsSince (Time time);
    
    /** Clear all the events in the logger. */
    void reset();
    
private:
    //==============================================================================
    static SectorLogger* currentLogger;
    
    CriticalSection lock;
    Array<LogMessage> m_storedEvents;
    uint m_numOfEventsToKeep = 1000;
    
    void checkIfStoredEventsAreFull();
};

//==============================================================================
/**
 Receives updates from the event listener. Create one of these by initializing it
 with a pointer to the logger to recieve events from. Upon creation it will get
 all events in the log.
 */

class LogMessageListener : public ChangeListener
{
public:
    //==============================================================================
    /** Instantiate the LogMessageListner with a pointer to the logger
     it should receive events from. */
    LogMessageListener();
    
    ~LogMessageListener();
    
    /** Override this method to handle receiving new events from the logger
     */
    virtual void handleNewEvent(LogMessage event) = 0;
    
    /** Call back from the SectorLogger
     */
    void changeListenerCallback(ChangeBroadcaster *source);
    
private:
    //==============================================================================
    SectorLogger* m_logger;
    Time m_time;
};



#endif  // SECTORLOGGER_H_INCLUDED
