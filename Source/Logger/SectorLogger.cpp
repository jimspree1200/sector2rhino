/*
  ==============================================================================

    SectorLogger.cpp
    Created: 12 Feb 2015 10:20:31am
    Author:  Jim Mazur

  ==============================================================================
*/

#include "SectorLogger.h"

LogMessage::LogMessage()
{
    
}

LogMessage::LogMessage (const String& message)
:
m_timeStamp(Time::getCurrentTime()),
m_eventSeverity(LogMessage::EventSeverity::Info),
m_eventMessage(message)
{
    
}

LogMessage::LogMessage (const EventSeverity& severity, const String& message)
:
m_timeStamp(Time::getCurrentTime()),
m_eventSeverity(severity),
m_eventMessage(message)
{
    
}

LogMessage::LogMessage(Time timeStamp, EventSeverity eventType, String message)
:
m_timeStamp(Time::getCurrentTime()),
m_eventSeverity(eventType),
m_eventMessage(message)
{
    
}

LogMessage::LogMessage(const LogMessage& other)
{
    m_timeStamp = other.m_timeStamp;
    m_eventSeverity = other.m_eventSeverity;
    m_eventMessage = other.m_eventMessage;
}

LogMessage& LogMessage::operator=(const LogMessage &other)
{
    m_timeStamp = other.m_timeStamp;
    m_eventSeverity = other.m_eventSeverity;
    m_eventMessage = other.m_eventMessage;
    return *this;
}

SectorLogger* SectorLogger::currentLogger = nullptr;

void SectorLogger::setCurrentLogger (SectorLogger* const newLogger) noexcept
{ currentLogger = newLogger; }

SectorLogger* SectorLogger::getCurrentLogger()  noexcept
{ return currentLogger; }

void SectorLogger::addEvent(const LogMessage& event)
{
    if (currentLogger != nullptr)
    {
        const ScopedLock sl (currentLogger->lock);
        currentLogger->checkIfStoredEventsAreFull();
        currentLogger->m_storedEvents.add(event);
        currentLogger->sendChangeMessage();
    }
    else
        DBG(event.getMessage());
}

void SectorLogger::addEvent(const juce::String& message)
{
    addEvent(LogMessage(message));
}

void SectorLogger::addInfo(const juce::String &message)
{
    addEvent(LogMessage(LogMessage::EventSeverity::Info, message));
}

void SectorLogger::addWarning(const juce::String &message)
{
    addEvent(LogMessage(LogMessage::EventSeverity::Warning, message));
}

void SectorLogger::addError(const juce::String &message)
{
    addEvent(LogMessage(LogMessage::EventSeverity::Error, message));
}

Array<LogMessage> SectorLogger::getAllEvents()
{
    const ScopedLock sl (lock);
    Array<LogMessage> events = m_storedEvents;
    return events;
}

Array<LogMessage> SectorLogger::getEventsSince(juce::Time time)
{
    Array<LogMessage> events;
    
    const ScopedLock sl (lock);
    for (int i = m_storedEvents.size()-1; i >= 0; i--) {
        if (m_storedEvents[i].getTime() <= time)
        {
            break;
        }
        events.insert(0, m_storedEvents[i]);
    }
    
    return events;
}

void SectorLogger::reset()
{
    const ScopedLock sl (lock);
    m_storedEvents.clear();
}

void SectorLogger::checkIfStoredEventsAreFull()
{
    if (m_storedEvents.size() >= m_numOfEventsToKeep)
    {
        m_storedEvents.removeRange(m_numOfEventsToKeep - 1, m_storedEvents.size() - m_numOfEventsToKeep + 1);
    }
}

LogMessageListener::LogMessageListener()
{
    m_logger = SectorLogger::getCurrentLogger();
    if (m_logger == nullptr)
        return;
    
    m_logger->addChangeListener(this);
    m_logger->sendChangeMessage();
}

LogMessageListener::~LogMessageListener()
{
    m_logger->removeChangeListener(this);
}

void LogMessageListener::changeListenerCallback(juce::ChangeBroadcaster *source)
{
    if (source != m_logger) return;
    
    Array<LogMessage> events;
    
    // Check to see if this was the first time this object is receiving the callback. When constructed the object will default to standard Time() value
    if (m_time == Time())
        events = m_logger->getAllEvents();
    else
        events = m_logger->getEventsSince(m_time);
    
    m_time = Time::getCurrentTime();
    
    for (auto event : events)
        handleNewEvent(event);
}